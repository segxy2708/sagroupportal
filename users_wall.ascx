﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="users_wall.ascx.vb" Inherits="users_wall" Debug ="true"  %>

<table >
<tr>
<td></td>
<td colspan="2">
    <asp:TextBox ID="user_tx" runat="server" Height="59px" TextMode="MultiLine" 
        Width="231px"></asp:TextBox></td>
<td></td>
</tr>
    
<tr>
<td></td>
<td align="right">
    <asp:SqlDataSource ID="wallpost_src" runat="server" 
        ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" 
        DeleteCommand="DELETE FROM [users_wall] WHERE [id] = @original_id AND [username] = @original_username AND [statement] = @original_statement AND (([every] = @original_every) OR ([every] IS NULL AND @original_every IS NULL)) AND [date] = @original_date AND [imageurl] = @original_imageurl" 
        InsertCommand="INSERT INTO [users_wall] ([username], [statement], [every], [date], [imageurl]) VALUES (@username, @statement, @every, @date, @imageurl)" 
        OldValuesParameterFormatString="original_{0}" 
        SelectCommand="SELECT * FROM [users_wall]" 
        
        UpdateCommand="UPDATE [users_wall] SET [username] = @username, [statement] = @statement, [every] = @every, [date] = @date, [imageurl] = @imageurl WHERE [id] = @original_id AND [username] = @original_username AND [statement] = @original_statement AND (([every] = @original_every) OR ([every] IS NULL AND @original_every IS NULL)) AND [date] = @original_date AND [imageurl] = @original_imageurl">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_username" Type="String" />
            <asp:Parameter Name="original_statement" Type="String" />
            <asp:Parameter Name="original_every" Type="Int32" />
            <asp:Parameter Name="original_date" Type="DateTime" />
            <asp:Parameter Name="original_imageurl" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:SessionParameter Name="username" SessionField="names" Type="String" />
            <asp:ControlParameter ControlID="user_tx" Name="statement" PropertyName="Text" 
                Type="String" />
            <asp:ControlParameter ControlID="CheckBox1" Name="every" PropertyName="Checked" 
                Type="Int32" />
            <asp:SessionParameter Name="date" SessionField="date" Type="DateTime" />
            <asp:SessionParameter Name="imageurl" SessionField="myImageUrl" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="statement" Type="String" />
            <asp:Parameter Name="every" Type="Int32" />
            <asp:Parameter Name="date" Type="DateTime" />
            <asp:Parameter Name="imageurl" Type="String" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_username" Type="String" />
            <asp:Parameter Name="original_statement" Type="String" />
            <asp:Parameter Name="original_every" Type="Int32" />
            <asp:Parameter Name="original_date" Type="DateTime" />
            <asp:Parameter Name="original_imageurl" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:CheckBox ID="CheckBox1" runat="server" 
        Text="for general view" /></td>
<td>
    <asp:Button ID="Button1" runat="server" Text="Post" />
    </td>
</tr>

</table>


