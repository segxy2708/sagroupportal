Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Imports System.Web.Mail
Imports System.Web.Services
Imports System.Web.Services.Protocols
'Imports System.Net.Mail
Imports system.Net
Imports System.Net.Dns
Imports System.Web.Security

Partial Class _Default
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter
    
    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        ' FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, True)
        login()
    End Sub

    Sub login()
        'Try
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT id FROM Directory WHERE (uname = @uname) AND (pw=@pw)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        Dim parm1 As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@uname"
        parm1.ParameterName = "@pw"
        parm.Value = txtUsername.Text
        parm1.Value = txtPassword.Text
        cmd.Parameters.Add(parm)
        cmd.Parameters.Add(parm1)
        Dim i As Object = cmd.ExecuteScalar()
        If i Is Nothing Then
            lblMessage.Text = "Invalid credentials supplied. Please, try again."
            lblMessage.ForeColor = Drawing.Color.Red

        Else

            Dim cmd1 As SqlCommand = myconnection.CreateCommand()
            cmd1.CommandType = CommandType.Text
            cmd1.CommandText = "SELECT fullname FROM Directory WHERE (id = @id)"
            'myconnection.Open()
            Dim parm2 As SqlParameter = cmd.CreateParameter()
            parm2.ParameterName = "@id"
            parm2.Value = i
            cmd1.Parameters.Add(parm2)
            Dim names As Object = cmd1.ExecuteScalar()

            Dim cmd2 As SqlCommand = myconnection.CreateCommand()
            cmd2.CommandType = CommandType.Text
            cmd2.CommandText = "SELECT location FROM Directory WHERE (id = @id)"
            'myconnection.Open()
            Dim parm3 As SqlParameter = cmd2.CreateParameter()
            parm3.ParameterName = "@id"
            parm3.Value = i
            cmd2.Parameters.Add(parm3)
            Dim company As Object = cmd2.ExecuteScalar()

            'Check Personalization
            'Check Personalization Theme
            Dim myconnection1 As SqlConnection = New SqlConnection(ConnectionString1)
            Dim cmd4 As SqlCommand = myconnection1.CreateCommand()
            cmd4.CommandType = CommandType.Text
            cmd4.CommandText = "SELECT PageType FROM Personalization WHERE (staffid = @staffid)"
            myconnection1.Open()
            Dim parm4 As SqlParameter = cmd4.CreateParameter()
            parm4.ParameterName = "@staffid"
            parm4.Value = i
            cmd4.Parameters.Add(parm4)
            Dim Theme As Object = cmd4.ExecuteScalar()

            'Check Personalization Skin
            Dim cmd5 As SqlCommand = myconnection1.CreateCommand()
            cmd5.CommandType = CommandType.Text
            cmd5.CommandText = "SELECT Theme FROM Personalization WHERE (staffid = @staffid)"
            'myconnection1.Open()
            Dim parm5 As SqlParameter = cmd5.CreateParameter()
            parm5.ParameterName = "@staffid"
            parm5.Value = i
            cmd5.Parameters.Add(parm5)
            Dim Skin As Object = cmd5.ExecuteScalar()
            'End Personalization
            'Check the response
            If Theme Is Nothing Then
                Session("Theme") = "sa_Glass_Theme.master"
            ElseIf Skin Is Nothing Then
                Session("Skin") = "Orange"
            Else
                Session("Theme") = Theme
                Session("Skin") = Skin
            End If

            Session("usernames") = txtUsername.Text
            Session("id") = i
            Session("names") = names
            Session("company") = company
            Session("datte") = Now.Date

            Session("myImageUrl") = getImageUrl()
            Session("role") = getRole()
            Session("Group") = getGroup()
            Session("Department") = getDepartment()
            Session("email") = getMailAddr()
            Session("phone") = getMobAdd()
            Session("ip") = Request.UserHostName

            lblMessage.Text = "Welcome " & names & ", please wait ..."
            lblMessage.ForeColor = Drawing.Color.Green
            btnLogin.Enabled = False
            btnPassword.Enabled = False
            '---------------check user log for the day
            ''Dim usercmd As SqlCommand = myconnection1.CreateCommand()
            ''usercmd.CommandType = CommandType.Text
            ''usercmd.CommandText = "SELECT logid FROM user_log WHERE (userid = @userid) AND (logDate=@logDate)"
            ' ''myconnection1.Open()
            ''Dim userparm As SqlParameter = usercmd.CreateParameter()
            ''Dim userparm1 As SqlParameter = usercmd.CreateParameter()
            ''userparm.ParameterName = "@userid"
            ''userparm1.ParameterName = "@logDate"
            ''userparm.Value = CInt(i)
            ''userparm1.Value = CDate(Format(Now.Date, "dd/MM/yyyy"))
            ''usercmd.Parameters.Add(userparm)
            ''usercmd.Parameters.Add(userparm1)
            ''Dim userlog As Object = usercmd.ExecuteScalar()
            'End Check
            Dim sysname, sysaddress

            sysname = Request.Path         'My.Computer.Name   'Dns.GetHostName
            sysaddress = Request.UserHostName
            'If userlog Is Nothing Then
            Dim logs As New PortalsTableAdapters.user_logTableAdapter
            logs.InsertUserlog(CInt(i), txtUsername.Text, names, company, getDepartment(), Format(DateTime.Now, "hh:mm:ss"), "", sysname, sysaddress, Now.Date)
            'End If
            If Session("urls") Is Nothing Then
                Response.AppendHeader("Refresh", "5; URL=sag_home.aspx")
            Else
                Response.Redirect(Session("urls"))
            End If

        End If
        myconnection.Close()
        'Catch ex As Exception
        '    lblMessage.Text = ex.Message
        '    lblMessage.ForeColor = Drawing.Color.Red
        'End Try
    End Sub

    Public Function getImageUrl()
        Dim str As String
        Dim query As String = "Select picloc from directory where fullname='" + Session("names") + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Public Function getRole()
        Dim str As String
        Dim query As String = "Select profilename from directory where fullname='" + Session("names") + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function


    Public Function getGroup()
        Dim str As String
        Dim query As String = "Select location from directory where fullname='" + Session("names") + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function
    Public Function getMailAddr()
        Dim str As String
        Dim query As String = "Select email from directory where fullname='" + Session("names") + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function
    Public Function getMobAdd()
        Dim str As String
        Dim query As String = "Select phone from directory where fullname='" + Session("names") + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function
    Public Function getDepartment()
       
        Dim str As String
        Dim query As String = "Select Department from directory where fullname='" + Session("names") + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Button1_ModalPopupExtender.Show()
        Me.txtUsername.Focus()
        Session.Clear()
        
    End Sub

    Protected Sub btnPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPassword.Click
        Panel1.Visible = True
        Panel2.Visible = False
    End Sub

    Protected Sub txtCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCancel.Click
        Panel1.Visible = False
        Panel2.Visible = True
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim i As String = passwordRtr()
        Panel2.Visible = True
        Session("usermail") = Me.txtFMail.Text.ToString
        Session("passwrd") = passwordRtr()
        'Dim m As MailMessage = New MailMessage()
        ''SmtpMail.SmtpServer = "sagroupng.com"

        'm.From = New MailAddress("info@sagroupng", "SA Intranet Portal")
        'm.To.Add(New MailAddress(txtFMail.Text, txtFUser.Text))
        'm.Subject = "Payment Confirmation"
        'm.IsBodyHtml = True
        'm.Body = "Hello,<p>below is your password <br></b> " & i & "<p> Thank you"

        'Dim clients As SmtpClient = New SmtpClient("208.88.73.108")
        'clients.EnableSsl = True
        'clients.Credentials.GetCredential("208.88.73.108", 2525, "auto")
        'clients.UseDefaultCredentials = True
        'clients.Send(m)
        'SmtpMail.Send(m)
        If Not i Is Nothing Then
            lblMessage.Text = "Your password has been sent to your mail " & Me.txtFMail.Text   'getusersemail(txtFUser.Text) ' <b>" & i & "</b>"
            lblMessage.ForeColor = Drawing.Color.DarkGreen
            txtFUser.Text = ""
            txtFMail.Text = ""
            sendmail()
        Else
            lblMessage.Text = "Invalid Username supplied"
            lblMessage.ForeColor = Drawing.Color.Red
            txtFUser.Text = ""
            txtFMail.Text = ""
        End If
    End Sub

    Function passwordRtr() As String
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT pw FROM Directory WHERE (uname = @uname)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@uname"
        parm.Value = txtFUser.Text
        cmd.Parameters.Add(parm)
        Dim i As Object = cmd.ExecuteScalar()
        Return i
    End Function

    Public Function getusersemail(ByVal name As String)
        Dim str As String
        Dim query As String = "Select email from directory where uname='" + name + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Sub sendmail()
        'Dim pass As String = passwordRtr()
        'Dim addr As String = 
        Dim mymail As MailMessage = New MailMessage()
        mymail.Subject = "Password Info"
        mymail.To = Session("usermail") ' addr
        mymail.Cc = Session("usermail") 'addr '"ooduwole@sagroupng.com"
        mymail.From = "support@sagroupng.com"
        Dim s As String = "your password is " & Session("passwrd")
        mymail.BodyFormat = MailFormat.Text
        mymail.Body = s
        SmtpMail.SmtpServer = "localhost"
        SmtpMail.Send(mymail)

    End Sub
End Class
