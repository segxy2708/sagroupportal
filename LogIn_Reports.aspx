<%@ Page Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="LogIn_Reports.aspx.vb" Inherits="LogIn_Reports" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type ="text/javascript" language ="javascript" >
    function printGridView() {
        //open new window set the height and width =0,set windows position at bottom

        var a = window.open('', '', 'left =' + screen.width + ',top=' + screen.height + ',width=0,height=0,toolbar=0,scrollbars=0,status=0');
        //write gridview data into newly open window 
        a.document.write(document.getElementById("tbl").innerHTML);

        a.document.close();
        a.focus();
        //call print
        a.print();
        a.close();
        return false;
    }

</script>
<table >
<tr>
<td><asp:Label ID="Label1" runat="server" Text="Select Date" Font-Bold="True"></asp:Label>
</td>
<td style="width: 3px" align="left">
    <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
    <br />
    <br />
    <br />
</td>
<td>
    </td>
</tr>
<tr>
<td></td>
<td>
 <asp:UpdatePanel ID ="ups" runat ="server" >
    <Triggers >
    <asp:AsyncPostBackTrigger ControlID ="Timer1" />
    </Triggers>
    <ContentTemplate >
    <asp:Timer ID="Timer1" runat ="server" Interval="3000" ></asp:Timer>
 <asp:Panel ID="panel1" runat="server" Height="500px" ScrollBars="Auto" Width="600px">
<div id ="tbl">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="logid"
            DataSourceID="LogSrc" BorderStyle="Solid" CaptionAlign="Left" 
            CellPadding="4" ForeColor="#333333" Width="90%" AllowPaging="True" 
            Height="100%">
            <Columns>
                
                
                <asp:BoundField DataField="fullname" HeaderText="Full Name" SortExpression="fullname" />
                <asp:BoundField DataField="company" HeaderText="Division" SortExpression="company" />
                <asp:BoundField DataField="department" HeaderText="Department" SortExpression="department" />
                
                <asp:BoundField DataField="timein" HeaderText="Time" SortExpression="timein" />
                
                
                <asp:BoundField DataField="systemips" HeaderText="System IP" SortExpression="systemips" />
            </Columns>
            <RowStyle BackColor="#E3EAEB" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        </div> 
    <asp:SqlDataSource ID="LogSrc" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT logid, userid, username, fullname, company, department, logDate, timein, timeout, systemname, systemips FROM user_log WHERE (logDate = @logDate)">
        <SelectParameters>
            <asp:SessionParameter Name="logDate" SessionField="Day" />
        </SelectParameters>
    </asp:SqlDataSource>
    &nbsp;&nbsp;
    <input id="Button1" class="Buttons" lang="Javascript" onclick="printGridView();"
        type="button" value="Print" /></asp:Panel>
        </ContentTemplate> 
        </asp:UpdatePanel> 
        </td>
<td></td>
</tr>
</table>
    &nbsp;
    
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

</asp:Content>

