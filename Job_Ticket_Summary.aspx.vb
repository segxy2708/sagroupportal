
Partial Class Job_Ticket_Summary
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Ticket Generated"
    End Sub

    Protected Sub Lists_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Lists.SelectedIndexChanged
        Me.Title = "Tickets Treated by " & Me.Lists.SelectedValue
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub
End Class
