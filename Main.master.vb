
Partial Class Main
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        Else
            lblName.Text = Session("names")
        End If
        
    End Sub

    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        Dim i As New PortalsTableAdapters.chatroomTableAdapter
        Try
            i.DeleteChatUser(Session("id"))
        Catch ex As Exception

        End Try
        Session.Clear()
        Response.AppendHeader("Refresh", "2; URL=Default.aspx")
    End Sub

End Class

