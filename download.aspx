<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="download.aspx.vb" Inherits="download" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    Please click on the link below to :<br />
    <br />
    1.
    <asp:LinkButton ID="LinkButton1" runat="server">Download Appraisal Forms</asp:LinkButton><br />
    <br />
    <asp:Panel ID="Panel1" runat="server" Height="150px" ScrollBars="Auto" Visible="False"
        Width="250px" EnableTheming="False">
    <br />
    <ul>
        <li>
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Appraiser/Sai-Appraisal Form 2010 new_group.doc"
                Target="_blank">Group Office Appraisal Form</asp:HyperLink></li><li>
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Appraiser/Sai-Appraisal Form 2010 new_insurance.doc"
                Target="_blank">SA Insurance Appraisal Form</asp:HyperLink></li><li>
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Appraiser/Sai-Appraisal Form 2010 new_life.doc"
                Target="_blank">SA Life Appraisal Form</asp:HyperLink></li><li>
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Appraiser/Sai-Appraisal Form 2010 new_lagoon.doc"
                Target="_blank">SA Lagoon Appraisal Form</asp:HyperLink></li><li>
            <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Appraiser/Sai-Appraisal Form 2010 new_properties.doc"
                Target="_blank">SA Properties Appraisal Form</asp:HyperLink></li><li>
            <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/Appraiser/Sai-Appraisal Form 2010 new_capital.doc"
                Target="_blank">SA Capital Appraisal Form</asp:HyperLink></li></ul>
    </asp:Panel>
    <p>
        2. &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" Visible="False">Open Website(s) Update Request</asp:LinkButton>&nbsp;
        <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/Web_Content_2_Change.aspx">Fill Update request</asp:HyperLink></p>
    <p>
        <asp:Panel ID="Panel2" runat="server" Height="50px" Width="250px" Visible="False" EnableTheming="False">
        <ul>
        <li>
            <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/File2download/Website Change-Update Request Form - Add New Content.pdf">New Content</asp:HyperLink></li><li>  <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/File2download/Website Change-Update Request Form - Change Existing.pdf">Change to Existing Content</asp:HyperLink></li><li ><asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/File2download/Website Change-Update Request Form - File Upload.pdf">File(s) upload for Clients' Download</asp:HyperLink>
        </li>
        </ul>
        </asp:Panel>
        &nbsp;</p>
    <p>
        3. &nbsp;<asp:LinkButton ID="LinkButton3" runat="server" 
            OnClientClick="window.open (&quot;StaffHandbook.html&quot;, &quot;Staff_Handbook&quot;,&quot;left=200,top=50,height=600,width=500,scrollbars=1,status=no,resizeable=no&quot;);">Staff Handbook</asp:LinkButton></p>
    <p>
        &nbsp;</p>
    <p>
        4.
        <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="javascript:onclick =Helps()" Visible="False">Intranet Manual</asp:LinkButton>
        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/Intranet Manual.pdf" Target="_blank">Intranet Manual</asp:HyperLink></p>
    <p>
        &nbsp;</p>
    <p>
        5.
        <asp:HyperLink ID="HyperLink12" runat="server" 
            NavigateUrl="~/WebContentChange/Microsoft Office Communicator Manual.doc">Office Communicators Manual</asp:HyperLink>
    </p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

