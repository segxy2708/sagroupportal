
Partial Class WebUpdate
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Web Update Reqest Form"
        Me.SubText1.Text = Session("Group")
        Me.SubText2.Text = Session("Group")
        Me.SubTxt3.Text = Session("Group")
        Me.EmailTxt1.Text = Session("email")
        Me.emailTxt2.Text = Session("email")
        Me.emailTxt3.Text = Session("email")
        Me.MobileTxt1.Text = Session("phone")
        Me.MobileTxt2.Text = Session("phone")
        Me.MobileTxt3.Text = Session("phone")
        Me.NameTxt1.Text = Session("names")
        Me.NameTxt2.Text = Session("names")
        Me.NameTxt3.Text = Session("names")

        hideAll()
        If Me.DropDownList1.SelectedIndex = Nothing Then
            hideAll()
        ElseIf Me.DropDownList1.SelectedIndex = 0 Then
            hideAll()
        ElseIf Me.DropDownList1.SelectedIndex = 1 Then
            Me.Panel1.Visible = True
        ElseIf Me.DropDownList1.SelectedIndex = 2 Then
            Me.Panel2.Visible = True
        ElseIf Me.DropDownList1.SelectedIndex = 3 Then
            Me.Panel3.Visible = True
        End If
    End Sub

    Sub hideAll()
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
        Me.Panel3.Visible = False
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub SubmitBut1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitBut1.Click
        Try
            Me.NewItemSource.Insert()
            errMess.Text = "Request Successful"
            errMess.Focus()
        Catch ex As Exception
            errMess.Text = ex.Message
            errMess.Focus()
        End Try

    End Sub

    Protected Sub SubmitBut3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitBut3.Click
        Dim filename As String = FileUpload1.FileName
        If Me.FileUpload1.HasFile Then
            If Me.FileUpload1.PostedFile.ContentLength < 102400 Then
                Try
                    System.IO.Directory.CreateDirectory(Server.MapPath(".") & "\Web Update Suggestion\" & Session("names"))
                    Dim upload As New System.Web.UI.WebControls.Parameter("", TypeCode.String, filename)
                    Me.FileUpload1.SaveAs(Server.MapPath(".") & "\Web Update Suggestion\" & Session("names") & "\" & FileUpload1.FileName)
                    Me.FileUploadSource.InsertParameters.Add(upload)
                    Me.FileUploadSource.Insert()
                    errMess.Focus()
                Catch ex As Exception
                    errMess.Text = ex.InnerException.ToString
                    errMess.Focus()
                End Try
                
            Else
                errMess.Text = "server error, please contact the administrator"
                errMess.Focus()
            End If

        Else

        End If

    End Sub

    Protected Sub SubmitBut2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitBut2.Click
        Try
            Me.ExistingItemSource.Insert()
            errMess.Focus()
        Catch ex As Exception
            errMess.Text = ex.Message
            errMess.Focus()
        End Try

    End Sub
End Class
