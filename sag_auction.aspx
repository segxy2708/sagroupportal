<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="sag_auction.aspx.vb" Inherits="sag_auction" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 871px">
        <tr>
            <td style="width: 38px">
                <strong>Items:</strong></td>
            <td style="width: 583px">
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1"
                    DataTextField="item_name" DataValueField="item_name">
                </asp:DropDownList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    SelectCommand="SELECT [item_name] FROM [items] WHERE ([quantity] > @quantity) ORDER BY [item_name]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="0" Name="quantity" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
                </asp:ScriptManagerProxy>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource2">
                    <ItemTemplate>
                        <table style="width: 551px; height: 93px">
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="item_nameLabel" runat="server" Font-Bold="True" Font-Size="14pt" Text='<%# Eval("item_name") %>'></asp:Label></td>
                                <td rowspan="3" style="width: 7px">
                                    <asp:Image ID="Image1" runat="server" Height="200px" ImageUrl='<%# Eval("item_pix", "{0}") %>' /></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="item_descriptionLabel" runat="server" Font-Size="12pt" Text='<%# Eval("item_description") %>'></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align: left">
                                    <asp:Label ID="quantityLabel" runat="server" Font-Bold="True" Text='<%# Eval("quantity") %>'></asp:Label>
                                    <strong>Available in store now</strong></td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    SelectCommand="SELECT [item_name], [item_description], [quantity], [status], [item_pix] FROM [items] WHERE ([item_name] = @item_name)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="item_name" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:LinkButton ID="ClickBid" runat="server">Click to Bid</asp:LinkButton></td>
        </tr>
        <tr>
            <td style="width: 38px">
            </td>
            <td style="width: 583px">
                &nbsp;<asp:Label ID="Label3" runat="server" Text=""></asp:Label>&nbsp;
                <asp:Panel ID="Bidpan" runat ="server" Visible="False">
                <table >
                <tr>
                <td></td>
                <td>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" />
                </td>
                </tr>
                <tr>
                <td style="width: 3px">
                    <asp:Label ID="Label2" runat="server" Text="Amount"></asp:Label></td>
                <td style="width: 3px">
                    <asp:TextBox ID="AmountTxt" runat="server"></asp:TextBox>
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                        ErrorMessage="Please enter a valid amount. e.g 1000" ValidationExpression="\d" ControlToValidate="AmountTxt">*</asp:RegularExpressionValidator>--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="AmountTxt"
                        Display="Dynamic" ErrorMessage="Please specify amount">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                <td style="width: 3px">
                    <asp:Label ID="Label1" runat="server" Text="Quantity"></asp:Label></td>
                <td style="width: 3px">
                    <asp:TextBox ID="QuantityTxt" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="QuantityTxt"
                        Display="Dynamic" ErrorMessage="Please specify a valid quantity e.g 5" ValidationExpression="\d">*</asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="QuantityTxt"
                        Display="Dynamic" ErrorMessage="Please Specify a quantity">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                <td style="height: 16px" align="center" colspan="2">
                <asp:Button ID="Bidbut" runat="server" Text="Bid" Width="69px" /></td>
                </tr>
                </table>
                </asp:Panel>
                <asp:SqlDataSource ID="bidSource" runat="server" ConflictDetection="CompareAllValues"
                    ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" DeleteCommand="DELETE FROM [bidders_details] WHERE [bidid] = @original_bidid AND (([name] = @original_name) OR ([name] IS NULL AND @original_name IS NULL)) AND (([date] = @original_date) OR ([date] IS NULL AND @original_date IS NULL)) AND (([time] = @original_time) OR ([time] IS NULL AND @original_time IS NULL)) AND (([items] = @original_items) OR ([items] IS NULL AND @original_items IS NULL)) AND (([quantity] = @original_quantity) OR ([quantity] IS NULL AND @original_quantity IS NULL)) AND (([unit_price] = @original_unit_price) OR ([unit_price] IS NULL AND @original_unit_price IS NULL)) AND (([amount] = @original_amount) OR ([amount] IS NULL AND @original_amount IS NULL))"
                    InsertCommand="INSERT INTO [bidders_details] ([name], [date], [time], [items], [quantity], [unit_price], [amount]) VALUES (@name, @date, @time, @items, @quantity, @unit_price, @amount)"
                    OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [bidders_details]"
                    UpdateCommand="UPDATE [bidders_details] SET [name] = @name, [date] = @date, [time] = @time, [items] = @items, [quantity] = @quantity, [unit_price] = @unit_price, [amount] = @amount WHERE [bidid] = @original_bidid AND (([name] = @original_name) OR ([name] IS NULL AND @original_name IS NULL)) AND (([date] = @original_date) OR ([date] IS NULL AND @original_date IS NULL)) AND (([time] = @original_time) OR ([time] IS NULL AND @original_time IS NULL)) AND (([items] = @original_items) OR ([items] IS NULL AND @original_items IS NULL)) AND (([quantity] = @original_quantity) OR ([quantity] IS NULL AND @original_quantity IS NULL)) AND (([unit_price] = @original_unit_price) OR ([unit_price] IS NULL AND @original_unit_price IS NULL)) AND (([amount] = @original_amount) OR ([amount] IS NULL AND @original_amount IS NULL))">
                    <DeleteParameters>
                        <asp:Parameter Name="original_bidid" Type="Int32" />
                        <asp:Parameter Name="original_name" Type="String" />
                        <asp:Parameter Name="original_date" Type="String" />
                        <asp:Parameter Name="original_time" Type="String" />
                        <asp:Parameter Name="original_items" Type="String" />
                        <asp:Parameter Name="original_quantity" Type="String" />
                        <asp:Parameter Name="original_unit_price" Type="String" />
                        <asp:Parameter Name="original_amount" Type="String" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="name" Type="String" />
                        <asp:Parameter Name="date" Type="String" />
                        <asp:Parameter Name="time" Type="String" />
                        <asp:Parameter Name="items" Type="String" />
                        <asp:Parameter Name="quantity" Type="String" />
                        <asp:Parameter Name="unit_price" Type="String" />
                        <asp:Parameter Name="amount" Type="String" />
                        <asp:Parameter Name="original_bidid" Type="Int32" />
                        <asp:Parameter Name="original_name" Type="String" />
                        <asp:Parameter Name="original_date" Type="String" />
                        <asp:Parameter Name="original_time" Type="String" />
                        <asp:Parameter Name="original_items" Type="String" />
                        <asp:Parameter Name="original_quantity" Type="String" />
                        <asp:Parameter Name="original_unit_price" Type="String" />
                        <asp:Parameter Name="original_amount" Type="String" />
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:SessionParameter Name="name" SessionField="names" Type="String" />
                        <asp:SessionParameter Name="date" SessionField="date" Type="String" />
                        <asp:SessionParameter Name="time" SessionField="time" Type="String" />
                        <asp:ControlParameter ControlID="DropDownList1" Name="items" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="QuantityTxt" Name="quantity" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="AmountTxt" Name="unit_price" PropertyName="Text"
                            Type="String" />
                        <asp:SessionParameter Name="amount" SessionField="amount" Type="String" />
                    </InsertParameters>
                </asp:SqlDataSource>
                </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

