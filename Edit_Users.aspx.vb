Imports System
Imports System.data
'Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Web.UI.WebControls
Partial Class Edit_Users
    Inherits System.Web.UI.Page

    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uploadInfo()
    End Sub

    Sub uploadInfo()
        Try
            Me.TablePanel.Visible = False
            Me.Title = "Edit User"
            Me.idTxt.Text = getSingleDetail("id", Me.DropDownList1.Text)
            Me.FirstNameTxt.Text = getSingleDetail("last", Me.DropDownList1.Text)
            Me.LastNameTxt.Text = getSingleDetail("first", Me.DropDownList1.Text)
            Me.PhoneNoTxt.Text = getSingleDetail("phone", Me.DropDownList1.Text)
            Me.faxTxt.Text = getSingleDetail("fax", Me.DropDownList1.Text)
            Me.emailTxt.Text = getSingleDetail("email", Me.DropDownList1.Text)
            Me.cellPhoneTxt.Text = getSingleDetail("last", Me.DropDownList1.Text)
            Me.address1Txt.Text = getSingleDetail("Address1", Me.DropDownList1.Text)
            Me.address2Txt.Text = getSingleDetail("address2", Me.DropDownList1.Text)
            Me.departmentList.SelectedValue = getSingleDetail("department", Me.DropDownList1.Text)
            Me.StateList.SelectedValue = getSingleDetail("state", Me.DropDownList1.Text)
            Me.UlevList.SelectedValue = getSingleDetail("profilename", Me.DropDownList1.Text)

            Me.SexList.SelectedValue = getSingleDetail("sex", Me.DropDownList1.Text)
            Me.titleList.SelectedValue = getSingleDetail("title", Me.DropDownList1.Text)
            Me.CountryList.SelectedValue = getSingleDetail("country", Me.DropDownList1.Text)
            Me.StateList.SelectedValue = getSingleDetail("state", Me.DropDownList1.Text)
            Me.buildingList.SelectedValue = getSingleDetail("building", Me.DropDownList1.Text)
        Catch ex As Exception
            Me.Title = ex.Message
        End Try
    End Sub

    Sub updatecode()
        'Me.idTxt.Text = getSingleDetail("id", Me.DropDownList1.Text)
        'Me.FirstNameTxt.Text = getSingleDetail("last", Me.DropDownList1.Text)
        'Me.LastNameTxt.Text = getSingleDetail("first", Me.DropDownList1.Text)
        Session("updatePhone") = Me.PhoneNoTxt.Text ' = getSingleDetail("phone", Me.DropDownList1.Text)
        Session("updateFax") = Me.faxTxt.Text '= getSingleDetail("fax", Me.DropDownList1.Text)
        Session("updateEmail") = Me.emailTxt.Text '= getSingleDetail("email", Me.DropDownList1.Text)
        Session("updateCellPhone") = Me.cellPhoneTxt.Text '= getSingleDetail("last", Me.DropDownList1.Text)
        Session("updateAddress1") = Me.address1Txt.Text '= getSingleDetail("Address1", Me.DropDownList1.Text)
        Session("updateAddress2") = Me.address2Txt.Text '= getSingleDetail("address2", Me.DropDownList1.Text)
        Me.departmentList.SelectedValue = getSingleDetail("department", Me.DropDownList1.Text)
        Me.StateList.SelectedValue = getSingleDetail("state", Me.DropDownList1.Text)
        Me.UlevList.SelectedValue = getSingleDetail("profilename", Me.DropDownList1.Text)

        Me.SexList.SelectedValue = getSingleDetail("sex", Me.DropDownList1.Text)
        Me.titleList.SelectedValue = getSingleDetail("title", Me.DropDownList1.Text)
        Me.CountryList.SelectedValue = getSingleDetail("country", Me.DropDownList1.Text)
        Me.StateList.SelectedValue = getSingleDetail("state", Me.DropDownList1.Text)
        Me.buildingList.SelectedValue = getSingleDetail("building", Me.DropDownList1.Text)
    End Sub

    Function getSingleDetail(ByVal item As String, ByVal name As String) As String

        Dim str As String
        Dim query As String = "Select " & item & " from directory where fullname='" & name & "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList1.SelectedIndexChanged
        Me.TablePanel.Visible = True
    End Sub

    Protected Sub SubmitBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitBut.Click
        Panel1.Visible = True
        Panel1.Focus()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Panel1.Visible = False
        Response.AppendHeader("Refresh", "5,URL=edit_user.aspx")
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
           
            Me.SubmitSource.Update()
            Me.messageLab.Text = Me.LastNameTxt.Text & " " & Me.FirstNameTxt.Text & " " & "updated successfully"
        Catch ex As Exception
            Me.messageLab.Text = ex.Message
        End Try
        Panel1.Visible = False
    End Sub

    
End Class
