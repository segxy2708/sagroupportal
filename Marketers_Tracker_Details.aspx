<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Marketers_Tracker_Details.aspx.vb" Inherits="Marketers_Tracker_Details" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DataList ID="DataList1" runat="server" DataKeyField="id" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            <table style="width: 373px">
                <tr>
                    <td align="left" colspan="2">
                        <asp:Label ID="movedateLabel" runat="server" Text='<%# Eval("movedate", "{0:D}") %>'></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 121px">
                        <strong>Marketer's Name:</strong></td>
                    <td align="left">
                        <asp:Label ID="marketernameLabel" runat="server" Text='<%# Eval("marketername") %>'></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 121px">
                        <strong>Company:</strong></td>
                    <td align="left">
                        <asp:Label ID="companyLabel" runat="server" Text='<%# Eval("company") %>'></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 121px">
                        <strong>Location:</strong></td>
                    <td align="left">
                        <asp:Label ID="locationLabel" runat="server" Text='<%# Eval("location") %>'></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 121px">
                        <strong>Client:</strong></td>
                    <td align="left">
                        <asp:Label ID="clientLabel" runat="server" Text='<%# Eval("client") %>'></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 121px">
                        <strong>Address:</strong></td>
                    <td align="left">
                        <asp:Label ID="addressLabel" runat="server" Text='<%# Eval("address") %>'></asp:Label></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 121px">
                        <strong>Status:</strong></td>
                    <td align="left">
                        <asp:Label ID="statusLabel" runat="server" Text='<%# Eval("status") %>'></asp:Label></td>
                    <td>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MarketerDBConnectionString %>"
        SelectCommand="SELECT * FROM [Movement] WHERE ([id] = @id)">
        <SelectParameters>
            <asp:SessionParameter Name="id" SessionField="I" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

