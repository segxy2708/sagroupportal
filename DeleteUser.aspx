<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="DeleteUser.aspx.vb" Inherits="DeleteUser" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table >
<tr>
<td>

</td>
<td></td>
<td align="center">
    <asp:Label ID="Label2" runat="server" Text="Delete User"></asp:Label></td>
<td style="width: 3px"></td>
</tr>
</table>
    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="700px" Height="200px">
    <table >

<tr>
<td></td>
<td colspan="2" rowspan="" align="center">
    <asp:Label ID="delmessg" runat="server" Text=""></asp:Label></td>
<td></td>
<td></td>
</tr>
<tr>
<td>
    <asp:Label ID="Label1" runat="server" Text="Delete By"></asp:Label>
</td>
<td>
    &nbsp;<asp:DropDownList ID="firstList" runat="server" AutoPostBack="True" AppendDataBoundItems="True" DataSourceID="GrpSrc" DataTextField="location" DataValueField="location" EnableViewState="False">
        <asp:ListItem>- Select Division -</asp:ListItem>
        
    </asp:DropDownList>
</td>
<td>
    <asp:DropDownList ID="List" runat="server" Visible="False" DataSourceID="DepDataSource" DataTextField="Department" DataValueField="Department" AppendDataBoundItems="True" AutoPostBack="True" EnableViewState="False">
        <asp:ListItem>- Select Department -</asp:ListItem>
    </asp:DropDownList>
</td>
<td style="width: 3px">
    <asp:DropDownList ID="nameList" runat="server" Visible="False" DataSourceID="uSname" DataTextField="fullname" DataValueField="fullname" EnableViewState="False" AppendDataBoundItems="True">
        <asp:ListItem>- Select User -</asp:ListItem>
    </asp:DropDownList></td>
</tr>

<tr>
<td>

</td>
<td></td>
<td>
    <asp:SqlDataSource ID="DepDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT DISTINCT [Department] FROM [directory] WHERE ([location] = @location)">
        <SelectParameters>
            <asp:ControlParameter ControlID="firstList" Name="location" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:Button ID="delButton" runat="server" Text="Delete" CssClass="Buttons" /></td>
<td style="width: 3px">
    <asp:SqlDataSource ID="uSname" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT DISTINCT [fullname] FROM [directory] WHERE (([location] = @location) AND ([Department] = @Department))" DeleteCommand="DELETE FROM directory WHERE (fullname = @fullname)">
        <SelectParameters>
            <asp:ControlParameter ControlID="firstList" Name="location" PropertyName="SelectedValue"
                Type="String" />
            <asp:ControlParameter ControlID="List" Name="Department" PropertyName="SelectedValue"
                Type="String" />
        </SelectParameters>
        <DeleteParameters>
            <asp:ControlParameter ControlID="nameList" Name="fullname" PropertyName="SelectedValue" />
        </DeleteParameters>
    </asp:SqlDataSource>
</td>
</tr>
<tr>
<td></td>
<td colspan="2">
    <asp:Panel ID="RequestPan" runat="server" Visible="False" Width="400px">
        <asp:Label ID="Label4" runat="server" Text="Click Yes to Continue or No to Quit"></asp:Label>
        <asp:Button ID="Button1" runat="server" Text="Yes" Width="47px" CssClass="Buttons" />&nbsp;<asp:Button
            ID="Button2" runat="server" Text="No" Width="39px" CssClass="Buttons" /></asp:Panel>
</td>
<td></td>
</tr>
</table> 
        
    </asp:Panel>
    <asp:Panel ID="Panel2" runat="server" Height="400px" ScrollBars="Auto" Width="700px" Visible="False">
        
        <table >
        <tr>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
        </tr>
        <tr>
        <td style="height: 63px">
        </td>
        <td style="height: 63px">
            <asp:Label ID="Label3" runat="server" Text="Division:"></asp:Label>
        </td>
        <td style="height: 63px">
            <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="True" DataSourceID="GrpSrc" DataTextField="location" DataValueField="location" AutoPostBack="True">
                <asp:ListItem>- Select Division -</asp:ListItem>
            </asp:DropDownList><asp:SqlDataSource ID="GrpSrc" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                SelectCommand="SELECT DISTINCT [location] FROM [directory]"></asp:SqlDataSource>
        </td>
        <td style="height: 63px">
        </td>
        </tr>
        <tr>
        <td style="height: 144px">
        </td>
        <td style="height: 144px" colspan="2">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="DeleteUsersSource" AllowPaging="True">
                <Columns>
                    
                    <asp:BoundField DataField="title" HeaderText="Title" SortExpression="title" />
                    <asp:BoundField DataField="first" HeaderText="First Name" SortExpression="first" />
                    <asp:BoundField DataField="last" HeaderText="Last Name" SortExpression="last" />
                    <asp:BoundField DataField="email" HeaderText="E-mail" SortExpression="email" />
                    
                    <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
                    <asp:ButtonField ButtonType ="Button" CommandName ="Delete" Text ="Delete" />
                    <asp:CommandField ShowDeleteButton="True" />
                    
                </Columns>
            </asp:GridView>
            &nbsp; &nbsp;
            <asp:SqlDataSource ID="DeleteUsersSource" runat="server" ConflictDetection="CompareAllValues"
                ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>" DeleteCommand="DELETE FROM directory WHERE (ID = @ID)"
                InsertCommand="INSERT INTO [directory] ([ID], [phone], [fax], [city], [state], [Address1], [Address2], [zipcode], [email], [Department], [location], [building], [last], [first], [MI], [picloc], [office], [Timestamp], [title], [background], [empnum], [listname], [uname], [pw], [admin], [surveyallow], [emailpass], [emailuser], [remotemail], [D11], [address3], [sharecal], [cellphone], [pager], [country], [supervisor], [calaccess], [projectview], [lastlogin], [visits], [hppopup], [status], [flash], [filelimit], [timezone], [getmail], [emailpass2], [emailuser2], [remotemail2], [emailpass3], [emailuser3], [remotemail3], [fullname], [background2], [background3], [spam], [blogcomments], [profilename], [remotemailtype], [remotemail2type], [remotemail3type], [blogdate], [blogtitle], [staff_level], [Sex], [Branch]) VALUES (@ID, @phone, @fax, @city, @state, @Address1, @Address2, @zipcode, @email, @Department, @location, @building, @last, @first, @MI, @picloc, @office, @Timestamp, @title, @background, @empnum, @listname, @uname, @pw, @admin, @surveyallow, @emailpass, @emailuser, @remotemail, @D11, @address3, @sharecal, @cellphone, @pager, @country, @supervisor, @calaccess, @projectview, @lastlogin, @visits, @hppopup, @status, @flash, @filelimit, @timezone, @getmail, @emailpass2, @emailuser2, @remotemail2, @emailpass3, @emailuser3, @remotemail3, @fullname, @background2, @background3, @spam, @blogcomments, @profilename, @remotemailtype, @remotemail2type, @remotemail3type, @blogdate, @blogtitle, @staff_level, @Sex, @Branch)"
                OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT ID, phone, fax, city, state, Address1, Address2, zipcode, email, Department, location, building, last, first, MI, picloc, office, Timestamp, title, background, empnum, listname, uname, pw, admin, surveyallow, emailpass, emailuser, remotemail, D11, address3, sharecal, cellphone, pager, country, supervisor, calaccess, projectview, lastlogin, visits, hppopup, status, flash, filelimit, timezone, getmail, emailpass2, emailuser2, remotemail2, emailpass3, emailuser3, remotemail3, fullname, background2, background3, spam, blogcomments, profilename, remotemailtype, remotemail2type, remotemail3type, blogdate, blogtitle, staff_level, Sex, Branch FROM directory WHERE (location = @location)"
                UpdateCommand="UPDATE [directory] SET [phone] = @phone, [fax] = @fax, [city] = @city, [state] = @state, [Address1] = @Address1, [Address2] = @Address2, [zipcode] = @zipcode, [email] = @email, [Department] = @Department, [location] = @location, [building] = @building, [last] = @last, [first] = @first, [MI] = @MI, [picloc] = @picloc, [office] = @office, [Timestamp] = @Timestamp, [title] = @title, [background] = @background, [empnum] = @empnum, [listname] = @listname, [uname] = @uname, [pw] = @pw, [admin] = @admin, [surveyallow] = @surveyallow, [emailpass] = @emailpass, [emailuser] = @emailuser, [remotemail] = @remotemail, [D11] = @D11, [address3] = @address3, [sharecal] = @sharecal, [cellphone] = @cellphone, [pager] = @pager, [country] = @country, [supervisor] = @supervisor, [calaccess] = @calaccess, [projectview] = @projectview, [lastlogin] = @lastlogin, [visits] = @visits, [hppopup] = @hppopup, [status] = @status, [flash] = @flash, [filelimit] = @filelimit, [timezone] = @timezone, [getmail] = @getmail, [emailpass2] = @emailpass2, [emailuser2] = @emailuser2, [remotemail2] = @remotemail2, [emailpass3] = @emailpass3, [emailuser3] = @emailuser3, [remotemail3] = @remotemail3, [fullname] = @fullname, [background2] = @background2, [background3] = @background3, [spam] = @spam, [blogcomments] = @blogcomments, [profilename] = @profilename, [remotemailtype] = @remotemailtype, [remotemail2type] = @remotemail2type, [remotemail3type] = @remotemail3type, [blogdate] = @blogdate, [blogtitle] = @blogtitle, [staff_level] = @staff_level, [Sex] = @Sex, [Branch] = @Branch WHERE [ID] = @original_ID AND (([phone] = @original_phone) OR ([phone] IS NULL AND @original_phone IS NULL)) AND (([fax] = @original_fax) OR ([fax] IS NULL AND @original_fax IS NULL)) AND (([city] = @original_city) OR ([city] IS NULL AND @original_city IS NULL)) AND (([state] = @original_state) OR ([state] IS NULL AND @original_state IS NULL)) AND (([Address1] = @original_Address1) OR ([Address1] IS NULL AND @original_Address1 IS NULL)) AND (([Address2] = @original_Address2) OR ([Address2] IS NULL AND @original_Address2 IS NULL)) AND (([zipcode] = @original_zipcode) OR ([zipcode] IS NULL AND @original_zipcode IS NULL)) AND (([email] = @original_email) OR ([email] IS NULL AND @original_email IS NULL)) AND (([Department] = @original_Department) OR ([Department] IS NULL AND @original_Department IS NULL)) AND (([location] = @original_location) OR ([location] IS NULL AND @original_location IS NULL)) AND (([building] = @original_building) OR ([building] IS NULL AND @original_building IS NULL)) AND (([last] = @original_last) OR ([last] IS NULL AND @original_last IS NULL)) AND (([first] = @original_first) OR ([first] IS NULL AND @original_first IS NULL)) AND (([MI] = @original_MI) OR ([MI] IS NULL AND @original_MI IS NULL)) AND (([picloc] = @original_picloc) OR ([picloc] IS NULL AND @original_picloc IS NULL)) AND (([office] = @original_office) OR ([office] IS NULL AND @original_office IS NULL)) AND (([Timestamp] = @original_Timestamp) OR ([Timestamp] IS NULL AND @original_Timestamp IS NULL)) AND (([title] = @original_title) OR ([title] IS NULL AND @original_title IS NULL)) AND (([background] = @original_background) OR ([background] IS NULL AND @original_background IS NULL)) AND (([empnum] = @original_empnum) OR ([empnum] IS NULL AND @original_empnum IS NULL)) AND (([listname] = @original_listname) OR ([listname] IS NULL AND @original_listname IS NULL)) AND (([uname] = @original_uname) OR ([uname] IS NULL AND @original_uname IS NULL)) AND (([pw] = @original_pw) OR ([pw] IS NULL AND @original_pw IS NULL)) AND (([admin] = @original_admin) OR ([admin] IS NULL AND @original_admin IS NULL)) AND (([surveyallow] = @original_surveyallow) OR ([surveyallow] IS NULL AND @original_surveyallow IS NULL)) AND (([emailpass] = @original_emailpass) OR ([emailpass] IS NULL AND @original_emailpass IS NULL)) AND (([emailuser] = @original_emailuser) OR ([emailuser] IS NULL AND @original_emailuser IS NULL)) AND (([remotemail] = @original_remotemail) OR ([remotemail] IS NULL AND @original_remotemail IS NULL)) AND (([D11] = @original_D11) OR ([D11] IS NULL AND @original_D11 IS NULL)) AND (([address3] = @original_address3) OR ([address3] IS NULL AND @original_address3 IS NULL)) AND (([sharecal] = @original_sharecal) OR ([sharecal] IS NULL AND @original_sharecal IS NULL)) AND (([cellphone] = @original_cellphone) OR ([cellphone] IS NULL AND @original_cellphone IS NULL)) AND (([pager] = @original_pager) OR ([pager] IS NULL AND @original_pager IS NULL)) AND (([country] = @original_country) OR ([country] IS NULL AND @original_country IS NULL)) AND (([supervisor] = @original_supervisor) OR ([supervisor] IS NULL AND @original_supervisor IS NULL)) AND (([calaccess] = @original_calaccess) OR ([calaccess] IS NULL AND @original_calaccess IS NULL)) AND (([projectview] = @original_projectview) OR ([projectview] IS NULL AND @original_projectview IS NULL)) AND (([lastlogin] = @original_lastlogin) OR ([lastlogin] IS NULL AND @original_lastlogin IS NULL)) AND (([visits] = @original_visits) OR ([visits] IS NULL AND @original_visits IS NULL)) AND (([hppopup] = @original_hppopup) OR ([hppopup] IS NULL AND @original_hppopup IS NULL)) AND (([status] = @original_status) OR ([status] IS NULL AND @original_status IS NULL)) AND (([flash] = @original_flash) OR ([flash] IS NULL AND @original_flash IS NULL)) AND (([filelimit] = @original_filelimit) OR ([filelimit] IS NULL AND @original_filelimit IS NULL)) AND (([timezone] = @original_timezone) OR ([timezone] IS NULL AND @original_timezone IS NULL)) AND (([getmail] = @original_getmail) OR ([getmail] IS NULL AND @original_getmail IS NULL)) AND (([emailpass2] = @original_emailpass2) OR ([emailpass2] IS NULL AND @original_emailpass2 IS NULL)) AND (([emailuser2] = @original_emailuser2) OR ([emailuser2] IS NULL AND @original_emailuser2 IS NULL)) AND (([remotemail2] = @original_remotemail2) OR ([remotemail2] IS NULL AND @original_remotemail2 IS NULL)) AND (([emailpass3] = @original_emailpass3) OR ([emailpass3] IS NULL AND @original_emailpass3 IS NULL)) AND (([emailuser3] = @original_emailuser3) OR ([emailuser3] IS NULL AND @original_emailuser3 IS NULL)) AND (([remotemail3] = @original_remotemail3) OR ([remotemail3] IS NULL AND @original_remotemail3 IS NULL)) AND (([fullname] = @original_fullname) OR ([fullname] IS NULL AND @original_fullname IS NULL)) AND (([background2] = @original_background2) OR ([background2] IS NULL AND @original_background2 IS NULL)) AND (([background3] = @original_background3) OR ([background3] IS NULL AND @original_background3 IS NULL)) AND (([spam] = @original_spam) OR ([spam] IS NULL AND @original_spam IS NULL)) AND (([blogcomments] = @original_blogcomments) OR ([blogcomments] IS NULL AND @original_blogcomments IS NULL)) AND (([profilename] = @original_profilename) OR ([profilename] IS NULL AND @original_profilename IS NULL)) AND (([remotemailtype] = @original_remotemailtype) OR ([remotemailtype] IS NULL AND @original_remotemailtype IS NULL)) AND (([remotemail2type] = @original_remotemail2type) OR ([remotemail2type] IS NULL AND @original_remotemail2type IS NULL)) AND (([remotemail3type] = @original_remotemail3type) OR ([remotemail3type] IS NULL AND @original_remotemail3type IS NULL)) AND (([blogdate] = @original_blogdate) OR ([blogdate] IS NULL AND @original_blogdate IS NULL)) AND (([blogtitle] = @original_blogtitle) OR ([blogtitle] IS NULL AND @original_blogtitle IS NULL)) AND (([staff_level] = @original_staff_level) OR ([staff_level] IS NULL AND @original_staff_level IS NULL)) AND (([Sex] = @original_Sex) OR ([Sex] IS NULL AND @original_Sex IS NULL)) AND (([Branch] = @original_Branch) OR ([Branch] IS NULL AND @original_Branch IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32"  />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="phone" Type="String" />
                    <asp:Parameter Name="fax" Type="String" />
                    <asp:Parameter Name="city" Type="String" />
                    <asp:Parameter Name="state" Type="String" />
                    <asp:Parameter Name="Address1" Type="String" />
                    <asp:Parameter Name="Address2" Type="String" />
                    <asp:Parameter DbType="Date" Name="zipcode" />
                    <asp:Parameter Name="email" Type="String" />
                    <asp:Parameter Name="Department" Type="String" />
                    <asp:Parameter Name="location" Type="String" />
                    <asp:Parameter Name="building" Type="String" />
                    <asp:Parameter Name="last" Type="String" />
                    <asp:Parameter Name="first" Type="String" />
                    <asp:Parameter Name="MI" Type="String" />
                    <asp:Parameter Name="picloc" Type="String" />
                    <asp:Parameter Name="office" Type="String" />
                    <asp:Parameter DbType="Date" Name="Timestamp" />
                    <asp:Parameter Name="title" Type="String" />
                    <asp:Parameter Name="background" Type="String" />
                    <asp:Parameter Name="empnum" Type="String" />
                    <asp:Parameter Name="listname" Type="String" />
                    <asp:Parameter Name="uname" Type="String" />
                    <asp:Parameter Name="pw" Type="String" />
                    <asp:Parameter Name="admin" Type="String" />
                    <asp:Parameter Name="surveyallow" Type="Int32" />
                    <asp:Parameter Name="emailpass" Type="String" />
                    <asp:Parameter Name="emailuser" Type="String" />
                    <asp:Parameter Name="remotemail" Type="String" />
                    <asp:Parameter Name="D11" Type="String" />
                    <asp:Parameter Name="address3" Type="String" />
                    <asp:Parameter Name="sharecal" Type="String" />
                    <asp:Parameter Name="cellphone" Type="String" />
                    <asp:Parameter Name="pager" Type="String" />
                    <asp:Parameter Name="country" Type="String" />
                    <asp:Parameter Name="supervisor" Type="String" />
                    <asp:Parameter Name="calaccess" Type="String" />
                    <asp:Parameter Name="projectview" Type="Int32" />
                    <asp:Parameter DbType="Date" Name="lastlogin" />
                    <asp:Parameter Name="visits" Type="Int32" />
                    <asp:Parameter Name="hppopup" Type="Int32" />
                    <asp:Parameter Name="status" Type="String" />
                    <asp:Parameter Name="flash" Type="String" />
                    <asp:Parameter Name="filelimit" Type="Int32" />
                    <asp:Parameter Name="timezone" Type="String" />
                    <asp:Parameter Name="getmail" Type="Int32" />
                    <asp:Parameter Name="emailpass2" Type="String" />
                    <asp:Parameter Name="emailuser2" Type="String" />
                    <asp:Parameter Name="remotemail2" Type="String" />
                    <asp:Parameter Name="emailpass3" Type="String" />
                    <asp:Parameter Name="emailuser3" Type="String" />
                    <asp:Parameter Name="remotemail3" Type="String" />
                    <asp:Parameter Name="fullname" Type="String" />
                    <asp:Parameter Name="background2" Type="String" />
                    <asp:Parameter Name="background3" Type="String" />
                    <asp:Parameter Name="spam" Type="String" />
                    <asp:Parameter Name="blogcomments" Type="Int32" />
                    <asp:Parameter Name="profilename" Type="String" />
                    <asp:Parameter Name="remotemailtype" Type="Int32" />
                    <asp:Parameter Name="remotemail2type" Type="Int32" />
                    <asp:Parameter Name="remotemail3type" Type="Int32" />
                    <asp:Parameter DbType="Date" Name="blogdate" />
                    <asp:Parameter Name="blogtitle" Type="String" />
                    <asp:Parameter Name="staff_level" Type="Int64" />
                    <asp:Parameter Name="Sex" Type="String" />
                    <asp:Parameter Name="Branch" Type="String" />
                    <asp:Parameter Name="original_ID" Type="String" />
                    <asp:Parameter Name="original_phone" Type="String" />
                    <asp:Parameter Name="original_fax" Type="String" />
                    <asp:Parameter Name="original_city" Type="String" />
                    <asp:Parameter Name="original_state" Type="String" />
                    <asp:Parameter Name="original_Address1" Type="String" />
                    <asp:Parameter Name="original_Address2" Type="String" />
                    <asp:Parameter DbType="Date" Name="original_zipcode" />
                    <asp:Parameter Name="original_email" Type="String" />
                    <asp:Parameter Name="original_Department" Type="String" />
                    <asp:Parameter Name="original_location" Type="String" />
                    <asp:Parameter Name="original_building" Type="String" />
                    <asp:Parameter Name="original_last" Type="String" />
                    <asp:Parameter Name="original_first" Type="String" />
                    <asp:Parameter Name="original_MI" Type="String" />
                    <asp:Parameter Name="original_picloc" Type="String" />
                    <asp:Parameter Name="original_office" Type="String" />
                    <asp:Parameter DbType="Date" Name="original_Timestamp" />
                    <asp:Parameter Name="original_title" Type="String" />
                    <asp:Parameter Name="original_background" Type="String" />
                    <asp:Parameter Name="original_empnum" Type="String" />
                    <asp:Parameter Name="original_listname" Type="String" />
                    <asp:Parameter Name="original_uname" Type="String" />
                    <asp:Parameter Name="original_pw" Type="String" />
                    <asp:Parameter Name="original_admin" Type="String" />
                    <asp:Parameter Name="original_surveyallow" Type="Int32" />
                    <asp:Parameter Name="original_emailpass" Type="String" />
                    <asp:Parameter Name="original_emailuser" Type="String" />
                    <asp:Parameter Name="original_remotemail" Type="String" />
                    <asp:Parameter Name="original_D11" Type="String" />
                    <asp:Parameter Name="original_address3" Type="String" />
                    <asp:Parameter Name="original_sharecal" Type="String" />
                    <asp:Parameter Name="original_cellphone" Type="String" />
                    <asp:Parameter Name="original_pager" Type="String" />
                    <asp:Parameter Name="original_country" Type="String" />
                    <asp:Parameter Name="original_supervisor" Type="String" />
                    <asp:Parameter Name="original_calaccess" Type="String" />
                    <asp:Parameter Name="original_projectview" Type="Int32" />
                    <asp:Parameter DbType="Date" Name="original_lastlogin" />
                    <asp:Parameter Name="original_visits" Type="Int32" />
                    <asp:Parameter Name="original_hppopup" Type="Int32" />
                    <asp:Parameter Name="original_status" Type="String" />
                    <asp:Parameter Name="original_flash" Type="String" />
                    <asp:Parameter Name="original_filelimit" Type="Int32" />
                    <asp:Parameter Name="original_timezone" Type="String" />
                    <asp:Parameter Name="original_getmail" Type="Int32" />
                    <asp:Parameter Name="original_emailpass2" Type="String" />
                    <asp:Parameter Name="original_emailuser2" Type="String" />
                    <asp:Parameter Name="original_remotemail2" Type="String" />
                    <asp:Parameter Name="original_emailpass3" Type="String" />
                    <asp:Parameter Name="original_emailuser3" Type="String" />
                    <asp:Parameter Name="original_remotemail3" Type="String" />
                    <asp:Parameter Name="original_fullname" Type="String" />
                    <asp:Parameter Name="original_background2" Type="String" />
                    <asp:Parameter Name="original_background3" Type="String" />
                    <asp:Parameter Name="original_spam" Type="String" />
                    <asp:Parameter Name="original_blogcomments" Type="Int32" />
                    <asp:Parameter Name="original_profilename" Type="String" />
                    <asp:Parameter Name="original_remotemailtype" Type="Int32" />
                    <asp:Parameter Name="original_remotemail2type" Type="Int32" />
                    <asp:Parameter Name="original_remotemail3type" Type="Int32" />
                    <asp:Parameter DbType="Date" Name="original_blogdate" />
                    <asp:Parameter Name="original_blogtitle" Type="String" />
                    <asp:Parameter Name="original_staff_level" Type="Int64" />
                    <asp:Parameter Name="original_Sex" Type="String" />
                    <asp:Parameter Name="original_Branch" Type="String" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:Parameter Name="ID" Type="String" />
                    <asp:Parameter Name="phone" Type="String" />
                    <asp:Parameter Name="fax" Type="String" />
                    <asp:Parameter Name="city" Type="String" />
                    <asp:Parameter Name="state" Type="String" />
                    <asp:Parameter Name="Address1" Type="String" />
                    <asp:Parameter Name="Address2" Type="String" />
                    <asp:Parameter DbType="Date" Name="zipcode" />
                    <asp:Parameter Name="email" Type="String" />
                    <asp:Parameter Name="Department" Type="String" />
                    <asp:Parameter Name="location" Type="String" />
                    <asp:Parameter Name="building" Type="String" />
                    <asp:Parameter Name="last" Type="String" />
                    <asp:Parameter Name="first" Type="String" />
                    <asp:Parameter Name="MI" Type="String" />
                    <asp:Parameter Name="picloc" Type="String" />
                    <asp:Parameter Name="office" Type="String" />
                    <asp:Parameter DbType="Date" Name="Timestamp" />
                    <asp:Parameter Name="title" Type="String" />
                    <asp:Parameter Name="background" Type="String" />
                    <asp:Parameter Name="empnum" Type="String" />
                    <asp:Parameter Name="listname" Type="String" />
                    <asp:Parameter Name="uname" Type="String" />
                    <asp:Parameter Name="pw" Type="String" />
                    <asp:Parameter Name="admin" Type="String" />
                    <asp:Parameter Name="surveyallow" Type="Int32" />
                    <asp:Parameter Name="emailpass" Type="String" />
                    <asp:Parameter Name="emailuser" Type="String" />
                    <asp:Parameter Name="remotemail" Type="String" />
                    <asp:Parameter Name="D11" Type="String" />
                    <asp:Parameter Name="address3" Type="String" />
                    <asp:Parameter Name="sharecal" Type="String" />
                    <asp:Parameter Name="cellphone" Type="String" />
                    <asp:Parameter Name="pager" Type="String" />
                    <asp:Parameter Name="country" Type="String" />
                    <asp:Parameter Name="supervisor" Type="String" />
                    <asp:Parameter Name="calaccess" Type="String" />
                    <asp:Parameter Name="projectview" Type="Int32" />
                    <asp:Parameter DbType="Date" Name="lastlogin" />
                    <asp:Parameter Name="visits" Type="Int32" />
                    <asp:Parameter Name="hppopup" Type="Int32" />
                    <asp:Parameter Name="status" Type="String" />
                    <asp:Parameter Name="flash" Type="String" />
                    <asp:Parameter Name="filelimit" Type="Int32" />
                    <asp:Parameter Name="timezone" Type="String" />
                    <asp:Parameter Name="getmail" Type="Int32" />
                    <asp:Parameter Name="emailpass2" Type="String" />
                    <asp:Parameter Name="emailuser2" Type="String" />
                    <asp:Parameter Name="remotemail2" Type="String" />
                    <asp:Parameter Name="emailpass3" Type="String" />
                    <asp:Parameter Name="emailuser3" Type="String" />
                    <asp:Parameter Name="remotemail3" Type="String" />
                    <asp:Parameter Name="fullname" Type="String" />
                    <asp:Parameter Name="background2" Type="String" />
                    <asp:Parameter Name="background3" Type="String" />
                    <asp:Parameter Name="spam" Type="String" />
                    <asp:Parameter Name="blogcomments" Type="Int32" />
                    <asp:Parameter Name="profilename" Type="String" />
                    <asp:Parameter Name="remotemailtype" Type="Int32" />
                    <asp:Parameter Name="remotemail2type" Type="Int32" />
                    <asp:Parameter Name="remotemail3type" Type="Int32" />
                    <asp:Parameter DbType="Date" Name="blogdate" />
                    <asp:Parameter Name="blogtitle" Type="String" />
                    <asp:Parameter Name="staff_level" Type="Int64" />
                    <asp:Parameter Name="Sex" Type="String" />
                    <asp:Parameter Name="Branch" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownList1" Name="location" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
        <td style="height: 144px">
        </td>
        </tr>
        </table>
        </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

