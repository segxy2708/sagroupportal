
Partial Class HR_Portal
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "HR Portal"
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("~/session_timeout.aspx")
        End If
        If Session("Department") = "Legal/Company Secretary" Or Session("Department") = "Legal" Or Session("Department") = "Human Resources" Or Session("Department") = "Human Capital" Or Session("Department") = "EXECUTIVES" Or Session("role") = "Super Administrator" Then
            HyperLink1.Visible = True
            HyperLink2.Visible = True
            lblMessage.Visible = False

        Else
            HyperLink1.Visible = False
            HyperLink2.Visible = False
            lblMessage.Visible = True
            lblMessage.Text = "You are not eligible to view this portal"
            lblMessage.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub
End Class
