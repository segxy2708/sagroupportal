
Partial Class File_Inbox
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = Session("names") & " File Inbox"
        If Me.MyFilePan.Visible = True Then
            Me.LinkButton4.Visible = True
        Else
            Me.LinkButton4.Visible = False
        End If
        If Me.DeptPan.Visible = True Then
            Me.LinkButton5.Visible = True
        Else
            Me.LinkButton5.Visible = False
        End If
        If Me.GroupPan.Visible = True Then
            Me.LinkButton6.Visible = True
        Else
            Me.LinkButton6.Visible = False
        End If

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        Me.FileOwner.Text = Session("names")

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim str As StringBuilder = New StringBuilder
        For i As Integer = 0 To DataList2.Items.Count
            Dim row As DataListItem = DataList2.Items(i)
            Dim isCheck As Boolean = CType(row.FindControl("chkList"), CheckBox).Checked
            If isCheck Then

            End If
        Next
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        myFileVisible()
        Me.LinkButton4.Visible = True
    End Sub

    Sub myFileVisible()
        Me.MyFilePan.Visible = True
    End Sub
    Sub myFileHide()
        Me.MyFilePan.Visible = False
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Me.DeptPan.Visible = True
        Me.LinkButton5.Visible = True
    End Sub

    Protected Sub LinkButton3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton3.Click
        Me.GroupPan.Visible = True
        Me.LinkButton6.Visible = True
    End Sub

    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton4.Click
        'Me.LinkButton4.Visible = False
        Me.MyFilePan.Visible = False
        Me.Panel1.Visible = False
    End Sub

    Protected Sub LinkButton5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton5.Click
        Me.DeptPan.Visible = False
    End Sub

    Protected Sub LinkButton6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton6.Click
        Me.GroupPan.Visible = False
    End Sub

    Protected Sub myGrpFileBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles myGrpFileBut.Click
        Panel1.Visible = True
    End Sub
End Class
