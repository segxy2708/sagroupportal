<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Marketers_Tracker.aspx.vb" Inherits="Marketers_Tracker" title="Marketer's Tracker" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label2" runat="server"></asp:Label>
    &nbsp; &nbsp; &nbsp;<br />
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp;
    <asp:Label ID="lblDate" runat="server"></asp:Label><br />
    <br />
    <asp:Panel ID="Panel1" runat="server">
    <table style="width: 626px">
        <tr>
            <td align="left" style="width: 143px">
            </td>
            <td style="width: 241px">
            </td>
            <td>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 143px">
                Subsidiary</td>
            <td style="width: 241px">
                <asp:DropDownList ID="drpCompany" runat="server" AutoPostBack="True" CssClass="text_styles"
                    Width="300px">
                    <asp:ListItem>- Select -</asp:ListItem>
                    <asp:ListItem>SA Insurance</asp:ListItem>
                    <asp:ListItem>SA Life</asp:ListItem>
                    <asp:ListItem>SA Pension</asp:ListItem>
                    <asp:ListItem>SA Group</asp:ListItem>
                    <asp:ListItem>SA Money</asp:ListItem>
                    <asp:ListItem>SA Properties</asp:ListItem>
                    <asp:ListItem>Lagoon Home</asp:ListItem>
                </asp:DropDownList></td>
            <td>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 143px; height: 24px;">
                Location:</td>
            <td style="width: 241px; height: 24px">
                <asp:TextBox ID="TextBox1" runat="server" CssClass="text_styles" Width="300px" AutoCompleteType="Disabled"></asp:TextBox>
                &nbsp;
            </td>
            <td style="height: 24px" valign="top">
                <asp:Button ID="Button2" runat="server" Text="Track" /></td>
            <td style="width: 3px; height: 24px;">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <asp:DataList ID="DataList1" runat="server" CellPadding="4" DataKeyField="id" DataSourceID="SqlDataSource1"
                    ForeColor="#333333">
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                    <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <HeaderTemplate>
                        <table border="0" style="width: 740px">
                            <tr>
                                <td style="width: 323px; color: #ffff99; height: 21px; background-color: #336666">
                Marketer's Name</td>
                                <td style="width: 253px; color: #ffff99; height: 21px; background-color: #336666">
                                    Status</td>
                                <td style="width: 253px; color: #ffff99; height: 21px; background-color: #336666">
                                    Location</td>
                                <td style="color: #ffff99; height: 21px; background-color: #336666">
                                    Action</td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                        <table border="0" style="width: 740px; background-color: #cc9900">
                            <tr>
                                <td style="width: 322px">
                                    <asp:Label ID="marketernameLabel" runat="server" Font-Bold="False" Text='<%# Eval("marketername") %>'></asp:Label></td>
                                <td style="width: 250px">
                                    <asp:Label ID="statusLabel" runat="server" Font-Bold="False" Text='<%# Eval("status") %>'></asp:Label></td>
                                <td style="width: 250px">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="False" Text='<%# Eval("location", "{0}") %>'></asp:Label></td>
                                <td>
                                    <asp:LinkButton ID="LinkButton3" runat="server" Font-Bold="False" CommandArgument="Marketers_Tracker_Details.aspx" CommandName="Edit">Details</asp:LinkButton></td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MarketerDBConnectionString %>"
                    SelectCommand="SELECT * FROM [Movement] WHERE (([status] = @status) AND ([company] = @company) AND ([location] = @location))">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Open" Name="status" Type="String" />
                        <asp:ControlParameter ControlID="drpCompany" Name="company" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="TextBox1" Name="location" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                &nbsp;
                &nbsp; &nbsp;
                <br />
            </td>
        </tr>
        <tr>
            <td align="left" colspan="4" style="text-align: center">
                <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

