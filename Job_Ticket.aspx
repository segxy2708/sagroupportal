<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Job_Ticket.aspx.vb" Inherits="Job_Ticket" title="Support Ticketing Portal" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="mainTab" >
<tr>
<td style="height: 20px">
    <asp:HyperLink ID="AdminView" runat="server" NavigateUrl="~/View_Job_Title.aspx" Visible="False">View Request</asp:HyperLink>&nbsp;
    <asp:HyperLink ID="EscalateLink" runat="server" NavigateUrl="~/Escalation.aspx">Escalation</asp:HyperLink></td>
    <td style="height: 20px">
        <asp:LinkButton ID="TrTicket" runat="server" CausesValidation="False" PostBackUrl="~/Job_Ticket_Summary.aspx">View Resolved Tickets:</asp:LinkButton></td>
</tr>
<tr>
<td style="height: 16px">
    &nbsp;</td>
    <td style="height: 16px">
        <asp:Label ID="mess" runat="server" Text=""></asp:Label>
    </td>
</tr>
<tr>
<td style="height: 18px">
    <asp:Label ID="Label8" runat="server" Text="Branch"></asp:Label></td>
<td style="height: 18px">
    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="True" DataSourceID="brnchList"
        DataTextField="Branch_List" DataValueField="Branch_List">
        <asp:ListItem>- Select -</asp:ListItem>
    </asp:DropDownList><asp:SqlDataSource ID="brnchList" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT [Branch_List] FROM [Branch_List] ORDER BY [Branch_List]"></asp:SqlDataSource>
</td>
</tr>
<tr>
<td>
<asp:Label runat="server" ID="lab1" Text ="Ticket Number:" ></asp:Label>
</td>
<td>
<asp:TextBox runat ="server" ID="TicketNumText" ReadOnly="True" Width="250px" CssClass="text_styles"></asp:TextBox>
</td>
</tr>
<tr>
<td>
<asp:Label runat="server" ID="Label1" Text ="Ticket Creator:" ></asp:Label>
</td>
<td>
    <asp:TextBox ID="CreatorTxt" runat="server" ReadOnly="True" Width="250px" CssClass="text_styles"></asp:TextBox></td>
</tr>
<tr>
<td>
<asp:Label runat="server" ID="Label2" Text ="Date & Time:" ></asp:Label>
</td>
<td>
    <asp:TextBox ID="DateTimeTxt" runat="server" ReadOnly="True" Width="250px" CssClass="text_styles"></asp:TextBox></td>
</tr>
<tr>
<td valign="middle">
<asp:Label runat="server" ID="Label5" Text ="Priority:" ></asp:Label>

</td>
<td style="width: 400px" valign="middle">
    <asp:RadioButtonList ID="PriorityList" runat="server" RepeatDirection="Horizontal" CssClass="text_styles" EnableTheming="False" ForeColor="White">
        <asp:ListItem>Low</asp:ListItem>
        <asp:ListItem>Medium</asp:ListItem>
        <asp:ListItem>High</asp:ListItem>
        <asp:ListItem>Very High</asp:ListItem>
    </asp:RadioButtonList></td>
</tr>
<tr>
<td>
<asp:Label runat="server" ID="Label3" Text ="Problem(s) Description:" ></asp:Label></td>
<td>
    <asp:TextBox ID="Symptomstxt" runat="server" TextMode="MultiLine" CssClass="text_styles" Width="450px" Height="115px"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Symptomstxt"
        Display="Dynamic" ErrorMessage="cannot be empty">*</asp:RequiredFieldValidator></td>
</tr>
<tr>
<td>
<asp:Label runat="server" ID="Label4" Text ="Affected Hardware:" ></asp:Label></td>
<td>
    <asp:TextBox ID="AffectedAssetTxt" runat="server" TextMode="MultiLine" Height="115px" Width="450px" CssClass="text_styles"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="AffectedAssetTxt"
        ErrorMessage="cannot be empty">*</asp:RequiredFieldValidator></td>
</tr>
<tr>
<td>
<asp:Label runat="server" ID="Label6" Text ="Attempted Correction(s):" ></asp:Label></td>
<td>
    <asp:TextBox ID="Detailtxt" runat="server" TextMode="MultiLine" Height="115px" Width="450px" CssClass="text_styles"></asp:TextBox></td>
</tr>
<tr>
<td>
<asp:Label runat="server" ID="Label7" Text ="Sign Off:" Visible="False" ></asp:Label>
</td>
<td>
    <asp:TextBox ID="SignOffTxt" runat="server" CssClass="text_styles" Width="250px" Visible="False"></asp:TextBox></td>
</tr>
<tr>
<td>
    &nbsp;</td>
<td>

    <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="Buttons" CausesValidation="False" />
    <ajaxToolkit:ConfirmButtonExtender ID="confbut" runat ="server" TargetControlID ="Button1" ConfirmOnFormSubmit ="false" ConfirmText ="Click OK to Submit or Cancel to quit Operation"></ajaxToolkit:ConfirmButtonExtender>

    <asp:SqlDataSource ID="JobTitleSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        InsertCommand="INSERT INTO Job_Title_Table(TicketNo, TicketCreator, DateTime, PersonalDetails, AffectedAsset, Priority, Symptoms, SignOff, Treated, senderMailAddr, Branch, IP) VALUES (@TicketNo, @TicketCreator, @DateTime, @PersonalDetails, @AffectedAsset, @Priority, @Symptoms, @SignOff, N'NO', @senderMailAddr, @Branch, @IP)"
        SelectCommand="SELECT * FROM [Job_Title_Table]" UpdateCommand="UPDATE Job_Title_Table SET Resolution = @Resolution WHERE (TicketNo = @TicketNo)">
        <InsertParameters>
            <asp:ControlParameter ControlID="TicketNumText" Name="TicketNo" PropertyName="Text" />
            <asp:ControlParameter ControlID="CreatorTxt" Name="TicketCreator" PropertyName="Text" />
            <asp:ControlParameter ControlID="Detailtxt" Name="PersonalDetails" PropertyName="Text" />
            <asp:ControlParameter ControlID="AffectedAssetTxt" Name="AffectedAsset" PropertyName="Text" />
            <asp:ControlParameter ControlID="PriorityList" Name="Priority" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="Symptomstxt" Name="Symptoms" PropertyName="Text" />
            <asp:ControlParameter ControlID="SignOffTxt" Name="SignOff" PropertyName="Text" />
            <asp:SessionParameter Name="senderMailAddr" SessionField="email" />
            <asp:ControlParameter ControlID="DropDownList1" Name="Branch" PropertyName="SelectedValue" />
            <asp:SessionParameter Name="IP" SessionField="ip" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Resolution" />
            <asp:Parameter Name="TicketNo" />
        </UpdateParameters>
    </asp:SqlDataSource>
</td>
</tr>
<tr>
<td>

</td>
<td>

</td>
</tr>

</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

