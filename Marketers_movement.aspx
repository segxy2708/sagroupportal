<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Marketers_movement.aspx.vb" Inherits="Marketers_movement" title="Marketer's Movement Chat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="Label2" runat="server"></asp:Label><br />
    &nbsp;<br />
    <asp:Panel ID="Panel1" runat="server">
    <table style="width: 512px">
        <tr>
            <td align="left" style="width: 233px">
            </td>
            <td>
            </td>
            <td>
            </td>
            <td style="width: 3px">
    <asp:Label ID="lblDate" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" style="width: 233px">
                Subsidiary</td>
            <td>
                <asp:DropDownList ID="drpCompany" runat="server" AutoPostBack="True" Width="300px" CssClass="text_styles" AppendDataBoundItems="True">
                    <asp:ListItem>- Select -</asp:ListItem>
                    <asp:ListItem>SA Insurance</asp:ListItem>
                    <asp:ListItem>SA Life</asp:ListItem>
                    <asp:ListItem>SA Pension</asp:ListItem>
                    <asp:ListItem>SA Group</asp:ListItem>
                    <asp:ListItem>SA Money</asp:ListItem>
                    <asp:ListItem>SA Properties</asp:ListItem>
                    <asp:ListItem>Lagoon Home</asp:ListItem>
                </asp:DropDownList></td>
            <td>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
            <td align="left" style="width: 233px">
                Marketer's Name:</td>
            <td>
                <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource1"
                    DataTextField="fullname" DataValueField="fullname" Width="300px" AutoPostBack="True" CssClass="text_styles" AppendDataBoundItems="True">
                    <asp:ListItem>- Select -</asp:ListItem>
                </asp:DropDownList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                    SelectCommand="SELECT [fullname] FROM [directory] WHERE (([location] = @location) AND ([Department] = @Department)) ORDER BY [fullname]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="drpCompany" Name="location" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:Parameter DefaultValue="Marketing" Name="Department" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="4">
                <br />
                <br />
                <asp:DataList ID="DataList1" runat="server" DataKeyField="id" DataSourceID="SqlDataSource2" Font-Size="XX-Small" Height="124px" Width="712px">
                    <ItemTemplate>
                        <table style="width: 847px" border ="0">
                            <tr>
                                <td style="width: 3291px; height: 21px; background-color: #cc9933;">
                                    <asp:Label ID="locationLabel" runat="server" Text='<%# Eval("location", "{0}") %>'></asp:Label></td>
                                <td style="width: 4354px; height: 21px; background-color: #cc9933; text-align: left;" align="left">
                                    <asp:Label ID="clientLabel" runat="server" Text='<%# Eval("client", "{0}") %>'></asp:Label></td>
                                <td style="width: 6534px; height: 21px; background-color: #cc9933; text-align: left;" align="left">
                                    <asp:Label ID="addressLabel" runat="server" Text='<%# Eval("address", "{0}") %>'></asp:Label></td>
                                <td style="width: 340px; height: 21px; background-color: #cc9933; text-align: right;">
                                    <asp:Label ID="statusLabel" runat="server" Text='<%# Eval("status", "{0}") %>'></asp:Label></td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <HeaderTemplate>
                        <table style="width: 847px">
                            <tr>
                                <td style="width: 631px; height: 21px; background-color: #336666">
                                    <strong><span style="color: #ffff99">Location</span></strong></td>
                                <td style="width: 1350px; height: 21px; background-color: #336666">
                                    <strong><span style="color: #ffff99">Client Name</span></strong></td>
                                <td style="width: 1318px; height: 21px; background-color: #336666">
                                    <strong><span style="color: #ffff99">Client Address:</span></strong></td>
                                <td style="width: 207px; height: 21px; background-color: #336666; text-align: right;">
                                    <strong><span style="color: #ffff99">Status</span></strong></td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                </asp:DataList><br />
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MarketerConnectionString %>"
                    SelectCommand="SELECT id, movedate, marketername, location, client, address, status, marketerid FROM Movement WHERE (marketerid LIKE @marketerid) AND (movedate = @movedate) ORDER BY id" ProviderName="<%$ ConnectionStrings:MarketerConnectionString.ProviderName %>">
                    <SelectParameters>
                        <asp:SessionParameter Name="marketerid" SessionField="id" />
                        <asp:ControlParameter ControlID="lblDate" DbType="DateTime" Name="movedate" PropertyName="Text" />
                    </SelectParameters>
                </asp:SqlDataSource>
                &nbsp;&nbsp;
                <br />
            </td>
        </tr>
        <tr>
            <td style="width: 233px" align="left">
            </td>
            <td>
            </td>
            <td>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
            <td style="width: 233px; height: 24px;" align="left">
                New Location:</td>
            <td style="height: 24px">
                <asp:TextBox ID="txtLocation" runat="server" Width="300px" CssClass="text_styles" AutoCompleteType="Disabled"></asp:TextBox></td>
            <td style="height: 24px">
            </td>
            <td style="width: 3px; height: 24px;">
            </td>
        </tr>
        <tr>
            <td style="width: 233px" align="left">
                Company Name:</td>
            <td>
                <asp:TextBox ID="txtClient" runat="server" Width="300px" CssClass="text_styles" AutoCompleteType="Disabled"></asp:TextBox></td>
            <td>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
            <td style="width: 233px" align="left">
                Address:</td>
            <td>
                <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" Width="300px" CssClass="text_styles"></asp:TextBox>&nbsp;<asp:Button
                    ID="Button1" runat="server" Text="Set New Location" /></td>
            <td>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" align="left" colspan="4">
                &nbsp;<asp:Label ID="Label1" runat="server" Font-Size="Small" Text="Previous Location with an 'Open' Status would be close. Are you sure you want to Set New Location for this Marketer?"
                    Visible="False"></asp:Label>
                <asp:LinkButton ID="LinkButton1" runat="server" Font-Size="Small" Visible="False">Yes</asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server" Font-Size="Small" Visible="False">No</asp:LinkButton></td>
        </tr>
        <tr>
            <td style="text-align: center;" align="left" colspan="4">
                <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
        </tr>
    </table>
    </asp:Panel>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

