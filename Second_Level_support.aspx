<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Second_Level_support.aspx.vb" Inherits="Second_Level_support" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table id="mainTab">
        <tr>
            <td style="height: 18px">
            </td>
            <td>
                <asp:Label ID="errorLab" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lab1" runat="server" Text="Ticket Number:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TicketNumText" runat="server" CssClass="text_styles" ReadOnly="True"
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Ticket Creator:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="CreatorTxt" runat="server" CssClass="text_styles" ReadOnly="True"
                    Width="250px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Date & Time:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="DateTimeTxt" runat="server" CssClass="text_styles" ReadOnly="True"
                    Width="250px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Personal Details:"></asp:Label>
            </td>
            <td>
                &nbsp;<asp:TextBox ID="Symptomstxt" runat="server" CssClass="text_styles" ReadOnly="True"
                    TextMode="MultiLine" Width="330px" Height="115px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Affected Hardware:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="AffectedAssetTxt" runat="server" CssClass="text_styles" Height="115px"
                    ReadOnly="True" TextMode="MultiLine" Width="330px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:Label ID="Label6" runat="server" Text="User Attempts:"></asp:Label></td>
            <td>
                <asp:TextBox ID="Detailtxt" runat="server" CssClass="text_styles" Height="115px"
                    ReadOnly="True" TextMode="MultiLine" Width="330px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:Label ID="Label5" runat="server" Text="Steps Taken (Full Details):"></asp:Label></td>
            <td>
                <asp:TextBox ID="ResolutionText" runat="server" CssClass="text_styles" Height="115px"
                    TextMode="MultiLine" Width="330px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ResolutionText"
                    Display="Dynamic" ErrorMessage="Text cannot be empty" SetFocusOnError="True">*</asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="height: 24px">
                <asp:Label ID="Label7" runat="server" Text="Sign Off:" Visible="False"></asp:Label>
            </td>
            <td style="height: 24px">
                <asp:TextBox ID="SignOffTxt" runat="server" CssClass="text_styles" Width="250px" Visible="False"></asp:TextBox></td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" CssClass="Buttons"
                    Text="Submit Resolution" />
                <asp:SqlDataSource ID="ResolutionSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    InsertCommand="INSERT INTO ResolutionsKB(Symptoms, Resolution, Asset) VALUES (@Symptoms, @Resolution, @Asset)"
                    SelectCommand="SELECT * FROM [ResolutionsKB]">
                    <InsertParameters>
                        <asp:ControlParameter ControlID="Symptomstxt" Name="Symptoms" PropertyName="Text" />
                        <asp:ControlParameter ControlID="ResolutionText" Name="Resolution" PropertyName="Text" />
                        <asp:ControlParameter ControlID="AffectedAssetTxt" Name="Asset" PropertyName="Text" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="complainDetails" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                    SelectCommand="SELECT TicketNo, DateTime, PersonalDetails, AffectedAsset, Symptoms FROM Job_Title_Table WHERE (TicketNo = @TicketNo)"
                    UpdateCommand="UPDATE Job_Title_Table SET StepsTaken = @StepsTaken, Escalator = @Escalator, SignOff = @SignOff WHERE (TicketNo = @TicketNo)">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="TicketNo" QueryStringField="ticketNo" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="TicketNumText" Name="TicketNo" PropertyName="Text" />
                        <asp:ControlParameter ControlID="ResolutionText" Name="StepsTaken" PropertyName="Text" />
                        <asp:SessionParameter Name="Escalator" SessionField="names" />
                        <asp:Parameter DefaultValue="NO" Name="SignOff" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

