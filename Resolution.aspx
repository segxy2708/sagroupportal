<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Resolution.aspx.vb" Inherits="Resolution" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table id="mainTab">
        <tr>
            <td style="height: 18px">
            </td>
            <td>
                <asp:Label ID="errorLab" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lab1" runat="server" Text="Ticket Number:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TicketNumText" runat="server" CssClass="text_styles" ReadOnly="True"
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Ticket Creator:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="CreatorTxt" runat="server" CssClass="text_styles" ReadOnly="True"
                    Width="250px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Date & Time:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="DateTimeTxt" runat="server" CssClass="text_styles" ReadOnly="True"
                    Width="250px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Problems Description:"></asp:Label>
            </td>
            <td>
            <asp:TextBox ID="Symptomstxt" runat="server" CssClass="text_styles" ReadOnly="True"
                    TextMode="MultiLine" Width="330px" Height ="115px"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Affected Hardware:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="AffectedAssetTxt" runat="server" CssClass="text_styles" Height="115px"
                    ReadOnly="True" TextMode="MultiLine" Width="330px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:Label ID="Label6" runat="server" Text="Users Attempts:"></asp:Label></td>
            <td>
            <asp:TextBox ID="Detailtxt" runat="server" CssClass="text_styles" Height="115px"
                    ReadOnly="True" TextMode="MultiLine" Width="330px"></asp:TextBox></td>
        </tr>
        <tr>
        <td>
        
        </td>
        <td>
        
        </td>
        </tr>
        <tr>
            <td>
                &nbsp;<asp:Label ID="Label5" runat="server" Text="Resolution"></asp:Label></td>
            <td>
                <asp:TextBox ID="ResolutionText" runat="server" CssClass="text_styles" Height="115px"
                    TextMode="MultiLine" Width="330px"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="height: 24px">
                <asp:Label ID="Label7" runat="server" Text="Sign Off:" Visible="False"></asp:Label>
            </td>
            <td style="height: 24px">
                <asp:TextBox ID="SignOffTxt" runat="server" CssClass="text_styles" Width="250px" Visible="False"></asp:TextBox></td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" CausesValidation="False" CssClass="Buttons"
                    Text="Submit Resolution" />
                    <ajaxToolkit:ConfirmButtonExtender ID="confbut" runat ="server" TargetControlID ="Button1" ConfirmOnFormSubmit ="false" ConfirmText ="Click OK to Submit or Cancel to quit Operation"></ajaxToolkit:ConfirmButtonExtender>
                <asp:SqlDataSource ID="ResolutionSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    InsertCommand="INSERT INTO ResolutionsKB(Symptoms, Resolution, Asset) VALUES (@Symptoms, @Resolution, @Asset)"
                    SelectCommand="SELECT * FROM [ResolutionsKB]">
                    <InsertParameters>
                        <asp:ControlParameter ControlID="Symptomstxt" Name="Symptoms" PropertyName="Text" />
                        <asp:ControlParameter ControlID="ResolutionText" Name="Resolution" PropertyName="Text" />
                        <asp:ControlParameter ControlID="AffectedAssetTxt" Name="Asset" PropertyName="Text" />
                    </InsertParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="complainDetails" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                    SelectCommand="SELECT TicketNo, DateTime, PersonalDetails, AffectedAsset, Symptoms FROM Job_Title_Table WHERE (TicketNo = @TicketNo)"
                    UpdateCommand="UPDATE Job_Title_Table SET Treated = @Treated, DateTreated = @DateTreated, Attendance = @Attendance, Month = @Month, SignOff = @SignOff, Resolution = @Resolution WHERE (TicketNo = @TicketNo)">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="TicketNo" QueryStringField="ticketNo" Type="String" />
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="TicketNumText" Name="TicketNo" PropertyName="Text" />
                        <asp:Parameter DefaultValue="NO" Name="SignOff" />
                        <asp:ControlParameter ControlID="ResolutionText" Name="Resolution" PropertyName="Text" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Panel ID="RequestPan" runat="server" Visible="False" Width="400px">
                    <asp:Label ID="Label8" runat="server" Text="Click Yes to Continue or No to Quit"></asp:Label>
                    <asp:Button ID="Button2" runat="server" CssClass="Buttons" Text="Yes" Width="47px" />&nbsp;<asp:Button
                        ID="Button3" runat="server" CssClass="Buttons" Text="No" Width="39px" /></asp:Panel>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

