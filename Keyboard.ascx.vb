Imports System.Windows.Forms
Partial Class Keyboard
    Inherits System.Web.UI.UserControl

    Property A_Press() As String
        Get

            Return "A"
        End Get
        Set(ByVal value As String)
            value = "A"
        End Set
    End Property
    Property B_Press() As String
        Get
            Return "B"
        End Get
        Set(ByVal value As String)
            value = "B"
        End Set
    End Property
    Property C_Press() As String
        Get
            Return "C"
        End Get
        Set(ByVal value As String)
            value = "C"
        End Set
    End Property
    Property D_Press() As String
        Get
            Return "D"
        End Get
        Set(ByVal value As String)
            value = "D"
        End Set
    End Property
    Property E_Press() As String
        Get
            Return "E"
        End Get
        Set(ByVal value As String)
            value = "E"
        End Set
    End Property
    Property F_Press() As String
        Get
            Return "F"
        End Get
        Set(ByVal value As String)
            value = "F"
        End Set
    End Property

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
    End Sub
End Class
