Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
'Imports System.Net.Mail
Imports System.Net
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.IO
Imports System.Web.Mail
Imports System.Windows.Forms


Partial Class Job_Ticket
    Inherits System.Web.UI.Page

    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter

    Dim tickNo As String
    Dim creator As String
    Dim myDateTime As String = Todays() + "  " + checkTime(DateTime.Now.TimeOfDay.Hours.ToString) + ":" + checkTime(DateTime.Now.TimeOfDay.Minutes.ToString) + ":" + checkTime(DateTime.Now.TimeOfDay.Seconds.ToString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If

        If getSignOff() = "NO" Then

            Response.Redirect("SignOffRequest.aspx")


        Else 'If getSignOff() = "yes" Then
            tickNo = detectDivision() + "/" + detectDepartment() + "/" + DateTime.Now.Millisecond.ToString + DateTime.Now.Year.ToString + DateTime.Now.Month.ToString + DateTime.Now.Minute.ToString
            creator = Session("names")

            Me.TicketNumText.Text = tickNo
            Me.CreatorTxt.Text = creator
            Me.DateTimeTxt.Text = myDateTime
            If Session("role") = "Super Administrator" Or Session("role") = "Administrator" Or Session("role") = "Admin" Then
                Me.AdminView.Visible = True
                Me.EscalateLink.Visible = True
                Me.TrTicket.Visible = True
            Else
                Me.AdminView.Visible = False
                Me.EscalateLink.Visible = False
                Me.TrTicket.Visible = False
            End If

        End If
        
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Public Function Todays()
        Dim thisday As String = DateTime.Now.Date.Year.ToString + "-" + DateTime.Now.Date.Month.ToString + "-" + DateTime.Now.Date.Day.ToString
        Return thisday
    End Function

    'Public Sub sendmail(ByVal from As String, ByVal tos As String)
    '    Dim mymail As MailMessage = New MailMessage()
    '    'Dim s As MailPriority = PriorityList.SelectedValue
    '    mymail.From = New MailAddress(from)
    '    mymail.To.Add(New MailAddress(tos))
    '    mymail.Subject = "Job Request"

    '    mymail.IsBodyHtml = False
    '    If Me.PriorityList.SelectedIndex = 0 Then
    '        mymail.Priority = MailPriority.Low
    '    ElseIf Me.PriorityList.SelectedIndex = 1 Then
    '        mymail.Priority = MailPriority.Normal
    '    ElseIf Me.PriorityList.SelectedIndex = 2 Then
    '        mymail.Priority = MailPriority.High
    '    ElseIf Me.PriorityList.SelectedIndex = 3 Then
    '        mymail.Priority = MailPriority.High
    '    End If
    '    Dim message As StringBuilder = New StringBuilder

    '    'message.Append(url)

    '    message.Append("you have a request from " + Session("names") + vbCrLf + " ")

    '    message.Append("Details: " + Me.Detailtxt.Text + vbCrLf + " ")

    '    message.Append("Affected Asset: " + Me.AffectedAssetTxt.Text + vbCrLf + " ")

    '    message.Append("Symptoms: " + Me.Symptomstxt.Text + vbCrLf + " ")


    '    'mymail.BodyFormat = MailFormat.Text
    '    mymail.Body = message.ToString

    '    Dim clients As SmtpClient = New SmtpClient("208.88.73.108")
    '    clients.Send(mymail)
    '    'SmtpMail.Send(mymail)

    'End Sub

    Sub clearText()
        Me.Detailtxt.Text = ""
        Me.AffectedAssetTxt.Text = ""
        Me.Symptomstxt.Text = ""
        Me.SignOffTxt.Text = ""
        Me.PriorityList.SelectedIndex = Nothing
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.DropDownList1.SelectedIndex = 0 Then
            Me.mess.Text = "please select branch"
        Else

            Dim Message2Submit As String = "You are about to submit the following Information:" + vbCrLf + "Ticket No: " + Me.TicketNumText.Text + vbCrLf + "Ticket Creator: " + Me.CreatorTxt.Text + vbCrLf + _
            "Date & Time:" + Me.DateTimeTxt.Text + vbCrLf + "Affected Asset: " + Me.AffectedAssetTxt.Text + vbCrLf + "Priority: " + Me.PriorityList.SelectedValue + vbCrLf + _
    "Symptoms: " + Me.Symptomstxt.Text + vbCrLf + "Requesting Officer:" + Me.SignOffTxt.Text + vbCrLf + vbCrLf + "Click OK to continue to Cancel to Edit"

            'Dim res As DialogResult = MessageBox.Show(Message2Submit, "Caution", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly)
            'If res = DialogResult.OK Then
            'Try

            Dim timeParam As New System.Web.UI.WebControls.Parameter("DateTime", TypeCode.String, myDateTime)
            Me.JobTitleSource.InsertParameters.Add(timeParam)
            'sendmail(Session("email"), "systems@sagroupng.com")
            '------------------------------Sending Email to Alert People-----------------------------
            'Dim mails As MailMessage = New MailMessage

            ''mails.From = New MailAddress(Session("email"), Session("name"))
            ''mails.To.Add(New MailAddress("systems@sagroupng.com", "System Support"))
            ''mails.Subject = "Job Request"
            ''mails.IsBodyHtml = True
            ''mails.Body = "you have a request from " + Session("names") & " Details:  " & Me.Detailtxt.Text & " Affected Asset: " & Me.Detailtxt.Text & " Symptoms: " & Me.Symptomstxt.Text

            ''Dim clients As SmtpClient = New SmtpClient("localhost")
            ''clients.UseDefaultCredentials = True
            '' ''clients.Host = " 208.88.73.108"
            ' ''clients.Credentials = New NetworkCredential("systems@sagroupng.com", "welcome")
            ' ''clients.EnableSsl = True

            ''clients.Send(mails)



            Dim mails As MailMessage = New MailMessage()
            'mails.To = "systems@sagroupng.com"
            mails.To = "ofajinmi@sagroupng.com"
            mails.From = Session("email")

            mails.Subject = "Job Request"
            mails.Body = "you have a request from " + Session("names") & " Details:  " & Me.Detailtxt.Text & " Affected Asset: " & Me.Detailtxt.Text & " Symptoms: " & Me.Symptomstxt.Text
            SmtpMail.SmtpServer = "localhost"
            SmtpMail.Send(mails)

            '---------------------End Mail Sender----------------------------------------------------
            Me.JobTitleSource.Insert()
            clearText()
            mess.Text = "Complain Registered Successfully"
            mess.Visible = True
            mess.Focus()
            'Catch ex As Exception
            '    mess.Text = "Error Registering Complain"
            '    mess.Focus()
            '    'MessageBox.Show("message not sent", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, False)
            'End Try
            'ElseIf res = DialogResult.Cancel Then

            'End If


        End If
    End Sub

    Private Function checkTime(ByVal s As String) As String
        If Convert.ToInt32(s) < 10 Then
            s = "0" + s
        End If
        Return s
    End Function

    Private Function detectDivision() As String
        If Session("Group") = "SA Group" Then
            Return "SAG"
        ElseIf Session("Group") = "SA Insurance" Then
            Return "SAI"
        ElseIf Session("Group") = "SA E-Business" Then
            Return "SAE"

        ElseIf Session("Group") = "SA Life" Then
            Return "SAL"
        ElseIf Session("Group") = "SA Capital" Then
            Return "SAC"
        ElseIf Session("Group") = "SA Lagoon" Then
            Return "SAL"
        ElseIf Session("Group") = "SA Pension" Then
            Return "SAP"
        End If
    End Function

    Private Function detectDepartment() As String
        If Session("Department") = "Information Technology" Then
            Return "ITG"
        ElseIf Session("Department") = "Finance" Then
            Return "FIN"
        ElseIf Session("Department") = "Asset Management" Then
            Return "ASM"
        ElseIf Session("Department") = "Admin" Then
            Return "ADM"
        ElseIf Session("Department") = "Financial Advisory" Then
            Return "FAG"
        ElseIf Session("Department") = "Human Resources" Or Session("Department") = "Human Capital" Then
            Return "HRG"
        ElseIf Session("Department") = "Internal Control" Then
            Return "ICD"
        ElseIf Session("Department") = "Investment" Then
            Return "INV"
        ElseIf Session("Department") = "Legal/Company Secretary" Or Session("Department") = "Legal" Or Session("Company Secretary") Then
            Return "LGD"
        ElseIf Session("Department") = "Managing Director" Then
            Return "MDS"
        ElseIf Session("Department") = "Marketing" Then
            Return "MKT"
        ElseIf Session("Department") = "Operations" Then
            Return "OPR"
        ElseIf Session("Department") = "Research" Then
            Return "RSH"
        ElseIf Session("Department") = "Stockbroking" Then
            Return "STC"
        ElseIf Session("Department") = "Technical" Then
            Return "TCH"
        End If
    End Function

    Private Function getSignOff() As String
        Dim str As String
        Dim stat As String = "SignOff"
        Dim query As String = "Select [" + stat + "] from Job_Title_Table WHERE TicketCreator='" + Session("names") + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            Str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function
End Class
