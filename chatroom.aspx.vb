Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Partial Class chatroom
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AppendHeader("Refresh", "120; URL=chatroom.aspx")
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    Protected Sub DataList1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DataList1.EditCommand
        Dim keys As Integer = DataList1.DataKeys(e.Item.ItemIndex).ToString()
        Session("Staffid") = keys
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd1 As SqlCommand = myconnection.CreateCommand()
        cmd1.CommandType = CommandType.Text
        cmd1.CommandText = "SELECT fullname FROM Directory WHERE (id = @id)"
        myconnection.Open()
        Dim parm2 As SqlParameter = cmd1.CreateParameter()
        parm2.ParameterName = "@id"
        parm2.Value = keys
        cmd1.Parameters.Add(parm2)
        Dim names As Object = cmd1.ExecuteScalar()

        Session("chatfriend") = names
        Response.Redirect(e.CommandArgument.ToString())
    End Sub

    
End Class
