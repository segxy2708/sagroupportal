Imports System.Web.Security
Partial Class sa_Glass_Theme
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("../session_timeout.aspx")
        Else
            lblName.Text = Session("names")
        End If
        Me.yearTxt.Text = DateTime.Now.Year.ToString

        'auth()

    End Sub

    Sub auth()
        If Not (Request.IsAuthenticated) Then
            FormsAuthentication.RedirectToLoginPage()
        End If
    End Sub

    Protected Sub btnLogout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogout.Click
        Dim i As New PortalsTableAdapters.chatroomTableAdapter
        Dim logs As New PortalsTableAdapters.user_logTableAdapter
        Try
            i.DeleteChatUser(Session("id"))
            logs.UpdateUserlog(Format(DateTime.Now, "hh:mm:ss"), Session("id"), Now.Date)
        Catch ex As Exception

        End Try
        Session.Clear()
        Response.AppendHeader("Refresh", "2; URL=.\")
    End Sub

End Class

