<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Profile_Page.aspx.vb" Inherits="Profile_Page"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="back" runat ="server"></asp:Panel>
    <table cellspacing="0" style="width: 80%; height: 100%">
        <caption>
        </caption>
        
        <tr>
            <td >
            
            </td>
            <td style="width: 128px">
            
            </td>
            <td align="left" valign="top" style="height: 50px; width: 715px;"> 
            
            <asp:Label ID="namelab" runat ="server" ></asp:Label>
            
            
            <asp:Label ID ="passlab" runat ="server" ></asp:Label>&nbsp;
                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False">Find Staff</asp:LinkButton>&nbsp;<asp:HyperLink 
                    ID="HyperLink1" runat="server" NavigateUrl="~/m_wall.aspx">my wall</asp:HyperLink>
            </td> 
            <td>
            <asp:panel ID="ImagePanel" runat ="server" >
            
            </asp:panel>
            </td>
        </tr>
        <tr>
        <td style="height: 16px">
        
        </td>
        <td style="height: 16px; width: 128px;">
        
        </td>
        <td style="height: 16px; width: 715px;">
        <asp:Label ID="messages" runat ="server" Visible ="false"  >*</asp:Label>
        </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="sideBar1" runat="server">
                </asp:Panel>
            </td>
            <td style="height: 50px; background-color: white">
                <asp:Panel ID="sideBar2" runat="server">
                </asp:Panel>
            </td>
            <td style="height: 50px; background-color: white; width: 715px;">
                <asp:Panel ID="Panel1" runat="server" Width="700px" BackColor="Transparent" Height="700px">
                <table style="width: 100%; height: 25px;">
                <tr style="background-color: #66cc00; width: 500px;">
                <td style="background-color: #336633; width: 15%; height: 21px; color: #ffffff;">
                    &nbsp;<asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" Width="100%" ForeColor="White">My Profile</asp:LinkButton></td>
                <td style="background-color: #336633; width: 15%; height: 21px; color: #ffffff;">&nbsp;
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" Width="100%" ForeColor="White">Change Picture</asp:LinkButton>
                </td>
                <td style="background-color: #336633; width: 15%; height: 21px; color: #ffffff;">
                    &nbsp;<asp:LinkButton runat ="server" ID ="CName" Width="100%" CausesValidation="False" ForeColor="White" >Change Password</asp:LinkButton></td>
                <td style="background-color: #336633; width: 15%; height: 21px; color: #ffffff;">
                    &nbsp;<asp:LinkButton ID="CCInfoBut" runat="server" CausesValidation="False" Width="100%" ForeColor="White">Change Contact Info</asp:LinkButton></td>
                </tr>                
                </table>
                <table style="width: 100%;">
                <tr style="width: 500px; background-color: #ffffff;">
                <td style="width: 197px; background-color: white;">
                    <asp:Panel ID="InfoPanel" runat="server" Width="675px" BackColor="White" ScrollBars="Auto" Height="100%">
                    <table style="background-color: #ffffff" >
                    <tr>
                    <td  align="center" style="height: 18px">
                        &nbsp;</td>
                    <td  align="center" style="height: 18px; width: 313px;">
                    <asp:Label id="CUnameLab" runat="server" Text="Change Password" Font-Bold="True"></asp:Label></td>
                    <td style="width: 160px; height: 18px;" align="center">
                    </td>
                    <td style="height: 18px; width: 157px;" align="center">
                        &nbsp;&nbsp;</td>
                     <td style="height: 18px; width: 7px;" align="center">
                     
                         &nbsp;</td>
                    </tr>
                    <tr>
                    <td></td>
                    <td style="width: 313px" colspan="2">
                    <ul>
                    <li>By changing your password you will be logged off and have to re-log in</li><li>Passwords are case-sensitive. Remember to check your CAPS lock key</li></ul></td>
                    <td></td>
                    <td style="width: 7px"></td>
                    <td></td>
                    </tr>
                    <tr>
                   <td style="width: 130px;">
                    <asp:Label ID="uFirstLab" runat ="server" Text ="Old Password" Font-Bold="True"></asp:Label></td>
                    <td style="width: 313px;" align="left" valign="top">
                    <asp:TextBox ID="uFirst" runat ="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Empty" ControlToValidate="uFirst">*</asp:RequiredFieldValidator><br />   
                     </td>
                    <td align="left" valign="top">
                    </td> 
                    <td  align="center" style="width: 157px;" valign="top">
                        &nbsp; &nbsp;
                        </td> 
                    </tr>
                    <tr>
                    
                    <td >
                        <asp:Label ID="uSecondLab" runat ="server" Text ="New Password" Font-Bold="True"></asp:Label></td>
                    <td style="width: 313px">
                     <asp:TextBox ID="uSecond" runat ="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="uSecond"
                            ErrorMessage="Empty">*</asp:RequiredFieldValidator>
                        &nbsp;&nbsp;
                    </td>
                    <td>
                    
                    </td>
                    <td style="width: 157px">
                    
                    </td>
                    </tr>
                    
                      <tr>
                    <td>
                    
                    <asp:Label ID="uCorfirmLab" runat ="server" Text ="Confirm Password" Font-Bold="True"></asp:Label></td>
                    <td style="width: 313px">
                     
                        <asp:TextBox ID="confirmPass" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Empty" ControlToValidate="confirmPass" Display="Dynamic">*</asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Password mismatch" Display="Dynamic" ControlToCompare ="uSecond" ControlToValidate ="confirmPass"></asp:CompareValidator></td>
                    <td>
                      <asp:Button ID="CULinkBut" runat="server" Text="Change" /></td>
                    <td style="width: 157px">
                    
                    </td>
                    </tr>
                    
                      <tr>
                    <td>
                        <asp:Label ID="Label10" runat="server" Text="Change Mobile No" Visible="False"></asp:Label></td>
                    <td style="width: 313px">
                        <asp:TextBox ID="mobileTxtV" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                ID="mobileTxtH" runat="server" Visible="False"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator2"
                            runat="server" ErrorMessage="Old Mobile Number Incorrect" ControlToCompare="mobileTxtH" ControlToValidate="mobileTxtV" Display="Dynamic">*</asp:CompareValidator></td>
                    <td>
                    
                    </td>
                    <td style="width: 157px">
                    
                    </td>
                    </tr>
                    <tr>
                    <td style="height: 31px">
                    
                    </td>
                    <td align="right" style="height: 31px; width: 313px;">
                    <asp:Label ID="errMsg" Text ="" runat ="server" Visible ="false "></asp:Label>
                    </td>
                    <td style="height: 31px">
                    
                    </td>
                    </tr>
                    <tr>
                    <td style="height: 16px">
                        
                    </td>
                    <td align="right" style="width: 313px; height: 16px;">
                      </td>
                    </tr>
                    </table>
                       
                    
                    </asp:Panel>
                     <asp:Panel ID="ImChPanel" runat="server" BackColor ="Transparent" ScrollBars ="Auto" Width="675px" Height="100%" Wrap="False" >
                     <table >
                     <tr>
                     <td>
                     
                     </td>
                     <td>
                         &nbsp;<asp:Panel ID="Panel4" runat="server" Height="50px" Width="125px" EnableTheming="False" Visible="False">
                         <asp:DataList ID="DataList1" runat="server" DataSourceID="ChProImg" GridLines="Both" RepeatDirection="Horizontal">
                         <ItemTemplate >
                         <asp:Image runat ="server" ID="ExProfImg" ImageUrl ='<%Eval("ImagePath") %>' Width="50px" Height="50px" />
                         </ItemTemplate>
                         </asp:DataList></asp:Panel>
                     </td>
                     <td>
                         &nbsp;</td>
                     </tr>
                     <tr>
                     <td>
                     <asp:Label ID="chImLab" Text ="Change Profile Image" runat ="server" ></asp:Label>
                     </td>
                     <td>
                     <asp:FileUpload ID="ImageUpload" runat ="server" />
                         <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ControlToValidate="ImageUpload"
                             ErrorMessage="Upload Jpegs,bitmaps,PNG and Gifs only." ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.png|.PNG|.bmp|.BMP|.jpeg|.JPEG)$"></asp:RegularExpressionValidator>
                     </td>
                     
                     </tr>
                     
                     <tr>
                     <td>
                     
                     </td>
                     <td>
                     <asp:Button ID="ChImBut" runat ="server" Text ="Change" />
                         <asp:SqlDataSource ID="ChProImg" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                             SelectCommand="SELECT Owner, ImagePath FROM ProfileImageLoc WHERE (Owner = @Owner)" InsertCommand="INSERT INTO ProfileImageLoc(Owner, ImagePath) VALUES (@Owner, @ImagePath)">
                             <InsertParameters>
                                 <asp:SessionParameter Name="Owner" SessionField="names" />
                             </InsertParameters>
                             <SelectParameters>
                                 <asp:SessionParameter Name="Owner" SessionField="names" />
                             </SelectParameters>
                         </asp:SqlDataSource>
                     </td>
                     </tr>
                     </table>
                     
                        </asp:Panel>
                    <asp:Panel ID="myProfPan" runat="server" Height="100%" Visible="true" Width="696px" BackColor="Gray" ScrollBars="Auto" Wrap="False">
                        <table style="background-color: white">
                            <tr>
                                <td style="height: 66px">
                                </td>
                                <td colspan="5" rowspan="1" style="width: 3px; height: 66px;">
                                    <asp:Label ID="mesg" runat="server" EnableTheming="False" Font-Size="XX-Large" Text="Label"
                                        Width="300px"></asp:Label></td>
                                <td style="height: 66px">
                                </td>
                                <td style="height: 66px">
                                    <asp:Panel ID="imgPan" runat="server">
                                    </asp:Panel>
                                </td>
                                <td style="width: 3px; height: 66px;">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 206px">
                                </td>
                                <td style="height: 206px">
                                    </td>
                                <td style="height: 206px">
                                    <asp:Panel ID="Panel2" runat="server" Height="200px" Width="300px">
                                    <asp:Label ID="Label2" runat="server" Text="First Name:" Width="100px"></asp:Label>&nbsp;
                                    <asp:Label ID="firstNameLab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                    <asp:Label ID="Label1" runat="server" Text="Last Name:" Width="100px"></asp:Label>&nbsp;
                                    <asp:Label ID="lastnameLab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                    <asp:Label ID="Label7" runat="server" Text="Sex:" Width="100px"></asp:Label>
                                        &nbsp;<asp:Label ID="sexLab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                    <asp:Label ID="Label4" runat="server" Text="State Of Origin:" Width="100px"></asp:Label>&nbsp;
                                    <asp:Label ID="StateLab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                <asp:Label ID="Label6" runat="server" Text="Phone No:" Width="100px"></asp:Label>&nbsp;
                                <asp:Label ID="phoneNo" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                    </asp:Panel>
                                </td>
                                <td style="height: 206px">
                                    <asp:Panel ID="Panel3" runat="server" Height="200px" Width="320px">
                                    <asp:Label ID="Label3" runat="server" Text="Division:" Width="100px"></asp:Label>&nbsp;
                                    <asp:Label ID="divLab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                    <asp:Label ID="Label5" runat="server" Text="Department:" Width="100px"></asp:Label>
                                        &nbsp;
                                    <asp:Label ID="deptLab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                        <asp:Label ID="Label9" runat="server" Text="DOB:" Width="100px"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="DOBlab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                    <asp:Label ID="Label8" runat="server" Text="E-Mail:" Width="100px"></asp:Label>
                                        &nbsp;
                                    <asp:Label ID="emailLab" runat="server" Text="Label"></asp:Label><br />
                                        <br />
                                        <asp:Label ID="Label13" runat="server" Text="Address" Width="100px"></asp:Label>
                                        &nbsp;
                                        <asp:Label ID="addressLabs" runat="server" Text=""></asp:Label></asp:Panel>
                                </td>
                                <td style="width: 107px; height: 206px;">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="height: 16px">
                                </td>
                                <td style="height: 16px;">
                                    &nbsp;</td>
                                <td style="width: 12%; height: 16px;">
                                    </td>
                                <td style="height: 16px">
                                    &nbsp;&nbsp;&nbsp;
                                    </td>
                                <td style="width: 107px; height: 30px;">
                                    </td>
                                <td style="height: 16px">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="height: 16px">
                                    &nbsp;</td>
                                <td style="height: 16px">
                                    &nbsp;
                                    </td>
                                <td style="height: 16px">
                                    </td>
                                <td style="height: 16px">
                                    &nbsp; &nbsp;&nbsp;</td>
                                <td style="width: 107px; height: 30px;">
                                    </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    </td>
                                <td>
                                    </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;
                                </td>
                                <td style="width: 107px; height: 30px;">
                                    </td>
                            </tr>
                            <tr>
                            <td>
                            
                            </td>
                            <td>
                                </td>
                            <td>
                                </td>
                            <td>
                                &nbsp;&nbsp;
                                </td>
                            <td style="height: 30px">
                                </td>
                            </tr>
                            
                            <tr>
                            <td>
                            
                            </td>
                            <td>
                                </td>
                            <td>
                                </td>
                            <td>
                                &nbsp;&nbsp;
                                </td>
                            <td style="height: 30px">
                                </td>
                            </tr>
                            
                            <tr>
                            <td style="height: 32px">
                            
                            </td>
                            <td style="height: 32px">
                                </td>
                            <td style="height: 32px">
                                </td>
                            <td style="height: 32px">
                                &nbsp;&nbsp;
                                </td>
                            <td style="height: 32px">
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="ContactInfoPan" runat="server" Height="100%" Width="696px" ScrollBars="Auto" Visible="False">
                    <table >
                    <tr>
                    <td style="width: 50px">
                    
                    </td>
                    <td align="center" colspan="2" rowspan="1">
                        <asp:Label ID="Label11" runat="server" Text="Change Address" Font-Bold="True"></asp:Label><br />
                        <asp:Label ID="statMsg" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                    
                    </td>
                    </tr>
                    <tr>
                    <td style="width: 50px; height: 42px;">
                    
                    </td>
                    <td style="height: 42px">
                        <asp:Label ID="Label12" runat="server" Text="New Address:" Font-Bold="True"></asp:Label>
                    </td>
                    <td style="height: 42px">
                        <asp:TextBox ID="chAddrTxt" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td style="height: 42px">
                        <asp:Button ID="chAddr" runat="server" Text="Change Address" CausesValidation="False" /></td>
                    </tr>
                    <tr>
                    <td style="width: 50px; height: 16px;">
                    
                    </td>
                    <td style="height: 16px">
                    
                    </td>
                    <td style="height: 16px">
                    
                    </td>
                    <td style="height: 16px">
                    
                    </td>
                    </tr>
                    <tr>
                    <td style="width: 50px">
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                        <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="Change Mobile No:"></asp:Label></td>
                    <td>
                    
                    </td>
                    </tr>
                    <tr>
                    <td style="width: 50px">
                    
                    </td>
                    <td>
                        <asp:Label ID="Label15" runat="server" Text="New Mobile No:" Font-Bold="True"></asp:Label></td>
                    <td>
                        <asp:TextBox ID="MobileNoTxt" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="field cannot be empty" ControlToValidate="MobileNoTxt">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="number invalid" ValidationExpression="(([+]\[2]\[3]\[4])|0)\d{10}" ControlToValidate="MobileNoTxt">*</asp:RegularExpressionValidator></td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="Change" /></td>
                    </tr>
                    <tr>
                    <td style="width: 50px">
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    </tr>
                    <tr>
                    <td style="width: 50px">
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                    
                    </td>
                    </tr>
                    <tr>
                    <td style="width: 50px">
                    
                    </td>
                    <td>
                    
                    </td>
                    <td>
                        <asp:SqlDataSource ID="chAddrSrc" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                            SelectCommand="SELECT * FROM [directory] WHERE ([fullname] = @fullname)" UpdateCommand="UPDATE directory SET Address1 = @Address1 WHERE (fullname = @fullname)">
                            <SelectParameters>
                                <asp:SessionParameter Name="fullname" SessionField="names" Type="String" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="chAddrTxt" Name="Address1" PropertyName="Text" />
                                <asp:SessionParameter Name="fullname" SessionField="names" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    
                    </td>
                    <td>
                    
                        <asp:SqlDataSource ID="mob_src" runat="server" 
                            ConflictDetection="CompareAllValues" 
                            ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>" 
                            DeleteCommand="DELETE FROM [directory] WHERE [ID] = @original_ID AND (([phone] = @original_phone) OR ([phone] IS NULL AND @original_phone IS NULL)) AND (([fax] = @original_fax) OR ([fax] IS NULL AND @original_fax IS NULL)) AND (([city] = @original_city) OR ([city] IS NULL AND @original_city IS NULL)) AND (([state] = @original_state) OR ([state] IS NULL AND @original_state IS NULL)) AND (([Address1] = @original_Address1) OR ([Address1] IS NULL AND @original_Address1 IS NULL)) AND (([Address2] = @original_Address2) OR ([Address2] IS NULL AND @original_Address2 IS NULL)) AND (([zipcode] = @original_zipcode) OR ([zipcode] IS NULL AND @original_zipcode IS NULL)) AND (([email] = @original_email) OR ([email] IS NULL AND @original_email IS NULL)) AND (([Department] = @original_Department) OR ([Department] IS NULL AND @original_Department IS NULL)) AND (([location] = @original_location) OR ([location] IS NULL AND @original_location IS NULL)) AND (([building] = @original_building) OR ([building] IS NULL AND @original_building IS NULL)) AND (([last] = @original_last) OR ([last] IS NULL AND @original_last IS NULL)) AND (([first] = @original_first) OR ([first] IS NULL AND @original_first IS NULL)) AND (([MI] = @original_MI) OR ([MI] IS NULL AND @original_MI IS NULL)) AND (([picloc] = @original_picloc) OR ([picloc] IS NULL AND @original_picloc IS NULL)) AND (([office] = @original_office) OR ([office] IS NULL AND @original_office IS NULL)) AND (([Timestamp] = @original_Timestamp) OR ([Timestamp] IS NULL AND @original_Timestamp IS NULL)) AND (([title] = @original_title) OR ([title] IS NULL AND @original_title IS NULL)) AND (([background] = @original_background) OR ([background] IS NULL AND @original_background IS NULL)) AND (([empnum] = @original_empnum) OR ([empnum] IS NULL AND @original_empnum IS NULL)) AND (([listname] = @original_listname) OR ([listname] IS NULL AND @original_listname IS NULL)) AND (([uname] = @original_uname) OR ([uname] IS NULL AND @original_uname IS NULL)) AND (([pw] = @original_pw) OR ([pw] IS NULL AND @original_pw IS NULL)) AND (([admin] = @original_admin) OR ([admin] IS NULL AND @original_admin IS NULL)) AND (([surveyallow] = @original_surveyallow) OR ([surveyallow] IS NULL AND @original_surveyallow IS NULL)) AND (([emailpass] = @original_emailpass) OR ([emailpass] IS NULL AND @original_emailpass IS NULL)) AND (([emailuser] = @original_emailuser) OR ([emailuser] IS NULL AND @original_emailuser IS NULL)) AND (([remotemail] = @original_remotemail) OR ([remotemail] IS NULL AND @original_remotemail IS NULL)) AND (([D11] = @original_D11) OR ([D11] IS NULL AND @original_D11 IS NULL)) AND (([address3] = @original_address3) OR ([address3] IS NULL AND @original_address3 IS NULL)) AND (([sharecal] = @original_sharecal) OR ([sharecal] IS NULL AND @original_sharecal IS NULL)) AND (([cellphone] = @original_cellphone) OR ([cellphone] IS NULL AND @original_cellphone IS NULL)) AND (([pager] = @original_pager) OR ([pager] IS NULL AND @original_pager IS NULL)) AND (([country] = @original_country) OR ([country] IS NULL AND @original_country IS NULL)) AND (([supervisor] = @original_supervisor) OR ([supervisor] IS NULL AND @original_supervisor IS NULL)) AND (([calaccess] = @original_calaccess) OR ([calaccess] IS NULL AND @original_calaccess IS NULL)) AND (([projectview] = @original_projectview) OR ([projectview] IS NULL AND @original_projectview IS NULL)) AND (([lastlogin] = @original_lastlogin) OR ([lastlogin] IS NULL AND @original_lastlogin IS NULL)) AND (([visits] = @original_visits) OR ([visits] IS NULL AND @original_visits IS NULL)) AND (([hppopup] = @original_hppopup) OR ([hppopup] IS NULL AND @original_hppopup IS NULL)) AND (([status] = @original_status) OR ([status] IS NULL AND @original_status IS NULL)) AND (([flash] = @original_flash) OR ([flash] IS NULL AND @original_flash IS NULL)) AND (([filelimit] = @original_filelimit) OR ([filelimit] IS NULL AND @original_filelimit IS NULL)) AND (([timezone] = @original_timezone) OR ([timezone] IS NULL AND @original_timezone IS NULL)) AND (([getmail] = @original_getmail) OR ([getmail] IS NULL AND @original_getmail IS NULL)) AND (([emailpass2] = @original_emailpass2) OR ([emailpass2] IS NULL AND @original_emailpass2 IS NULL)) AND (([emailuser2] = @original_emailuser2) OR ([emailuser2] IS NULL AND @original_emailuser2 IS NULL)) AND (([remotemail2] = @original_remotemail2) OR ([remotemail2] IS NULL AND @original_remotemail2 IS NULL)) AND (([emailpass3] = @original_emailpass3) OR ([emailpass3] IS NULL AND @original_emailpass3 IS NULL)) AND (([emailuser3] = @original_emailuser3) OR ([emailuser3] IS NULL AND @original_emailuser3 IS NULL)) AND (([remotemail3] = @original_remotemail3) OR ([remotemail3] IS NULL AND @original_remotemail3 IS NULL)) AND (([fullname] = @original_fullname) OR ([fullname] IS NULL AND @original_fullname IS NULL)) AND (([background2] = @original_background2) OR ([background2] IS NULL AND @original_background2 IS NULL)) AND (([background3] = @original_background3) OR ([background3] IS NULL AND @original_background3 IS NULL)) AND (([spam] = @original_spam) OR ([spam] IS NULL AND @original_spam IS NULL)) AND (([blogcomments] = @original_blogcomments) OR ([blogcomments] IS NULL AND @original_blogcomments IS NULL)) AND (([profilename] = @original_profilename) OR ([profilename] IS NULL AND @original_profilename IS NULL)) AND (([remotemailtype] = @original_remotemailtype) OR ([remotemailtype] IS NULL AND @original_remotemailtype IS NULL)) AND (([remotemail2type] = @original_remotemail2type) OR ([remotemail2type] IS NULL AND @original_remotemail2type IS NULL)) AND (([remotemail3type] = @original_remotemail3type) OR ([remotemail3type] IS NULL AND @original_remotemail3type IS NULL)) AND (([blogdate] = @original_blogdate) OR ([blogdate] IS NULL AND @original_blogdate IS NULL)) AND (([blogtitle] = @original_blogtitle) OR ([blogtitle] IS NULL AND @original_blogtitle IS NULL)) AND (([staff_level] = @original_staff_level) OR ([staff_level] IS NULL AND @original_staff_level IS NULL)) AND (([Sex] = @original_Sex) OR ([Sex] IS NULL AND @original_Sex IS NULL)) AND (([Branch] = @original_Branch) OR ([Branch] IS NULL AND @original_Branch IS NULL)) AND (([Creator] = @original_Creator) OR ([Creator] IS NULL AND @original_Creator IS NULL))" 
                            InsertCommand="INSERT INTO [directory] ([ID], [phone], [fax], [city], [state], [Address1], [Address2], [zipcode], [email], [Department], [location], [building], [last], [first], [MI], [picloc], [office], [Timestamp], [title], [background], [empnum], [listname], [uname], [pw], [admin], [surveyallow], [emailpass], [emailuser], [remotemail], [D11], [address3], [sharecal], [cellphone], [pager], [country], [supervisor], [calaccess], [projectview], [lastlogin], [visits], [hppopup], [status], [flash], [filelimit], [timezone], [getmail], [emailpass2], [emailuser2], [remotemail2], [emailpass3], [emailuser3], [remotemail3], [fullname], [background2], [background3], [spam], [blogcomments], [profilename], [remotemailtype], [remotemail2type], [remotemail3type], [blogdate], [blogtitle], [staff_level], [Sex], [Branch], [Creator]) VALUES (@ID, @phone, @fax, @city, @state, @Address1, @Address2, @zipcode, @email, @Department, @location, @building, @last, @first, @MI, @picloc, @office, @Timestamp, @title, @background, @empnum, @listname, @uname, @pw, @admin, @surveyallow, @emailpass, @emailuser, @remotemail, @D11, @address3, @sharecal, @cellphone, @pager, @country, @supervisor, @calaccess, @projectview, @lastlogin, @visits, @hppopup, @status, @flash, @filelimit, @timezone, @getmail, @emailpass2, @emailuser2, @remotemail2, @emailpass3, @emailuser3, @remotemail3, @fullname, @background2, @background3, @spam, @blogcomments, @profilename, @remotemailtype, @remotemail2type, @remotemail3type, @blogdate, @blogtitle, @staff_level, @Sex, @Branch, @Creator)" 
                            OldValuesParameterFormatString="original_{0}" 
                            SelectCommand="SELECT * FROM [directory] where ([fullname]=@fullname)" 
                            UpdateCommand="UPDATE directory SET phone = @phone WHERE (fullname = @fullname)">
                            <DeleteParameters>
                                
                            </DeleteParameters>
                            <InsertParameters>
                                </InsertParameters>
                            <UpdateParameters>
                                <asp:ControlParameter ControlID="MobileNoTxt" Name="phone" 
                                    PropertyName="Text" />
                                <asp:SessionParameter Name="fullname" SessionField="names" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    
                    </td>
                    </tr>
                    </table>
                    </asp:Panel>
                    <br />
                    <asp:SqlDataSource ID="PasswdUpdateSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                        SelectCommand="SELECT * FROM [directory]" UpdateCommand="UPDATE directory SET pw = @pw WHERE (fullname = @fullname)">
                        <UpdateParameters>
                            <asp:ControlParameter ControlID="uSecond" Name="pw" PropertyName="Text" />
                            <asp:SessionParameter Name="fullname" SessionField="names" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="chPicSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                        SelectCommand="SELECT * FROM [directory] WHERE ([fullname] = @fullname)" UpdateCommand="UPDATE directory SET picloc = @picloc WHERE (fullname = @fullname)">
                        <SelectParameters>
                            <asp:SessionParameter Name="fullname" SessionField="names" Type="String" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:SessionParameter Name="fullname" SessionField="names" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </td>
                </tr>
                </table>
                </asp:Panel>
            
            </td>
            <td>
            
            </td>
        </tr>
        <tr>
            <td>
            
            </td>
            <td style="width: 128px">
            
            </td>
            <td style="width: 715px">
            
            </td>
            <td>
            
            </td>
        </tr>
        <tr>
            <td>
            
            </td>
            <td style="width: 128px">
            
            </td>
            <td style="width: 715px">
            
            </td>
            <td>
            
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

