<%@ Control Language="VB" AutoEventWireup="false" CodeFile="details.ascx.vb" Inherits="details" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<strong><span style="font-family: Tahoma">STAFF DETAILS</span><br />
</strong><br />
<asp:DataList ID="DataList1" runat="server" DataKeyField="ID" DataSourceID="SqlDataSource1"
    Font-Names="Tahoma" Font-Size="XX-Small" Width="136px">
    <ItemTemplate>
        <table style="width: 178px">
            <tr>
                <td colspan="3" style="width: 236px; text-align: center">
                    <asp:Image ID="Image1" runat="server" BorderStyle="Solid" BorderWidth="1px" Height="100px"
                        ImageUrl='<%# Eval("picloc", "{0}") %>' Width="90px" /></td>
            </tr>
            <tr>
                <td colspan="3" style="width: 236px; text-align: left">
                    <strong><span style="font-size: 9pt">Name</span></strong></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left; width: 236px; height: 24px;" valign="top">
                    <asp:Label ID="fullnameLabel" runat="server" Font-Size="9pt" Text='<%# Eval("fullname") %>'></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <strong><span style="font-size: 9pt">Subsidiary</span></strong></td>
            </tr>
            <tr>
                <td style="text-align: left; height: 24px;" valign="top">
                    <asp:Label ID="locationLabel" runat="server" Font-Size="9pt" Text='<%# Eval("location") %>'></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <strong><span style="font-size: 9pt">Department</span></strong></td>
            </tr>
            <tr>
                <td style="height: 24px; text-align: left" valign="top">
                    <asp:Label ID="DepartmentLabel" runat="server" Font-Size="9pt" Text='<%# Eval("Department") %>'></asp:Label></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <strong><span style="font-size: 9pt">Mobile N<span style="text-decoration: underline">o</span></span></strong></td>
            </tr>
            <tr>
                <td style="height: 24px; text-align: left" valign="top">
                    <asp:Label ID="phoneLabel" runat="server" Font-Size="9pt" Text='<%# Eval("phone") %>'></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left">
                    <strong><span style="font-size: 9pt">E-mail</span></strong></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: left">
                    <span style="font-size: 9pt">
                    <asp:Label ID="emailLabel" runat="server" Font-Size="9pt" Text='<%# Eval("email") %>'></asp:Label></span></td>
            </tr>
        </table>
    </ItemTemplate>
</asp:DataList>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
    SelectCommand="SELECT TOP 1 * FROM [directory] WHERE ([ID] <> @ID) ORDER BY NEWID()">
    <SelectParameters>
        <asp:SessionParameter Name="ID" SessionField="id" Type="Int32" />
    </SelectParameters>
</asp:SqlDataSource>
