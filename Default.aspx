<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome to Standard Alliance Group Intranet Portal</title>
    <link href="sagportal.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/sa.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 100px;
}
body,td,th {
	color: #003300;
}
-->
</style>
    <script language="javascript">
        function onBeforeUnloadAction() {

            var flag = confirm("do you want to exit?");

            var mesg = "You have not entered all the required fields. " +
                      "If you exit this window without submitting this data, " +
                      "your request will be deleted from the system. " +
                      "Would you like to continue to exit this window?";

            if (flag) {
                window.beforeunload = null;
            } else {

                return mesg;
            }

        }

        window.onbeforeunload = onBeforeUnloadAction;
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="right">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <!--DWLayoutTable-->
            <tr>
                <td colspan="2" valign="top" style="height: 47px">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="login_tops">
                        <!--DWLayoutTable-->
                        <tr>
                            <td width="1026" height="47">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="4" height="353">
                    &nbsp;
                </td>
                <td valign="top" style="width: 100%">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="login_pg">
                        <!--DWLayoutTable-->
                        <tr>
                            <td width="53" height="120">
                                &nbsp;
                            </td>
                            <td width="680" valign="top" style="width: 646px">
                                <img name="intranet_logo" src="images/intranet_logo.gif" width="680" height="120"
                                    border="0" id="intranet_logo" alt="" />
                            </td>
                            <td width="289" rowspan="2" valign="top">
                                <table border="0" cellpadding="0" cellspacing="0" style="width: 118%">
                                    <!--DWLayoutTable-->
                                    <tr>
                                        <td width="288" height="355" valign="middle" style="text-align: left">
                                            &nbsp;<asp:Panel ID="Panel2" runat="server" Height="50px" Width="125px">
                                                <table bgcolor="#FFFFFF" style="width: 389px">
                                                    <!--DWLayoutTable-->
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td style="text-align: left; width: 493px;" align="left">
                                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Font-Bold="False" ForeColor="White"
                                                                BackColor="#FFC080" BorderColor="#C00000" BorderStyle="Solid" BorderWidth="1px" />
                                                            <br />
                                                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left">
                                                            Username:
                                                        </td>
                                                        <td style="text-align: left; width: 493px;">
                                                            <asp:TextBox CssClass="text_styles" ID="txtUsername" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername"
                                                                ErrorMessage="Username required before login">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left">
                                                            Password:
                                                        </td>
                                                        <td style="text-align: left; width: 493px;">
                                                            <asp:TextBox CssClass="text_styles" ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword"
                                                                ErrorMessage="Password required before login">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 493px">
                                                            <asp:Label ID="Label1" runat="server" Text="Hide" Visible="false"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td align="left" style="width: 493px">
                                                            <asp:Button CssClass="Buttons" ID="btnLogin" runat="server" Text="Login" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 20px">
                                                        </td>
                                                        <td align="left" style="width: 493px; height: 20px;">
                                                            <asp:Button ID="btnPassword" runat="server" CssClass="Buttons" Text="Forgot Password?"
                                                                CausesValidation="False" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="Panel1" runat="server" EnableViewState="False" Height="50px" Visible="False"
                                                Width="125px">
                                                <table style="width: 390px; height: 82px">
                                                    <tr>
                                                        <td style="height: 24px">
                                                            Username:
                                                        </td>
                                                        <td style="height: 24px">
                                                            <asp:TextBox ID="txtFUser" runat="server" AutoCompleteType="Disabled" CssClass="text_styles"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtFUser"
                                                                ErrorMessage="*">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="height: 24px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            E-mail:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtFMail" runat="server" AutoCompleteType="Disabled" CssClass="text_styles"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFMail"
                                                                Display="Dynamic" ErrorMessage="cannot be empty">cannot be empty</asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFMail"
                                                                ErrorMessage="invalid e-mail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">invalid e-mail</asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:Button CssClass="Buttons" ID="btnSend" runat="server" Text="Send Password" />
                                                            <asp:Button CssClass="Buttons" ID="txtCancel" runat="server" Text="Cancel" CausesValidation="False" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Label ID="lblMessage2" runat="server"></asp:Label></asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="235">
                                &nbsp;
                            </td>
                            <td valign="top" style="width: 646px">
                                <%-- <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                                              </asp:ToolkitScriptManager>
            <asp:Button ID="Button1" runat="server" Text="Button" Visible="False"
                CausesValidation="False" />
            <asp:ModalPopupExtender ID="Button1_ModalPopupExtender" runat="server"
                DynamicServicePath="" Enabled="True" TargetControlID="Button1"
                PopupControlID ="Panel3" BackgroundCssClass ="watermarker"
                DropShadow="True" RepositionMode="None" CacheDynamicResults="True"
                OkControlID="Button1">
            </asp:ModalPopupExtender>
            <br />

            <asp:Panel ID="Panel3" runat="server" style="text-align: center">
                Welcome to Standard Alliance Group Intranet Portal</asp:Panel>--%>
                                <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                                </asp:ToolkitScriptManager>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>