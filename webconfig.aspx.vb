﻿Imports System.Web.Configuration
Imports System.Configuration

Partial Class webconfig
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ' Set the root path of the Web application that contains the
        ' Web.config file that you want to access.
        Dim configPath As String = "~" '"/MyAppRoot"
        Dim strkey As String
        Dim strvalue As String 
        ' Get the configuration object to access the related Web.config file.
        Dim config As Configuration = WebConfigurationManager.OpenWebConfiguration(configPath)

        ' Get the object related to the <identity> section.
        Dim section As New IdentitySection
        section = config.GetSection("system.web/identity")
        Dim connstrsec As New ConnectionStringsSection

        Dim objAppsettings As AppSettingsSection = CType(config.GetSection("appSettings"), AppSettingsSection)


        objAppsettings.Settings(strkey).Value = strvalue
        config.Save()


        ' Read the <identity> section.
        Dim identity As New StringBuilder
        identity.Append("Impersonate: ")
        identity.Append(section.Impersonate.ToString())
        'connstrsec 
        identity.Append(" connection string is:")
        identity.Append(ConfigurationManager.ConnectionStrings(0).ConnectionString)
        ' Display the <identity> information.
        ConfigId.Text = identity.ToString()
    End Sub
End Class
