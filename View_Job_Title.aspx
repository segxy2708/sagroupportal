<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="View_Job_Title.aspx.vb" Inherits="View_Job_Title" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    &nbsp;
    <asp:UpdatePanel ID ="ups" runat ="server" >
    <Triggers >
    <asp:AsyncPostBackTrigger ControlID ="Timer1" />
    </Triggers>
    <ContentTemplate >
    <asp:Timer ID="Timer1" runat ="server" Interval="3000" ></asp:Timer>
    <asp:Panel ID="Panel1" runat="server" Height="350px" ScrollBars="Auto" Width="100%">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="TicketNo"
        DataSourceID="SelectJobsSource" GridLines="None" CellPadding="4" ForeColor="#333333" ToolTip="Unattended Request" AllowPaging="True" AllowSorting="True">
        <Columns>
            <asp:HyperLinkField HeaderText ="Ticket No" DataTextField ="TicketNo" DataNavigateUrlFields ="TicketNo" DataNavigateUrlFormatString ="details.aspx?ticketNo={0}" SortExpression ="TicketNo" />
            
            <asp:BoundField DataField="TicketCreator" HeaderText="Ticket Creator" SortExpression="TicketCreator" />
            <asp:BoundField DataField="Branch" HeaderText="Branch" SortExpression="Branch" />
            
            <asp:BoundField DataField="DateTime" HeaderText="Date &amp; Time" SortExpression="DateTime" />
            <asp:BoundField DataField="Symptoms" HeaderText="Problems" SortExpression="Symptoms" >
                <ControlStyle Height="10px" Width="40px" />
                <ItemStyle Height="10px" Width="40px" />
            </asp:BoundField>
            <asp:BoundField DataField="AffectedAsset" HeaderText="Affected Hardware" SortExpression="AffectedAsset" />
            <asp:BoundField DataField="PersonalDetails" HeaderText="Attempts" SortExpression="PersonalDetails" >
                <ItemStyle Height="20px" Width="30px" Wrap="True" CssClass ="Grid" />
            </asp:BoundField>
            
            <asp:BoundField DataField="Resolution" HeaderText="Resolution" SortExpression="Resolution" />
            <asp:BoundField DataField="SignOff" HeaderText="Requesting Officer" SortExpression="SignOff" />
             <asp:BoundField DataField="StepsTaken" HeaderText="1st Level Support Steps Taken" SortExpression="StepsTaken" />
                <asp:BoundField DataField="Resolution" HeaderText="Resolution" SortExpression="Resolution" />
        </Columns>
        <RowStyle BackColor="#E3EAEB" HorizontalAlign="Justify" />
        <EmptyDataRowStyle BackColor="LightGoldenrodYellow" />
        <PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#7C6F57" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
        <br />
        <br />
        <br />
       
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    
    <br />
    <br />
        <asp:LinkButton ID="LinkButton1" runat="server"></asp:LinkButton><asp:LinkButton
            ID="LinkButton2" runat="server">LinkButton</asp:LinkButton><br />
     <asp:Panel ID="Panel2" runat="server" Height="350px" Visible="False" Width="100%" ScrollBars="Auto">
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="TicketNo"
        DataSourceID="SelectJobsSource1" GridLines="None" CellPadding="4" ForeColor="#333333" ToolTip="Attended Request" AllowPaging="True" AllowSorting="True">
                <Columns>
                    <asp:HyperLinkField HeaderText ="Ticket No" DataTextField ="TicketNo" DataNavigateUrlFields ="TicketNo" DataNavigateUrlFormatString ="detailss.aspx?ticketNo={0}" SortExpression ="TicketNo" />
                    <asp:BoundField DataField="TicketCreator" HeaderText="Ticket Creator" SortExpression="TicketCreator" />
                    <asp:BoundField DataField="DateTime" HeaderText="DateTime" SortExpression="DateTime" />
                    <asp:BoundField DataField="PersonalDetails" HeaderText="Attempts" SortExpression="PersonalDetails" />
                    <asp:BoundField DataField="AffectedAsset" HeaderText="Affected Hardware" SortExpression="AffectedAsset" />
                    <asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="Priority" />
                    <asp:BoundField DataField="Symptoms" HeaderText="Problems" SortExpression="Symptoms" />
                    
                    <asp:BoundField DataField="SignOff" HeaderText="SignOff" SortExpression="SignOff" />
                    
                    <asp:BoundField DataField="DateTreated" HeaderText="DateTreated" SortExpression="DateTreated" />
                    <asp:BoundField DataField="Attendance" HeaderText="Attendance" SortExpression="Attendance" />
                    <asp:BoundField DataField="Month" HeaderText="Month" SortExpression="Month" />
                     <asp:BoundField DataField="StepsTaken" HeaderText="1st Level Support Steps Taken" SortExpression="StepsTaken" />
                <asp:BoundField DataField="Resolution" HeaderText="Resolution" SortExpression="Resolution" />
                </Columns>
                <RowStyle BackColor="#E3EAEB" HorizontalAlign="Justify" />
                <EmptyDataRowStyle BackColor="LightGoldenrodYellow" />
                <PagerStyle HorizontalAlign="Center" BackColor="#666666" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
        </asp:Panel>
    <asp:SqlDataSource ID="SelectJobsSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT TicketNo, TicketCreator, DateTime, PersonalDetails, AffectedAsset, Priority, Symptoms, Resolution, SignOff, Treated, DateTreated, Attendance, Month, StepsTaken, Branch, IP FROM Job_Title_Table WHERE (Treated = 'NO') ORDER BY DateTime DESC, Month DESC"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SelectJobsSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT TicketNo, TicketCreator, DateTime, PersonalDetails, AffectedAsset, Priority, Symptoms, Resolution, SignOff, Treated, DateTreated, Attendance, Month, StepsTaken FROM Job_Title_Table WHERE (Treated = 'YES')">
    </asp:SqlDataSource>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

