Imports System.Web.Mail

Partial Class admin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Administrator"
        If Session("role") = "Super Administrator" Or Session("role") = "Administrator" Then
            Me.Panel1.Visible = True
            Me.Label1.Visible = False
            If Session("names") = "Fajinmi  Olanrewaju" Or Session("names") = "Emmanuel  Haruna" Or Session("names") = "Oluwasegun  Oduwole" Then
                Me.Button1.Visible = True
                Me.Button3.Visible = True
                Me.Button4.Visible = True
            Else
                Me.Button1.Visible = False
                Me.Button3.Visible = False
                Me.Button4.Visible = False
                Me.Button6.Visible = False
            End If
        Else
            Me.Panel1.Visible = False
            Me.Label1.Visible = True
            Response.AppendHeader("Refresh", "10; URL=sag_home.aspx")
        End If
        Try
            If Session.IsNewSession Then
                HttpContext.Current.Response.Redirect("session_timeout.aspx")
            Else
                'lblName.Text = Session("names")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Redirect("deleteuser.aspx")
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.Redirect("CreateUsers.aspx")
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Response.Redirect("Edit_users.aspx")
    End Sub

    Protected Sub Button4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button4.Click
        Response.Redirect("LogIn_Reports.aspx")
    End Sub

    'Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
    '    Dim mymail As MailMessage = New MailMessage()
    '    'Dim s As MailPriority = PriorityList.SelectedValue
    '    mymail.From = "ooduwole@sagroupng.com"
    '    mymail.To = "oolowosejeje@sainsuranceng.com"
    '    mymail.Cc = "ooduwole@sagroupng.com"
    '    mymail.Subject = "Your Intranet Password"
    '    ' 
    '    mymail.BodyFormat = MailFormat.Text

    '    mymail.Body = "your username is olasunbo and password is your surname"

    '    mymail.BodyFormat = MailFormat.Text


    '    SmtpMail.Send(mymail)

    'End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button5.Click
        Response.Redirect("staff_window.aspx")
    End Sub

    Protected Sub Button6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button6.Click
        Response.Redirect("web_update_request.aspx")
    End Sub
End Class
