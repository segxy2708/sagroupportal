Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Partial Class View_Staff_Profile
    Inherits System.Web.UI.Page

    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter
    Private im As Image
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If

        If Not IsPostBack Then
            'Me.grp.SelectedIndex = 0
            'Me.deps.SelectedIndex = 0
            'Me.names1.SelectedIndex = 0
        End If
        If Request.QueryString() Is Nothing Or Request.QueryString("search") = "" Then
            Me.InfoPanel.Visible = False
        Else
            Dim profilename As String = Request.QueryString("search")
            Dim welmesg As String = profilename + "'s Profile"
            Me.mesg.Text = welmesg
            profileGet(Request.QueryString("search"))
            Me.InfoPanel.Visible = True
        End If

        ' Me.QSearch.Visible = False
        'Me.NameSrch.Visible = False

       
    End Sub

    Private Function getParams(ByVal s As String, ByVal q As String) As String
        Dim str As String
        Dim query As String = "Select (" + q + ") from directory where fullname='" + s + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception
            Return Nothing
        End Try
    End Function



    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub findBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles findBut.Click
        Dim profilename As String = Me.names1.SelectedValue
        Dim welmesg As String = profilename + "'s Profile"
        If Me.names1.SelectedIndex = 0 Then
            Me.InfoPanel.Visible = False
        Else

            Me.mesg.Text = welmesg
            profileGet(profilename)
        
        End If
    End Sub

    Sub profileGet(ByVal profilename As String)
        im = New Image
        im.ID = "images1"
        If getParams(profilename, "picloc") = Nothing Or getParams(profilename, "picloc") = "" Then
            im.ImageUrl = "/images/AVATAR_19.jpg"
        Else
            im.ImageUrl = getParams(profilename, "picloc")
        End If
        'im.ImageUrl = getParams(profilename, "picloc")
        im.AlternateText = "image"
        im.Height = 120
        im.Width = 120
        Try
            Me.imgPan.Controls.Add(im)
            Me.firstNameLab.Text = getParams(profilename, "first")
            Me.lastnameLab.Text = getParams(profilename, "last")
            Me.sexLab.Text = getParams(profilename, "Sex")
            Me.divLab.Text = getParams(profilename, "location")
            Me.DOBlab.Text = getParams(profilename, "zipcode")
            Me.deptLab.Text = getParams(profilename, "Department")
            Me.StateLab.Text = getParams(profilename, "State")
            Me.emailLab.Text = getParams(profilename, "email")
            Me.phoneNo.Text = getParams(profilename, "phone")
            Me.InfoPanel.Visible = True
        Catch ex As Exception
            ' Response.AppendHeader("Refresh ", "1; URL=view_staff_profile.aspx")
        End Try
    End Sub
    Protected Sub grp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grp.SelectedIndexChanged
        ' Me.deps.SelectedIndex = 0
        'Me.names1.SelectedIndex = 0
        If Me.grp.SelectedIndex = 1 Then
            Me.GridView1.DataSource = Me.AllSource
            Me.Label15.Visible = False
            Me.SrchNameTxt.Visible = False
            Me.TxtSrchName.Visible = False
            Me.NameSrch.Visible = True
        Else

        End If
    End Sub

    Protected Sub deps_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deps.SelectedIndexChanged
        'Me.names1.SelectedIndex = 0
    End Sub

    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SelTypList.SelectedIndexChanged
        If Me.SelTypList.SelectedIndex = 0 Then
            Me.NameSrch.Visible = True
            Me.QSearch.Visible = False
        ElseIf Me.SelTypList.SelectedIndex = 1 Then
            Me.QSearch.Visible = True
            Me.NameSrch.Visible = False
        End If
    End Sub

    Protected Sub TxtSrchName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtSrchName.Click
        Me.NameSrch.Visible = True
        Me.NameSrch.Height = 300
    End Sub
End Class
