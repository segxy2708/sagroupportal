<%@ Page Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="Staff_Window.aspx.vb" Inherits="Staff_Window" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table >
<tr>
<td style="width: 3px">
</td>
<td>
    <asp:Label ID="Label14" runat="server" Text="Select Search Type:" Font-Bold="True"></asp:Label>
</td>
<td>
    <asp:RadioButtonList ID="SelTypList" runat="server" AutoPostBack="True">
        <asp:ListItem>Quick Search</asp:ListItem>
        <asp:ListItem>Advance Search</asp:ListItem>
    </asp:RadioButtonList>
</td>
<td>
</td>
</tr>
</table>
    <asp:Label ID="Label1" runat="server" Text="Division" Visible="False"></asp:Label> &nbsp;&nbsp;
    <asp:DropDownList ID="DropDownList1" runat="server" 
    AppendDataBoundItems="True" AutoPostBack="True" DataSourceID="listsrc" 
    DataTextField="location" DataValueField="location" Visible="False">
        <asp:ListItem>- Select -</asp:ListItem>
    </asp:DropDownList>
    <asp:TextBox ID="name_txt" runat="server" Visible="False"></asp:TextBox>
    
    &nbsp;<asp:Button ID="srch_but" runat="server" Text="Search" 
    Visible="False" />
&nbsp;<asp:SqlDataSource ID="listsrc" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT DISTINCT [location] FROM [directory]"></asp:SqlDataSource>
    <asp:Panel ID="Panel1" runat="server" Height="1000px" Width="800px" ScrollBars="Auto">
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            DataKeyNames="ID" DataSourceID="staffsrc" AllowPaging="True" 
            Height="100%" CaptionAlign="Left" CellPadding="4" CellSpacing="2" 
            EnableModelValidation="True" ForeColor="#333333" GridLines="Horizontal" 
            HorizontalAlign="Justify" PageSize="20" ShowFooter="True">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
            <asp:BoundField DataField="title" HeaderText="title" SortExpression="title" />
                <asp:BoundField DataField="fullname" HeaderText="Full Name" SortExpression="fullname" />
                <asp:BoundField DataField="location" HeaderText="location" SortExpression="location" />
                <asp:BoundField DataField="Branch" HeaderText="Branch" SortExpression="Branch" />
                <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
                
                <asp:BoundField DataField="building" HeaderText="building" SortExpression="building" />
                <asp:BoundField DataField="Address1" HeaderText="Address" SortExpression="Address1" />
                <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                <asp:BoundField DataField="uname" HeaderText="User Name" SortExpression="uname" />
                <asp:BoundField DataField="pw" HeaderText="Password" SortExpression="pw" />
                
             </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        </asp:GridView>
        <asp:SqlDataSource ID="staffsrc" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
            SelectCommand="SELECT * FROM [directory] where location=@location">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="location" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="txt_src" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
           
            SelectCommand="SELECT ID, phone, fax, city, state, Address1, Address2, zipcode, email, Department, location, building, last, first, MI, picloc, office, Timestamp, title, background, empnum, listname, uname, pw, admin, surveyallow, emailpass, emailuser, remotemail, D11, address3, sharecal, cellphone, pager, country, supervisor, calaccess, projectview, lastlogin, visits, hppopup, status, flash, filelimit, timezone, getmail, emailpass2, emailuser2, remotemail2, emailpass3, emailuser3, remotemail3, fullname, background2, background3, spam, blogcomments, profilename, remotemailtype, remotemail2type, remotemail3type, blogdate, blogtitle, staff_level, Sex, Branch, Creator FROM directory WHERE (fullname LIKE '%' + @fullname + '%')">
            <SelectParameters>
                <asp:ControlParameter ControlID="name_txt" Name="fullname" 
                    PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

