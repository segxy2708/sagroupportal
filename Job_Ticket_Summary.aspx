<%@ Page Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="Job_Ticket_Summary.aspx.vb" Inherits="Job_Ticket_Summary" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="Panel1" runat="server" Height="600px" Width="800px" ScrollBars="Auto">
        <br />
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Select Support Staff:"></asp:Label>
        &nbsp;&nbsp;
        <asp:DropDownList ID="Lists" runat="server" DataSourceID="supprot_staff"
            DataTextField="fullname" DataValueField="fullname" AutoPostBack="True">
        </asp:DropDownList><asp:SqlDataSource ID="supprot_staff" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
            SelectCommand="SELECT fullname FROM directory WHERE (Department = @Department) ORDER BY fullname">
            <SelectParameters>
                <asp:Parameter DefaultValue="Administrator" Name="profilename" Type="String" />
                <asp:Parameter DefaultValue="Super Administrator" Name="profilename2" Type="String" />
                <asp:Parameter DefaultValue="Information Technology" Name="Department" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <br />
        <div id ="tbl" >
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="TicketNo"
            DataSourceID="JobTicketSummary" AllowPaging="True" CellPadding="4" ForeColor="#333333" BorderStyle="Solid" AllowSorting="True">
            <Columns>
                <asp:HyperLinkField HeaderText ="Ticket No" DataTextField ="TicketNo" DataNavigateUrlFields ="TicketNo" DataNavigateUrlFormatString ="detailss.aspx?ticketNo={0}" SortExpression ="TicketNo" />
                <asp:BoundField DataField="TicketCreator" HeaderText="Ticket Creator" SortExpression="TicketCreator" />
                <asp:BoundField DataField="DateTime" HeaderText="Ticket Date" SortExpression="DateTime" />
                <asp:BoundField DataField="PersonalDetails" HeaderText="Steps Taken by User" SortExpression="PersonalDetails" />
                <asp:BoundField DataField="DateTreated" HeaderText="Date Treated" SortExpression="DateTreated" />
                <asp:BoundField DataField="StepsTaken" HeaderText="1st Level Support Steps Taken" SortExpression="StepsTaken" />
                <asp:BoundField DataField="Resolution" HeaderText="Resolution" SortExpression="Resolution" />
                
                
                
            </Columns>
            <RowStyle BackColor="#E3EAEB" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        </div>
        
        <input id="Button1" type="button" value="Print" onclick ="printGridView();" lang ="Javascript" class="Buttons" />
        <asp:SqlDataSource ID="JobTicketSummary" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
            SelectCommand="SELECT * FROM [Job_Title_Table] WHERE ([Attendance] = @Attendance) AND Treated='YES'">
            <SelectParameters>
                <asp:ControlParameter ControlID="Lists" Name="Attendance" PropertyName="SelectedValue"
                    Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

