Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Partial Class Appearance
    Inherits System.Web.UI.Page
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session.IsNewSession Then
                HttpContext.Current.Response.Redirect("session_timeout.aspx")
                'Else
                '    Response.Redirect("UnderConstruction.aspx")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim appear As New PortalsTableAdapters.PersonalizationTableAdapter
        Try
            'check Staff Details in the appearance table
            Dim myconnection As SqlConnection = New SqlConnection(ConnectionString1)
            Dim cmd As SqlCommand = myconnection.CreateCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT PageType FROM Personalization WHERE (staffid = @staffid)"
            myconnection.Open()
            Dim parm As SqlParameter = cmd.CreateParameter()
            parm.ParameterName = "@staffid"
            parm.Value = Session("id")
            cmd.Parameters.Add(parm)
            Dim i As Object = cmd.ExecuteScalar()
            If i Is Nothing Then
                appear.InsertAppearance(Session("id"), DropDownList1.SelectedValue, DropDownList2.SelectedValue)
                lblMessage.Text = "Your profile Appearance has been create successfully. You will be redirected to the Home Page shortly"
            Else
                appear.UpdateAppearance(DropDownList1.SelectedValue, DropDownList2.SelectedValue, Session("id"))
                lblMessage.Text = "Your profile Appearance has been updated successfully. You will be redirected to the Home Page shortly"
            End If
            Session("Theme") = DropDownList1.SelectedValue
            Session("Skin") = DropDownList2.SelectedValue
            Response.AppendHeader("Refresh", "5; URL=sag_home.aspx")
        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.ForeColor = Drawing.Color.Red
        End Try
    End Sub
End Class
