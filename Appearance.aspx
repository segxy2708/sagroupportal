<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Appearance.aspx.vb" Inherits="Appearance" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 729px">
        <tr>
            <td style="width: 192px" valign="top">
                <strong>Background Theme</strong></td>
            <td valign="top">
            </td>
            <td style="width: 425px">
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 192px">
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1"
                    DataTextField="ThemeName" DataValueField="Themes">
                </asp:DropDownList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    SelectCommand="SELECT [ThemeName], [Themes], [SkinName], [Skin] FROM [Appearance] ORDER BY [id]">
                </asp:SqlDataSource>
            </td>
            <td valign="top">
                </td>
            <td style="width: 425px">
            </td>
        </tr>
        <tr>
            <td style="width: 192px" valign="top">
                <strong>Controls Skin</strong></td>
            <td valign="top">
            </td>
            <td style="width: 425px">
            </td>
        </tr>
        <tr>
            <td style="width: 192px" valign="top">
                <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource1"
                    DataTextField="SkinName" DataValueField="Skin">
                </asp:DropDownList></td>
            <td valign="top">
            </td>
            <td style="width: 425px">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="Button1" runat="server" Text="Save Setting" /></td>
        </tr>
        <tr>
            <td style="width: 192px">
            </td>
            <td>
            </td>
            <td style="width: 425px">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

