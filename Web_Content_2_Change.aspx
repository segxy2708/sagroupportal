<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Web_Content_2_Change.aspx.vb" Inherits="Web_Content_2_Change" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table >
<tr>
<td>
</td>
<td align="center" colspan="2">
    <asp:Label ID="messg" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label></td>
<td style="width: 250px">
</td>
<td style="width: 3px">
</td>
</tr>

<tr>
<td>
</td>
<td valign="top">
    <asp:DropDownList ID="ActionList" runat="server" AutoPostBack="True">
        <asp:ListItem>- Select -</asp:ListItem>
        <asp:ListItem>Add New Content</asp:ListItem>
        <asp:ListItem>Changes to Made to Existing Content</asp:ListItem>
        <asp:ListItem>Files Upload for Clients Download</asp:ListItem>
    </asp:DropDownList><br />
    &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" Visible="False">Click to Suggest New Content</asp:LinkButton></td>
<td style="width: 500px">
    <asp:Panel ID="Panel1" runat="server" Height="300px" ScrollBars="Auto" Visible="False"
        Width="500px">
        <table >
        <tr>
        <td style="height: 13px">
            <asp:Label ID="Label1" runat="server" Text="Expected Text:"></asp:Label></td>
        <td style="height: 13px">
            <asp:TextBox ID="ExpectTxt" runat="server" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="height: 40px">
        <span >The Caption for the above text/content should be</span>
        </td>
        <td style="height: 40px"><asp:TextBox ID="capTxt" runat="server"></asp:TextBox><br />
        </td>
        </tr>
        <tr><td>
            Under which menu are you expecting the Above Page Displayed</td><td>
        <asp:TextBox ID="pgMenuTxt" runat="server"></asp:TextBox></td></tr>
        <tr><td>
            Do you want it on a new page
        </td><td>
        <asp:RadioButtonList ID="deciBut" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" EnableViewState="False">
            <asp:ListItem>Yes</asp:ListItem>
            <asp:ListItem>No</asp:ListItem>
        </asp:RadioButtonList></td></tr>
        <tr><td style="height: 26px">
            <asp:Label ID="Label6" runat="server" Text="which page shold it be appended to "
                Visible="False"></asp:Label></td><td style="height: 26px">
        <asp:TextBox ID="pgNameTxt" runat="server" Visible="False"></asp:TextBox></td></tr>
        </table>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server" Height="300px" ScrollBars="Auto" Visible="False" Width="500px">
        <table style="width: 484px" >
        <tr>
        <td style="width: 254px" >
        <span>Does the Need Change occur in more than a Page</span> 
        
        </td>
        <td><asp:RadioButtonList ID="change2List" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" EnableViewState="False">
            <asp:ListItem>Yes</asp:ListItem>
            <asp:ListItem>No</asp:ListItem>
        </asp:RadioButtonList></td>
        
        </tr>
        <tr>
        <td colspan="3" align="center">
        
        <asp:Panel ID="Panel3" runat="server" Height="150px" Visible="False" Width="300px" ScrollBars="Auto">
        <table >
        <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="Please enter the site page(s) name?"></asp:Label></td>
        </tr>
        <tr>
        <td style="height: 26px"><asp:TextBox ID="pgTxt1" runat="server"></asp:TextBox>
        </td>
        </tr>
        <tr>
        <td>
            
            <asp:TextBox ID="pgTxt2" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td>
            <asp:TextBox ID="PgTxt3" runat="server"></asp:TextBox></td>
        </tr>
        
        </table>
            
            <br />
            </asp:Panel>
        </td>
        </tr>
        <tr>
        <td style="width: 254px">
       <span >Current Context:</span>
        </td>
        <td>
        <asp:TextBox ID="CurrTxt" runat="server" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 254px">
            <span>Expected Context:</span></td>
        <td>
        <asp:TextBox ID="ExpectedTxt" runat="server" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 254px"></td>
        <td></td>
        </tr>
        <tr>
        <td style="width: 254px"></td>
        <td></td>
        </tr>
        </table>
        </asp:Panel>
         <asp:Panel ID="Panel4" runat="server" Height="300px" ScrollBars="Auto" Width="500px">
         <table >
         <tr>
         <td style="width: 75px" colspan="2" align="center">
             <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="File(s) Upload for Clients' Download"></asp:Label></td>
         
         </tr>
         <tr>
         <td style="height: 26px"><span >Expected Attachment:</span></td>
         <td style="height: 26px"><asp:FileUpload ID="FileUpload1" runat="server" /></td>
         </tr>
         <tr>
         <td><span >The Caption for the above text/content should be</span></td>
         <td><asp:TextBox ID="CaptionTxt" runat="server"></asp:TextBox></td>
         </tr>
         <tr>
         <td><span >Under which menu are you expecting the Above Page Displayed</span></td>
         <td><asp:TextBox ID="DisplayTxt" runat="server"></asp:TextBox></td>
         </tr>
         <tr>
         <td><span >Do you want it on a new page</span></td>
         <td><asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" EnableViewState="False">
            <asp:ListItem>Yes</asp:ListItem>
            <asp:ListItem>No</asp:ListItem>
        </asp:RadioButtonList></td>
       
         </tr>
         <tr>
         <td><span id ="lab1" runat ="server"  >Which page shold it be appended to:</span></td>
          <td><asp:TextBox ID="appendTxt3" runat="server" Visible="False"></asp:TextBox></td>
         </tr>
         <tr>
         <td>
             <asp:Label ID="Label4" runat="server" Text="Brief Info:"></asp:Label></td>
         <td>
             <asp:TextBox ID="BriefInfoTxt" runat="server" TextMode="MultiLine" Width="235px"></asp:TextBox></td>
         </tr>
         </table>
        </asp:Panel>
    
</td>
<td style="width: 3px">
    &nbsp;</td>
</tr>

<tr>
<td style="height: 16px">
</td>
<td style="height: 16px">
    &nbsp;</td>
<td style="height: 16px">
    &nbsp;</td>
<td style="width: 3px; height: 16px;">
</td>
</tr>

<tr>
<td style="height: 24px">
    <asp:Label ID="Label3" runat="server" Text="Mobile No:" Visible="False"></asp:Label>
</td>
<td style="height: 24px">
    <asp:LinkButton ID="LinkButton2" runat="server" Visible="False">Click to Suggest Change of Existing Content</asp:LinkButton>
    &nbsp; &nbsp;
</td>
<td style="height: 24px">
    &nbsp;
    <asp:Button ID="Button2" runat="server" Text="Submit" /></td>
<td style="width: 3px; height: 24px;">
    <asp:DropDownList ID="NameList" runat="server" Visible="False">
    </asp:DropDownList></td>
</tr>

<tr>
<td style="height: 16px">
</td>
<td style="height: 16px">
</td>
<td style="height: 16px">
    <asp:SqlDataSource ID="NewContentSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        InsertCommand="INSERT INTO SuggestionTab(Subsidiary, Name, MobileNo, email, Action, Caption, PageName, CurrentText, ExpectedText, Date, Time) VALUES (@Subsidiary, @Name, @MobileNo, @email, @Action, @Caption, @PageName, @CurrentText, @ExpectedText, @Date, @Time)"
        SelectCommand="SELECT * FROM [SuggestionTab]">
        <InsertParameters>
            <asp:ControlParameter ControlID="capTxt" Name="Caption" PropertyName="Text" />
            <asp:Parameter Name="CurrentText" />
            <asp:ControlParameter ControlID="ExpectTxt" Name="ExpectedText" PropertyName="Text" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="ExistSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT * FROM [SuggestionTab]" InsertCommand="INSERT INTO SuggestionTab(Subsidiary, Name, MobileNo, email, Action, CurrentText, ExpectedText, Date, Time, DisplayMenu, PageName) VALUES (@Subsidiary, @Name, @MobileNo, @email, @Action, @CurrentText, @ExpectedText, @Date, @Time, @DisplayMenu, @PageName)">
        <InsertParameters>
            <asp:Parameter Name="Action" />
            <asp:ControlParameter ControlID="CurrTxt" Name="CurrentText" PropertyName="Text" />
            <asp:ControlParameter ControlID="ExpectedTxt" Name="ExpectedText" PropertyName="Text" />
            <asp:Parameter Name="DisplayMenu" />
            <asp:SessionParameter Name="PageName" SessionField="pagenames" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="FileUploadSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT * FROM [SuggestionTab]" InsertCommand="INSERT INTO SuggestionTab(Subsidiary, Name, MobileNo, email, Action, Caption, PageName, DisplayMenu, CurrentText, ExpectedText, BriefInfo, UploadURL, Date, Time) VALUES (@Subsidiary, @Name, @MobileNo, @email, @Action, @Caption, @PageName, @DisplayMenu, @CurrentText, @ExpectedText, @BriefInfo, @UploadURL, @Date, @Time)">
        <InsertParameters>
            <asp:ControlParameter ControlID="CaptionTxt" Name="Caption" PropertyName="Text" />
            <asp:Parameter Name="PageName" />
            <asp:ControlParameter ControlID="DisplayTxt" Name="DisplayMenu" PropertyName="Text" />
            <asp:Parameter Name="CurrentText" />
            <asp:ControlParameter ControlID="ExpectedTxt" Name="ExpectedText" PropertyName="Text" />
            <asp:ControlParameter ControlID="BriefInfoTxt" Name="BriefInfo" PropertyName="Text" />
        </InsertParameters>
    </asp:SqlDataSource>
</td>
<td style="width: 3px; height: 16px;">
</td>
</tr>

<tr>
<td style="height: 16px">
</td>
<td style="height: 16px">
    <asp:LinkButton ID="LinkButton3" runat="server" Visible="False">Click to Suggest New Content</asp:LinkButton></td>
<td style="height: 16px">
  
</td>
<td style="width: 3px; height: 16px;">
</td>
</tr>
</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

