<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Escalation.aspx.vb" Inherits="Escalation" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="Panel1" runat="server" Height="50%" Width="99%">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
            DataKeyNames="TicketNo" DataSourceID="escalate" ForeColor="#333333" GridLines="None">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:HyperLinkField HeaderText ="Ticket No" DataTextField ="TicketNo" DataNavigateUrlFields ="TicketNo" DataNavigateUrlFormatString ="EscalateDetails.aspx?ticketNo={0}" />
                <asp:BoundField DataField="TicketCreator" HeaderText="Ticket Creator" SortExpression="TicketCreator" />
                <asp:BoundField DataField="PersonalDetails" HeaderText="Personal Details" SortExpression="PersonalDetails" />
                <asp:BoundField DataField="AffectedAsset" HeaderText="Affected Asset" SortExpression="AffectedAsset" />
                <asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="Priority" />
                <asp:BoundField DataField="Symptoms" HeaderText="Symptoms" SortExpression="Symptoms" />
                <asp:BoundField DataField="StepsTaken" HeaderText="Steps Taken" SortExpression="StepsTaken" />
                <asp:BoundField DataField="Escalator" HeaderText="Escalator" SortExpression="Escalator" />
            </Columns>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="escalate" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
            SelectCommand="SELECT [TicketNo], [TicketCreator], [PersonalDetails], [AffectedAsset], [Priority], [Symptoms], [StepsTaken], [Escalator] FROM [Job_Title_Table] WHERE ([StepsTaken] IS NOT NULL)">
        </asp:SqlDataSource>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

