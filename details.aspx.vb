Imports System
Imports System.Data
Imports System.Data.SqlClient
Partial Class details
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter

    Dim today As String = DateTime.Now.Date.ToString + " : " + DateTime.Now.TimeOfDay.ToString
    Dim mon As String = DateTime.Now.Month.ToString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Request Details"
        Session("tickNo") = getTicketNo()
        Session("getDatetime") = getDatetime()
        Session("PersonalDetails") = getPDetails()
        Session("AffectedAsset") = getAfAsset()
        Session("symptoms") = getSymptoms()
        Session("tickCreator") = getTicketCreator()
        If Request.QueryString Is Nothing Then

        Else
            Dim name As New System.Web.UI.WebControls.Parameter("Attendance", TypeCode.String, Session("names"))
            Dim treate As New System.Web.UI.WebControls.Parameter("Treated", TypeCode.String, "YES")
            Dim submitTime As New System.Web.UI.WebControls.Parameter("DateTreated", TypeCode.String, today)
            Dim month As New System.Web.UI.WebControls.Parameter("Month", TypeCode.String, mon)

            ' Me.complainDetails.UpdateParameters.Add(name)
            'Me.complainDetails.UpdateParameters.Add(treate)
            'Me.complainDetails.UpdateParameters.Add(submitTime)
            'Me.complainDetails.UpdateParameters.Add(month)
            'Me.complainDetails.Update()
        End If
    End Sub

    Protected Sub ResLinBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ResLinBut.Click
        Response.Redirect("resolution.aspx")
    End Sub
    Public Function getTicketNo()
        Dim strs As String = Request.QueryString("ticketNo")
        Dim str As String
        Dim query As String = "SELECT TicketNo FROM Job_Title_Table WHERE TicketNo ='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Public Function getDatetime()
        Dim strs As String = Request.QueryString("ticketNo")
        Dim str As String
        Dim query As String = "SELECT DateTime FROM Job_Title_Table WHERE TicketNo ='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function
    Public Function getPDetails()
        Dim strs As String = Request.QueryString("ticketNo")
        Dim str As String
        Dim query As String = "SELECT PersonalDetails FROM Job_Title_Table WHERE TicketNo ='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Public Function getAfAsset()
        Dim strs As String = Request.QueryString("ticketNo")
        Dim str As String
        Dim query As String = "SELECT AffectedAsset FROM Job_Title_Table WHERE TicketNo ='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Public Function getSymptoms()
        Dim strs As String = Request.QueryString("ticketNo")
        Dim str As String
        Dim query As String = "SELECT Symptoms FROM Job_Title_Table WHERE TicketNo ='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Public Function getTicketCreator()
        Dim strs As String = Request.QueryString("ticketNo")
        Dim str As String
        Dim query As String = "SELECT TicketCreator FROM Job_Title_Table WHERE TicketNo ='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Back.Click
        Response.Redirect("View_Job_Title.aspx")
    End Sub
End Class
