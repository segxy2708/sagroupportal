<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="View_Staff_Profile.aspx.vb" Inherits="View_Staff_Profile" title="Staff Profile Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table >
<tr>
<td style="width: 3px">
</td>
<td>
    <asp:Label ID="Label14" runat="server" Text="Select Search Type:" Font-Bold="True"></asp:Label>
</td>
<td>
    <asp:RadioButtonList ID="SelTypList" runat="server" AutoPostBack="True">
        <asp:ListItem>Quick Search</asp:ListItem>
        <asp:ListItem>Advance Search</asp:ListItem>
    </asp:RadioButtonList>
</td>
<td>
</td>
</tr>
</table>
    <br />
    <asp:Panel ID="NameSrch" runat="server" Height="130px" Width="800px" ScrollBars="Auto" Visible="False">
    <table >
    <tr>
    <td style="width: 50px">
    </td>
    <td>
        <asp:Label ID="Label15" runat="server" Text="Enter Name:" Font-Bold="True"></asp:Label>
    </td>
    <td>
        <asp:TextBox ID="SrchNameTxt" runat="server"></asp:TextBox>
        &nbsp;&nbsp;
        <asp:Button ID="TxtSrchName" runat="server" Text="Search" /></td>
    <td>
        &nbsp;</td>
    </tr>
    <tr>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td>
    </td>
    </tr>
    
      <tr>
    <td>
    </td>
    <td colspan="2">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
            DataKeyNames="ID" DataSourceID="srchTxtSrc" ForeColor="#333333" GridLines="None" AllowPaging="True">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
            <asp:BoundField DataField="title" HeaderText="Title" SortExpression="title" />
            <asp:HyperLinkField HeaderText ="Fullname" DataTextField ="fullname" DataNavigateUrlFields ="fullname" DataNavigateUrlFormatString ="view_staff_profile.aspx?search={0}" ></asp:HyperLinkField>
                <asp:BoundField DataField="phone" HeaderText="phone" SortExpression="phone" />
                <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
                <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department" />
                <asp:BoundField DataField="location" HeaderText="Division" SortExpression="location" />
                
            </Columns>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <asp:SqlDataSource ID="srchTxtSrc" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
            SelectCommand="SELECT * FROM [directory] WHERE ([fullname] LIKE '%' + @fullname + '%')">
            <SelectParameters>
                <asp:ControlParameter ControlID="SrchNameTxt" Name="fullname" PropertyName="Text"
                    Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </td>
    <td>
    </td>
    <td>
    </td>
    </tr>
    </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="QSearch" runat="server" Height="130px" Width="800px" ScrollBars="Auto" Visible="False">
    <table>
        <tr>
            <td style="height: 16px">
            </td>
            <td style="height: 16px">
                <asp:Label ID="Label10" runat="server" Text="Subsidiary"></asp:Label></td>
            <td style="height: 16px">
                <asp:Label ID="Label11" runat="server" Text="Department"></asp:Label></td>
                <td style="height: 16px">
                    <asp:Label ID="Label12" runat="server" Text="Names"></asp:Label>
                </td>
            <td style="height: 16px">
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Search By:" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="grp" runat="server" AppendDataBoundItems="True" DataSourceID="Groups" DataTextField="location" DataValueField="location" AutoPostBack="True">
                    <asp:ListItem>------- Select ------</asp:ListItem>
                    <asp:ListItem>All</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="deps" runat="server" AppendDataBoundItems="True" DataSourceID="Departments" DataTextField="Department" DataValueField="Department" AutoPostBack="True" EnableViewState="False">
                    <asp:ListItem>------- Select ------</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="names1" runat="server" AppendDataBoundItems="True" DataSourceID="Friends" DataTextField="fullname" DataValueField="fullname" EnableViewState="False">
                    <asp:ListItem>------- Select ------</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="findBut" runat="server" Text="Search" TabIndex="1" /></td>
        </tr>
        <tr>
            <td style="height: 16px">
            </td>
            <td style="height: 16px">
                <asp:SqlDataSource ID="Groups" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                    SelectCommand="SELECT DISTINCT [location] FROM [directory]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="AllSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                    SelectCommand="SELECT * FROM [directory]"></asp:SqlDataSource>
            </td>
            <td style="height: 16px">
                <asp:SqlDataSource ID="Departments" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                    SelectCommand="SELECT DISTINCT Department FROM directory WHERE (location = @location)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="grp" Name="location" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td style="height: 16px">
                <asp:SqlDataSource ID="Friends" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
                    SelectCommand="SELECT DISTINCT [fullname] FROM [directory] WHERE (([location] = @location) AND ([Department] = @Department)) ORDER BY [fullname]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="grp" Name="location" PropertyName="SelectedValue"
                            Type="String" />
                        <asp:ControlParameter ControlID="deps" Name="Department" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                </td>
            <td style="height: 16px">
            </td>
        </tr>
        </table>
    </asp:Panel>
    
        
        <table >
        <tr>
        <td style ="height :50px;"></td>
        </tr>
        <tr>
        <td>
        
        </td>
        <td style="width: 585px">
        <asp:Panel ID="InfoPanel" runat="server" Height="60%" Width="100%" Visible="False">
<table >
<tr>
<td style="width: 7px; height: 66px">

</td>
<td style="width: 3px; height: 66px;" colspan="4" rowspan="1">
    <asp:Label ID="mesg" runat="server" Text="Label" Width ="100%" Font-Size="Large" EnableTheming="False"></asp:Label></td>
<td style="height: 66px">

</td>
<td style="height: 66px">
 <asp:Panel ID="imgPan" runat="server">
    </asp:Panel>
</td>
<td style="width: 3px; height: 66px;">
   
</td>
</tr>

<tr>
<td style="width: 7px">

</td>
<td style="width: 85px; height: 30px;">
    &nbsp;<asp:Panel ID="Panel1" runat="server" Height="200px" Width="300px">
    <asp:Label ID="Label2" runat="server" Text="First Name:" Width="100px"></asp:Label>&nbsp;
    <asp:Label ID="firstNameLab" runat="server" Text="Label"></asp:Label>
        <br />
        <br />
    <asp:Label ID="Label1" runat="server" Text="Last Name:" Width="100px"></asp:Label>&nbsp;
    <asp:Label ID="lastnameLab" runat="server" Text="Label"></asp:Label><br />
        <br />
<asp:Label ID="Label7" runat="server" Text="Sex:" Width="100px"></asp:Label>&nbsp;
        <asp:Label ID="sexLab" runat="server" Text="Label"></asp:Label><br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="State Of Origin:" Width="100px"></asp:Label>&nbsp;
        <asp:Label ID="StateLab" runat="server" Text="Label"></asp:Label><br />
        <br />
    <asp:Label id="Label13" runat="server" Text="Phone No:" Width="100px"></asp:Label>&nbsp;
    <asp:Label id="phoneNo" runat="server" Text="Label"></asp:Label></asp:Panel>
</td>
<td style="width: 7px">
    &nbsp;<asp:Panel ID="Panel2" runat="server" Height="200px" Width="350px">
    <asp:Label ID="Label3" runat="server" Text="Division:" Width="100px"></asp:Label>&nbsp;
<asp:Label ID="divLab" runat="server" Text="Label"></asp:Label><br />
        <br />
        <asp:Label ID="Label5" runat="server" Text="Department:" Width="100px"></asp:Label>&nbsp;
        <asp:Label ID="deptLab" runat="server" Text="Label"></asp:Label><br />
        <br />
        <asp:Label ID="Label9" runat="server" Text="DOB:" Width="100px"></asp:Label>&nbsp;
        <asp:Label ID="DOBlab" runat="server" Text="Label"></asp:Label><br />
        <br />
        <asp:Label ID="Label8" runat="server" Text="E-Mail:" Width="100px"></asp:Label>
        &nbsp;
        <asp:Label ID="emailLab" runat="server" Text="Label"></asp:Label></asp:Panel>
</td>
<td style="width: 4px">
    </td>
<td>
    &nbsp;</td>
<td style="width:50px;"></td>
</tr>

<tr>
<td style="width: 7px">

</td>
<td style="width: 85px; height: 30px;">
    &nbsp;</td>
<td style="width :7px;">
</td>
<td>
    &nbsp;</td>
<td style="width: 4px">
    &nbsp;</td>
<td>
    &nbsp;</td>
</tr>

<tr>
<td style="width: 7px">
    &nbsp;</td>
<td style="width: 85px; height: 30px">
    &nbsp;
</td>
<td style="width: 7px">
    &nbsp;</td>
<td>
    &nbsp;</td>
<td>
    &nbsp;</td>
    </tr>
    <tr>
    <td style="width: 7px">
    
    </td>
    <td style="width: 85px; height: 30px">
        </td>
    <td style="width: 7px">
        </td>
    <td>
        &nbsp;</td>
        <td>
        </td>
    </tr>
    <tr>
    <td style="width: 7px">
    
    </td>
    <td>
        &nbsp;</td>
    <td style="width: 7px">
        &nbsp;</td>
    <td>
    
    </td>
    <td>
    
    </td>
    </tr>

</table>
    </asp:Panel>
            <br />
            <br />
        </td>
        <td style="width: 3px">
        
        </td>
        </tr>
        </table>

    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

