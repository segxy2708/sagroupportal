﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wallpost.ascx.vb" Inherits="wallpost" %>
<%@ Register src="comments.ascx" tagname="comments" tagprefix="uc1" %>

<script type ="text/javascript" language ="javascript" >
    function hide(item) {
        //document.getElementById(item).style.display='none'
        item.style.display = 'none';
    }
</script>
<script language ="javascript" type ="text/javascript" src ="Scripts/jquery-1.4.1.js"></script>
<asp:SqlDataSource ID="post_src" runat="server" 
    ConflictDetection="CompareAllValues" 
    ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" 
    DeleteCommand="DELETE FROM [users_wall] WHERE [id] = @original_id AND [username] = @original_username AND [statement] = @original_statement AND (([every] = @original_every) OR ([every] IS NULL AND @original_every IS NULL)) AND [date] = @original_date AND [imageurl] = @original_imageurl" 
    InsertCommand="INSERT INTO [users_wall] ([username], [statement], [every], [date], [imageurl]) VALUES (@username, @statement, @every, @date, @imageurl)" 
    OldValuesParameterFormatString="original_{0}" 
    SelectCommand="SELECT * FROM [users_wall] WHERE ([username] = @username) ORDER BY [date]" 
    
    UpdateCommand="UPDATE [users_wall] SET [username] = @username, [statement] = @statement, [every] = @every, [date] = @date, [imageurl] = @imageurl WHERE [id] = @original_id AND [username] = @original_username AND [statement] = @original_statement AND (([every] = @original_every) OR ([every] IS NULL AND @original_every IS NULL)) AND [date] = @original_date AND [imageurl] = @original_imageurl">
    <DeleteParameters>
        <asp:Parameter Name="original_id" Type="Int32" />
        <asp:Parameter Name="original_username" Type="String" />
        <asp:Parameter Name="original_statement" Type="String" />
        <asp:Parameter Name="original_every" Type="Int32" />
        <asp:Parameter Name="original_date" Type="DateTime" />
        <asp:Parameter Name="original_imageurl" Type="String" />
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="username" Type="String" />
        <asp:Parameter Name="statement" Type="String" />
        <asp:Parameter Name="every" Type="Int32" />
        <asp:Parameter Name="date" Type="DateTime" />
        <asp:Parameter Name="imageurl" Type="String" />
    </InsertParameters>
    <SelectParameters>
        <asp:SessionParameter Name="username" SessionField="names" Type="String" />
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="username" Type="String" />
        <asp:Parameter Name="statement" Type="String" />
        <asp:Parameter Name="every" Type="Int32" />
        <asp:Parameter Name="date" Type="DateTime" />
        <asp:Parameter Name="imageurl" Type="String" />
        <asp:Parameter Name="original_id" Type="Int32" />
        <asp:Parameter Name="original_username" Type="String" />
        <asp:Parameter Name="original_statement" Type="String" />
        <asp:Parameter Name="original_every" Type="Int32" />
        <asp:Parameter Name="original_date" Type="DateTime" />
        <asp:Parameter Name="original_imageurl" Type="String" />
    </UpdateParameters>
</asp:SqlDataSource>
<asp:DataList ID="DataList1" runat="server" CellPadding="4" DataKeyField="id" 
    DataSourceID="post_src" ForeColor="#333333" >
    <AlternatingItemStyle BackColor="White" />
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    <ItemStyle BackColor="#EFF3FB" />
    <ItemTemplate>
    <table >
    <tr>
    <td valign="top">
    <asp:Image ID ="img" runat ="server" ImageUrl ='<%# Eval("imageUrl") %>' Width="50px" Height="50px" />
    </td>
    <td ><a href="" ><asp:Label ID="usernameLabel" runat="server" Text='<%# Eval("username") %>' /></a>
    <br />
    <asp:Label ID="statementLabel" runat="server" Text='<%# Eval("statement") %>' />
    <br />
    Posted on <asp:Label ID="Label1" runat="server" Text='<%# Eval("date") %>' opacity="2" /> 
          <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click" >Comment</asp:LinkButton>  
    </td>
    </tr>
    <tr>
    <td></td>
    <td colspan ="1">

        <asp:Panel ID="Panel1" runat="server" Visible="true">
            <uc1:comments ID="comments1" runat="server" />
        </asp:Panel>

    </td>
    </tr>
    </table>
        
    </ItemTemplate>
    <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
</asp:DataList>

