<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="SignOffRequest.aspx.vb" Inherits="SignOffRequest" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table >
<tr>
<td>
</td>
<td>
</td>
<td>
</td>
<td>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
<td>
</td>
<td>
</td>
</tr>
<tr>
<td>
</td>
<td><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="TicketNo" DataSourceID="signOffSrc">
    <Columns>
        <asp:BoundField DataField="TicketNo" HeaderText="Ticket No" ReadOnly="True" SortExpression="TicketNo" />
        <asp:BoundField DataField="PersonalDetails" HeaderText="Personal Details" SortExpression="PersonalDetails" />
        <asp:BoundField DataField="AffectedAsset" HeaderText="Affected Asset" SortExpression="AffectedAsset" />
        <asp:BoundField DataField="Treated" HeaderText="Treated" SortExpression="Treated" />
        <asp:BoundField DataField="SignOff" HeaderText="Sign Off Status" SortExpression="SignOff" />
        <asp:ButtonField ButtonType ="Button" CommandName ="signoff" Text ="Sign Off" />
    </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="signOffSrc" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT TicketNo, PersonalDetails, AffectedAsset, Treated, SignOff FROM Job_Title_Table WHERE (TicketCreator = @TicketCreator) AND (Treated = @Treated) AND (SignOff <> @SignOff)" UpdateCommand="UPDATE Job_Title_Table SET SignOff = @SignOff WHERE (TicketCreator = @TicketCreator)">
        <SelectParameters>
            <asp:SessionParameter Name="TicketCreator" SessionField="names" Type="String" />
            <asp:Parameter DefaultValue="YES" Name="Treated" />
            <asp:Parameter DefaultValue="YES" Name="SignOff" />
        </SelectParameters>
        <UpdateParameters>
            <asp:SessionParameter DefaultValue="" Name="SignOff" SessionField="signoff" />
            <asp:SessionParameter DefaultValue="" Name="TicketCreator" SessionField="names" />
        </UpdateParameters>
    </asp:SqlDataSource>
</td>
    
<td>
</td>
<td>
</td>
</tr>
<tr>
<td>
</td>
<td>
</td>
<td>
</td>
<td>
</td>
</tr>
</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

