
Partial Class Staff_Window
    Inherits System.Web.UI.Page

    Protected Sub srch_but_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles srch_but.Click
        Me.GridView1.DataSourceID = "txt_src"
    End Sub

    Protected Sub SelTypList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SelTypList.SelectedIndexChanged
        If Me.SelTypList.SelectedIndex = 0 Then
            Me.DropDownList1.Visible = False
            Me.srch_but.Visible = True
            Me.name_txt.Visible = True
            Me.Label1.Visible = True
        ElseIf Me.SelTypList.SelectedIndex = 1 Then
            Me.srch_but.Visible = False
            Me.name_txt.Visible = False
            Me.DropDownList1.Visible = True
            Me.Label1.Visible = True
        Else
            Me.srch_but.Visible = False
            Me.name_txt.Visible = False
            Me.DropDownList1.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
       
    End Sub
End Class
