Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Partial Class transfer
    Inherits System.Web.UI.Page
    Protected Sub Upload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Upload.Click
        uploads()
    End Sub

    Sub uploads()
        Dim savePaths As String = Server.MapPath(".") & "\" & Session("names") + "\"
        Dim docpath As String = Server.MapPath(".") & "\" & Session("names") & "\My Documets" & "\docs\"
        Dim pixpath As String = Server.MapPath(".") & "\" & Session("names") & "\My Documets" & "\images\"
        Dim otherspath As String = Server.MapPath(".") & "\" & Session("names") & "\My Documets" & "\others\"
        Dim rPath As String = Nothing
        Dim stts As String = Path.GetFileName(FileUpload.FileName)




        Dim username As String = Session("names")

        Dim myTime As String = DateTime.Now.TimeOfDay.ToString
        Dim myDate As String = DateTime.Now.Date.ToString


        Try
            System.IO.Directory.CreateDirectory(Server.MapPath(".") & "\" & Session("names"))
        Catch ex As Exception
            ' Me.Title = ex.Message
        End Try
        Try
            System.IO.Directory.CreateDirectory(Server.MapPath(".") & "\" & Session("names") & "\My Documets")
        Catch ex As Exception
            ' Me.Title = ex.Message
        End Try
        Try
            System.IO.Directory.CreateDirectory(docpath)
        Catch ex As Exception
            ' Me.Title = ex.Message
        End Try

        Try
            System.IO.Directory.CreateDirectory(pixpath)
        Catch ex As Exception
            ' Me.Title = ex.Message
        End Try


        If IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".exe" Then

        ElseIf IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".doc" Or IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".docx" Or _
            IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".pdf" Or IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".xls" Or _
            IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".xlst" Or IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".ppt" Then
            savePaths = docpath
            rPath = Session("names") & "\My Documets" & "\docs\"
        ElseIf IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".jpg" Or IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".jpeg" Or _
            IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".bmp" Or IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".gif" Or _
            IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".tif" Or IO.Path.GetExtension(FileUpload.PostedFile.FileName) = ".bitmap" Then
            savePaths = pixpath
            rPath = (Session("names") & "\My Documets" & "\images\")
        Else
            savePaths = otherspath
            rPath = Session("names") & "\My Documets" & "\others\"
        End If


        Dim filename As String = newPath(stts, savePaths) 'Path.GetFileName(FileUpload.FileName)
        Dim filePath As String = Server.MapPath("~/") + filename

        Try
            If FileUpload.HasFile Then

                If FileUpload.PostedFile.ContentLength < 102400000 Then

                    Dim loc As String = "Files\" + FileUpload.FileName.ToString
                    Dim path As String = rpath & filename ' Session("names") + "\" + filename
                    Dim myFileSize As Integer = FileUpload.PostedFile.ContentLength
                    Dim myFileType As String = FileUpload.PostedFile.ContentType

                    Dim location As New System.Web.UI.WebControls.Parameter("FileName", TypeCode.String, filename) '1
                    Dim fileType As New System.Web.UI.WebControls.Parameter("Location", TypeCode.String, filePath) '2
                    Dim userParams As New System.Web.UI.WebControls.Parameter("User", TypeCode.String, username) '3
                    Dim fileSize As New System.Web.UI.WebControls.Parameter("FileSize", TypeCode.String, myFileSize.ToString) '4
                    Dim fileTypes As New System.Web.UI.WebControls.Parameter("FileType", TypeCode.String, myFileType)  '5
                    Dim timeCreate As New System.Web.UI.WebControls.Parameter("TimeCreated", TypeCode.String, myTime) '6
                    Dim dateCreate As New System.Web.UI.WebControls.Parameter("DateCreated", TypeCode.String, myDate) '7
                    Dim myfileloc As New System.Web.UI.WebControls.Parameter("FileLoc", TypeCode.String, path) '8

                    Me.StoreUploadDetails.InsertParameters.Add(userParams)
                    Me.StoreUploadDetails.InsertParameters.Add(location)
                    Me.StoreUploadDetails.InsertParameters.Add(fileType)
                    Me.StoreUploadDetails.InsertParameters.Add(fileSize)
                    Me.StoreUploadDetails.InsertParameters.Add(timeCreate)
                    Me.StoreUploadDetails.InsertParameters.Add(dateCreate)
                    Me.StoreUploadDetails.InsertParameters.Add(fileTypes)
                    Me.StoreUploadDetails.InsertParameters.Add(myfileloc)


                    'If Me.FileUpload.PostedFile.ContentType = "" Then

                    'End If

                    Me.StoreUploadDetails.Insert()
                    FileUpload.SaveAs(savePaths + filename)


                    retMessage.Text = "File Upload Successful " + message4FileChange(FileUpload.FileName, filename)
                    Response.AppendHeader("Refresh", "5; URL=transfer.aspx")

                Else
                    retMessage.Text = "Cannot Upload file, File size greater than 10Mb"
                End If
            End If
        Catch ex As Exception
            retMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub
    
    Public Function newPath(ByVal filename As String, ByVal paths As String) As String
        Dim tempFileName As String = ""
        Dim path2check As String = paths + "\" + filename
        Try
            If File.Exists(path2check) Then
                Dim counter As Integer = 2
                While File.Exists(path2check)
                    filename = counter.ToString + filename
                    path2check = paths + "\" + filename
                    counter = counter + 1
                End While
            End If
            Return filename
        Catch ex As Exception

        End Try
        
    End Function

    Function message4FileChange(ByVal old As String, ByVal new1 As String) As String
        Dim rtstr
        If old <> new1 Then
            rtstr = " your file " + old + " already exist. Filename has been change to  " + new1
        Else
            rtstr = ""
        End If
        Return rtstr
    End Function

    
    'Protected Sub FileUpload_UploadedComplete(ByVal sender As Object, ByVal e As AjaxControlToolkit.AsyncFileUploadEventArgs) 'Handles FileUpload.UploadedComplete
    '    System.Threading.Thread.Sleep(5000)

    '    Dim savePaths As String = Server.MapPath(".") & "\" & Session("names") + "\"
    '    Dim docpath As String = Server.MapPath(".") & "\" & Session("names") & "\My Documets" & "\docs\"
    '    Dim pixpath As String = Server.MapPath(".") & "\" & Session("names") & "\My Documets" & "\images\"
    '    Dim otherspath As String = Server.MapPath(".") & "\" & Session("names") & "\My Documets" & "\others\"

    '    Dim stts As String = Path.GetFileName(e.filename)




    '    Dim username As String = Session("names")

    '    Dim myTime As String = DateTime.Now.TimeOfDay.ToString
    '    Dim myDate As String = DateTime.Now.Date.ToString


    '    Try
    '        System.IO.Directory.CreateDirectory(Server.MapPath(".") & "\" & Session("names"))
    '    Catch ex As Exception
    '        ' Me.Title = ex.Message
    '    End Try
    '    Try
    '        System.IO.Directory.CreateDirectory(Server.MapPath(".") & "\" & Session("names") & "\My Documets")
    '    Catch ex As Exception
    '        ' Me.Title = ex.Message
    '    End Try
    '    Try
    '        System.IO.Directory.CreateDirectory(docpath)
    '    Catch ex As Exception
    '        ' Me.Title = ex.Message
    '    End Try

    '    Try
    '        System.IO.Directory.CreateDirectory(pixpath)
    '    Catch ex As Exception
    '        ' Me.Title = ex.Message
    '    End Try


    '    If IO.Path.GetExtension(e.filename) = ".exe" Then

    '    ElseIf IO.Path.GetExtension(e.filename) = ".doc" Or IO.Path.GetExtension(e.filename) = ".docx" Or _
    '        IO.Path.GetExtension(e.filename) = ".pdf" Or IO.Path.GetExtension(e.filename) = ".xls" Or _
    '        IO.Path.GetExtension(e.filename) = ".xlst" Or IO.Path.GetExtension(e.filename) = ".ppt" Then
    '        savePaths = docpath
    '    ElseIf IO.Path.GetExtension(e.filename) = ".jpg" Or IO.Path.GetExtension(e.filename) = ".jpeg" Or _
    '        IO.Path.GetExtension(e.filename) = ".bmp" Or IO.Path.GetExtension(e.filename) = ".gif" Or _
    '        IO.Path.GetExtension(e.filename) = ".tif" Or IO.Path.GetExtension(e.filename) = ".bitmap" Then
    '        savePaths = pixpath

    '    Else
    '        savePaths = otherspath
    '    End If


    '    Dim filename As String = newPath(stts, savePaths) 'Path.GetFileName(FileUpload.FileName)
    '    Dim filePath As String = Server.MapPath("~/") + filename

    '    Try
    '        If FileUpload.HasFile Then

    '            If FileUpload.PostedFile.ContentLength < 102400000 Then

    '                Dim loc As String = "Files\" + e.filename.ToString
    '                Dim path As String = Session("names") + "\" + filename
    '                Dim myFileSize As Integer = FileUpload.PostedFile.ContentLength
    '                Dim myFileType As String = FileUpload.PostedFile.ContentType

    '                Dim location As New System.Web.UI.WebControls.Parameter("FileName", TypeCode.String, filename) '1
    '                Dim fileType As New System.Web.UI.WebControls.Parameter("Location", TypeCode.String, filePath) '2
    '                Dim userParams As New System.Web.UI.WebControls.Parameter("User", TypeCode.String, username) '3
    '                Dim fileSize As New System.Web.UI.WebControls.Parameter("FileSize", TypeCode.String, myFileSize.ToString) '4
    '                Dim fileTypes As New System.Web.UI.WebControls.Parameter("FileType", TypeCode.String, myFileType)  '5
    '                Dim timeCreate As New System.Web.UI.WebControls.Parameter("TimeCreated", TypeCode.String, myTime) '6
    '                Dim dateCreate As New System.Web.UI.WebControls.Parameter("DateCreated", TypeCode.String, myDate) '7
    '                Dim myfileloc As New System.Web.UI.WebControls.Parameter("FileLoc", TypeCode.String, path) '8

    '                Me.StoreUploadDetails.InsertParameters.Add(userParams)
    '                Me.StoreUploadDetails.InsertParameters.Add(location)
    '                Me.StoreUploadDetails.InsertParameters.Add(fileType)
    '                Me.StoreUploadDetails.InsertParameters.Add(fileSize)
    '                Me.StoreUploadDetails.InsertParameters.Add(timeCreate)
    '                Me.StoreUploadDetails.InsertParameters.Add(dateCreate)
    '                Me.StoreUploadDetails.InsertParameters.Add(fileTypes)
    '                Me.StoreUploadDetails.InsertParameters.Add(myfileloc)


    '                'If Me.FileUpload.PostedFile.ContentType = "" Then

    '                'End If

    '                Me.StoreUploadDetails.Insert()
    '                FileUpload.SaveAs(savePaths + filename)


    '                retMessage.Text = "File Upload Successful " + message4FileChange(FileUpload.FileName, filename)
    '                Response.AppendHeader("Refresh", "5; URL=transfer.aspx")

    '            Else
    '                retMessage.Text = "Cannot Upload file, File size greater than 10Mb"
    '            End If
    '        End If
    '    Catch ex As Exception
    '        retMessage.Text = ex.Message
    '    End Try
    '    'uploads()
    'End Sub

    'Protected Sub FileUpload_UploadedFileError(ByVal sender As Object, ByVal e As AjaxControlToolkit.AsyncFileUploadEventArgs) Handles FileUpload.UploadedFileError
    '    Me.lblStatus.Visible = True
    '    Me.lblStatus.Text = e.statusMessage
    'End Sub
End Class
