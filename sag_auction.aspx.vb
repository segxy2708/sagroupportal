Imports ASP
Partial Class sag_auction
    Inherits System.Web.UI.Page
    Dim glob As global_asax
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session.IsNewSession Then
                HttpContext.Current.Response.Redirect("session_timeout.aspx")
            Else
                'lblName.Text = Session("names")
            End If
        Catch ex As Exception

        End Try
        Me.Title = "" 'remains()
        Session("date") = DateTime.Now.Day & "/" & DateTime.Now.Month & "/" & DateTime.Now.Year
        Session("time") = DateTime.Now.Hour & ":" & DateTime.Now.Minute & ":" & DateTime.Now.Second
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    

    Private Function remains() As Integer
        Dim query As String = "Select [quantity] from items WHERE [item_name]='" & Me.DropDownList1.SelectedValue & "'"
        glob = New global_asax
        Dim remain As Integer = Convert.ToInt32(glob.getData(ConnectionString, query))
        Return remain
    End Function

    
    Protected Sub Bidbut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Bidbut.Click
        If remains() < Convert.ToInt32(Me.QuantityTxt.Text) Then
            Me.Label3.Text = "Error Bidding, Quantity requested less than what's available"
        Else
            Session("amount") = Convert.ToInt32(Me.QuantityTxt.Text) * Convert.ToInt32(Me.AmountTxt.Text)
            Me.bidSource.Insert()
            Me.Label3.Text = "Bid Successful"
            Me.AmountTxt.Text = ""
            Me.QuantityTxt.Text = ""
            Response.AppendHeader("Refresh", "7,URL=sag_auction.aspx")
        End If

    End Sub

    Protected Sub ClickBid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClickBid.Click
        Me.Bidpan.Visible = True
    End Sub
End Class
