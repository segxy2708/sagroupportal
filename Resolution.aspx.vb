Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Partial Class Resolution
    Inherits System.Web.UI.Page
   
    Dim today As String = DateTime.Now.Date.ToString + " : " + DateTime.Now.TimeOfDay.ToString
    Dim mon As String = DateTime.Now.Month.ToString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Resolution"
        Me.TicketNumText.Text = Session("tickNo")
        Me.DateTimeTxt.Text = Session("getDatetime")
        Me.Detailtxt.Text = Session("PersonalDetails")
        Me.AffectedAssetTxt.Text = Session("AffectedAsset")
        Me.Symptomstxt.Text = Session("symptoms")
        Me.CreatorTxt.Text = Session("tickCreator")
    End Sub

    'Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
    '    Me.RequestPan.Visible = True
    '    Me.RequestPan.Focus()
    '    Me.Button1.Visible = False
    'End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Response.AppendHeader("Refresh", "7,URL=Resolution.aspx")
        RequestPan.Visible = False
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Button1.Visible = False
        Try

            Dim name As New System.Web.UI.WebControls.Parameter("Attendance", TypeCode.String, Session("names"))
            Dim treate As New System.Web.UI.WebControls.Parameter("Treated", TypeCode.String, "YES")
            Dim submitTime As New System.Web.UI.WebControls.Parameter("DateTreated", TypeCode.String, today)
            Dim month As New System.Web.UI.WebControls.Parameter("Month", TypeCode.String, mon)

            Me.complainDetails.UpdateParameters.Add(name)
            Me.complainDetails.UpdateParameters.Add(treate)
            Me.complainDetails.UpdateParameters.Add(submitTime)
            Me.complainDetails.UpdateParameters.Add(month)
            Me.ResolutionSource.Insert()
            Me.complainDetails.Update()
            Me.errorLab.Text = "Issues Resolved Successfully"
            Me.errorLab.Focus()
            Response.AppendHeader("Refresh", "7,URL=Job_Ticket.aspx")
        Catch ex As SqlException
            errorLab.Text = "Please Try again, Server maintainance is going on" '"Resolutions Boxes are empty, pls check"
            errorLab.Visible = True
        Catch ex As Exception
            errorLab.Text = "error, pls check input boxes"
        End Try

    End Sub
End Class
