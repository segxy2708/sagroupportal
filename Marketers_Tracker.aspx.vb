Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Partial Class Marketers_Tracker
    Inherits System.Web.UI.Page

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim i As New MarketerTableAdapters.MovementTableAdapter

        DataList1.DataSourceID = ""

        DataList1.DataSource = i.GetDataByFourDetails(drpCompany.Text, Now.Date, "%" & TextBox1.Text & "%", "Open")
        DataList1.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Display Date
        If Session("Department") = "Marketing" Or Session("id") = 363 Or Session("id") = 590 Or Session("id") = 13 Then
            lblDate.Text = Now.Date
            lblDate.Visible = True
            Panel1.Visible = True
        Else
            lblDate.Visible = False
            Panel1.Visible = False
            Label2.Text = "You are not Eligible to view this portal"
            Label2.ForeColor = Drawing.Color.Red
        End If
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub DataList1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DataList1.EditCommand
        Dim keys As Integer = DataList1.DataKeys(e.Item.ItemIndex).ToString()
        Session("I") = keys
        Response.Redirect(e.CommandArgument.ToString())
    End Sub

    Protected Sub DataList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataList1.SelectedIndexChanged

    End Sub
End Class
