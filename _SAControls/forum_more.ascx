<%@ Control Language="VB" AutoEventWireup="false" CodeFile="forum_more.ascx.vb" Inherits="_SAControls_forum_more" %>
<table style="font-size: 8pt; width: 360px; font-family: Tahoma">
    <tr>
        <td colspan="3">
            &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="javascript:onclick =forum()">Add New</asp:HyperLink></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: right">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="postid" DataSourceID="SqlDataSource1" Height="89px" Width="251px">
                <ItemTemplate>
                    <table style="width: 417px">
                        <tr>
                            <td style="width: 226px; text-align: left">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="forum_detail.aspx"
                                    CommandName="Edit" Font-Names="Tahoma" Font-Size="8pt" Text='<%# Eval("posttitle") %>'></asp:LinkButton></td>
                            <td style="width: 253px">
                                <span style="font-size: 10pt; font-family: Tahoma">Posted By:</span><asp:Label ID="postbyLabel"
                                    runat="server" Font-Names="Tahoma" Font-Size="10pt" Text='<%# Eval("postby") %>'></asp:Label><span
                                        style="font-size: 10pt; font-family: Tahoma"></span></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right">
                                <strong><span style="font-size: 8pt">Latest Comments: </span></strong>
                                <asp:Label ID="lblComments2" runat="server" Font-Names="Tahoma" Font-Size="XX-Small"
                                    ForeColor="#CC0000" Text='<%# Eval("comments2", "{0}") %>'></asp:Label></td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>"
                SelectCommand="SELECT postid, postdate, postby, posttitle, comments, status, comments2 FROM forum ORDER BY postid DESC">
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td style="width: 289px">
        </td>
        <td style="width: 175px; text-align: right">
        </td>
    </tr>
</table>
