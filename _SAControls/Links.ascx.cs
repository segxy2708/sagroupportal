using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _SAControls_Links : System.Web.UI.UserControl
{
    IssuesUtilities Util = new IssuesUtilities();
	
    protected void Page_Load(object sender, EventArgs e)
    {
        //Util.LoadExternalWeblinks(Session["location"].ToString());
        try
        {

            DataTable tt = new DataTable();
            tt = Util.LoadExternalWeblinks(Session["location"].ToString());

            DataTable tbl = new DataTable();
            tbl.Columns.Add("linkname");
            tbl.Columns.Add("linkURL");

             
            DataRow rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link1"].ToString();
            rr["linkURL"] = tt.Rows[0]["companyurl"].ToString();
            tbl.Rows.Add(rr);

            rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link2"].ToString();
            rr["linkURL"] = tt.Rows[0]["whatnew"].ToString();
            tbl.Rows.Add(rr);

            rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link3"].ToString();
            rr["linkURL"] = tt.Rows[0]["search"].ToString();
            tbl.Rows.Add(rr);

            rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link4"].ToString();
            rr["linkURL"] = tt.Rows[0]["news"].ToString();
            tbl.Rows.Add(rr);

            rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link5"].ToString();
            rr["linkURL"] = tt.Rows[0]["maps"].ToString();
            tbl.Rows.Add(rr);

            rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link6"].ToString();
            rr["linkURL"] = tt.Rows[0]["travel"].ToString();
            tbl.Rows.Add(rr);

            rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link7"].ToString();
            rr["linkURL"] = tt.Rows[0]["finance"].ToString();
            tbl.Rows.Add(rr);

            rr = tbl.NewRow();
            rr["linkname"] = tt.Rows[0]["link8"].ToString();
            rr["linkURL"] = tt.Rows[0]["weather"].ToString();
            tbl.Rows.Add(rr);

            dlnews.DataSource = tbl;
            dlnews.DataBind();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }
    protected void dlnews_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            Label lbl = (Label)e.Item.FindControl("lbl");
            if (lbl != null)
            {
                lbl.Text = Convert.ToString(e.Item.ItemIndex + 1) + "::";
            }
        }
    }



}
