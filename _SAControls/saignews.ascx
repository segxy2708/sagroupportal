<%@ Control Language="VB" ClassName="saignews" %>

<script runat="server">

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.AppendHeader("Refresh", "1200; URL=Default44.aspx")
    End Sub
</script>

<span style="font-size: 10pt; color: #666666; font-family: Tahoma"></span><asp:DataList ID="DataList1" runat="server" DataKeyField="newsid"
        DataSourceID="SqlDataSource1" Width="424px">
        <ItemTemplate>
            <table style="width: 432px">
                <tr>
                    <td style="width: 302px">
                        <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="#C04000" Text='<%# Eval("newstitle") %>'></asp:LinkButton></td>
                    <td>
                        <asp:Label ID="newsdateLabel" runat="server" ForeColor="Gray" Text='<%# Eval("newsdate", "{0:d}") %>'></asp:Label></td>
                    <td style="width: 135px">
                        <asp:Label ID="newstimeLabel" runat="server" ForeColor="Gray" Text='<%# Eval("newstime", "{0:t}") %>'></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Label ID="newsLabel" runat="server" ForeColor="Gray" Text='<%# Eval("news") %>'
                            Width="360px"></asp:Label></td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>"
    SelectCommand="SELECT TOP (5) newsid, newsdate, newstime, newstitle, news, status FROM SAIG_News ORDER BY newsid DESC, newsdate DESC, newstime DESC">
</asp:SqlDataSource>
