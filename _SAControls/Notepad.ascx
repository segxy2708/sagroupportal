<%@ Control Language="VB" ClassName="Notepad" %>
<%@ Import Namespace="System"%>
<%@Import Namespace="System.data" %>
<%@Import Namespace="System.data.oledb"%>
<%@Import Namespace="System.data.sqlclient"%>
<%@Import Namespace="System.Configuration"%>
<%@Import Namespace="System.Collections"%>
<%@Import Namespace="System.Web"%>
<%@Import Namespace="System.Web.UI"%>
<%@Import Namespace="Microsoft.VisualBasic"%>
<%@Import Namespace="System.Web.UI.HTMLcontrols"%>
<%@Import Namespace="System.Web.UI.Webcontrols"%>
<%@Import Namespace="System.IO"%>
<%@Import Namespace="Microsoft"%>

<script runat="server">
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString2").ConnectionString
    Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Retrieve HOD Fullname from the Database based on the Department and Organisation of the User
        Dim cmdname As SqlCommand = myconnection.CreateCommand()
        cmdname.CommandType = CommandType.Text
        cmdname.CommandText = "SELECT staff FROM Notepad WHERE (staff = @staff)"
        'myconnection.Open()
        Dim cml As SqlParameter = cmdname.CreateParameter()
        cml.ParameterName = "@staff"
        cml.Value = Session("id")
        cmdname.Parameters.Add(cml)
        Dim msg As Object = cmdname.ExecuteScalar()
        '---------End Retrieval------------
        Dim notepads As New DirectoryTableAdapters.NotepadTableAdapter
        If msg Is Nothing Then
            notepads.InsertNotepad(Trim(Session("id")), txtNotePad.Text)
            lblMessage.Text = "Your Note has been saved successfully."
            Response.AppendHeader("Refresh", "5; URL=sag_home.aspx")
        Else
            notepads.UpdateNotepad(txtNotePad.Text, Trim(Session("id")))
            lblMessage.Text = "Your Note has been updated successfully."
            Response.AppendHeader("Refresh", "5; URL=sag_home.aspx")
        End If

        '---------End Retrieval------------

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        'Retrieve User's Note From the  database
        Dim cmdname As SqlCommand = myconnection.CreateCommand()
        cmdname.CommandType = CommandType.Text
        cmdname.CommandText = "SELECT Message FROM Notepad WHERE (staff = @staff)"
        myconnection.Open()
        Dim cml As SqlParameter = cmdname.CreateParameter()
        cml.ParameterName = "@staff"
        cml.Value = Session("id")
        cmdname.Parameters.Add(cml)
        Dim message As Object = cmdname.ExecuteScalar()
        '---------End Retrieval------------
        If Not message Is Nothing Then
            txtNotePad.Text = message
            'Else
            '    txtNotePad.Text = "No Message in your Notepad."
            '    txtNotePad.ForeColor = Drawing.Color.Gray
        End If
        'End Retrival
        
    End Sub
</script>

<table style="width: 212px">
    <tr>
        <td colspan="3" rowspan="3" style="width: 302px">
            <asp:TextBox ID="txtNotePad" runat="server" Height="100px" Width="204px" Wrap="False" Font-Names="Cambria" Font-Size="X-Small" TextMode="MultiLine"></asp:TextBox></td>
    </tr>
    <tr>
    </tr>
    <tr>
    </tr>
    <tr>
        <td colspan="3" rowspan="1" style="text-align: center; width: 302px;">
            <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="3" rowspan="1" style="height: 21px; text-align: left; width: 302px;">
            <asp:LinkButton ID="btnNote" runat="server" OnClick="LinkButton1_Click">Update Notepad</asp:LinkButton></td>
    </tr>
</table>
