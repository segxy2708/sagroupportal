using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _SAControls_Projects : System.Web.UI.UserControl
{
    IssuesUtilities Util = new IssuesUtilities(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        dlnews.DataSource = Util.LoadProjects();
        dlnews.DataBind(); 
    }
    protected void dlnews_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            Label lbl = (Label)e.Item.FindControl("lbl");
            if (lbl != null)
            {
                lbl.Text = Convert.ToString(e.Item.ItemIndex + 1) + "::"; 
            }
        }
    }




}
