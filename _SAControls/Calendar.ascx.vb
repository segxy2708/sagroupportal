Imports System
Imports System.data
Imports System.data.SQLClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols

Partial Class _SAControls_Calendar
    Inherits System.Web.UI.UserControl

    'Public calendar1
    Public mycalendar
    Public mycalendar2
    Public mycalendar3
    Public button2

    Public icon, picture, visibledate1, visibledate2, caltime, maincalrollup, status

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged

    End Sub

    Protected Sub Calendar1_DayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        On Error Resume Next
        visibledate1 = DateAdd("m", -1, Now())
        visibledate2 = DateAdd("m", 1, Now())

        Dim myconnection As SqlConnection
        Dim myDataAdapter As SqlDataAdapter
        Dim Mydataset As DataSet
        Dim mydatatable As DataTable
        Dim myrow As DataRow
        Dim eventdate = "", strevents = ""
        myconnection = New SqlConnection(Application("dbdsn"))

        If maincalrollup = 1 Then
            myDataAdapter = New SqlDataAdapter("Select dte,text_field,id,caltype,color from maincal Union ALL Select dte,text_field,id,caltype,color from orgcal", myconnection)
        Else
            myDataAdapter = New SqlDataAdapter("Select * from maincal order by DTE,Starttime", myconnection)
        End If

        Mydataset = New DataSet()
        myDataAdapter.Fill(Mydataset, "cal")


        For Each myrow In Mydataset.Tables("cal").Rows
            eventdate = myrow("dte")
            If eventdate = (e.Day.Date) Then

                If caltime = 1 Then
                    icon = "<font face='Arial' size='1'>" & (myrow("starttime").toshorttimestring) & " - " & (myrow("endtime").toshorttimestring) & "<br>" & (myrow("Text_field"))
                Else
                    icon = "<font face='Arial' size='1'>" & (myrow("Text_field"))
                End If

                If myrow("caltype") = 2 Then
                    strevents = "<br>" & "<a href='../orgcal/eventdetail.aspx?id=" & (myrow("id")) & "'>" & (icon) & "</a>"
                End If

                If myrow("caltype") = 3 Then
                    strevents = "<br>" & "<a href='maincal/eventdetail.aspx?id=" & (myrow("id")) & "'>" & (icon) & "</a>"
                End If

                Dim img As New Image
                img.ImageUrl = Server.MapPath("images") + "\star.gif"

                'img = System.Drawing.Image.FromFile(Server.MapPath("images") + "\star.gif");  

                ' e.Cell.Controls.Add(New LiteralControl(strevents.ToString()))
                Dim ct As New Control()
                Dim tx As New LiteralControl(strevents.ToString())
                ct.Controls.Add(img)
                'ct.Controls.Add(tx)
                e.Cell.ToolTip = myrow("actualtext")
                e.Cell.BackColor = Drawing.Color.GreenYellow
            End If
        Next

        Page.DataBind()
        myconnection.Close()
    End Sub
End Class
