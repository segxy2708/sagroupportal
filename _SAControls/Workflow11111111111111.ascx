<%@ Control Language="VB" AutoEventWireup="false"  Inherits="Workflow" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<%@ Import Namespace="System"%>
<%@Import Namespace="System.data" %>
<%@Import Namespace="System.data.oledb"%>
<%@Import Namespace="System.data.sqlclient"%>
<%@Import Namespace="System.Configuration"%>
<%@Import Namespace="System.Collections"%>
<%@Import Namespace="System.Web"%>
<%@Import Namespace="System.Web.UI"%>
<%@Import Namespace="Microsoft.VisualBasic"%>
<%@Import Namespace="System.Web.UI.HTMLcontrols"%>
<%@Import Namespace="System.Web.UI.Webcontrols"%>
<%@Import Namespace="System.IO"%>
<%@Import Namespace="Microsoft"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<style type="text/css">
<!--
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
                                                                                                                   .style1
                                                                                                                   {
                                                                                                                       width: 60px;
                                                                                                                       height: 6px;
                                                                                                                   }
                                                                                                                   .style2
                                                                                                                   {
                                                                                                                       height: 6px;
                                                                                                                   }
-->

</style>


&nbsp;<table border="0" cellpadding="1" cellspacing="1" width="100%">
<tr>
        <td style="width: 60px">
            <a class="smallie" href="./_SARequisitions/Requisitions.aspx"><span style="color: Maroon;
                text-decoration;">::Requisitions:</span></a>              </td>
        <td align="left" width="60" style="font-size: 12pt; font-family: Verdana" >
            <asp:LinkButton ID="lnkReq" runat="server" CssClass="smallie"></asp:LinkButton></td>
        <td width="70">
            <a class="smallie" href="./_SAMedicals/Medical.aspx"><span style="color: Maroon">
                :</span></a></td>
        <td align="left" style="font-size: 12pt; font-family: Verdana" width="60">
            &nbsp;</td>
    </tr>
    <tr style="font-weight: bold; font-size: 12pt; color: Maroon; font-family: Verdana;
        text-decoration: underline">
        <td style="width: 130px">
            <a class="smallie" href="./_SALeaves/LeaveEdit.aspx?uid=<%= Session["userid"].ToString() %>"><span style="color: Maroon">
                ::Leave Applications:       <td align="left" style="font-size: 12pt; font-family: Verdana" width="60">
            <asp:LinkButton ID="lnkLeave" runat="server" CssClass="smallie"></asp:LinkButton></td>
        <td style="font-size: 12pt; font-family: Verdana" width="70">            </td>
        <td align="left" style="font-size: 12pt; font-family: Verdana; text-decoration: underline"
            width="60">            </td>
    </tr>
    <tr style="font-size: 12pt; font-family: Verdana">
        <td align="left" valign="top" class="style1">
            <a class="smallie" href="./_SALoans/LoanApplication.aspx">::Loan Applications:</a>        </td>
        <td align="left" valign="top" width="60" class="style2">
            <asp:LinkButton ID="lnkLoan" runat="server" CssClass="smallie"></asp:LinkButton></td>
        <td align="left" valign="top" width="70" class="style2">
        <a style="visibility:hidden;"  class="smallie" href="#">::Call Memo:</a>              </td>
        <td align="left" valign="top" width="60" class="style2">
            </td>
    </tr>
    <tr>
        <td style="width: 60px">
            <asp:HyperLink ID="HyperLink1" runat="server" Font-Names="Tahoma" Font-Size="Small"
                NavigateUrl="~/_SAQuery/QueryEdit.aspx">: : Query Application:</asp:HyperLink></td>
        <td align="left" width="60">
            <asp:Label ID="Label3" runat="server" Width="60px"></asp:Label></td>
        <td width="70">
            <asp:Label ID="Label2" runat="server" Width="80px"></asp:Label></td>
        <td align="left" width="60">
            <asp:Label ID="Label4" runat="server" Width="69px"></asp:Label></td>
    </tr>
    <tr>
        <td style="width: 60px">
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/_SAMemo/MemoEdit.aspx" Font-Names="Mangal">::Memo Application</asp:HyperLink></td>
        <td align="left" width="60">
        </td>
        <td width="70">
        </td>
        <td align="left" width="60">
        </td>
    </tr>
    <tr>
      <td height="10" colspan="4" align="left" valign="top"></td>
    </tr>
</table>
