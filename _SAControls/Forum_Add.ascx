<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Forum_Add.ascx.vb" Inherits="_SAControls_Forum_Add" %>
<script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}

// ]]>
</script>

<table style="width: 493px">
    <tr>
        <td colspan="4" style="height: 15px; text-align: center">
            <asp:Label ID="lblPost" runat="server" Font-Names="Tahoma" Font-Size="8pt"></asp:Label>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
            <br />
            <br />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="javascript:window.close()"
                Visible="False">Close</asp:HyperLink></td>
    </tr>
    <tr>
        <td style="width: 149px; height: 15px; text-align: center;">
            <span style="font-size: 10pt; font-family: Tahoma"></span><asp:Panel ID="Panel1" runat="server">
                <table id="TABLE1" onclick="return TABLE1_onclick()" style="width: 408px">
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <span style="font-size: 10pt; font-family: Tahoma">&nbsp;New Forum Topic</span></td>
                        <td style="text-align: center">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <strong><span style="font-size: 8pt; font-family: Tahoma">Title:</span></strong></td>
                        <td>
            <asp:TextBox ID="txtTitle" runat="server" Width="408px"></asp:TextBox></td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="txtTitle" ErrorMessage="Forum Title cannot be empty">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong><span style="font-size: 8pt; font-family: Tahoma">Comment:</span></strong></td>
                        <td>
            <asp:TextBox ID="txtComment" runat="server" Height="88px" TextMode="MultiLine" Width="408px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="txtComment" ErrorMessage="Comment cannot be empty">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 26px">
                        </td>
                        <td style="height: 26px; text-align: right">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" /></td>
                        <td style="height: 26px; text-align: right">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 26px; text-align: center">
                            &nbsp; &nbsp; &nbsp;
                        </td>
                        <td style="height: 26px; text-align: center">
                            &nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
            <span style="font-size: 10pt; font-family: Tahoma"></span>
        </td>
    </tr>
</table>
