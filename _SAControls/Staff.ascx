<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Staff.ascx.cs" Inherits="_SAControls_Staff" %>
<TABLE id="Table4" 	width="100%" border="0" bordercolor="gainsboro" cellpadding="3">
	<TR class="documentMain">
		<TD class="documentText" vAlign="top"><asp:DataList ID="rpt" runat="server" Width="240px" OnSelectedIndexChanged="rpt_SelectedIndexChanged">
          <itemtemplate>
            <table border="0" width="100%">
              
              <tr background="images2/pics.jpg">
                <td rowspan="6" width="125" align="left" valign="top" ><asp:Image runat="server" ImageUrl='<%# "../Docs/Pictures/" + DataBinder.Eval(Container, "DataItem.Picture") %>' Height="80" Width="80"   />                        </td>
                <td><span class="admintextbold">Name:</span></td>
                <td><span class="smallie"> <%# DataBinder.Eval(Container.DataItem, "Fullname") %> </span> </td>
              </tr>
              <tr align=left>
                <td><span class="admintextbold">Company:</span></td>
                <td align=left><span class="smallie"> <%# DataBinder.Eval(Container.DataItem, "Location")%> </span> </td>
              </tr>              
              <tr align=left>
                <td><span class="admintextbold">Location:</span></td>
                <td><span class="smallie"> <%# DataBinder.Eval(Container.DataItem, "Office")%> </span> </td>
              </tr>
              <tr align=left>
                <td><span class="admintextbold">Dept:</span></td>
                <td><span class="smallie"> <%# DataBinder.Eval(Container.DataItem, "Department")%> </span> </td>
              </tr>
              
              <tr>
                <td colspan="3"><span class="admintextbold">Email:</span><span class="smallie"><%# DataBinder.Eval(Container.DataItem, "Email") %></span></td>
              </tr>
            </table>
              <asp:LinkButton ID="btnMessage" runat="server">Send Message</asp:LinkButton>
          </itemtemplate>
	    </asp:DataList></TD>
  </TR>
	<TR>
		<TD vAlign="middle" align="Right" height="10">
            &nbsp;</TD>
	</TR>
</TABLE>
