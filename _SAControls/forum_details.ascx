<%@ Control Language="VB" AutoEventWireup="false" CodeFile="forum_details.ascx.vb" Inherits="_SAControls_forum_details" %>
<style type="text/css">
    .style1
    {
        width: 85px;
    }
    .style2
    {
        width: 317px;
    }
</style>
<table style="width: 472px">
<tr>
<td></td>
    
<td><asp:ValidationSummary ID="ValidationSummary1" runat="server" /></td>
</tr>
    <tr>
        <td colspan="3" style="text-align: center">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="postid" DataSourceID="SqlDataSource1">
                <ItemTemplate>
                    <table style="width: 520px">
                        <tr>
                            <td style="width: 96px">
                                <span style="font-size: 8pt; font-family: Tahoma"><strong>Post Title:</strong></span></td>
                            <td style="width: 279px; text-align: left;">
                                <asp:Label ID="posttitleLabel" runat="server" Font-Bold="False" Font-Names="Tahoma"
                                    Font-Size="8pt" Text='<%# Eval("posttitle") %>'></asp:Label></td>
                            <td>
                                <asp:Label ID="postdateLabel" runat="server" Font-Bold="False" Font-Names="Tahoma"
                                    Font-Size="8pt" Text='<%# Eval("postdate", "{0:d}") %>'></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="width: 96px">
                                <span style="font-size: 8pt; font-family: Tahoma"><strong>Author:</strong></span></td>
                            <td style="width: 279px; text-align: left;">
                                <asp:Label ID="postbyLabel" runat="server" Font-Bold="False" Font-Names="Tahoma"
                                    Font-Size="8pt" Text='<%# Eval("postby") %>'></asp:Label></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 96px" valign=top>
                                <span style="font-size: 8pt; font-family: Tahoma"><strong>Discussion:</strong></span></td>
                            <td colspan="2" style="text-align: left">
                                <asp:Label ID="commentsLabel" runat="server" Font-Bold="False" Font-Names="Tahoma"
                                    Font-Size="8pt" Text='<%# Eval("comments") %>'></asp:Label></td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>==========================================================<br />
            <strong><span style="font-size: 10pt; font-family: Tahoma">Comments</span></strong>
            <br />
            ==========================================================<asp:DataList ID="DataList2"
                runat="server" DataKeyField="commentid" DataSourceID="SqlDataSource2" CellPadding="4" ForeColor="#333333">
                <ItemTemplate>
                    <table style="width: 520px">
                        <tr>
                            <td style="text-align: justify" valign="top" class="style1" rowspan="2">
                                
                                <asp:Image ID="Image1" runat="server" BorderColor="#006600" BorderStyle="Solid" 
                                    BorderWidth="1px" Height="50px" ImageUrl='<%# Eval("picx") %>' Width="50px" />
                                
                            </td>
                            <td style="text-align: justify" valign="top" class="style2" rowspan="2">
                                <asp:Label ID="commentsLabel" runat="server" Font-Names="Tahoma" 
                                    Font-Size="8pt" Text='<%# Eval("comments") %>'></asp:Label>
                            </td>
                            <td style="text-align: center" valign="top">
                                <asp:Label ID="Label2" runat="server" Font-Names="Tahoma" Font-Size="8pt" Text='<%# Eval("commentdate", "{0:d}") %>' Font-Bold="True"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="text-align: center" valign="top">
                                <strong><span style="font-size: 8pt; font-family: Tahoma">By:
                                <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Names="Tahoma" 
                                    Font-Size="8pt" Text='<%# Eval("commentby") %>'></asp:Label>
                                </span></strong>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <AlternatingItemStyle BackColor="White" />
                <ItemStyle BackColor="#E3EAEB" />
                <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            </asp:DataList><br />
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>"
                SelectCommand="SELECT * FROM [forum] WHERE ([postid] = @postid)">
                <SelectParameters>
                    <asp:SessionParameter Name="postid" SessionField="PostID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="background-color: #C0C0C0">
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>"
                SelectCommand="SELECT * FROM [forum_comment] WHERE ([postid] = @postid) ORDER BY [postid] DESC">
                <SelectParameters>
                    <asp:SessionParameter Name="postid" SessionField="PostID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <table style="width: 504px">
                <tr>
                    <td style="width: 25px" valign="top">
                        <span style="font-size: 8pt; font-family: Tahoma"><strong>Comment:</strong></span></td>
                    <td>
                        <asp:TextBox ID="txtComment" runat="server" Height="88px" TextMode="MultiLine" Width="408px"></asp:TextBox></td>
                    <td valign="top">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="txtComment" 
                            ErrorMessage="comment text box cannot be empty">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <asp:Label ID="lblPost" runat="server"></asp:Label></td>
                    <td valign="top">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" />&nbsp;<asp:Button 
                            ID="Button1" runat="server" Text="back" CausesValidation="False" 
                            style="height: 26px" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
    </tr>
</table>
