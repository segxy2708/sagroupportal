<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Forums.ascx.cs" Inherits="_SAControls_Forums" %>
<TABLE id="Table4" 	width="100%" height="115px" border="0" bordercolor="gainsboro" cellpadding="1">
	<TR class="documentMain">
		<TD class="documentText" vAlign="top">
					<asp:datalist id="dlnews" ShowHeader="False" Width="100%" BorderStyle="None"
						CellPadding="1" BackColor="Transparent" BorderWidth="0px" BorderColor="#CCCCCC"  
						  Height="16px" runat="server" RepeatColumns="1" OnItemDataBound="dlnews_ItemDataBound">
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
						<AlternatingItemStyle CssClass="gridh"></AlternatingItemStyle>
						<ItemStyle ForeColor="#000066" CssClass="gridh"></ItemStyle>
								<ItemTemplate>
								<table border="0" cellpadding="0" cellspacing="2">
								<tr valign="top">
								<td><asp:Label ID="lbl" CssClass="" runat="server" Text="::"></asp:Label></td>
								<td>
									<a class="smallie" style="COLOR: Maroon;width :100px" href ='discuss/topicsub.aspx?message_id=<%# DataBinder.Eval(Container, "DataItem.message_id") %>'>
										<%#DataBinder.Eval(Container.DataItem, "topic")%>
									</a>
									</td>
									 <td>
									 <asp:Label ID="Label1" Width="90px" CssClass="admintextmedium" runat="server" Text=<%#DataBinder.Eval(Container.DataItem, "date_entered","{0:dd-MMM-yyyy}")%> ></asp:Label> 
									 </td>
									</tr>  
									</table>
								</ItemTemplate> 
					</asp:datalist></TD>
	</TR>
	<TR>
		<TD vAlign="middle" align="Right" height="10">
			 
				<asp:hyperlink id="HyperLink1" CssClass="Normal" Font-Size="X-Small" Width="48px" runat="server" NavigateUrl="../ContactsList.aspx">more...</asp:hyperlink>
		</TD>
	</TR>
</TABLE>
