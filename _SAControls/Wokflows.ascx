<%@ Control Language="VB" ClassName="Wokflows" %>
<%@ Register Assembly="GMDatePicker" Namespace="GrayMatterSoft" TagPrefix="cc1" %>
<%@ Import Namespace="System"%>
<%@Import Namespace="System.data" %>
<%@Import Namespace="System.data.oledb"%>
<%@Import Namespace="System.data.sqlclient"%>
<%@Import Namespace="System.Configuration"%>
<%@Import Namespace="System.Collections"%>
<%@Import Namespace="System.Web"%>
<%@Import Namespace="System.Web.UI"%>
<%@Import Namespace="Microsoft.VisualBasic"%>
<%@Import Namespace="System.Web.UI.HTMLcontrols"%>
<%@Import Namespace="System.Web.UI.Webcontrols"%>
<%@Import Namespace="System.IO"%>
<%@Import Namespace="Microsoft"%>

<script runat="server">
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString2").ConnectionString
    Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        'Retrieve Query Count  From Database
        Dim cmdname As SqlCommand = myconnection.CreateCommand()
        cmdname.CommandType = CommandType.Text
        cmdname.CommandText = "SELECT Count(Status2) FROM Query WHERE (QTo = @QTo)"
        myconnection.Open()
        Dim cml As SqlParameter = cmdname.CreateParameter()
        cml.ParameterName = "@QTo"
        cml.Value = Session("name")
        cmdname.Parameters.Add(cml)
        Dim names As Object = cmdname.ExecuteScalar()
        
        'lblCount.Text = names

        '---------End Retrieval------------
        
        'Retrieve Read Query Count  From Database
        Dim cmdname112 As SqlCommand = myconnection.CreateCommand()
        cmdname112.CommandType = CommandType.Text
        cmdname112.CommandText = "SELECT Count(Status2) FROM Query WHERE ((QTo = @QTo) AND (Status2 = @Status2))"
        'myconnection.Open()
        Dim cml112 As SqlParameter = cmdname112.CreateParameter()
        Dim cml212 As SqlParameter = cmdname112.CreateParameter()
        cml112.ParameterName = "@QTo"
        cml212.ParameterName = "@Status2"
        cml112.Value = Session("name")
        cml212.Value = "Read"
        cmdname112.Parameters.Add(cml112)
        cmdname112.Parameters.Add(cml212)

        Dim names112 As Object = cmdname112.ExecuteScalar()
        
        lblRead2.Text = names112

        '---------End Retrieval------------
        
        'Retrieve Unread Query Count  From Database
        Dim cmdname1121 As SqlCommand = myconnection.CreateCommand()
        cmdname1121.CommandType = CommandType.Text
        cmdname1121.CommandText = "SELECT Count(Status2) FROM Query WHERE ((QTo = @QTo) AND (Status2 = @Status2))"
        'myconnection.Open()
        Dim cml1121 As SqlParameter = cmdname1121.CreateParameter()
        Dim cml2121 As SqlParameter = cmdname1121.CreateParameter()
        cml1121.ParameterName = "@QTo"
        cml2121.ParameterName = "@Status2"
        cml1121.Value = Session("name")
        cml2121.Value = "Unread"
        cmdname1121.Parameters.Add(cml1121)
        cmdname1121.Parameters.Add(cml2121)

        Dim names1121 As Object = cmdname1121.ExecuteScalar()
        
        lblUnread2.Text = names1121

        '---------End Retrieval------------
        
        'Retrieve Memo Count  From Database
        Dim cmdname1 As SqlCommand = myconnection.CreateCommand()
        cmdname1.CommandType = CommandType.Text
        cmdname1.CommandText = "SELECT Count(mStatus2) FROM New_Memo WHERE (mTo = @mTo) OR (mTo2 = @mTo2)"
        'myconnection.Open()
        Dim cml1 As SqlParameter = cmdname1.CreateParameter()
        Dim cml2 As SqlParameter = cmdname1.CreateParameter()
        cml1.ParameterName = "@mTo"
        cml2.ParameterName = "@mTo2"
        cml1.Value = Session("name")
        cml2.Value = Session("name")
        cmdname1.Parameters.Add(cml1)
        cmdname1.Parameters.Add(cml2)
        Dim names1 As Object = cmdname1.ExecuteScalar()
        
        'lblCount1.Text = names1

        '---------End Retrieval------------
        
        'Retrieve Read Memeo Count  From Database
        Dim cmdname11 As SqlCommand = myconnection.CreateCommand()
        cmdname11.CommandType = CommandType.Text
        cmdname11.CommandText = "SELECT Count(mStatus2) FROM New_Memo WHERE ((mTo = @mTo) OR (mTo2 = @mTo2)) AND (mStatus2 = @mStatus2)"
        'myconnection.Open()
        Dim cml11 As SqlParameter = cmdname11.CreateParameter()
        Dim cml21 As SqlParameter = cmdname11.CreateParameter()
        Dim cml31 As SqlParameter = cmdname11.CreateParameter()
        cml11.ParameterName = "@mTo"
        cml21.ParameterName = "@mTo2"
        cml31.ParameterName = "@mStatus2"
        cml11.Value = Session("name")
        cml21.Value = Session("name")
        cml31.Value = "Read"
        cmdname11.Parameters.Add(cml11)
        cmdname11.Parameters.Add(cml21)
        cmdname11.Parameters.Add(cml31)
        Dim names11 As Object = cmdname11.ExecuteScalar()
        
        lblRead1.Text = names11

        '---------End Retrieval------------
        
        'Retrieve Unread Memeo Count  From Database
        Dim cmdname111 As SqlCommand = myconnection.CreateCommand()
        cmdname111.CommandType = CommandType.Text
        cmdname111.CommandText = "SELECT Count(mStatus2) FROM New_Memo WHERE ((mTo = @mTo) OR (mTo2 = @mTo2)) AND (mStatus2 = @mStatus2)"
        'myconnection.Open()
        Dim cml111 As SqlParameter = cmdname111.CreateParameter()
        Dim cml211 As SqlParameter = cmdname111.CreateParameter()
        Dim cml311 As SqlParameter = cmdname111.CreateParameter()
        cml111.ParameterName = "@mTo"
        cml211.ParameterName = "@mTo2"
        cml311.ParameterName = "@mStatus2"
        cml111.Value = Session("name")
        cml211.Value = Session("name")
        cml311.Value = "Unread"
        cmdname111.Parameters.Add(cml111)
        cmdname111.Parameters.Add(cml211)
        cmdname111.Parameters.Add(cml311)
        Dim names111 As Object = cmdname111.ExecuteScalar()
        
        lblUnread.Text = names111

        '---------End Retrieval------------

    End Sub
</script><style type="text/css">
<!--
body,td,th {
	font-size: 10px;
	color: #990000;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
a:link {
	text-decoration: none;
	color: #990000;
}
a:visited {
	text-decoration: none;
	color: #990000;
}
a:hover {
	text-decoration: underline;
	color: #990000;
}
a:active {
	text-decoration: none;
	color: #990000;
}
a {
	font-size: 10px;
	color: #993300;
	font-family: Verdana, Arial, Helvetica, sans-serif;
}
.style1 {color: #993300}
.style2 {font-weight: bold}
.style4 {
	color: #990000;
	font-size: 10pt;
}
-->
</style>

&nbsp;
<style type="text/css">
<!--
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
}
style1 {
	color: #FFFFFF;
	font-size: 13px;
	font-weight: bold;
}
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: underline;
}
a:active {
	text-decoration: none;
                                                                                                                   .style1
                                                                                                                   {
                                                                                                                       width: 60px;
                                                                                                                       height: 6px;
                                                                                                                   }
                                                                                                                   .style2
                                                                                                                   {
                                                                                                                       height: 6px;
                                                                                                                   }
-->
</style>
<table width="100%" border="1" cellpadding="1" cellspacing="1" bordercolor="#999933">
    <tr>
        <td style="width: 63px; height: 21px">        </td>
        <td style="height: 21px" width="70">&nbsp;Read</td>
        <td align="left" style="font-size: 12pt; color: #0000ff; font-family: Verdana; height: 21px"
            width="60">          <span class="style4">Unread</span></td>
    </tr>
    <tr>
        <td style="width: 63px">
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/_SARequisitions/Requisitions.aspx"
                Width="208px">: :Requisition:</asp:HyperLink></td>
      <td width="70">      </td>
        <td align="left" style="font-size: 12pt; color: #0000ff; font-family: Verdana" width="60">
            <span style="color: #0000ff">&nbsp;</span></td>
    </tr>
    <tr style="font-weight: bold; font-size: 12pt; color: #0000ff; font-family: Verdana;
        text-decoration: underline">
        <td style="width: 63px">
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/_SALeaves/LeaveEdit.aspx?"
                Width="208px">: :Leave Application:</asp:HyperLink></td>
        <td style="font-size: 12pt; font-family: Verdana" width="70">
            <asp:Label ID="lblCount" runat="server"></asp:Label></td>
        <td align="left" style="font-size: 12pt; font-family: Verdana; text-decoration: underline"
            width="60">
            <asp:Label ID="lblCount1" runat="server"></asp:Label></td>
    </tr>
    <tr style="font-size: 12pt; font-family: Verdana">
        <td align="left" class="style1" valign="top" style="width: 63px">
            <a class="smallie" href="./_SALoans/LoanApplication.aspx">::Loan Applications:</a></td>
        <td align="left" class="style2" valign="top" width="70">        </td>
        <td align="left" class="style2" valign="top" width="60">        </td>
    </tr>
    <tr>
        <td style="width: 63px">
            <span style="text-decoration: underline">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/_SAQuery/QueryEdit.aspx"
                    Width="208px">: :Query:</asp:HyperLink></span></td>
        <td width="70">
            <asp:Label ID="lblRead2" runat="server"></asp:Label></td>
        <td align="left" width="60">
            <asp:Label ID="lblUnread2" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td style="width: 63px">
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/_SAMemo/MemoEdit.aspx"
                Width="208px">::Memo Application</asp:HyperLink></td>
        <td width="70">
            <asp:Label ID="lblRead1" runat="server"></asp:Label></td>
        <td align="left" width="60">
            <asp:Label ID="lblUnread" runat="server"></asp:Label></td>
    </tr>
</table>
