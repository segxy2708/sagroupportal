<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Contacts.ascx.cs" Inherits="_SAControls_Contacts" %>
<TABLE id="Table4" 	width="75%" height="80px" border="0" bordercolor="gainsboro" cellpadding="1">
	<TR class="documentMain">
		<TD class="documentText" vAlign="top">
					<asp:datalist id="dlnews" ShowHeader="False" Width="100%" BorderStyle="None"
						CellPadding="1" BackColor="Transparent" BorderWidth="0px" BorderColor="#CCCCCC"  
						  Height="16px" runat="server" RepeatColumns="1" OnItemDataBound="dlnews_ItemDataBound">
						<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#669999"></SelectedItemStyle>
						<AlternatingItemStyle CssClass="gridh"></AlternatingItemStyle>
						<ItemStyle ForeColor="#000066" CssClass="gridh"></ItemStyle>
								<ItemTemplate> 
									<table border="0" cellpadding="2" cellspacing="2">
								<tr valign="top">
								<td><asp:Label ID="lbl" CssClass="" runat="server" Text="::"></asp:Label></td>
								<td>
									<a style="width:120px" class="smallie" href ='Contacts2.aspx?ID=<%# DataBinder.Eval(Container, "DataItem.ID") %>'>
										<%#DataBinder.Eval(Container.DataItem, "Fullname")%>
									</a>
									</td> 
									</tr>  
									</table> 
								</ItemTemplate> 
					</asp:datalist></TD>
	</TR>
	<TR>
		<TD vAlign="middle" align="Right" height="10">
			 
				<asp:hyperlink id="HyperLink1" CssClass="Normal" Font-Size="X-Small" Width="48px" runat="server" NavigateUrl="../ContactsList.aspx">more...</asp:hyperlink>
		</TD>
	</TR>
</TABLE>
