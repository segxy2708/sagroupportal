
Partial Class _SAControls_forum_more
    Inherits System.Web.UI.UserControl

    Protected Sub DataList1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DataList1.EditCommand
        Dim keys As Integer = DataList1.DataKeys(e.Item.ItemIndex).ToString()
        Session("PostID") = keys
        Response.Redirect(e.CommandArgument.ToString())
    End Sub

    Protected Sub DataList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataList1.SelectedIndexChanged

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
    End Sub
End Class
