<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Workflow.ascx.cs" Inherits="_SAControls_Workflow" %>


<table border="0" cellpadding="1" cellspacing="1" width="100%">
<tr>
        <td style="width: 60px">
            <a class="smallie" href="./_SARequisitions/Requisitions.aspx"><span style="color: Maroon;
                text-decoration: underline">::Requisitions:</span></a>        </td>
        <td align="left" width="60" style="font-size: 12pt; font-family: Verdana" >
            <asp:LinkButton ID="lnkReq" runat="server" CssClass="smallie"></asp:LinkButton></td>
        <td width="70">
            <a class="smallie" href="./_SAMedicals/Medical.aspx"><span style="color: Maroon">
                ::Medicals:</span></a>        </td>
        <td align="left" style="font-size: 12pt; font-family: Verdana" width="60">
            <asp:LinkButton ID="lnkMedicals" runat="server" CssClass="smallie"></asp:LinkButton></td>
    </tr>
    <tr style="font-size: 12pt; font-family: Verdana">
        <td style="width: 60px">
            <a class="smallie" href="./_SAMemo/Memo.aspx"><span style="width: 194px; color: Maroon;
                text-decoration: underline" id="SPAN1" language="javascript" onclick="return SPAN1_onclick()">::Memo:</span></a><span style="color: Maroon;
                    text-decoration: underline"> </span>        </td>
        <td align="left" style="font-size: 12pt; color: Maroon; font-family: Verdana;
            text-decoration: underline" width="60">
        <!--    <asp:LinkButton ID="lnkCustomer" runat="server" CssClass="smallie"></asp:LinkButton></td> -->
        <td style="font-size: 12pt; font-family: Verdana" width="70">
           <a class="smallie" href="./_SAEmployees/Training.aspx"><span style="color: Maroon;
                text-decoration: underline">::Traning:</span></a></td>
        <td align="left" style="font-size: 12pt; font-family: Verdana" width="60">
           <asp:LinkButton ID="lnkTraining" runat="server" CssClass="smallie" Width="103px"></asp:LinkButton></td>
    </tr>
    <tr style="font-weight: bold; font-size: 12pt; color: Maroon; font-family: Verdana;
        text-decoration: underline">
        <td style="width: 130px">
            <a class="smallie" href="./_SALeaves/Leave.aspx?uid=<%= Session["userid"].ToString() %>"><span style="color: Maroon">
                ::Leave Applications:</span></a>        </td>
        <td align="left" style="font-size: 12pt; font-family: Verdana" width="60">
            <asp:LinkButton ID="lnkLeave" runat="server" CssClass="smallie"></asp:LinkButton></td>
        <td style="font-size: 12pt; font-family: Verdana" width="70">            </td>
        <td align="left" style="font-size: 12pt; font-family: Verdana; text-decoration: underline"
            width="60">            </td>
    </tr>
    <tr style="font-size: 12pt; font-family: Verdana">
        <td align="left" style="width: 60px" valign="top">
            <a class="smallie" href="./_SALoans/Loans.aspx?uid=<%= Session["userid"].ToString() %>">::Loan Applications:</a>        </td>
        <td align="left" valign="top" width="60">
            <asp:LinkButton ID="lnkLoan" runat="server" CssClass="smallie"></asp:LinkButton></td>
        <td align="left" valign="top" width="70">
        <a style="visibility:hidden;"  class="smallie" href="#">::Call Memo:</a>        </td>
        <td align="left" valign="top" width="60">
            <asp:LinkButton ID="lnkMemo" runat="server" CssClass="smallie"></asp:LinkButton></td>
    </tr>
    <tr>
        <td style="width: 60px">
            <asp:Label ID="Label1" runat="server" Width="75px"></asp:Label></td>
        <td align="left" width="60">
            <asp:Label ID="Label3" runat="server" Width="60px"></asp:Label></td>
        <td width="70">
            <asp:Label ID="Label2" runat="server" Width="80px"></asp:Label></td>
        <td align="left" width="60">
            <asp:Label ID="Label4" runat="server" Width="69px"></asp:Label></td>
    </tr>
    <tr>
      <td height="10" colspan="4" align="left" valign="top"></td>
    </tr>
</table>
