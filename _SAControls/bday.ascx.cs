using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class _SAControls_bday : System.Web.UI.UserControl
{
    protected System.Data.SqlClient.SqlConnection myConn;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            sort();
        }

        sort();
    }
   
    public void sort()
{
    //string sort = dpsort.SelectedItem.Value;

    try
    {
        myConn.Open();
    }
    catch (Exception se)
    {
        Response.Write(se.Message);
    }
    System.Data.SqlClient.SqlCommand myCmd = new System.Data.SqlClient.SqlCommand("spS_bdaytoday", myConn);
    myCmd.CommandType = CommandType.StoredProcedure;

    SqlDataAdapter xx = new SqlDataAdapter(myCmd);
   SqlDataReader getjd = myCmd.ExecuteReader();
    if (getjd.HasRows)
    {
        bdays.Visible = true;
        bdays.DataSource = getjd;
        bdays.DataBind();
    }
    else
    {
        bdays.Visible = false;
    }
    getjd.Close();
    myConn.Close();
}

       override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.Configuration.AppSettingsReader configurationAppSettings = new System.Configuration.AppSettingsReader();
        this.myConn = new System.Data.SqlClient.SqlConnection();
        //this.appcon.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.appcon_ItemCommand);
        //this.todayn.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.todayn_ItemCommand);

        // 
        // myConn
        // 
        //this.btnsch.Click += new EventHandler(btnsch_Click);
        this.myConn.ConnectionString = ((string)(configurationAppSettings.GetValue("ConnectionString2", typeof(string))));

    }

 
   
}
