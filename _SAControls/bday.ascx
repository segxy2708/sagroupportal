<%@ Control Language="C#" AutoEventWireup="true" CodeFile="bday.ascx.cs" Inherits="_SAControls_bday" %>
<link href="../css/Default.css" rel="stylesheet" type="text/css" />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top" style="height: 13px">
        <strong>Today</strong></td>
  </tr>
  <tr>
    <td align="left" valign="top"><asp:datagrid id="bdays" runat="server" CssClass="welcometxtnew" AllowSorting="False" AutoGenerateColumns="False"
																			Width="100%" CellPadding="2" CellSpacing="1" BorderWidth="0px" BorderStyle="None"  Visible="True" PageSize="100">
                  <AlternatingItemStyle BackColor="WhiteSmoke" BorderColor="#80FF80" />
                  <HeaderStyle CssClass="grid" />
                  <Columns>
                     <asp:BoundColumn Visible="False" DataField="id" HeaderText="id"></asp:BoundColumn>
					<asp:BoundColumn DataField="fullname" HeaderText="Full Name"></asp:BoundColumn>
	                <asp:ButtonColumn Text="View Details" CommandName="details"></asp:ButtonColumn>
                  </Columns>
              </asp:DataGrid>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="height: 13px"><strong>Bday This Month</strong></td>
  </tr>
  <tr>
    <td align="left" valign="top"></td>
  </tr>
</table>
