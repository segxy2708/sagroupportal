using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _SAControls_Staff : System.Web.UI.UserControl
{
    IssuesUtilities Util = new IssuesUtilities(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable tbl = new DataTable(); 
        tbl = Util.LoadThisMember(Session["userid"].ToString());
        tbl.Columns.Add("picture");  
        string res = "";
        res = tbl.Rows[0]["picloc"].ToString();
        //Response.Write(Server.MapPath(res)); 
        //tbl.Rows[0]["picture"] = Server.MapPath(res);
        tbl.Rows[0]["picture"] = res.Substring(res.LastIndexOf(@"/")+1);
        //Response.Write(tbl.Rows[0]["picture"].ToString());
        rpt.DataSource = tbl;
        rpt.DataBind(); 
    }


    protected void rpt_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
