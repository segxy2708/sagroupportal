Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Partial Class _SAControls_forum_details
    Inherits System.Web.UI.UserControl
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT comments FROM forum_comment WHERE (postid = @postid)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@postid"
        parm.Value = Session("PostID")
        cmd.Parameters.Add(parm)
        Dim i As Object = cmd.ExecuteScalar()
        'lblTitles.Text = i
        Session("Main") = i
        'Response.Write(i)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim i As New DirectoryTableAdapters.forum_commentTableAdapter
        Dim coms As New DirectoryTableAdapters.forumTableAdapter
        i.InsertForumComment(Now.Date, Session("PostID"), Session("names"), txtComment.Text, "0", "~/" & Session("myImageUrl"))
        coms.UpdateForumComment(txtComment.Text, Session("PostID"))
        lblPost.Text = "Comment posted successfully"
        Response.AppendHeader("Refresh", "5; URL=forum_detail.aspx")
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Response.Redirect("sag_home.aspx")
    End Sub
End Class
