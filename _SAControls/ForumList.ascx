<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ForumList.ascx.vb" Inherits="_SAControls_ForumList" %>
<script language="javascript">

    function Comp() {
        window.open("Update_News.aspx", "Check_Mail", "left=400,top=200,height=400,width=600,scrollbars=yes,status=no,resizeable=yes");
    }
    function forum() {
        window.open("Forum_Info.aspx", "Forum_Details", "left=150,top=100,height=300,width=600,scrollbars=yes,status=no,resizeable=yes");
    }

</script>

<table style="width: 401px; font-size: 8pt; font-family: Tahoma;">
    <tr>
        <td colspan="3">
            &nbsp;<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="javascript:onclick =forum()">Add New</asp:HyperLink></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: right">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="postid" DataSourceID="SqlDataSource1" Width="468px">
                <ItemTemplate>
                    <table style="width: 414px">
                        <tr>
                            <td colspan="2" style="width: 189px; text-align: left">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="forum_detail.aspx"
                                    CommandName="Edit" Font-Names="Tahoma" Font-Size="8pt" Text='<%# Eval("posttitle") %>'></asp:LinkButton></td>
                            <td colspan="2" style="width: 270px">
                                <span style="font-size: 10pt; font-family: Tahoma">
                                Posted By:</span><asp:Label ID="postbyLabel" runat="server" Text='<%# Eval("postby") %>' Font-Names="Tahoma" Font-Size="10pt"></asp:Label><span
                                        style="font-size: 10pt; font-family: Tahoma"></span></td>
                        </tr>
                        <tr>
                            <td style="width: 122px; text-align: left">
                                <strong><span style="font-size: 7pt">Latest Comments:</span></strong></td>
                            <td colspan="3">
                                <asp:Label id="lblComments2" runat="server" Text='<%# Eval("comments2", "{0}") %>' Font-Size="XX-Small" Font-Names="Tahoma" ForeColor="#CC0000"></asp:Label></td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>"
                SelectCommand="SELECT TOP (5) postid, postdate, postby, posttitle, comments, status, comments2 FROM forum ORDER BY postid DESC"></asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td style="height: 15px">
        </td>
        <td style="width: 289px; height: 15px;">
            <asp:Label ID="Label1" runat="server"></asp:Label></td>
        <td style="width: 227px; text-align: right; height: 15px;">
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Forum_more.aspx">More Topic...</asp:HyperLink></td>
    </tr>
</table>
