
Partial Class _SAControls_Forum_Add
    Inherits System.Web.UI.UserControl

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim i As New DirectoryTableAdapters.forumTableAdapter

        i.InsertForumInfo(Now.Date, Session("names"), txtTitle.Text, txtComment.Text, "0", "No comment Yet", "~/" & Session("myImageUrl"))
        lblPost.Text = "Comment posted successfully"
        HyperLink1.Visible = True
        Panel1.Visible = False
        'Response.AppendHeader("Refresh", "1; URL=default44.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
        HyperLink1.Visible = False
    End Sub
End Class
