<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Calendar.ascx.vb" Inherits="_SAControls_Calendar" %>
<table id="Table4" border="0" bordercolor="gainsboro" width="100%" height="170px" cellpadding="3">
    <tr class="documentMain">
        <td align="center" valign="top" class="documentText">
      <asp:Calendar ID="Calendar1" runat="server" BackColor="Azure" BorderColor="SeaGreen"
                BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt"
                ForeColor="#663399" Height="170px" ShowGridLines="True" Width="98%">
                <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />
                <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />
                <SelectorStyle BackColor="#FFCC66" />
                <OtherMonthDayStyle ForeColor="#CC9966" />
                <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />
                <DayHeaderStyle BackColor="PaleGreen" Font-Bold="True" Height="1px" />
                <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC" />
            </asp:Calendar>        </td>
  </tr>
    <tr>
      <td align="right" height="8" valign="middle"></td>
    </tr>
    <tr>
        <td align="right" height="10" valign="middle">
          <asp:HyperLink ID="HyperLink1" runat="server" CssClass="Normal" Font-Size="X-Small" NavigateUrl="../maincal/calendar.aspx"
                Width="48px">more...</asp:HyperLink>        </td>
  </tr>
</table>
