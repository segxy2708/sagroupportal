<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="WebUpdate.aspx.vb" Inherits="WebUpdate" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
<tr>
<td style="height: 24px">
</td>
<td style="height: 75px" valign="top">
    <asp:Label ID="Label34" runat="server" Text="Please Select Kind Of Operation"></asp:Label>
</td>
<td style="width: 229px; height: 24px" valign="top" >
    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
        <asp:ListItem>- Select -</asp:ListItem>
        <asp:ListItem>Add New Content</asp:ListItem>
        <asp:ListItem>Change  to Existing Content</asp:ListItem>
        <asp:ListItem>File(s) upload for Clients' download</asp:ListItem>
    </asp:DropDownList>
</td>
<td style="height: 24px">
</td>
</tr>
<tr>
<td>

</td>
<td>
<asp:Label runat ="server" ID ="errMess" Text =""></asp:Label>
</td>
</tr>
</table>
    <asp:Panel ID="Panel1" runat="server" Height="100%" Width="100%" ScrollBars="Auto">
    <table >
<tr>
<td>

</td>
<td colspan="3" rowspan="1" align="center" valign="top">
    <asp:Label ID="Label1" runat="server" Text="Personal Information"></asp:Label>
</td>
<td>

</td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 324px">

</td>
<td>

</td>
<td>

</td>
</tr>


<tr>
<td style="height: 16px">

</td>
<td style="height: 50px; width: 324px;" align="left" valign="middle">
    <asp:Label ID="Label2" runat="server" Text="Subsidiary:"></asp:Label></td>
<td style="height: 16px" align="center" valign="middle">
    <asp:TextBox ID="SubText1" runat="server" Width="300px" ReadOnly="True"></asp:TextBox>
</td>
<td style="height: 16px;">

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 324px; height: 50px" align="left" valign="middle">
    <asp:Label ID="Label3" runat="server" Text="Your Name:"></asp:Label></td>
<td align="center" valign="middle">
    <asp:TextBox ID="NameTxt1" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="height: 50px; width: 324px;" align="left" valign="middle">
    <asp:Label ID="Label4" runat="server" Text="Mobile No:"></asp:Label></td>
<td align="center" valign="middle">
    <asp:TextBox ID="MobileTxt1" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="height: 50px; width: 324px;">
    <asp:Label ID="Label5" runat="server" Text="Your E-mail:"></asp:Label></td>
<td>
    <asp:TextBox ID="EmailTxt1" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td style="height: 16px">

</td>
<td style="width: 324px; height: 16px;" align="center" colspan="3">
    <asp:Label ID="Label6" runat="server" Text="Change Information"></asp:Label></td>
<td align="center" valign="top" style="height: 16px">
    </td>
<td style="height: 16px">

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 324px">

</td>
<td>

</td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 324px">
    <asp:Label ID="Label7" runat="server" Text="Expected Text/Context: (copy and paste especially from Microsoft Word)"></asp:Label></td>
<td>
    <asp:TextBox ID="ExpextedText1" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 324px">
    <asp:Label ID="Label8" runat="server" Text="The Caption for the above text/content is:"></asp:Label></td>
<td>
    <asp:TextBox ID="CaptionTxt1" runat="server" Width="300px"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 324px">
    <asp:Label ID="Label9" runat="server" Text="Under Which Menu are you expecting the above displayed"></asp:Label></td>
<td>
    <asp:TextBox ID="menuTxt1" runat="server" Width="300px"></asp:TextBox></td>
<td>

</td>
</tr>

<tr>
<td style="height: 42px">

</td>
<td style="height: 42px; width: 324px;">
    <asp:Label ID="Label10" runat="server" Text="Do you want it on a new page:"></asp:Label></td>
<td style="height: 42px">
    <asp:RadioButtonList ID="AnswerList1" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem>Yes</asp:ListItem>
        <asp:ListItem>No</asp:ListItem>
    </asp:RadioButtonList></td>
<td style="height: 42px">

</td>
</tr>

<tr>
<td>

</td>
<td style="width: 324px">
    <asp:Label ID="Label11" runat="server" Text="If above is no, which page should it be appended to"></asp:Label></td>
<td>
    <asp:TextBox ID="AppendTxt" runat="server" Width="300px"></asp:TextBox></td>
<td>

</td>
</tr>
<tr>
<td style ="height :50px;">
</td>
<td>
</td>
<td></td>
</tr>
<tr>
<td></td>
<td align="right">
    <asp:Button ID="SubmitBut1" runat="server" Text="Submit" /></td>
<td></td>
</tr>
</table>
    
    </asp:Panel>
    
    <asp:Panel ID="Panel2" runat="server" Height="100%" Width="100%" ScrollBars="Auto">
    <table >
<tr>
<td>

</td>
<td colspan="3" rowspan="1" align="center" valign="top">
    <asp:Label ID="Label12" runat="server" Text="Personal Information"></asp:Label>
</td>
<td>

</td>
<td>

</td>
</tr>


<tr>
<td style="height: 16px">

</td>
<td style="height: 16px">

</td>
<td style="height: 16px">

</td>
<td style="height: 16px">

</td>
</tr>


<tr>
<td style="height: 16px">

</td>
<td style="height: 50px; width: 324px;" align="left" valign="middle">
    <asp:Label ID="Label13" runat="server" Text="Subsidiary:"></asp:Label></td>
<td style="height: 16px" align="center" valign="middle">
    <asp:TextBox ID="SubText2" runat="server" Width="300px" ReadOnly="True"></asp:TextBox>
</td>
<td style="height: 16px;">

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 100px; height: 50px" align="left" valign="middle">
    <asp:Label ID="Label14" runat="server" Text="Your Name:"></asp:Label></td>
<td align="center" valign="middle">
    <asp:TextBox ID="NameTxt2" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="height: 50px" align="left" valign="middle">
    <asp:Label ID="Label15" runat="server" Text="Mobile No:"></asp:Label></td>
<td align="center" valign="middle">
    <asp:TextBox ID="MobileTxt2" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="height: 50px">
    <asp:Label ID="Label16" runat="server" Text="Your E-mail:"></asp:Label></td>
<td>
    <asp:TextBox ID="emailTxt2" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td align="center" colspan="3">
    <asp:Label ID="Label17" runat="server" Text="Change Information"></asp:Label></td>
<td align="center" valign="top" colspan="" rowspan="">
    </td>
<td>

</td>
</tr>


<tr>
<td style="height: 62px">

</td>
<td style="height: 62px">

</td>
<td style="height: 62px">

</td>
<td style="height: 62px">

</td>
</tr>


<tr>
<td style="height: 103px">

</td>
<td style="width: 100px; height: 103px;">
    <asp:Label ID="Label21" runat="server" Text="Does the needed change occur in more than a page"></asp:Label></td>
<td style="height: 103px">
    <asp:RadioButtonList ID="AnswerList2" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem>Yes</asp:ListItem>
        <asp:ListItem>No</asp:ListItem>
    </asp:RadioButtonList></td>
<td style="height: 103px">

</td>
</tr>


<tr>
<td>

</td>
<td>
    &nbsp;<asp:Label ID="Label22" runat="server" Text="If above is no, which page should it be appended to"></asp:Label></td>
<td>
    <asp:TextBox ID="AppendTxt2" runat="server" Width="300px"></asp:TextBox>
</td>
<td>

</td>
</tr>


<tr>
<td style="height: 24px">

</td>
<td style="height: 24px">
    &nbsp;<asp:Label ID="Label18" runat="server" Text="Current Text/Content: (copy and paste especially from Microsoft Word)"></asp:Label></td>
<td style="height: 24px">
    &nbsp;<asp:TextBox ID="CurrentTxt2" runat="server" Width="300px" Height="100px" TextMode="MultiLine"></asp:TextBox></td>
<td style="height: 24px">

</td>
</tr>

<tr>
<td style="height: 42px">

</td>
<td style="height: 42px">
    &nbsp;<asp:Label ID="Label19" runat="server" Text="Expected Text/Content:"></asp:Label></td>
<td style="height: 42px">
    <asp:TextBox ID="ExpectedTxt2" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
<td style="height: 42px">

</td>
</tr>

<tr>
<td>

</td>
<td>
    </td>
<td>
    </td>
<td>

</td>
</tr>
<tr>
<td style ="height :50px;">
</td>
<td>
</td>
<td></td>
</tr>
<tr>
<td></td>
<td>
    <asp:Button ID="SubmitBut2" runat="server" Text="Submit" /></td>
<td></td>
</tr>

</table>
    
    </asp:Panel>
    
    <asp:Panel ID="Panel3" runat="server" Height="100%" Width="100%" ScrollBars="Auto">
    <table >
<tr>
<td style="height: 16px">

</td>
<td colspan="3" rowspan="1" align="center" valign="top" style="height: 16px">
    <asp:Label ID="Label23" runat="server" Text="Personal Information"></asp:Label>
</td>
<td style="height: 16px">

</td>
<td style="height: 16px">

</td>
</tr>


<tr>
<td>

</td>
<td>

</td>
<td>

</td>
<td>

</td>
</tr>


<tr>
<td style="height: 16px">

</td>
<td style="height: 50px; width: 324px;" align="left" valign="middle">
    <asp:Label ID="Label24" runat="server" Text="Subsidiary:"></asp:Label></td>
<td style="height: 16px" align="center" valign="middle">
    <asp:TextBox ID="SubTxt3" runat="server" Width="300px" ReadOnly="True"></asp:TextBox>
</td>
<td style="height: 16px;">

</td>
</tr>


<tr>
<td>

</td>
<td style="width: 100px; height: 50px" align="left" valign="middle">
    <asp:Label ID="Label25" runat="server" Text="Your Name:"></asp:Label></td>
<td align="center" valign="middle">
    <asp:TextBox ID="NameTxt3" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="height: 50px" align="left" valign="middle">
    <asp:Label ID="Label26" runat="server" Text="Mobile No:"></asp:Label></td>
<td align="center" valign="middle">
    <asp:TextBox ID="MobileTxt3" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td style="height: 50px">
    <asp:Label ID="Label27" runat="server" Text="Your E-mail:"></asp:Label></td>
<td align="center" valign="middle">
    <asp:TextBox ID="emailTxt3" runat="server" Width="300px" ReadOnly="True"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td align="center" colspan="3">
    <asp:Label ID="Label28" runat="server" Text="Change Information"></asp:Label></td>
<td align="center" valign="top">
    </td>
<td>

</td>
</tr>


<tr>
<td style="height: 16px">

</td>
<td style="height: 16px">

</td>
<td style="height: 16px">

</td>
<td style="height: 16px">

</td>
</tr>


<tr>
<td style="height: 16px">

</td>
<td style="width: 100px; height: 16px;">
    <asp:Label ID="Label20" runat="server" Text="Attachment"></asp:Label></td>
<td style="height: 16px">
    <asp:FileUpload ID="FileUpload1" runat="server" />
    <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ControlToValidate="FileUpload1"
        Display="Dynamic" EnableClientScript="False" ErrorMessage="Upload Jpegs,bitmaps,PNG and Gifs only."
        ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.png|.PNG|.bmp|.BMP|.jpeg|.JPEG|.txt|.TXT|.doc|.DOC|.xls|XLS|.xlst|.XLST|.xpt|.XPT|.htm|.html|.HTML|.HTM)$">*</asp:RegularExpressionValidator></td>
<td style="height: 16px">

</td>
</tr>


<tr>
<td>

</td>
<td>
    <asp:Label ID="Label30" runat="server" Text="The Caption for the above text/content is:"></asp:Label></td>
<td>
    <asp:TextBox ID="CaptionTxt3" runat="server" Width="300px"></asp:TextBox></td>
<td>

</td>
</tr>


<tr>
<td>

</td>
<td>
    <asp:Label ID="Label31" runat="server" Text="Under Which Menu are you expecting the above displayed"></asp:Label></td>
<td>
    <asp:TextBox ID="ExpectedTxt3" runat="server" Width="300px"></asp:TextBox></td>
<td>

</td>
</tr>

<tr>
<td style="height: 42px">

</td>
<td style="height: 42px">
    <asp:Label ID="Label32" runat="server" Text="Do you want it on a new page:"></asp:Label></td>
<td style="height: 42px">
    <asp:RadioButtonList ID="AnswerList3" runat="server" RepeatDirection="Horizontal">
        <asp:ListItem>Yes</asp:ListItem>
        <asp:ListItem>No</asp:ListItem>
    </asp:RadioButtonList></td>
<td style="height: 42px">

</td>
</tr>

<tr>
<td>

</td>
<td>
    <asp:Label ID="Label33" runat="server" Text="If above is no, which page should it be appended to"></asp:Label></td>
<td>
    <asp:TextBox ID="AppendTxt3" runat="server" Width="300px"></asp:TextBox></td>
<td>

</td>
</tr>
<tr>
<td style="height :110px;">
</td>
<td style="height: 110px">
    <asp:Label ID="Label29" runat="server" Text="Brief Information on the attached"></asp:Label></td>
<td style="height: 110px">
    <asp:TextBox ID="BriefIntro" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
</tr>
<tr>
<td></td>
<td>
    <asp:Button ID="SubmitBut3" runat="server" Text="Submit" /></td>
<td></td>
</tr>

</table>
    
    </asp:Panel>
    <asp:SqlDataSource ID="NewItemSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT * FROM [SuggestionTab]" InsertCommand="INSERT INTO SuggestionTab(Subsidiary, Name, MobileNo, email, Action, Caption, PageName, DisplayMenu, CurrentText, ExpectedText, BriefInfo, UploadURL) VALUES (@Subsidiary, @Name, @MobileNo, @email, @Action, @Caption, @PageName, @DisplayMenu, @CurrentText, @ExpectedText, @BriefInfo, @UploadURL)">
        <InsertParameters>
            <asp:ControlParameter ControlID="SubText1" Name="Subsidiary" PropertyName="Text" />
            <asp:ControlParameter ControlID="NameTxt1" Name="Name" PropertyName="Text" />
            <asp:ControlParameter ControlID="MobileTxt1" Name="MobileNo" PropertyName="Text" />
            <asp:ControlParameter ControlID="EmailTxt1" Name="email" PropertyName="Text" />
            <asp:ControlParameter ControlID="AnswerList1" Name="Action" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="CaptionTxt1" Name="Caption" PropertyName="Text" />
            <asp:ControlParameter ControlID="menuTxt1" Name="PageName" PropertyName="Text" />
            <asp:ControlParameter ControlID="AppendTxt" Name="DisplayMenu" PropertyName="Text" />
            <asp:Parameter Name="CurrentText" />
            <asp:ControlParameter ControlID="ExpextedText1" Name="ExpectedText" PropertyName="Text" />
            <asp:Parameter Name="BriefInfo" />
            <asp:Parameter Name="UploadURL" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="ExistingItemSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" SelectCommand="SELECT * FROM [SuggestionTab]" InsertCommand="INSERT INTO SuggestionTab(Subsidiary, Name, MobileNo, email, Action, Caption, PageName, DisplayMenu, CurrentText, ExpectedText, BriefInfo, UploadURL) VALUES (@Subsidiary, @Name, @MobileNo, @email, @Action, @Caption, @PageName, @DisplayMenu, @CurrentText, @ExpectedText, @BriefInfo, @UploadURL)">
        <InsertParameters>
            <asp:ControlParameter ControlID="SubText2" Name="Subsidiary" PropertyName="Text" />
            <asp:ControlParameter ControlID="NameTxt2" Name="Name" PropertyName="Text" />
            <asp:ControlParameter ControlID="MobileTxt2" Name="MobileNo" PropertyName="Text" />
            <asp:ControlParameter ControlID="emailTxt2" Name="email" PropertyName="Text" />
            <asp:ControlParameter ControlID="AnswerList2" Name="Action" PropertyName="SelectedValue" />
            <asp:Parameter Name="Caption" />
            <asp:ControlParameter ControlID="AppendTxt2" Name="PageName" PropertyName="Text" />
            <asp:Parameter Name="DisplayMenu" />
            <asp:ControlParameter ControlID="CurrentTxt2" Name="CurrentText" PropertyName="Text" />
            <asp:ControlParameter ControlID="ExpectedTxt2" Name="ExpectedText" PropertyName="Text" />
            <asp:Parameter Name="BriefInfo" />
            <asp:Parameter Name="UploadURL" />
        </InsertParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="FileUploadSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" SelectCommand="SELECT * FROM [SuggestionTab]" InsertCommand="INSERT INTO SuggestionTab(Subsidiary, Name, MobileNo, email, Action, Caption, PageName, DisplayMenu, CurrentText, ExpectedText, BriefInfo, UploadURL) VALUES (@Subsidiary, @Name, @MobileNo, @email, @Action, @Caption, @PageName, @DisplayMenu, @CurrentText, @ExpectedText, @BriefInfo, @UploadURL)">
        <InsertParameters>
            <asp:ControlParameter ControlID="SubTxt3" Name="Subsidiary" PropertyName="Text" />
            <asp:ControlParameter ControlID="NameTxt3" Name="Name" PropertyName="Text" />
            <asp:ControlParameter ControlID="MobileTxt3" Name="MobileNo" PropertyName="Text" />
            <asp:ControlParameter ControlID="emailTxt3" Name="email" PropertyName="Text" />
            <asp:ControlParameter ControlID="AnswerList3" Name="Action" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="CaptionTxt3" Name="Caption" PropertyName="Text" />
            <asp:ControlParameter ControlID="AppendTxt3" Name="PageName" PropertyName="Text" />
            <asp:Parameter Name="DisplayMenu" />
            <asp:Parameter Name="CurrentText" />
            <asp:ControlParameter ControlID="ExpectedTxt3" Name="ExpectedText" PropertyName="Text" />
            <asp:ControlParameter ControlID="BriefIntro" Name="BriefInfo" PropertyName="Text" />
            <asp:Parameter Name="UploadURL" />
        </InsertParameters>
    </asp:SqlDataSource>
    
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

