﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="comments.ascx.vb" Inherits="comments" %>
<table>
    <tr>
        <td>
        </td>
        <td colspan="2">
            <asp:TextBox ID="user_tx" runat="server" style="margin-bottom: 0px" 
                TextMode="MultiLine"></asp:TextBox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td align="right">
            <asp:SqlDataSource ID="wallpost_src" runat="server" 
                ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" 
                DeleteCommand="DELETE FROM [users_wall] WHERE [id] = @original_id AND [username] = @original_username AND [statement] = @original_statement AND (([every] = @original_every) OR ([every] IS NULL AND @original_every IS NULL)) AND [date] = @original_date AND [imageurl] = @original_imageurl AND (([comment] = @original_comment) OR ([comment] IS NULL AND @original_comment IS NULL)) AND (([comment_on] = @original_comment_on) OR ([comment_on] IS NULL AND @original_comment_on IS NULL))" 
                InsertCommand="INSERT INTO [users_wall] ([username], [statement], [every], [date], [imageurl], [comment], [comment_on]) VALUES (@username, @statement, @every, @date, @imageurl, @comment, @comment_on)" 
                OldValuesParameterFormatString="original_{0}" 
                SelectCommand="SELECT * FROM [users_wall]" 
                UpdateCommand="UPDATE [users_wall] SET [username] = @username, [statement] = @statement, [every] = @every, [date] = @date, [imageurl] = @imageurl, [comment] = @comment, [comment_on] = @comment_on WHERE [id] = @original_id AND [username] = @original_username AND [statement] = @original_statement AND (([every] = @original_every) OR ([every] IS NULL AND @original_every IS NULL)) AND [date] = @original_date AND [imageurl] = @original_imageurl AND (([comment] = @original_comment) OR ([comment] IS NULL AND @original_comment IS NULL)) AND (([comment_on] = @original_comment_on) OR ([comment_on] IS NULL AND @original_comment_on IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="original_id" Type="Int32" />
                    <asp:Parameter Name="original_username" Type="String" />
                    <asp:Parameter Name="original_statement" Type="String" />
                    <asp:Parameter Name="original_every" Type="Int32" />
                    <asp:Parameter Name="original_date" Type="DateTime" />
                    <asp:Parameter Name="original_imageurl" Type="String" />
                    <asp:Parameter Name="original_comment" Type="String" />
                    <asp:Parameter Name="original_comment_on" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:SessionParameter Name="username" SessionField="names" Type="String" />
                    <asp:ControlParameter ControlID="user_tx" Name="statement" PropertyName="Text" 
                        Type="String" />
                    <asp:Parameter Name="every" Type="Int32" />
                    <asp:SessionParameter Name="date" SessionField="date" Type="DateTime" />
                    <asp:SessionParameter Name="imageurl" SessionField="myImageUrl" Type="String" />
                    <asp:Parameter DefaultValue="yes" Name="comment" Type="String" />
                    <asp:SessionParameter DefaultValue="" Name="comment_on" 
                        SessionField="commenton" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="username" Type="String" />
                    <asp:Parameter Name="statement" Type="String" />
                    <asp:Parameter Name="every" Type="Int32" />
                    <asp:Parameter Name="date" Type="DateTime" />
                    <asp:Parameter Name="imageurl" Type="String" />
                    <asp:Parameter Name="comment" Type="String" />
                    <asp:Parameter Name="comment_on" Type="String" />
                    <asp:Parameter Name="original_id" Type="Int32" />
                    <asp:Parameter Name="original_username" Type="String" />
                    <asp:Parameter Name="original_statement" Type="String" />
                    <asp:Parameter Name="original_every" Type="Int32" />
                    <asp:Parameter Name="original_date" Type="DateTime" />
                    <asp:Parameter Name="original_imageurl" Type="String" />
                    <asp:Parameter Name="original_comment" Type="String" />
                    <asp:Parameter Name="original_comment_on" Type="String" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </td>
        <td>
            <asp:Button ID="Button1" runat="server" Text="Comment" />
        </td>
    </tr>
</table>

