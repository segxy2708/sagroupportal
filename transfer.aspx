<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="transfer.aspx.vb" Inherits="transfer" title="File Transfer Page" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div >
<div>
<b>&nbsp;Upload File</b>
    <asp:FileUpload ID="FileUpload" runat="server" Width="333px" />
     <%--<AjaxToolKit:AsyncFileUpload ID="FileUpload" Width="400px" runat="server" 
        OnClientUploadError="uploadError" 
        OnClientUploadStarted="StartUpload"
        OnClientUploadComplete="UploadComplete"
        CompleteBackColor="Lime" UploaderStyle="Modern" 
        ErrorBackColor="Red" 
        ThrobberID="Throbber" 
        UploadingBackColor="#66CCFF"
        onUploadedComplete="FileUpload_UploadedComplete" />
        
        <asp:Label ID="Throbber" runat="server" Style="display: none">
            <img src="indicator.gif" align="absmiddle" alt="loading" />
        </asp:Label>
        <br />
        <br />
        <asp:Label ID="lblStatus" runat="server" Style="font-family: Arial; font-size: small;"></asp:Label>
--%>
    <asp:Button ID="Upload" runat="server" Text="Upload" Width="70px" />
    <ajaxToolkit:ConfirmButtonExtender ID="confbut" runat ="server" TargetControlID ="Upload" ConfirmOnFormSubmit ="false" ConfirmText ="Click OK to Submit or Cancel to quit Operation"></ajaxToolkit:ConfirmButtonExtender>
    <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ControlToValidate="FileUpload"
        Display="Dynamic" ErrorMessage="Upload Jpegs,bitmaps,PNG and Gifs only." 
        ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.png|.PNG|.bmp|.BMP|.jpeg|.JPEG|.txt|.TXT|.doc|.DOC|.docx|.DOCX|.pdf|.PDF|.xls|XLS|.xlst|.XLST|.rar|RAR|.xpt|.XPT|.htm|.html|.HTML|.HTM)$">*</asp:RegularExpressionValidator><br />
    &nbsp;<asp:Label ID="retMessage" runat="server"></asp:Label>
    <asp:SqlDataSource ID="StoreUploadDetails" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        InsertCommand="INSERT INTO User_File_Details([User], Location, FileType, FileSize, DateCreated, TimeCreated, FileName, FileLoc, Sender) VALUES ( @User , @Location, @FileType, @FileSize, @DateCreated, @TimeCreated, @FileName, @FileLoc, N'Myself')"
        SelectCommand="SELECT * FROM [User_File_Details]">
    </asp:SqlDataSource>
        
</div>
</div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

