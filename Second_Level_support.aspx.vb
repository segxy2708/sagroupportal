Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Partial Class Second_Level_support
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
        Me.Title = "Second Level Support"
        Me.TicketNumText.Text = Session("tickNo")
        Me.DateTimeTxt.Text = Session("getDatetime")
        Me.Detailtxt.Text = Session("PersonalDetails")
        Me.AffectedAssetTxt.Text = Session("AffectedAsset")
        Me.Symptomstxt.Text = Session("symptoms")
        Me.CreatorTxt.Text = Session("tickCreator")
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            If Me.ResolutionText.Text Is Nothing Or Me.ResolutionText.Text = "" Then
                Me.ResolutionText.Focus()
            Else
                Me.complainDetails.Update()
                Me.errorLab.Text = "Escalation Successful"
                Me.errorLab.Focus()
            End If
            
        Catch ex As Exception
            Me.errorLab.Text = ex.Message
            Me.errorLab.Focus()
        End Try
    End Sub
End Class
