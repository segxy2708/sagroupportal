<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="file.aspx.vb" Inherits="file" title ="File Sharing Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table >
    <tr>
        <td colspan="3" style="text-align: center">
    <asp:Label ID="errmsg" runat="server" Text="Label" BackColor="#FFFFC0" BorderColor="#C04000" BorderStyle="Solid" BorderWidth="1px"></asp:Label></td>
    </tr>
<tr>
<td style="width: 125px; height: 77px">
</td>
<td style="width: 407px; height: 77px;">
File to Share:
    <asp:DropDownList ID="FileList" runat="server" DataSourceID="File2Upload" DataTextField="FileName" DataValueField="FileName" AppendDataBoundItems="True">
    <asp:ListItem>- Please Select File -</asp:ListItem>
    </asp:DropDownList>&nbsp;
    <asp:SqlDataSource ID="File2Upload" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT FileName FROM User_File_Details WHERE ([User] = @User ) ORDER BY FileName">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="names" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
</td>
<td style="width: 220px; height: 77px;">
<asp:Button ID="Submit" runat ="server" Text ="Submit" />
<ajaxToolkit:ConfirmButtonExtender ID="confbut" runat ="server" TargetControlID ="Submit" ConfirmOnFormSubmit ="false" ConfirmText ="Click OK to Submit or Cancel to quit Operation"></ajaxToolkit:ConfirmButtonExtender>
</td>
</tr>
<tr>
<td style="width: 50px; height: 22px;">
    <asp:Label ID="use" runat="server" Visible="False"></asp:Label></td>
<td style="width: 407px; height: 22px;">
    &nbsp;Share With:<br />
    <br />
    <asp:checkBox ID="SFriend" runat="server" Text="Colleague" AutoPostBack="True" Font-Bold="True" />&nbsp;
        <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" Text="My Group(s)" Font-Bold="True" /></td>
<td style="width: 220px; height: 22px">
</td>
</tr>
<tr>
<td style="width: 50px; height: 50px;">
    </td>
<td style="width: 407px; height: 50px;" align="left" valign="top">
    <asp:Panel ID="Friends_Panel" runat="server" Height="150px" ScrollBars="Auto"
        Visible="False" Width="300px" BorderColor="Green" BorderStyle="Solid" BorderWidth="1px">
    <asp:CheckBoxList ID="FriendsList" runat="server" DataSourceID="Friends" DataTextField="fullname" DataValueField="fullname" Visible="False" RepeatLayout="Flow">
    </asp:CheckBoxList><asp:SqlDataSource ID="Friends" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT [fullname] FROM [directory]"></asp:SqlDataSource>
    </asp:Panel>
        <asp:Panel ID="crtGrpPan" runat="server" Height="150px" Visible="False" Width="300px" ScrollBars="Auto" EnableTheming="True">
                <asp:CheckBoxList ID="memberGrpList" runat="server" DataSourceID="Friends" DataTextField="fullname"
                    DataValueField="fullname" EnableViewState="False">
                </asp:CheckBoxList><br />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp;
                <asp:SqlDataSource ID="createGrpSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    InsertCommand="INSERT INTO GrpCreation(Creator, GroupName, members) VALUES (@Creator, @GroupName, @members)"
                    SelectCommand="SELECT * FROM [GrpCreation]">
                </asp:SqlDataSource>
            </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" Height="150px" Visible="False" ScrollBars="Auto" Width="300px" BorderColor="Green" BorderStyle="Solid" BorderWidth="1px">
            <asp:RadioButtonList AutoPostBack="True" ID="selGrpList" runat="server" EnableViewState="False">
                <asp:ListItem>New Group</asp:ListItem>
                <asp:ListItem>Existing Group</asp:ListItem>
            </asp:RadioButtonList><br />
            
            
            <br />
            <asp:Panel ID="selGrpPan" runat="server" Height="140px" Visible="False" Width="300px" ScrollBars="Auto">
                <asp:Label ID="Label2" runat="server" Text="Select Group"></asp:Label>&nbsp;
                <asp:DropDownList ID="MyPGrp" runat="server" DataSourceID="SelGrp" DataTextField="GroupName"
                    DataValueField="GroupName" AutoPostBack="True">
                </asp:DropDownList><br />
                <asp:SqlDataSource ID="SelGrp" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    SelectCommand="SELECT DISTINCT [GroupName] FROM [GrpCreation] WHERE ([Creator] = @Creator)">
                    <SelectParameters>
                        <asp:SessionParameter Name="Creator" SessionField="names" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:Label ID="Label4" runat="server" Text="Members List:"></asp:Label>
                <asp:CheckBoxList ID="grpMembers" runat="server" DataSourceID="GrpMember" DataTextField="members"
                    DataValueField="members">
                </asp:CheckBoxList><asp:SqlDataSource ID="GrpMember" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    SelectCommand="SELECT [members] FROM [GrpCreation] WHERE ([GroupName] = @GroupName)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="MyPGrp" Name="GroupName" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
            </asp:Panel>
            <br />
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT DISTINCT [location] FROM [directory]"></asp:SqlDataSource>
        </asp:Panel>
    
    <br />
    &nbsp;
    
    
    </td>
    <td align="left" valign="top" style="width: 220px; height: 50px">
                <asp:Label ID="Label1" runat="server" Text="Group Name" Font-Bold="True" Visible="False"></asp:Label><asp:TextBox ID="NewGrpTxt" runat="server" EnableTheming="False" Visible="False"></asp:TextBox>
        <asp:Button ID="newGrpBut" runat="server" Text="Create" Visible="False" /><br />
        <asp:ListBox ID="ListBox1" runat="server" EnableViewState="False" Visible="False"></asp:ListBox><asp:SqlDataSource ID="StoreUploadDetails" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        InsertCommand="INSERT INTO User_File_Details([User], Location, FileType, FileSize, DateCreated, TimeCreated, FileName, FileLoc, Sender) VALUES ( @User , @Location, @FileType, @FileSize, @DateCreated, @TimeCreated, @FileName, @FileLoc, @Sender)"
        SelectCommand="SELECT * FROM [User_File_Details]">
        <InsertParameters>
            <asp:ControlParameter ControlID="FileList" Name="FileName" PropertyName="SelectedValue" />
        </InsertParameters>
    </asp:SqlDataSource>
        </td>

</tr>
    <tr>
        <td style="width: 50px; height: 22px">
        </td>
        <td style="width: 407px; height: 22px">
<asp:CheckBox ID="Department" runat="server" AutoPostBack="True" Text ="Departments" Font-Bold="True"/>&nbsp;</td>
        <td style="height: 22px; width: 220px;">
        </td>
    </tr>
    <tr>
        <td style="width: 50px; height: 51px;">
        </td>
        <td align="left" style="width: 407px; height: 51px;">
        <asp:Panel ID="Departments_Panel" runat="server" Height="150px" Visible="False" ScrollBars="Auto" Width="300px" BorderColor="Green" BorderStyle="Solid" BorderWidth="1px">
    
    <asp:CheckBoxList ID="DepartmentsList" runat="server" DataSourceID="Departments" DataTextField="Department" DataValueField="Department" Visible="False">
    </asp:CheckBoxList><asp:SqlDataSource ID="Departments" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT DISTINCT [Department] FROM [directory]"></asp:SqlDataSource>
        </asp:Panel>
        </td>
        <td style="width: 220px; height: 51px">
        </td>
    </tr>
    <tr>
        <td style="width: 50px; height: 22px">
        </td>
        <td style="width: 407px; height: 22px">
        <asp:CheckBox ID="Group" runat="server" AutoPostBack="True" Text ="Subsidiary" Font-Bold="True" /></td>
        <td style="height: 22px; width: 220px;">
        </td>
    </tr>
    <tr>
        <td style="width: 50px; height: 51px;">
        </td>
        <td align="left" style="width: 407px; height: 51px;">
        <asp:Panel ID="Groups_Panel" runat="server" Height="150px" Visible="False" ScrollBars="Auto" Width="300px" BorderColor="Green" BorderStyle="Solid" BorderWidth="1px">
    <asp:CheckBoxList ID="GroupList" runat="server" DataSourceID="Groups" DataTextField="location" DataValueField="location" Visible="False">
    </asp:CheckBoxList><asp:SqlDataSource ID="Groups" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT DISTINCT [location] FROM [directory]"></asp:SqlDataSource>
        </asp:Panel>
        </td>
        <td valign="bottom" style="width: 220px">
</td>
    </tr>
    <tr>
    <td>
    </td>
    <td>
        </td>
    <td style="width: 220px">
    </td>
    </tr>
    <tr>
    <td>
    </td>
    <td>
        &nbsp;
    </td>
    <td style="width: 220px">
    </td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

