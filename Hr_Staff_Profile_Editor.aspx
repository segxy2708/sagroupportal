<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="Hr_Staff_Profile_Editor.aspx.vb" Inherits="Hr_Staff_Profile_Editor" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                        <asp:Label ID="LblHeading" runat="server" CssClass="SectionName" ForeColor="DimGray"></asp:Label>
                                                                                                       <asp:Label ID="Label10" runat="server" CssClass="admintextbig2" Width="451px">Search 
                                                                    Criterial</asp:Label>
                                        
                                <asp:Panel ID="Panel2" runat="server">
                                </asp:Panel>
    <br />
    <table style="width: 879px">
        <tr>
            <td style="width: 141px">
                            
                                                            <asp:Label ID="lblError" runat="server"></asp:Label></td>
            <td style="width: 142px">
            </td>
            <td style="width: 120px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">
                &nbsp;<asp:Label ID="Label11" runat="server" CssClass="admintextbold">Date:</asp:Label>
                                                                        <asp:Label ID="lblApplicationDate" runat="server"></asp:Label></td>
            <td style="width: 142px; text-align: right">
                Company:</td>
            <td style="width: 120px">
                <asp:Label ID="lblCompany" runat="server"></asp:Label></td>
            <td style="text-align: left" colspan="2">
                                                                        <asp:Label ID="Label5" runat="server" CssClass="admintextbold">Department:</asp:Label>
                                                                        <asp:Label ID="lblDepartment" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                    
                                                                        <asp:Label ID="Label1" runat="server" CssClass="admintextbold">Department:</asp:Label></td>
            <td style="width: 142px">
                                                                    
                                                                        Staff:</td>
            <td style="width: 120px">
                &nbsp;</td>
            <td>
                                                                   
                                                                        &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                    
                                                                        <asp:DropDownList ID="drpDepart" runat="server" AutoPostBack="True" 
                                                                            onselectedindexchanged="drpDepart_SelectedIndexChanged">
                                                                            <asp:ListItem>Engineering</asp:ListItem>
                                                                            <asp:ListItem>Admin</asp:ListItem>
                                                                            <asp:ListItem>Marketing</asp:ListItem>
                                                                            <asp:ListItem>Human Resources</asp:ListItem>
                                                                            <asp:ListItem>Finance</asp:ListItem>
                                                                            <asp:ListItem>Operations</asp:ListItem>
                                                                            <asp:ListItem>Information Technology</asp:ListItem>
                                                                            <asp:ListItem>Customer Care</asp:ListItem>
                                                                            <asp:ListItem>Human Capital</asp:ListItem>
                                                                            <asp:ListItem>Legal/Company Secretary</asp:ListItem>
                                                                            <asp:ListItem>EXECUTIVES</asp:ListItem>
                                                                            <asp:ListItem>Technical</asp:ListItem>
                                                                            <asp:ListItem>Investment</asp:ListItem>
                                                                            <asp:ListItem>Internal Control/Audit</asp:ListItem>
                                                                            <asp:ListItem>Research</asp:ListItem>
                                                                            <asp:ListItem>Stockbroking</asp:ListItem>
                                                                            <asp:ListItem>Asset Management</asp:ListItem>
                                                                            <asp:ListItem>Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Deputy Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Financial Advisory</asp:ListItem>
                                                                            <asp:ListItem>Leveraged Finance</asp:ListItem>
                                                                        </asp:DropDownList></td>
            <td style="width: 142px">
                                                                    
                                                                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
                                                                            DataSourceID="SqlDataSource1" DataTextField="fullname" DataValueField="ID" 
                                                                            onselectedindexchanged="DropDownList1_SelectedIndexChanged" 
                                                                            style="width: 87px" Width="1px">
                                                                        </asp:DropDownList>
                                                                    
            </td>
            <td style="width: 120px">
                                                                    
                                                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                                                            ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>" 
                                                                            SelectCommand="SELECT * FROM [directory] WHERE (([location] = @location) AND ([Department] = @Department))">
                                                                            <SelectParameters>
                                                                                <asp:SessionParameter Name="location" SessionField="Group" Type="String" />
                                                                                <asp:ControlParameter ControlID="drpDepart" Name="Department" 
                                                                                    PropertyName="SelectedValue" Type="String" />
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
            </td>
            <td>
                                                                   
                                                                        &nbsp;</td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                    
                                                                   
                                                                        <asp:Label ID="Label2" runat="server" CssClass="admintextbold" 
                                                                            style="font-weight: 700">Staff Details</asp:Label></td>
            <td style="width: 142px">
            </td>
            <td style="width: 120px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                   
                                                                        Department:</td>
            <td style="width: 142px">
                                                                    
                                                                     Level:</td>
            <td style="width: 120px">
                &nbsp;</td>
            <td>
                Supervisor:</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                     &nbsp;<asp:DropDownList ID="drpDepart1" runat="server" 
                                                                            onselectedindexchanged="drpDepart_SelectedIndexChanged" AutoPostBack="True">
                                                                            <asp:ListItem>Engineering</asp:ListItem>
                                                                            <asp:ListItem>Admin</asp:ListItem>
                                                                            <asp:ListItem>Marketing</asp:ListItem>
                                                                            <asp:ListItem>Human Resources</asp:ListItem>
                                                                            <asp:ListItem>Finance</asp:ListItem>
                                                                            <asp:ListItem>Operations</asp:ListItem>
                                                                            <asp:ListItem>Information Technology</asp:ListItem>
                                                                            <asp:ListItem>Customer Care</asp:ListItem>
                                                                            <asp:ListItem>Human Capital</asp:ListItem>
                                                                            <asp:ListItem>Legal/Company Secretary</asp:ListItem>
                                                                            <asp:ListItem>EXECUTIVES</asp:ListItem>
                                                                            <asp:ListItem>Technical</asp:ListItem>
                                                                            <asp:ListItem>Investment</asp:ListItem>
                                                                            <asp:ListItem>Internal Control/Audit</asp:ListItem>
                                                                            <asp:ListItem>Research</asp:ListItem>
                                                                            <asp:ListItem>Stockbroking</asp:ListItem>
                                                                            <asp:ListItem>Asset Management</asp:ListItem>
                                                                            <asp:ListItem>Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Deputy Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Financial Advisory</asp:ListItem>
                                                                            <asp:ListItem>Leveraged Finance</asp:ListItem>
                                                                        </asp:DropDownList></td>
            <td style="width: 142px">
                                                                    
                                                                        <asp:DropDownList ID="DropDownList3" runat="server" 
                                                                        DataSourceID="SqlDataSource2" DataTextField="description" 
                                                                        DataValueField="description">
                                                                        </asp:DropDownList>
            </td>
            <td style="width: 120px">
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                                                        ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>" 
                                                                        
                                                                        
                                                                        
                                                                            SelectCommand="SELECT ID, phone, fax, city, state, Address1, Address2, zipcode, email, Department, location, building, last, first, MI, picloc, office, Timestamp, title, background, empnum, listname, uname, pw, admin, surveyallow, emailpass, emailuser, remotemail, D11, address3, sharecal, cellphone, pager, country, supervisor, calaccess, projectview, lastlogin, visits, hppopup, status, flash, filelimit, timezone, getmail, emailpass2, emailuser2, remotemail2, emailpass3, emailuser3, remotemail3, fullname, background2, background3, spam, blogcomments, profilename, remotemailtype, remotemail2type, remotemail3type, blogdate, blogtitle, staff_level FROM directory WHERE (location = @location) AND (Department = @Department)">
                                                                            <SelectParameters>
                                                                                <asp:SessionParameter Name="location" SessionField="company" Type="String" />
                                                                                <asp:ControlParameter ControlID="drpDepart1" Name="Department" 
                                                                                PropertyName="SelectedValue" Type="String" />
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
            </td>
            <td>
                &nbsp;<asp:DropDownList ID="DropDownList2" runat="server" 
                                                                        DataSourceID="SqlDataSource3" DataTextField="fullname" DataValueField="ID">
                                                                        </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                     &nbsp;&nbsp; Employed Date:<asp:TextBox ID="txtEmpDate" runat="server"></asp:TextBox>
                                                                        <ajaxToolkit:CalendarExtender ID="txtEmpDate_CalendarExtender" 
                                                                         runat="server" Enabled="True" TargetControlID="txtEmpDate" PopupButtonID="txtEmpDate">
                                                                     </ajaxToolkit:CalendarExtender>
                                                                        </td>
            <td style="width: 142px">
                                                                    
                                                                        &nbsp;</td>
            <td style="width: 120px">
            &nbsp;Designation:</td>
            <td>
                                                                    
                                                                        <asp:TextBox ID="txtDesignation" runat="server"></asp:TextBox></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                     &nbsp;</td>
            <td style="width: 142px">
                                                                    
                                                                        &nbsp;</td>
            <td style="width: 120px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 141px">
                                                                     &nbsp;</td>
            <td style="width: 142px">
                                                                    
                                                                        &nbsp;</td>
            <td style="width: 120px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 141px">
                &nbsp;<b>Remuneration Details</b></td>
            <td style="width: 142px">
            </td>
            <td style="width: 120px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">
                &nbsp;Annual Basic Salary:
                                                                    
                                                                        <asp:TextBox ID="txtAnnualBasic" runat="server"></asp:TextBox></td>
            <td style="width: 142px">
                &nbsp;Annual Gross Salary:
                                                                    
                                                                        <asp:TextBox ID="txtAnnualGross" runat="server"></asp:TextBox></td>
            <td style="width: 120px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">
                &nbsp;Monthly Basic Salary:
                                                                    
                                                                        <asp:TextBox ID="txtMonthlyBasic" runat="server"></asp:TextBox></td>
            <td style="width: 142px">
                &nbsp;Monthly Gross Salary:
                                                                        <asp:TextBox ID="txtMonthlyGross" runat="server"></asp:TextBox></td>
            <td style="width: 120px">
            </td>
            <td>
                                                                    
                                                                        <asp:Button ID="Button1" runat="server" Text="Update" onclick="Button1_Click" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 141px">
            </td>
            <td style="width: 142px">
            </td>
            <td style="width: 120px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                                                        ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>" 
                                                                        SelectCommand="SELECT [description] FROM [staff_level]"></asp:SqlDataSource>
                                                                    &nbsp;<asp:Label ID="lblMessage" runat="server"></asp:Label>
                                 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

