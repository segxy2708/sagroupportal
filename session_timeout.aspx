<%@ Page Language="VB" AutoEventWireup="false" CodeFile="session_timeout.aspx.vb" Inherits="session_timeout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Session Timeout</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            Session timeout due to the error from your last request, you have been logged out.<br />
            click
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/">here</asp:HyperLink>
            to log in.<br />
            <br />
            N.B: All your previous unsaved data have been lost.<br />
            <br />
            <br />
            <asp:Label ID="Label1" runat="server"></asp:Label></div>
    
    </div>
    </form>
</body>
</html>
