'Imports System.Windows.Forms
Imports System
Imports System.IO
Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Web.Mail
Partial Class file
    Inherits System.Web.UI.Page

    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter

    Protected Sub Friend_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles SFriend.CheckedChanged
        If Me.SFriend.Checked Then
            Me.Friends_Panel.Visible = True
            Me.FriendsList.Visible = True
            'Me.Panel1.Visible = False
            'Me.CheckBox1.Checked = False
            Me.crtGrpPan.Visible = False


            Me.Label1.Visible = False
            Me.NewGrpTxt.Visible = False
            Me.newGrpBut.Visible = False
        Else
            Me.Friends_Panel.Visible = False
            Me.FriendsList.Visible = False
        End If

    End Sub

    Protected Sub Group_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Group.CheckedChanged
        If Me.Group.Checked Then
            Me.Groups_Panel.Visible = True
            Me.GroupList.Visible = True

            
        Else
            Me.GroupList.Visible = False
            Me.Groups_Panel.Visible = False
        End If

    End Sub

    Protected Sub Department_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Department.CheckedChanged
        If Me.Department.Checked Then
            Me.DepartmentsList.Visible = True
            Me.Departments_Panel.Visible = True

        Else
            Me.DepartmentsList.Visible = False
            Me.Departments_Panel.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.errmsg.Visible = False
        
       
    End Sub

   

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

   
    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        use.Text = Session("names")

    End Sub

    Protected Function selectedItems(ByVal s As CheckBoxList) As String
        Dim MyItems As String = "rhe"
        Try

            Dim counter As Integer = 0

            For counter = 0 To s.Items.Count - 1
                If s.Items(counter).Selected Then
                    MyItems = s.Items(counter).Text & vbCrLf
                    'items = vbCrLf & items
                End If

            Next
            '    Label1.Text = items
            'Response.Write(selectedItems(Me.DepartmentsList))

        Catch ex As Exception

        End Try
        Return myItems
    End Function

    Protected Sub DepartmentsList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DepartmentsList.SelectedIndexChanged
        'Dim s As String = Me.DepartmentsList.SelectedValue
        'Me.Submit.Text = s
       
    End Sub
    Protected Sub newGrpBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newGrpBut.Click
        'Me.createGrpSource.Insert()
        Try
            'grpMembers(Me.memberGrpList)
            test(Me.memberGrpList)
        Catch ex As Exception
            Me.Title = ex.Message
        End Try

    End Sub


    Protected Sub Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Submit.Click
        If Me.FileList.SelectedIndex = 0 Then
            Me.errmsg.Text = "You have not selected any file to share"
            Me.errmsg.Visible = True
            Me.errmsg.ForeColor = Drawing.Color.Red
        Else

            Try
                If Not Me.NewGrpTxt.Text Is Nothing Then
                    userCHKs(Me.FriendsList, Me.NewGrpTxt.Text)
                ElseIf Not Me.MyPGrp.SelectedValue Is Nothing Then
                    userCHKs(Me.FriendsList, Me.MyPGrp.SelectedValue)
                Else
                    userCHK(Me.FriendsList)
                End If

            Catch ex As Exception
                Me.Title = ex.Message
            End Try


            'Me.createGrpSource.Insert()
            Try
                'grpMembers(Me.memberGrpList)
                test(Me.FriendsList)
            Catch ex As Exception
                Me.Title = ex.Message
            End Try
            Try
                'chk(Me.FriendsList, ListBox1)
                'userCHK(Me.FriendsList)
            Catch ex As Exception
                Me.Title = ex.Message
            End Try
            Try
                'chk(Me.DepartmentsList, ListBox1)
                userCHK(Me.DepartmentsList)
            Catch ex As Exception

            End Try
            Try
                'chk(Me.GroupList, ListBox1)
                userCHK(Me.GroupList)
            Catch ex As Exception

            End Try

            Me.errmsg.Text = "You have successfully shared " & Me.FileList.SelectedValue
            Me.errmsg.Visible = True
            Me.errmsg.ForeColor = Drawing.Color.Blue
            Response.AppendHeader("Refresh", "5; URL=file.aspx")
        End If

        ' Dim sb As StringBuilder
        

    End Sub

    Sub chk(ByVal ch As CheckBoxList, ByVal list As ListBox)
        Dim sb As StringBuilder = New StringBuilder

        Dim locs As String = getFileInfo("Location")
        Dim typ As String = getFileInfo("FileType")
        Dim siz As String = getFileInfo("FileSize")
        Dim flocs As String = getFileInfo("FileLoc")
        Dim dats As String = DateTime.Now.Date.ToString
        Dim tims As String = DateTime.Now.TimeOfDay.ToString
        'Dim fname As String = Me.FileList.SelectedValue
        Dim sende As String = Session("names")

        Dim parLocs As New System.Web.UI.WebControls.Parameter("Location", TypeCode.String, locs)
        Dim parTyp As New System.Web.UI.WebControls.Parameter("FileType", TypeCode.String, typ)
        Dim parSiz As New System.Web.UI.WebControls.Parameter("FileSize", TypeCode.String, siz)
        Dim parFlocs As New System.Web.UI.WebControls.Parameter("FileLoc", TypeCode.String, flocs)
        Dim parDats As New System.Web.UI.WebControls.Parameter("DateCreated", TypeCode.String, dats)
        Dim parTims As New System.Web.UI.WebControls.Parameter("TimeCreated", TypeCode.String, tims)
        Dim fileSender As New System.Web.UI.WebControls.Parameter("Sender", TypeCode.String, sende)


        For Each i As ListItem In ch.Items
            If i.Selected = True Then
                Dim useN As String = i.Text
                Dim parUseN As New System.Web.UI.WebControls.Parameter("User", TypeCode.String, useN)


                Me.StoreUploadDetails.InsertParameters.Add(parLocs)
                Me.StoreUploadDetails.InsertParameters.Add(parTyp)
                Me.StoreUploadDetails.InsertParameters.Add(parSiz)
                Me.StoreUploadDetails.InsertParameters.Add(parFlocs)
                Me.StoreUploadDetails.InsertParameters.Add(parDats)
                Me.StoreUploadDetails.InsertParameters.Add(parTims)
                Me.StoreUploadDetails.InsertParameters.Add(parUseN)
                Me.StoreUploadDetails.InsertParameters.Add(fileSender)
                Me.StoreUploadDetails.Insert()


                list.Items.Add(i.Text)
                sb.Append(i.Text)
                sb.Append(" ")
            End If
        Next
        'System.Windows.Forms.MessageBox.Show(sb.ToString, "message", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Information, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly, False)
    End Sub

    Sub userCHK(ByVal chks As CheckBoxList)
        Dim locs As String = getFileInfo("Location")
        Dim typ As String = getFileInfo("FileType")
        Dim siz As String = getFileInfo("FileSize")
        Dim flocs As String = getFileInfo("FileLoc")
        Dim dats As String = DateTime.Now.Date.ToString
        Dim tims As String = DateTime.Now.TimeOfDay.ToString

        Dim sende As String = Session("names")

        Try
            Dim query As String = ""

            For i As Integer = 0 To chks.Items.Count - 1
                If chks.Items(i).Selected Then
                    query += String.Format("INSERT INTO User_File_Details([User], Location, FileType, FileSize, DateCreated, TimeCreated, FileName, FileLoc, Sender) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", chks.Items(i).Value, locs, typ, siz, dats, tims, Me.FileList.SelectedValue, flocs, sende) ' ( @User , @Location, @FileType, @FileSize, @DateCreated, @TimeCreated, @FileName, @FileLoc, @Sender)")
                End If
            Next i

            Dim ConnSQL As SqlConnection = New SqlConnection(ConnectionString1)
            ConnSQL.Open()
            Dim myCommand As SqlCommand = New SqlCommand(query, ConnSQL)
            ' perform all the inserts at once
            myCommand.ExecuteNonQuery()
            myCommand = Nothing
            ConnSQL.Close()

        Catch ex As Exception

        End Try
    End Sub
    Sub userCHKs(ByVal chks As CheckBoxList, ByVal namegroup As String)
        Dim locs As String = getFileInfo("Location")
        Dim typ As String = getFileInfo("FileType")
        Dim siz As String = getFileInfo("FileSize")
        Dim flocs As String = getFileInfo("FileLoc")
        Dim dats As String = DateTime.Now.Date.ToString
        Dim tims As String = DateTime.Now.TimeOfDay.ToString
        Dim sende As String = Session("names")
        Try
            Dim query As String = ""

            For i As Integer = 0 To chks.Items.Count - 1
                If chks.Items(i).Selected Then
                    query += String.Format("INSERT INTO User_File_Details([User], Location, FileType, FileSize, DateCreated, TimeCreated, FileName, FileLoc, Sender, GroupName) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", chks.Items(i).Value, locs, typ, siz, dats, tims, Me.FileList.SelectedValue, flocs, sende, namegroup) ' ( @User , @Location, @FileType, @FileSize, @DateCreated, @TimeCreated, @FileName, @FileLoc, @Sender)")
                End If
            Next i

            Dim ConnSQL As SqlConnection = New SqlConnection(ConnectionString1)
            ConnSQL.Open()
            Dim myCommand As SqlCommand = New SqlCommand(query, ConnSQL)
            ' perform all the inserts at once
            myCommand.ExecuteNonQuery()
            myCommand = Nothing
            ConnSQL.Close()

        Catch ex As Exception

        End Try
    End Sub



    Private Function getFileInfo(ByVal str As String) As String
        Dim strs As String = Me.FileList.SelectedValue
        Dim query As String = "Select [" + str + "] from User_File_Details WHERE FileName='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString1)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Sub grpMembe(ByVal check As CheckBoxList)
        Dim sources As New SqlDataSource(ConnectionString1, "Select* from [GrpCreation]")

        For Each i As ListItem In check.Items
            If i.Selected = True Then
                Dim p As New String(i.Text)
                Dim name As String = i.Text
                'sources.InsertCommand = "INSERT INTO GrpCreation(Creator, GroupName, members) VALUES (" + Session("names") + "," + Me.NewGrpTxt.Text + "," + name + ")"
                sources.InsertCommand = "INSERT INTO GrpCreation(Creator, GroupName, members) VALUES (@Creator, @GroupName, @members)"


                Dim CrName As New System.Web.UI.WebControls.Parameter("Creator", TypeCode.String, Session("names"))
                Dim GrName As New System.Web.UI.WebControls.Parameter("GroupName", TypeCode.String, Me.NewGrpTxt.Text)
                Dim nameParam As New System.Web.UI.WebControls.Parameter("members", TypeCode.String, name)
                'Me.createGrpSource.InsertParameters.Add(nameParam)
                'Me.createGrpSource.InsertParameters.Add(CrName)
                'Me.createGrpSource.InsertParameters.Add(GrName)
                'Me.createGrpSource.Insert()
                sources.InsertParameters.Add(nameParam)
                sources.InsertParameters.Add(CrName)
                sources.InsertParameters.Add(GrName)

                sources.Insert()
            End If

        Next i
    End Sub

    Sub test(ByVal check As CheckBoxList)
        ' Dim sources As New SqlDataSource(ConnectionString1, "Select* from [GrpCreation]")
        Dim query As String = ""
        Dim myname As String = Session("names")
        For i As Integer = 0 To check.Items.Count - 1
            If check.Items(i).Selected Then
                query += String.Format("INSERT INTO GrpCreation(Creator, GroupName, members) VALUES ('{0}','{1}','{2}')", myname, Me.NewGrpTxt.Text, check.Items(i).Value)
                sendmail(Me.getemailaddr(check.Items(i).Value))
            End If
        Next i

        Dim ConnSQL As SqlConnection = New SqlConnection(ConnectionString1)
        ConnSQL.Open()
        Dim myCommand As SqlCommand = New SqlCommand(query, ConnSQL)
        ' perform all the inserts at once
        myCommand.ExecuteNonQuery()
        myCommand = Nothing
        ConnSQL.Close()
    End Sub


    Protected Sub selGrpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles selGrpList.SelectedIndexChanged
        If Me.selGrpList.SelectedValue = Nothing Then

        ElseIf Me.selGrpList.SelectedValue = "New Group" Then
            ' Me.crtGrpPan.Visible = True
            Me.selGrpPan.Visible = False
            Me.Label1.Visible = True
            Me.NewGrpTxt.Visible = True
            'Me.newGrpBut.Visible = True
        ElseIf Me.selGrpList.SelectedValue = "Existing Group" Then
            Me.crtGrpPan.Visible = False
            Me.selGrpPan.Visible = True
            Me.Label1.Visible = False
            Me.NewGrpTxt.Visible = False
            Me.newGrpBut.Visible = False
        End If
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.Checked Then
            Me.Panel1.Visible = True
            'Me.Friends_Panel.Visible = False
            'Me.SFriend.Checked = False

            Me.Label1.Visible = False
            Me.NewGrpTxt.Visible = False
            Me.newGrpBut.Visible = False
        Else
            Me.Panel1.Visible = False
        End If
    End Sub

    Public Function getemailaddr(ByVal name As String)
        Dim str As String
        Dim query As String = "Select email from directory where uname='" + name + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Sub sendmail(ByVal name As String)
        'Dim pass As String = passwordRtr()
        'Dim addr As String = 
        Dim mymail As MailMessage = New MailMessage()
        mymail.Subject = "File Sharing"
        mymail.To = name  ' addr
        'mymail.Cc = Session("usermail") 'addr '"ooduwole@sagroupng.com"
        mymail.From = Session("email") '"support@sagroupng.com"
        Dim s As String = "A file had just be shared with you by " & Session("names")
        mymail.BodyFormat = MailFormat.Text
        mymail.Body = s
        SmtpMail.SmtpServer = "localhost"
        SmtpMail.Send(mymail)

    End Sub

    
End Class
