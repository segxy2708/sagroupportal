Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.IO
Partial Class sag_home
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Button1_ModalPopupExtender.Show()
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
        'If (Not Me.IsPostBack) Then
        '    Dim i As Integer = 1
        '    For Each part As WebPart In WebPartManager1.WebParts
        '        If TypeOf part Is GenericWebPart Then
        '            part.Title = String.Format("SA Group Module. {0}", i)
        '            i += 1
        '        End If
        '    Next
        'End If

        For Each part As WebPart In WebPartManager1.WebParts
            If part.Controls.Contains(News1) Then
                part.AllowMinimize = True
            End If
        Next

        Dim ps As GenericWebPart = CType(Calendar1.Parent, GenericWebPart)
        ps.Title = "SA Group Calendar"
        ps.TitleUrl = ""
        Dim info As GenericWebPart = CType(News1.Parent, GenericWebPart)
        info.Title = "World Outlook"
        info.TitleUrl = ""
        Dim forums As GenericWebPart = CType(ForumList1.Parent, GenericWebPart)
        forums.Title = "SA Group Forum"
        forums.TitleUrl = ""
        Dim Notepad As GenericWebPart = CType(Notepad1.Parent, GenericWebPart)
        Notepad.Title = "Staff NotePad"
        Notepad.TitleUrl = ""
        Dim Due As GenericWebPart = CType(Panel1.Parent, GenericWebPart)
        Due.Title = "Over Due Appointment"
        Due.TitleUrl = ""

        Dim aNew As GenericWebPart = CType(ImageButton1.Parent, GenericWebPart)
        aNew.Title = "aNew"
        aNew.TitleUrl = ""
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Session("dates") = Calendar1.SelectedDate.Date()
        'Label1.Text = Calendar1.SelectedDate
        Response.Redirect("schedule.aspx")
    End Sub

    

    Protected Sub DataList1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DataList1.EditCommand
        Dim keys As Integer = DataList1.DataKeys(e.Item.ItemIndex).ToString()
        'Session("I") = keys
        Dim i As New PortalsTableAdapters.schedulesTableAdapter
        i.DeleteSchedule(CInt(keys))
        'Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        'Dim cmd As SqlCommand = myconnection.CreateCommand()
        'cmd.CommandType = CommandType.Text
        'cmd.CommandText = "SELECT id FROM directory WHERE (fullname like @fullname)"
        'myconnection.Open()
        'Dim parm As SqlParameter = cmd.CreateParameter()
        'parm.ParameterName = "@fullname"
        'parm.Value = "%" & Trim(DropDownList2.Text) & "%"
        'cmd.Parameters.Add(parm)
        'Dim i As Object = cmd.ExecuteScalar()
        Response.Redirect(e.CommandArgument.ToString())
        'Response.AppendHeader("Refresh", "5; URL=sag_home.aspx")
    End Sub

    
    Protected Sub DataList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataList1.SelectedIndexChanged

    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub
End Class
