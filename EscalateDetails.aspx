<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="EscalateDetails.aspx.vb" Inherits="EscalateDetails" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table >
<tr>
<td style="width: 75px; height: 16px">

</td>
<td>

</td>
<td>

</td>
</tr>
<tr>
<td style="width: 75px; height: 16px">

</td>
<td>
<asp:DataList id="details" runat="server" DataSourceID="complainDetails" DataKeyField="TicketNo">
        <ItemTemplate>
            Ticket No:
            <asp:Label ID="TicketNoLabel" runat="server" Text='<%# Eval("TicketNo") %>'></asp:Label><br  />
            Date and Time:
            <asp:Label ID="DateTimeLabel" runat="server" Text='<%# Eval("DateTime") %>'></asp:Label><br  />
            Users Attempts:
            <asp:Label ID="PersonalDetailsLabel" runat="server" Text='<%# Eval("PersonalDetails") %>'>
                </asp:Label><br  />
            Affected Hardware:
            <asp:Label ID="AffectedAssetLabel" runat="server" Text='<%# Eval("AffectedAsset") %>'>
                </asp:Label><br  />
            Problem Descriptions:
            <asp:Label ID="SymptomsLabel" runat="server" Text='<%# Eval("Symptoms") %>'></asp:Label><br  />
            Attempted Solution:
            <asp:Label ID="attendLab" runat ="server" Text ='<%# Eval("StepsTaken") %>'></asp:Label>
            <br  />
        </ItemTemplate>
    </asp:DataList>
</td>
<td>

</td>
</tr>

<tr>
<td style="height: 16px; width: 75px;">

</td>
<td style="height: 16px">
    <asp:SqlDataSource ID="complainDetails" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT TicketNo, DateTime, PersonalDetails, AffectedAsset, Symptoms, StepsTaken FROM Job_Title_Table WHERE (TicketNo = @TicketNo)"
        UpdateCommand="UPDATE Job_Title_Table SET Treated = @Treated, DateTreated = @DateTreated, Attendance = @Attendance, Month = @Month WHERE (TicketNo = @TicketNo)">
        <SelectParameters>
            <asp:QueryStringParameter Name="TicketNo" QueryStringField="ticketNo" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="TicketNo" QueryStringField="ticketNo" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Escalation.aspx">Back</asp:HyperLink>
    &nbsp;
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Resolution.aspx">Resolution</asp:HyperLink></td>
<td style="height: 16px">

</td>
</tr>

</table>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

