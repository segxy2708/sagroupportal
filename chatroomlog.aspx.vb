Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Partial Class chatroomlog
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            lblUsername.Text = Session("names")
            Dim myconnection As SqlConnection = New SqlConnection(ConnectionString1)
            Dim cmd As SqlCommand = myconnection.CreateCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT status FROM chatroom WHERE (staffid = @staffid)"
            myconnection.Open()
            Dim parm As SqlParameter = cmd.CreateParameter()
            parm.ParameterName = "@staffid"
            parm.Value = Session("id")
            cmd.Parameters.Add(parm)
            Dim i As Object = cmd.ExecuteScalar()
            If i Is Nothing Then
                Me.Panel1.Visible = True
            Else
                Me.Panel1.Visible = False
                lblMessage.Text = "You will redirect shortly, please wait"
                'lblMessage.ForeColor = Drawing.Color.Blue
                Response.AppendHeader("Refresh", "5; URL=chatroom.aspx")
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    Protected Sub btnChatLog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChatLog.Click
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT id FROM Directory WHERE (uname = @uname) AND (pw=@pw)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        Dim parm1 As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@uname"
        parm1.ParameterName = "@pw"
        parm.Value = Session("usernames")
        parm1.Value = txtPassword.Text
        cmd.Parameters.Add(parm)
        cmd.Parameters.Add(parm1)
        Dim i As Object = cmd.ExecuteScalar()
        If i Is Nothing Then
            lblMessage.Text = "Invalid Password supply, pls try again."
            lblMessage.ForeColor = Drawing.Color.Red
        Else
            Dim users As New PortalsTableAdapters.chatroomTableAdapter
            Try
                users.InsertChatUser(i, Session("names"), 1, Now.Date, Now.Date)
            Catch ex As Exception
                lblMessage.Text = ex.Message
            End Try

            lblMessage.Text = "please wait, you will be redirected soon"
            lblMessage.ForeColor = Drawing.Color.White

            Response.AppendHeader("Refresh", "5; URL=chatroom.aspx")
        End If
        
    End Sub
End Class
