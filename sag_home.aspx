<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="sag_home.aspx.vb" Inherits="sag_home" title="SAGroup Portal" %>

<%@ Register Src="_SAControls/ForumList.ascx" TagName="ForumList" TagPrefix="uc7" %>

<%@ Register Src="_SAControls/forum_details.ascx" TagName="forum_details" TagPrefix="uc4" %>
<%@ Register Src="_SAControls/forum_more.ascx" TagName="forum_more" TagPrefix="uc5" %>
<%@ Register Src="_SAControls/Notepad.ascx" TagName="Notepad" TagPrefix="uc6" %>

<%@ Register Src="Controls/News.ascx" TagName="News" TagPrefix="uc3" %>

<%@ Register Src="details.ascx" TagName="details" TagPrefix="uc2" %>

<%@ Register Src="Keyboard.ascx" TagName="Keyboard" TagPrefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;<asp:WebPartManager ID="WebPartManager1" runat="server">
        <Personalization InitialScope="Shared" />
    </asp:WebPartManager>
    
    
    <br />
    <table style="width: 498px">
        <tr>
            <td style="width: 230px; height: 45px;" valign="top">
                &nbsp;</td>
            <td colspan="2" style="height: 45px;" valign="top">
                <table style="width:100%;">
                    <tr>
                        <td valign ="top">
                <asp:WebPartZone ID="WebPartZone2" runat="server" BorderColor="#CCCCCC" Font-Names="Verdana" Padding="6" Width="120px">
                    <ZoneTemplate>
                        <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="White"
                            BorderWidth="1px" Font-Names="Verdana" Font-Size="9pt"
                            ForeColor="Black" Height="217px" Width="299px" NextPrevFormat="FullMonth">
                            <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                            <TodayDayStyle BackColor="#CCCCCC" />
                            <OtherMonthDayStyle ForeColor="#999999" />
                            <NextPrevStyle Font-Size="8pt" ForeColor="#333333" Font-Bold="True" VerticalAlign="Bottom" />
                            <DayHeaderStyle Font-Bold="True" Font-Size="8pt" />
                            <TitleStyle BackColor="White" Font-Bold="True" Font-Size="12pt" ForeColor="#333399" BorderColor="Black" BorderWidth="4px" />
                        </asp:Calendar>
                    </ZoneTemplate>
                    <EmptyZoneTextStyle Font-Size="0.8em" />
                    <PartStyle Font-Size="0.8em" ForeColor="#333333" />
                    <TitleBarVerbStyle Font-Size="0.6em" Font-Underline="False" ForeColor="White" />
                    <MenuLabelHoverStyle ForeColor="#FFCC66" />
                    <MenuPopupStyle BackColor="#990000" BorderColor="#CCCCCC" BorderWidth="1px" Font-Names="Verdana"
                        Font-Size="0.6em" />
                    <MenuVerbStyle BorderColor="#990000" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                    <PartTitleStyle BackColor="#990000" Font-Bold="True" Font-Size="0.8em" ForeColor="White" />
                    <MenuVerbHoverStyle BackColor="#FFFBD6" BorderColor="#CCCCCC" BorderStyle="Solid"
                        BorderWidth="1px" ForeColor="#333333" />
                    <PartChromeStyle BackColor="#FFFBD6" BorderColor="#FFCC66" Font-Names="Verdana" ForeColor="#333333" />
                    <HeaderStyle Font-Size="0.7em" ForeColor="#CCCCCC" HorizontalAlign="Center" />
                    <MenuLabelStyle ForeColor="White" />
                </asp:WebPartZone>
                        </td>
                        <td valign="top">
                <asp:WebPartZone ID="WebPartZone6" runat="server" BorderColor="#CCCCCC" 
                                Font-Names="Verdana" Padding="6" Width="120px">
                    <EmptyZoneTextStyle Font-Size="0.8em" />
                    <PartStyle Font-Size="0.8em" ForeColor="#333333" />
                    <TitleBarVerbStyle Font-Size="0.6em" Font-Underline="False" ForeColor="White" />
                    <ZoneTemplate>
                        <asp:Panel ID="Panel1" runat="server" Width="271px">
                            <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1" 
                                DataKeyField="appid" DataMember="DefaultView">
                                <ItemTemplate>
                                    subject:
                                    <asp:Label ID="subjectLabel" runat="server" Text='<%# Eval("subject") %>' />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="sag_home.aspx" 
                                        CommandName="Edit" onclick="LinkButton1_Click">Delete</asp:LinkButton>
                                    <br />
                                    date:
                                    <asp:Label ID="dateLabel" runat="server" Text='<%# Eval("date", "{0:D}") %>' />
                                    <br />
                                    time:
                                    <asp:Label ID="timeLabel" runat="server" Text='<%# Eval("time") %>' />
                                    <br />
                                    <br />
                                </ItemTemplate>
                            </asp:DataList>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" 
                                
                                SelectCommand="SELECT [appid],[subject], [date], [time] FROM [schedules] WHERE (([staff] = @staff) AND ([date] &lt; @date))">
                                <SelectParameters>
                                    <asp:SessionParameter Name="staff" SessionField="id" Type="String" />
                                    <asp:SessionParameter Name="date" SessionField="datte" Type="DateTime" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:Panel>
                    </ZoneTemplate>
                    <MenuLabelHoverStyle ForeColor="#FFCC66" />
                    <MenuPopupStyle BackColor="#990000" BorderColor="#CCCCCC" BorderWidth="1px" Font-Names="Verdana"
                        Font-Size="0.6em" />
                    <MenuVerbStyle BorderColor="#990000" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                    <PartTitleStyle BackColor="#990000" Font-Bold="True" Font-Size="0.8em" ForeColor="White" />
                    <MenuVerbHoverStyle BackColor="#FFFBD6" BorderColor="#CCCCCC" BorderStyle="Solid"
                        BorderWidth="1px" ForeColor="#333333" />
                    <PartChromeStyle BackColor="#FFFBD6" BorderColor="#FFCC66" Font-Names="Verdana" ForeColor="#333333" />
                    <HeaderStyle Font-Size="0.7em" ForeColor="#CCCCCC" HorizontalAlign="Center" />
                    <MenuLabelStyle ForeColor="White" />
                </asp:WebPartZone>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 230px; height: 45px;" valign="top">
                &nbsp;</td>
            <td colspan="2" style="height: 45px;" valign="top">
                <asp:WebPartZone ID="WebPartZone1" runat="server" Width="771px" Height="100px" 
                    PartChromeType="TitleAndBorder" TitleBarVerbButtonType="Link" 
                    WebPartVerbRenderMode="TitleBar" BorderColor="#CCCCCC" Font-Names="Verdana" 
                    Padding="6">
                    <ZoneTemplate>
                        <uc3:News ID="News1" runat="server" />
                    </ZoneTemplate>
                    <EmptyZoneTextStyle Font-Size="0.8em" />
                    <PartStyle Font-Size="0.8em" ForeColor="#333333" />
                    <TitleBarVerbStyle Font-Size="0.6em" Font-Underline="False" ForeColor="White" />
                    <MenuLabelHoverStyle ForeColor="#FFCC66" />
                    <MenuPopupStyle BackColor="#990000" BorderColor="#CCCCCC" BorderWidth="1px" Font-Names="Verdana"
                        Font-Size="0.6em" />
                    <MenuVerbStyle BorderColor="#990000" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                    <PartTitleStyle BackColor="#990000" Font-Bold="True" Font-Size="0.8em" ForeColor="White" />
                    <MenuVerbHoverStyle BackColor="#FFFBD6" BorderColor="#CCCCCC" BorderStyle="Solid"
                        BorderWidth="1px" ForeColor="#333333" />
                    <PartChromeStyle BackColor="#FFFBD6" BorderColor="#FFCC66" Font-Names="Verdana" ForeColor="#333333" />
                    <HeaderStyle Font-Size="0.7em" ForeColor="#CCCCCC" HorizontalAlign="Center" />
                    <MenuLabelStyle ForeColor="White" />
                </asp:WebPartZone>
            </td>
        </tr>
        <tr>
            <td style="width: 230px; height: 55px" valign="top">
            </td>
            <td colspan="1" style="width: 409px; height: 55px" valign="top">
                <asp:WebPartZone ID="WebPartZone3" runat="server" Width="247px" Height="100px" PartChromeType="TitleAndBorder" TitleBarVerbButtonType="Link" WebPartVerbRenderMode="TitleBar" BorderColor="#CCCCCC" Font-Names="Verdana" Padding="6">
                    <EmptyZoneTextStyle Font-Size="0.8em" />
                    <PartStyle Font-Size="0.8em" ForeColor="#333333" />
                    <TitleBarVerbStyle Font-Size="0.6em" Font-Underline="False" ForeColor="White" />
                    <MenuLabelHoverStyle ForeColor="#FFCC66" />
                    <MenuPopupStyle BackColor="#990000" BorderColor="#CCCCCC" BorderWidth="1px" Font-Names="Verdana"
                        Font-Size="0.6em" />
                    <MenuVerbStyle BorderColor="#990000" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                    <PartTitleStyle BackColor="#990000" Font-Bold="True" Font-Size="0.8em" ForeColor="White" />
                    <ZoneTemplate>
                        <uc7:ForumList ID="ForumList1" runat="server" />
                    </ZoneTemplate>
                    <MenuVerbHoverStyle BackColor="#FFFBD6" BorderColor="#CCCCCC" BorderStyle="Solid"
                        BorderWidth="1px" ForeColor="#333333" />
                    <PartChromeStyle BackColor="#FFFBD6" BorderColor="#FFCC66" Font-Names="Verdana" ForeColor="#333333" />
                    <HeaderStyle Font-Size="0.7em" ForeColor="#CCCCCC" HorizontalAlign="Center" />
                    <MenuLabelStyle ForeColor="White" />
                </asp:WebPartZone>
            </td>
            <td style="width: 174px; height: 55px" valign="top">
                <asp:WebPartZone ID="WebPartZone5" runat="server" Width="179px" Height="100px" PartChromeType="TitleAndBorder" TitleBarVerbButtonType="Link" WebPartVerbRenderMode="TitleBar" BorderColor="#CCCCCC" Font-Names="Verdana" Padding="6">
                    <EmptyZoneTextStyle Font-Size="0.8em" />
                    <PartStyle Font-Size="0.8em" ForeColor="#333333" />
                    <TitleBarVerbStyle Font-Size="0.6em" Font-Underline="False" ForeColor="White" />
                    <MenuLabelHoverStyle ForeColor="#FFCC66" />
                    <MenuPopupStyle BackColor="#990000" BorderColor="#CCCCCC" BorderWidth="1px" Font-Names="Verdana"
                        Font-Size="0.6em" />
                    <MenuVerbStyle BorderColor="#990000" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                    <PartTitleStyle BackColor="#990000" Font-Bold="True" Font-Size="0.8em" ForeColor="White" />
                    <ZoneTemplate>
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/WebSite/images/aNew_Mag_Page_1.jpg"
                    Width="150px" 
                            OnClientClick='window.open("WebSite/SA Group Journal new FA Crv.htm", "aNew_Message","left=0,top=0,height=1000,width=1200,scrollbars=yes,status=no,resizeable=yes");' />
                    </ZoneTemplate>
                    <MenuVerbHoverStyle BackColor="#FFFBD6" BorderColor="#CCCCCC" BorderStyle="Solid"
                        BorderWidth="1px" ForeColor="#333333" />
                    <PartChromeStyle BackColor="#FFFBD6" BorderColor="#FFCC66" Font-Names="Verdana" ForeColor="#333333" />
                    <HeaderStyle Font-Size="0.7em" ForeColor="#CCCCCC" HorizontalAlign="Center" />
                    <MenuLabelStyle ForeColor="White" />
                </asp:WebPartZone>
                <asp:WebPartZone ID="WebPartZone4" runat="server" Width="179px" PartChromeType="TitleAndBorder" TitleBarVerbButtonType="Link" WebPartVerbRenderMode="TitleBar" BorderColor="#CCCCCC" Font-Names="Verdana" Padding="6" Visible="False">
                    <EmptyZoneTextStyle Font-Size="0.8em" />
                    <PartStyle Font-Size="0.8em" ForeColor="#333333" />
                    <TitleBarVerbStyle Font-Size="0.6em" Font-Underline="False" ForeColor="White" />
                    <MenuLabelHoverStyle ForeColor="#FFCC66" />
                    <MenuPopupStyle BackColor="#990000" BorderColor="#CCCCCC" BorderWidth="1px" Font-Names="Verdana"
                        Font-Size="0.6em" />
                    <MenuVerbStyle BorderColor="#990000" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                    <PartTitleStyle BackColor="#990000" Font-Bold="True" Font-Size="0.8em" ForeColor="White" />
                    <ZoneTemplate>
                        <uc6:Notepad ID="Notepad1" runat="server" />
                    </ZoneTemplate>
                    <MenuVerbHoverStyle BackColor="#FFFBD6" BorderColor="#CCCCCC" BorderStyle="Solid"
                        BorderWidth="1px" ForeColor="#333333" />
                    <PartChromeStyle BackColor="#FFFBD6" BorderColor="#FFCC66" Font-Names="Verdana" ForeColor="#333333" />
                    <HeaderStyle Font-Size="0.7em" ForeColor="#CCCCCC" HorizontalAlign="Center" />
                    <MenuLabelStyle ForeColor="White" />
                </asp:WebPartZone>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 9px" valign="top">
                <br /><iframe src="http://a.forecabox.com/get/23581" width="728" height="90" marginwidth="0" 
marginheight="0" frameborder="0" scrolling="no" allowtransparency="true"></iframe></td>
        </tr>
        <tr>
            <td colspan="3" valign="top">
                &nbsp;<asp:CatalogZone ID="CatalogZone1" runat="server">
                    <ZoneTemplate>
                        <asp:PageCatalogPart ID="PageCatalogPart1" runat="server" />
                    </ZoneTemplate>
                </asp:CatalogZone>
            </td>
        </tr>
    </table>
</asp:Content>

