Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Partial Class DeleteUser
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter

    Protected Sub firstList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles firstList.SelectedIndexChanged
        Me.List.Visible = True
        ' Me.Title = getNoSelect()
        If getNoSelect() = 0 Then
            Me.List.Visible = False
            Me.nameList.Visible = False
        End If
    End Sub

    Public Function getNoSelect()
        Dim str As String
        Dim s As String = Me.firstList.SelectedValue
        Dim query As String = "Select distinct count(Department) from directory WHERE [location]= '" + s + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function
    'DELETE FROM [User]
    'WHERE     (UserID = 'Ajayi')
    Public Function getNo2Select()
        Dim str As String
        Dim s As String = Me.List.SelectedValue
        Dim query As String = "Select distinct count(fullname) from directory WHERE [Department]= '" + s + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Sub DeleteRow(ByVal ids As Integer)
        Dim query As String = "DELETE FROM directory WHERE (ID = ids)" '"Select distinct count(fullname) from directory WHERE [Department]= '" + s + "'"
        'myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        'myDataTable = New DataTable()
        'myTableAdapter.Fill(myDataTable)
        Dim conn As New SqlConnection(ConnectionString)
        Dim mycommand As New SqlCommand(query, conn)
        mycommand.Parameters.Add("ID", TypeCode.Int16, ids)
        conn.Open()
        mycommand.ExecuteNonQuery()
        conn.Close()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Delete User Page"
    End Sub

    Protected Sub List_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles List.SelectedIndexChanged
        Me.nameList.Visible = True
        If getNo2Select() = 0 Then
            Me.nameList.Visible = False
        End If
    End Sub

    Protected Sub delButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delButton.Click
        Me.RequestPan.Visible = True
        Me.delButton.Visible = False
        Me.RequestPan.Focus()
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    ' Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.RowDeleting
    'Me.DeleteUsersSource.Delete()

    'End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        Dim id As Integer = Me.GridView1.DataKeyNames(e.RowIndex)
        DeleteRow(id)
        GridView1.DataBind()
        'Me.DeleteUsersSource.Delete()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim sel As String = Me.nameList.SelectedValue
        'Dim res As DialogResult = MessageBox.Show("You are about to delete user" + sel + vbCrLf + "click OK to continue or CANCEL to exit", "caution", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        'Select Case res
        '    Case res.OK
        If Me.nameList.SelectedValue = "" Then
            Me.delmessg.Text = "Please select a user to delete"
        Else
            Try
                Me.uSname.Delete()
                Me.delmessg.Text = "User Deleted Successfully"
                Response.AppendHeader("Refresh", "7; URL=deleteuser.aspx")
            Catch ex As Exception
                Me.Title = "'"
                'MessageBox.Show(ex.Message, "error", Windows.Forms.MessageBoxButtons.OK)
            End Try
        End If

        'Case res.Cancel

        'End Select

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Response.AppendHeader("Refresh", "1,URL=DeleteUser.aspx")
        RequestPan.Visible = False
    End Sub
End Class
