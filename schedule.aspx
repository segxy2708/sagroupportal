<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="schedule.aspx.vb" Inherits="schedule" title="Calendar Scheduler" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <table style="width: 705px">
            <tr>
                <td colspan="4" style="text-align: center">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" BackColor="#FFE0C0"
                        BorderColor="Red" BorderStyle="Solid" BorderWidth="1px" />
                    <br />
                    <asp:Label ID="lblMessage" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    Subject</td>
                <td style="width: 544px">
                    <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox></td>
                <td style="width: 2260px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSubject"
                        ErrorMessage="Appointment Subject Required">*</asp:RequiredFieldValidator></td>
                <td>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    Details</td>
                <td style="width: 544px">
                    <asp:TextBox ID="txtDetails" runat="server" Height="52px" TextMode="MultiLine"></asp:TextBox></td>
                <td style="width: 2260px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDetails"
                        ErrorMessage="Appointment Detail Required">*</asp:RequiredFieldValidator></td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Date</td>
                <td style="width: 544px">
                    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox></td>
                <td style="width: 2260px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Time</td>
                <td style="width: 544px">
                    <asp:TextBox ID="txtTime" runat="server"></asp:TextBox></td>
                <td style="width: 2260px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="width: 544px">
                    <asp:Button ID="btnSave" runat="server" Text="Save" /></td>
                <td style="width: 2260px">
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="width: 544px">
                </td>
                <td style="width: 2260px">
                </td>
                <td>
                </td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <strong>Today's Appointment</strong><br />
    <asp:DataList ID="DataList1" runat="server" CellPadding="4" DataKeyField="appid"
        DataSourceID="SqlDataSource1" ForeColor="#333333">
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <AlternatingItemStyle BackColor="White" />
        <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
        <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
        <HeaderTemplate>
            <table style="width: 570px">
                <tr>
                    <td style="width: 229px">
                        <strong>Subject</strong></td>
                    <td style="width: 98px">
                        <strong>Date</strong></td>
                    <td style="width: 142px">
                        <strong>Time</strong></td>
                    <td>
                        <strong>Status</strong></td>
                </tr>
            </table>
        </HeaderTemplate>
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
        <ItemTemplate>
            <table style="width: 570px">
                <tr>
                    <td style="width: 226px">
                        <asp:Label ID="subjectLabel" runat="server" Text='<%# Eval("subject") %>'></asp:Label></td>
                    <td style="width: 99px">
                        <asp:Label ID="dateLabel" runat="server" Text='<%# Eval("date", "{0:d}") %>'></asp:Label></td>
                    <td style="width: 142px">
                        <asp:Label ID="timeLabel" runat="server" Text='<%# Eval("time") %>'></asp:Label></td>
                    <td>
                        <asp:Label ID="statusLabel" runat="server" Text='<%# Eval("status") %>'></asp:Label></td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList><asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT * FROM [schedules] WHERE (([staff] = @staff) AND ([date] = @date)) ORDER BY [date], [time]">
        <SelectParameters>
            <asp:SessionParameter Name="staff" SessionField="id" Type="String" />
            <asp:SessionParameter Name="date" SessionField="dates" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <strong>Other Day's Appointment<br />
        <asp:DataList ID="DataList2" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Snow"
            BorderWidth="1px" CellPadding="2" DataSourceID="SqlDataSource2" ForeColor="Black">
            <FooterStyle BackColor="Tan" />
            <AlternatingItemStyle BackColor="PaleGoldenrod" />
            <SelectedItemStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
            <HeaderTemplate>
                <table style="width: 570px">
                    <tr>
                        <td style="width: 229px">
                            <strong><span style="color: #ffffff">Subject</span></strong></td>
                        <td style="width: 98px">
                            <strong><span style="color: #ffffff">Date</span></strong></td>
                        <td style="width: 142px">
                            <strong><span style="color: #ffffff">Time</span></strong></td>
                        <td>
                            <strong><span style="color: #ffffff">Status</span></strong></td>
                    </tr>
                </table>
            </HeaderTemplate>
            <HeaderStyle BackColor="Maroon" Font-Bold="True" />
            <ItemTemplate>
                <table style="width: 570px">
                    <tr>
                        <td style="width: 226px; height: 18px;">
                            <asp:Label ID="subjectLabel" runat="server" Font-Bold="False" ForeColor="#004000"
                                Text='<%# Eval("subject") %>'></asp:Label></td>
                        <td style="width: 99px; height: 18px">
                            <asp:Label ID="dateLabel" runat="server" Font-Bold="False" ForeColor="#004000" Text='<%# Eval("date", "{0:d}") %>'></asp:Label></td>
                        <td style="width: 142px; height: 18px;">
                            <asp:Label ID="timeLabel" runat="server" Font-Bold="False" ForeColor="#004000" Text='<%# Eval("time") %>'></asp:Label></td>
                        <td style="height: 18px">
                            <asp:Label ID="statusLabel" runat="server" Font-Bold="False" ForeColor="#004000"
                                Text='<%# Eval("status") %>'></asp:Label></td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT * FROM [schedules] WHERE (([staff] = @staff) AND ([date] <> @date)) ORDER BY [date], [time]">
            <SelectParameters>
                <asp:SessionParameter Name="staff" SessionField="id" Type="String" />
                <asp:SessionParameter Name="date" SessionField="dates" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </strong>
</asp:Content>

