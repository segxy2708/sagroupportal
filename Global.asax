<%@ Application Language="VB" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System" %>
<script RunAt="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
        Dim i As New PortalsTableAdapters.chatroomTableAdapter
        Try
            i.DeleteChatUser(Session("id"))
        Catch ex As Exception

        End Try
        Session.Clear()
        HttpContext.Current.Response.Redirect("`~/session_timeout.aspx")
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
        Session.Clear()
        Session("urls") = Request.Path
        HttpContext.Current.Response.Redirect("~/session_timeout.aspx")
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends.
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer
        ' or SQLServer, the event is not raised.
        Dim i As New PortalsTableAdapters.chatroomTableAdapter
        Try
            i.DeleteChatUser(Session("id"))
        Catch ex As Exception

        End Try
        Session.Clear()
        Session("urls") = Request.Path
        HttpContext.Current.Response.Redirect("~/session_timeout.aspx")
    End Sub

    Public Function readHtml(ByVal url As String)
        Dim result As String
        Dim objResponse As System.Net.WebResponse
        Dim objRequest As System.Net.WebRequest = System.Net.HttpWebRequest.Create(url)
        objResponse = objRequest.GetResponse
        Dim sr As System.IO.StreamReader = New System.IO.StreamReader(objResponse.GetResponseStream)
        Using sr
            result = sr.ReadToEnd
            sr.Close()
        End Using
        Return result
    End Function

    Public Function getData(ByVal connectionstring As String, ByVal query As String) As String
        Dim str As String

        ' Dim query As String = "Select profilename from directory where fullname='" + Session("names") + "'"
        Dim myTableAdapter As System.Data.SqlClient.SqlDataAdapter = New System.Data.SqlClient.SqlDataAdapter(query, connectionstring)
        Dim myDataTable As System.Data.DataTable = New System.Data.DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Private Sub EncryptAppSettings()

        Dim objConfig As Configuration = WebConfigurationManager.OpenWebConfiguration("~") 'Request.ApplicationPath)
        Dim objAppsettings As AppSettingsSection = CType(objConfig.GetSection("appSettings"), AppSettingsSection)
        If Not (objAppsettings.SectionInformation.IsProtected) Then

            objAppsettings.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider")
            objAppsettings.SectionInformation.ForceSave = True
            objConfig.Save(ConfigurationSaveMode.Modified)
        End If
    End Sub

    Private Sub DecryptAppSettings()

        Dim objConfig As Configuration = WebConfigurationManager.OpenWebConfiguration("~") 'Request.ApplicationPath)
        Dim objAppsettings As AppSettingsSection = CType(objConfig.GetSection("appSettings"), AppSettingsSection)
        If (objAppsettings.SectionInformation.IsProtected) Then

            objAppsettings.SectionInformation.UnprotectSection()
            objAppsettings.SectionInformation.ForceSave = True
            objConfig.Save(ConfigurationSaveMode.Modified)
        End If
    End Sub

    Private Sub EncryptConnSettings()

        Dim objConfig As Configuration = WebConfigurationManager.OpenWebConfiguration("~") 'Request.ApplicationPath)
        Dim objConfigSection As ConfigurationSection = objConfig.ConnectionStrings

        ' Dim objAppsettings As AppSettingsSection = CType(objConfig.GetSection("appSettings"), AppSettingsSection)
        'If Not (objAppsettings.SectionInformation.IsProtected) Then

        '    objAppsettings.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider")
        '    objAppsettings.SectionInformation.ForceSave = True
        '    objConfig.Save(ConfigurationSaveMode.Modified)
        'End If

        If Not (objConfigSection.SectionInformation.IsProtected) Then
            objConfigSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider")
            objConfigSection.SectionInformation.ForceSave = True
            objConfig.Save(ConfigurationSaveMode.Modified)
        End If
    End Sub

    Public Shared Sub SetUserLocale()
        Dim Request As HttpRequest = HttpContext.Current.Request
        If Request.UserLanguages Is Nothing Then
            Return
        End If

        Dim Lang As String = Request.UserLanguages(0)
        If Lang IsNot Nothing Then
            If Lang.Length < 3 Then
                Lang = Lang + "-" + Lang.ToUpper()
            End If
            Try
                Dim culture As New Globalization.CultureInfo(Lang)
                System.Threading.Thread.CurrentThread.CurrentCulture = culture
                Threading.Thread.CurrentThread.CurrentUICulture = culture
            Catch

            End Try
        End If
    End Sub

    Protected Sub Application_BeginRequest(sender As Object, e As System.EventArgs)
        SetUserLocale()
        changeurl()
    End Sub

    Private Sub changeurl()
        Dim curruntpath As String = Request.Path.ToLower()

        curruntpath = curruntpath.Trim()
        Dim isredirected As Boolean = curruntpath.EndsWith(".aspx")
        Dim isredirected2 As Boolean = curruntpath.EndsWith(".html")
        Dim isredirected3 As Boolean = curruntpath.EndsWith(".jpg")
        Dim isredirected4 As Boolean = curruntpath.EndsWith(".jpeg")
        Dim isredirected5 As Boolean = curruntpath.EndsWith(".gif")
        Dim isredirected6 As Boolean = curruntpath.EndsWith(".png")
        Dim atr As String = curruntpath.Substring(curruntpath.IndexOf("/"))

        If (Not (isredirected) And Not (isredirected2) And Not (isredirected3) And Not (isredirected4) And Not (isredirected5) And Not (isredirected6)) Then

            Dim obj As HttpContext = HttpContext.Current
            Dim finalurl As String = curruntpath + ".aspx"

            If (System.IO.File.Exists(Server.MapPath(finalurl))) Then

                obj.RewritePath(finalurl)

            Else

                obj.RewritePath("Error.aspx")

            End If
            If (curruntpath.EndsWith(".aspx")) Then

                'Dim obj As HttpContext = HttpContext.Current
                obj.RewritePath("Tohideextentions.aspx")
            End If
        End If

    End Sub
</script>