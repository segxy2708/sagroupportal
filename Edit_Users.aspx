<%@ Page Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="Edit_Users.aspx.vb" Inherits="Edit_Users" title="Untitled Page" %>

<%@ Register Src="Keyboard.ascx" TagName="Keyboard" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:Label ID="errMess" runat="server" Text=" " Visible="true"></asp:Label>
    <asp:Label ID="Label3" runat="server" Text="Staff Name:"></asp:Label>&nbsp;
    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="nameSource"
        DataTextField="fullname" DataValueField="fullname">
    </asp:DropDownList>&nbsp;
    <asp:SqlDataSource ID="nameSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT DISTINCT [fullname] FROM [directory]"></asp:SqlDataSource>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp;
    <asp:Label ID="messageLab" runat="server"></asp:Label>
<asp:Panel ID="TablePanel" runat ="server" Height ="100%" Width ="100%" ScrollBars ="Both">
<table >
<tr>
<td colspan="7" style="height: 20px;" align="center">
    </td>
</tr>
<tr>
<td style="width: 50px;" rowspan="20">

</td>
<td>

</td>
<td style="width: 122px">
    <asp:Label ID="Label2" runat="server" Text="UserID"></asp:Label></td>
<td>
<asp:TextBox ID="idTxt" runat ="server" ReadOnly ="true"  ></asp:TextBox>
</td>
</tr>
<tr>
<td style="height: 26px">

</td>
<td style="width: 122px; height: 26px;">
    <asp:Label ID="FirstNameLab" runat="server" Text="First Name:" Width="114px"></asp:Label></td>
<td style="height: 26px">
<asp:TextBox ID="FirstNameTxt" runat="server" ReadOnly="True"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FirstNameTxt"
        Display="Dynamic" ErrorMessage="First name cannot be empty">*</asp:RequiredFieldValidator></td>
<td style="width: 50px; height: 26px;">
<asp:Label ID="LastNameLab" runat="server" Text="Last Name:" Width="99px"></asp:Label></td>

<td style="width: 167px; height: 26px;">
<asp:TextBox ID="LastNameTxt" runat="server" ReadOnly="True"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="LastNameTxt"
        Display="Dynamic" ErrorMessage="Last name cannot be empty">*</asp:RequiredFieldValidator></td>
<td style="width :113px; height: 26px;">
    </td>
<td style="height: 26px">
    
</td>
</tr>
<tr>
<td>

</td>
<td style="width: 122px">
    <asp:Label ID="SexLabs" runat="server" Text="Sex:" Width="119px"></asp:Label></td>
<td>
    &nbsp;<asp:DropDownList ID="SexList" runat="server">
        <asp:ListItem>Male</asp:ListItem>
        <asp:ListItem>Female</asp:ListItem>
    </asp:DropDownList></td>
<td style="width: 50px">
<asp:Label ID="titleLab" runat="server" Text="Title:"></asp:Label></td>

<td style="width: 167px">
    &nbsp;<asp:DropDownList ID="titleList" runat="server">
        <asp:ListItem>Chief</asp:ListItem>
        <asp:ListItem>Dr</asp:ListItem>
        <asp:ListItem>Engr</asp:ListItem>
        <asp:ListItem>HRH</asp:ListItem>
        <asp:ListItem>Miss</asp:ListItem>
        <asp:ListItem>Mr</asp:ListItem>
        <asp:ListItem>Mrs</asp:ListItem>
        <asp:ListItem>Ms</asp:ListItem>
        <asp:ListItem>Prof</asp:ListItem>
        <asp:ListItem>Sir</asp:ListItem>
    </asp:DropDownList></td>
<td style="width :113px">

</td>
</tr>
<tr>
<td style="height: 40px">

</td>
<td style="width: 122px; height: 40px;">
    <asp:Label ID="PhoneNoLab" runat="server" Text="Phone No:"></asp:Label></td>
<td style="height: 40px">
<asp:TextBox ID="PhoneNoTxt" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="PhoneNoTxt"
        Display="Dynamic" ErrorMessage="phone no cannot be empty">*</asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="PhoneNoTxt"
        Display="Dynamic" ErrorMessage="Invalid Format" ValidationExpression="^\+?[0-9]+(,[0-9]{3})*(\.[0-9]{2})?">*</asp:RegularExpressionValidator></td>
<td style="width: 50px; height: 40px;">
<asp:Label ID="FaxLab" runat="server" Text="Fax:"></asp:Label></td>

<td style="height: 40px; width: 167px;">
<asp:TextBox ID="faxTxt" runat="server"></asp:TextBox>
</td>
<td style="width :113px; height: 40px;">

</td>
</tr>
<tr>
<td style="height: 26px">

</td>
<td style="width: 122px; height: 26px;">
    &nbsp;<asp:Label ID="emailLab" runat="server" Text="e-mail:" Width="86px"></asp:Label></td>
<td style="height: 26px">
    <asp:TextBox ID="emailTxt" runat="server" EnableViewState="False"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="emailTxt"
        Display="Dynamic" ErrorMessage="Enter e-mail Address">*</asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="not valid"
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="emailTxt">*</asp:RegularExpressionValidator></td>
<td style="width: 50px; height: 26px;">
<asp:Label ID="AltEmaiLab" runat="server" Text="Alternate e-mail:" Width="124px" Visible="False"></asp:Label></td>

<td style="width: 167px; height: 26px;">
<asp:TextBox ID="altemailTxt" runat="server" Visible="False"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="not valid"
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="altemailTxt" Display="Dynamic">*</asp:RegularExpressionValidator></td>
<td style="width :113px; height: 26px;">

</td>
</tr>
<tr>
<td style="height: 26px">

</td>
<td style="width: 122px; height: 26px;">
    <asp:Label ID="cellPhoneLab" runat="server" Text="Cell Phone:" Width="109px"></asp:Label></td>
<td style="height: 26px">
<asp:TextBox ID="cellPhoneTxt" runat="server"></asp:TextBox></td>
<td style="width: 50px; height: 26px;">
<asp:Label ID="DepartmentLab" runat="server" Text="Department:"></asp:Label></td>

<td style="width: 167px; height: 26px;">
    &nbsp;<asp:DropDownList ID="departmentList" runat="server">
        <asp:ListItem>- Choose Data -</asp:ListItem>
        <asp:ListItem>Admin</asp:ListItem>
        <asp:ListItem>Asset Management</asp:ListItem>
        <asp:ListItem>Customer Care</asp:ListItem>
        <asp:ListItem>Executives</asp:ListItem>
        <asp:ListItem>Finance</asp:ListItem>
        <asp:ListItem>Human Capital</asp:ListItem>
        <asp:ListItem>Information Technology</asp:ListItem>
        <asp:ListItem>Internal Control</asp:ListItem>
        <asp:ListItem>Investment</asp:ListItem>
        <asp:ListItem>Legal</asp:ListItem>
        <asp:ListItem>Marketing</asp:ListItem>
        <asp:ListItem>Research</asp:ListItem>
        <asp:ListItem>Technical</asp:ListItem>
        
    </asp:DropDownList></td>
<td style="width :113px; height: 26px;">

</td>
</tr>
<tr>
<td style="height: 42px">

</td>
<td style="width: 122px; height: 42px;">
<asp:Label ID="Branchlab" runat="server" Text="Branch:"></asp:Label>
</td>
<td style="height: 42px">
    <asp:DropDownList ID="BranchList" runat="server">
        <asp:ListItem>Abuja</asp:ListItem>
        <asp:ListItem>Apapa</asp:ListItem>
        <asp:ListItem>Head Office</asp:ListItem>
        <asp:ListItem>Group Office</asp:ListItem>
        <asp:ListItem>Ikeja</asp:ListItem>
        <asp:ListItem>Port Harcourt</asp:ListItem>
        <asp:ListItem>Warri</asp:ListItem>
    </asp:DropDownList></td>
<td style="width: 50px; height: 42px;">
    &nbsp;<asp:Label ID="Address1Lab" runat="server" Text="Address1:" Width="108px"></asp:Label></td>

<td style="width: 167px; height: 42px;">
    <asp:TextBox ID="address1Txt" runat="server" Width="342px" TextMode="MultiLine"></asp:TextBox></td>
<td style="width :113px; height: 42px;">

</td>
</tr>
<tr>
<td>

</td>
<td style="width: 122px">
    <asp:Label ID="cityLab" runat="server" Text="City:"></asp:Label></td>
<td>
    <asp:TextBox ID="cityTxt" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="cityTxt"
        Display="Dynamic" ErrorMessage="City cannot be empty">*</asp:RequiredFieldValidator></td>
<td style="width: 50px">
    <asp:Label ID="address2Lab" runat="server" Text="Address2:"></asp:Label></td>

<td style="width: 167px">
    <asp:TextBox ID="address2Txt" runat="server" Width="343px" TextMode="MultiLine"></asp:TextBox></td>
<td style="width :113px">

</td>
</tr>
<tr>
<td style="height: 48px">

</td>
<td style="width: 122px; height: 48px;">
    <asp:Label ID="DOBLab" runat="server" Text="DOB:" Visible="False"></asp:Label></td>
<td style="height: 48px">
    <asp:TextBox ID="dobTxt" runat="server" Visible="False"></asp:TextBox></td>
<td style="width: 50px; height: 48px;">
<asp:Label ID="stateLab" runat="server" Text="State of Origin:"></asp:Label></td>

<td style="width: 167px; height: 48px;">
    <asp:DropDownList ID="StateList" runat="server">
    <asp:Listitem>Abia</asp:Listitem>
         <asp:Listitem>Adamawa</asp:Listitem>
      <asp:Listitem>Akwa ibom</asp:Listitem> 
      <asp:Listitem>Anambra</asp:Listitem>
      <asp:Listitem>Bayelsa</asp:Listitem>
      <asp:Listitem>Benue</asp:Listitem>
      <asp:Listitem>Borno</asp:Listitem>
      <asp:Listitem>Cross River</asp:Listitem>
      <asp:Listitem>Delta</asp:Listitem>
      <asp:Listitem>Ebonyi</asp:Listitem>
      <asp:Listitem>Edo</asp:Listitem>
      <asp:Listitem>Ekiti</asp:Listitem>
	  <asp:Listitem>Enugu</asp:Listitem>
	  <asp:Listitem>FCT</asp:Listitem>
      <asp:Listitem>Gombe</asp:Listitem>
      <asp:Listitem>Imo</asp:Listitem>
      <asp:Listitem>Jigawa</asp:Listitem>
      <asp:Listitem>Kaduna</asp:Listitem>
      <asp:Listitem>Kano</asp:Listitem>
      <asp:Listitem>Katsina</asp:Listitem>
      <asp:Listitem>Kebbi</asp:Listitem>
      <asp:Listitem>Kogi</asp:Listitem>
      <asp:Listitem>Kwara</asp:Listitem>
      <asp:Listitem>Lagos</asp:Listitem>
      <asp:Listitem>Nassarawa</asp:Listitem>
      <asp:Listitem>Niger</asp:Listitem>
      <asp:Listitem>Ogun</asp:Listitem>
	  <asp:Listitem>Ondo</asp:Listitem>
      <asp:Listitem>Osun</asp:Listitem>
      <asp:Listitem>Oyo</asp:Listitem>
      <asp:Listitem>Plateau</asp:Listitem>
      <asp:Listitem>Rivers</asp:Listitem>
      <asp:Listitem>Sokoto</asp:Listitem>
      <asp:Listitem>Taraba</asp:Listitem>
      <asp:Listitem>Yobe</asp:Listitem>
      <asp:Listitem>Zamfara</asp:Listitem> 
    </asp:DropDownList></td>
<td style="width :113px; height: 48px;">

</td>
</tr>
<tr>
<td style="height: 26px">

</td>
<td style="width: 122px; height: 26px;">
    <asp:Label ID="orgLab" runat="server" Text="Organisation:"></asp:Label></td>
<td style="height: 26px">
    <asp:DropDownList ID="orgList" runat="server">
        <asp:ListItem>SA Group</asp:ListItem>
        <asp:ListItem>SA Capital</asp:ListItem>
        <asp:ListItem>SA E-Business</asp:ListItem>
        <asp:ListItem>SA Insurance</asp:ListItem>
        <asp:ListItem>SA Lagoon</asp:ListItem>
        <asp:ListItem>SA Life</asp:ListItem>
        <asp:ListItem>SA Pension</asp:ListItem>
        <asp:ListItem>SA Properties</asp:ListItem>
    </asp:DropDownList></td>
<td style="width: 50px; height: 26px;">
<asp:Label ID="countryLab" runat="server" Text="Country:"></asp:Label></td>

<td style="width: 167px; height: 26px;">
    <asp:DropDownList ID="CountryList" runat ="server" >
      <asp:ListItem>Nigeria</asp:ListItem>
      <asp:Listitem>Argentina</asp:Listitem>
      <asp:Listitem>Australia</asp:Listitem>
      <asp:Listitem>Belgium</asp:Listitem>
      <asp:Listitem>Bermuda</asp:Listitem>
      <asp:Listitem>Bolivia</asp:Listitem>
      <asp:Listitem>Brazil</asp:Listitem>
      <asp:Listitem>Bulgaria</asp:Listitem>
      <asp:Listitem>Canada</asp:Listitem>
      <asp:Listitem>Chile</asp:Listitem>
	  <asp:Listitem>China</asp:Listitem>
      <asp:Listitem>Colombia</asp:Listitem>
      <asp:Listitem>Costa Rica</asp:Listitem>
      <asp:Listitem>Cuba</asp:Listitem>
      <asp:Listitem>Cyprus</asp:Listitem>
      <asp:Listitem>Denmark</asp:Listitem>
      <asp:Listitem>Dominican Republic</asp:Listitem>
      <asp:Listitem>Ecuador</asp:Listitem>
      <asp:Listitem>Egypt</asp:Listitem>
      <asp:Listitem>Estonia</asp:Listitem>
      <asp:Listitem>Ethiopia</asp:Listitem>
      <asp:Listitem>Fiji</asp:Listitem>
      <asp:Listitem>Finland</asp:Listitem>
      <asp:Listitem>France</asp:Listitem>
	  <asp:Listitem>Georgia</asp:Listitem>
      <asp:Listitem>Germany</asp:Listitem>
      <asp:Listitem>Greece</asp:Listitem>
      <asp:Listitem>Greenland</asp:Listitem>
      <asp:Listitem>Grenada</asp:Listitem>
      <asp:Listitem>Guam</asp:Listitem>
      <asp:Listitem>Haiti</asp:Listitem>
      <asp:Listitem>Honduras</asp:Listitem>
      <asp:Listitem>Hong Kong</asp:Listitem>
      <asp:Listitem>Hungary</asp:Listitem>
      <asp:Listitem>Iceland</asp:Listitem>
      <asp:Listitem>India</asp:Listitem>
      <asp:Listitem>Indonesia</asp:Listitem>
	  <asp:Listitem>Iran</asp:Listitem>
      <asp:Listitem>Iraq</asp:Listitem>
      <asp:Listitem>Ireland</asp:Listitem>
      <asp:Listitem>Israel</asp:Listitem>
      <asp:Listitem>Italy</asp:Listitem>
      <asp:Listitem>Jamaica</asp:Listitem>
	  <asp:Listitem>Japan</asp:Listitem>
      <asp:Listitem>Jordan</asp:Listitem>
      <asp:Listitem>Kenya</asp:Listitem>
      <asp:Listitem>Korea-North</asp:Listitem>
      <asp:Listitem>Korea-South</asp:Listitem>
      <asp:Listitem>Kuwait</asp:Listitem>
	  <asp:Listitem>Lebanon</asp:Listitem>
	  <asp:Listitem>Liberia</asp:Listitem>
      <asp:Listitem>Libya</asp:Listitem>
      <asp:Listitem>Madagascar</asp:Listitem>
      <asp:Listitem>Malaysia</asp:Listitem>
      <asp:Listitem>Mexico</asp:Listitem>
      <asp:Listitem>Monaco</asp:Listitem>
      <asp:Listitem>Morocco</asp:Listitem>
      <asp:Listitem>Netherlands</asp:Listitem>
      <asp:Listitem>New Zealand</asp:Listitem>
      <asp:Listitem>Nicaragua</asp:Listitem>
	  <asp:Listitem>Niger</asp:Listitem>
      <asp:Listitem>Norway</asp:Listitem>
      <asp:Listitem>Pakistan</asp:Listitem>
      <asp:Listitem>Panama</asp:Listitem>
      <asp:Listitem>Paraguay</asp:Listitem>
      <asp:Listitem>Peru</asp:Listitem>
	  <asp:Listitem>Philippines</asp:Listitem>
      <asp:Listitem>Poland</asp:Listitem>
      <asp:Listitem>Portugal</asp:Listitem>
      <asp:Listitem>Puerto Rico</asp:Listitem>
      <asp:Listitem>Romania</asp:Listitem>
      <asp:Listitem>Russia</asp:Listitem>
	  <asp:Listitem>Saudi Arabia</asp:Listitem>
	  <asp:Listitem>Singapore</asp:Listitem>
      <asp:Listitem>Soth Africa</asp:Listitem>
      <asp:Listitem>Spain</asp:Listitem>
      <asp:Listitem>Sri Lanka</asp:Listitem>
      <asp:Listitem>Sweden</asp:Listitem>
      <asp:Listitem>Switzerland</asp:Listitem>
      <asp:Listitem>Syria</asp:Listitem>
      <asp:Listitem>Thailand</asp:Listitem>
      <asp:Listitem>Turkey</asp:Listitem>
      <asp:Listitem>Uganda</asp:Listitem>
	  <asp:Listitem>United Arab Emirates</asp:Listitem>
      <asp:Listitem>United Kingdon</asp:Listitem>
      <asp:Listitem>United States</asp:Listitem>
      <asp:Listitem>Uruguay</asp:Listitem>
      <asp:Listitem>Venezuela</asp:Listitem>
      <asp:Listitem>Vietnam</asp:Listitem>
	  <asp:Listitem>Virgin Islands</asp:Listitem>
      <asp:Listitem>Yemen</asp:Listitem>
      <asp:Listitem>Zambia</asp:Listitem>
      <asp:Listitem>Zimbabwe</asp:Listitem>
</asp:DropDownList></td>
<td style="width :113px; height: 26px;">

</td>
</tr>
<tr>
<td style="height: 40px">

</td>
<td style="width: 122px; height: 40px;">
    <asp:Label ID="BuildingLab" runat="server" Text="Building:"></asp:Label></td>
<td style="height: 40px">
    <asp:DropDownList ID="buildingList" runat="server">
        <asp:ListItem>annex</asp:ListItem>
        <asp:ListItem>main</asp:ListItem>
    </asp:DropDownList></td>
<td style="width: 50px; height: 40px;">
<asp:Label ID="Label20" runat="server" Text="Profile Pic:" Visible="False"></asp:Label></td>

<td style="width: 167px; height: 40px;">
    <asp:FileUpload ID="FileUpload1" runat="server" Visible="False" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="FileUpload1"
        Display="Dynamic" ErrorMessage="Pls upload a photo">*</asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator 
id="FileUpLoadValidator" runat="server" 
ErrorMessage="Upload Jpegs,bitmaps,PNG and Gifs only." 
ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.JPG|.gif|.GIF|.png|.PNG|.bmp|.BMP|.jpeg|.JPEG)$" 
ControlToValidate="FileUpload1" Display="Dynamic">*</asp:RegularExpressionValidator>
    </td>
<td style="width :113px; height: 40px;">

</td>
</tr>
<tr>
<td>

</td>
<td style="width: 122px">
    </td>
<td>
    &nbsp;</td>
<td style="width: 50px">
</td>

<td style="width: 167px">
    &nbsp;</td>
<td style="width :113px">

</td>
</tr>
<tr>
<td style="height: 15px">

</td>
<td style="width: 122px; height: 15px;">
    </td>
<td style="height: 15px">

</td>
<td style="width: 50px; height: 15px;">
</td>

<td style="width: 167px; height: 15px;">

</td>
<td style="width :113px; height: 15px;">

</td>
</tr>
<tr>
<td style="height: 16px">

</td>
<td style="width: 122px; height: 16px;">
    </td>
<td style="height: 16px">
    &nbsp;</td>
<td style="width: 50px; height: 16px;">
</td>

<td style="width: 167px; height: 16px;">
    &nbsp;</td>
<td style="width :113px; height: 16px;">

</td>
</tr>
<tr>
<td style="height: 12px">

</td>
<td style="width: 122px; height: 12px;">
    <asp:Label ID="Label25" runat="server" Text="User Name:" Visible="False"></asp:Label></td>
<td style="height: 12px">
<asp:TextBox ID="uNametxt" runat="server" Visible="False"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="uNametxt"
        Display="Dynamic" ErrorMessage="Supply user name">*</asp:RequiredFieldValidator></td>
<td style="width: 50px; height: 12px;">
    <asp:Label ID="Label1" runat="server" Text="Users Level"></asp:Label></td>

<td style="height: 12px; width: 167px;">
    <asp:DropDownList ID="UlevList" runat="server">
        <asp:ListItem>Basic User</asp:ListItem>
        <asp:ListItem>Administrator</asp:ListItem>
        <asp:ListItem>Super Administrator</asp:ListItem>
    </asp:DropDownList></td>
<td style="width :113px; height: 12px;">

</td>
</tr>
<tr>
<td style="height: 42px">

</td>
<td style="width: 122px; height: 42px;">
    <asp:Label ID="Label27" runat="server" Text="Password:" Visible="False"></asp:Label></td>
<td style="height: 42px">
<asp:TextBox ID="pass1" runat="server" TextMode="Password" Visible="False"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="cannot be empty" ControlToValidate="pass1" Display="Dynamic">*</asp:RequiredFieldValidator></td>
<td style="width: 50px; height: 42px;">
<asp:Label ID="Label28" runat="server" Text="Retype Password:" Visible="False"></asp:Label></td>

<td style="width: 167px; height: 42px;">
<asp:TextBox ID="pass2" runat="server" TextMode="Password" Visible="False"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="cannot be empty. " Display="Dynamic" ControlToValidate="pass2">*</asp:RequiredFieldValidator>
    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="password mismatch" ControlToCompare="pass2" ControlToValidate="pass1" Display="Dynamic">*</asp:CompareValidator></td>
<td style="width :113px; height: 42px;">

</td>
</tr>
<tr>
<td>

</td>
<td style="width: 122px">
    </td>
<td>
    &nbsp;</td>
<td style="width: 50px">
</td>

<td style="width: 167px">
    &nbsp;</td>
<td style="width :113px">

</td>
</tr>
<tr>
<td style="height: 16px">

</td>
<td style="width: 122px; height: 16px;">
    </td>
<td style="height: 16px" colspan="3">
    &nbsp;</td>
<td style="width: 50px; height: 16px;">
</td>

<td style="width: 167px; height: 16px;">
    &nbsp;</td>
<td style="width :113px; height: 16px;">

</td>
</tr>
<tr>
<td style="height: 28px">

</td>
<td style="width: 122px; height: 28px;">
    </td>
<td style="height: 28px">
    &nbsp;<asp:SqlDataSource ID="SubmitSource" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>" SelectCommand="SELECT * FROM [directory]" InsertCommand="INSERT INTO directory(ID, phone, fax, state, Address1, Address2, email, Department, location, building, last, first, picloc, office, title, uname, pw, emailuser, country, fullname, profilename, Branch, Creator, Sex) VALUES (@ID, @phone, @fax, @state, @Address1, @Address2, @email, @Department, @location, @building, @last, @first, @picloc, @office, @title, @uname, @pw, @emailuser, @country, @fullname, @profilename, @Branch, @Creator, @Sex)" UpdateCommand="UPDATE directory SET phone = @phone, fax = @fax, city = @city, state = @state, Address1 = @address1, Address2 = @address2, email = @email, Department = @department, building = @building, last = @last, first = @first, office = @office, title = @title, cellphone = @cellphone, country = @country, Sex = @Sex, Branch = @Branch WHERE (ID = @ID)">
        <InsertParameters>
            <asp:ControlParameter ControlID="PhoneNoTxt" Name="phone" PropertyName="Text" />
            <asp:ControlParameter ControlID="faxTxt" Name="fax" PropertyName="Text" />
            <asp:ControlParameter ControlID="StateList" Name="state" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="address1Txt" Name="Address1" PropertyName="Text" />
            <asp:ControlParameter ControlID="address2Txt" Name="Address2" PropertyName="Text" />
            <asp:ControlParameter ControlID="emailTxt" Name="email" PropertyName="Text" />
            <asp:ControlParameter ControlID="departmentList" Name="Department" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="orgList" Name="location" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="buildingList" Name="building" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="LastNameTxt" Name="last" PropertyName="Text" />
            <asp:ControlParameter ControlID="FirstNameTxt" Name="first" PropertyName="Text" />
            <asp:ControlParameter ControlID="orgList" Name="office" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="titleList" Name="title" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="uNametxt" Name="uname" PropertyName="Text" />
            <asp:ControlParameter ControlID="pass1" Name="pw" PropertyName="Text" />
            <asp:ControlParameter ControlID="emailTxt" Name="emailuser" PropertyName="Text" />
            <asp:ControlParameter ControlID="CountryList" Name="country" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="UlevList" Name="profilename" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="BranchList" Name="Branch" PropertyName="SelectedValue" />
            <asp:SessionParameter Name="Creator" SessionField="names" />
            <asp:ControlParameter ControlID="SexList" Name="Sex" PropertyName="SelectedValue" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="PhoneNoTxt" Name="phone" PropertyName="Text" />
            <asp:ControlParameter ControlID="faxTxt" Name="fax" PropertyName="Text" />
            <asp:ControlParameter ControlID="cityTxt" Name="city" PropertyName="Text" />
            <asp:ControlParameter ControlID="StateList" Name="state" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="address1Txt" Name="address1" PropertyName="Text" />
            <asp:ControlParameter ControlID="address2Txt" Name="address2" PropertyName="Text" />
            <asp:ControlParameter ControlID="departmentList" Name="department" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="buildingList" Name="building" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="LastNameTxt" Name="last" PropertyName="Text" />
            <asp:ControlParameter ControlID="FirstNameTxt" Name="first" PropertyName="Text" />
            <asp:ControlParameter ControlID="orgList" Name="office" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="titleList" Name="title" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="cellPhoneTxt" Name="cellphone" PropertyName="Text" />
            <asp:ControlParameter ControlID="CountryList" Name="country" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="SexList" Name="Sex" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="BranchList" Name="Branch" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="idTxt" Name="ID" PropertyName="Text" />
            <asp:ControlParameter ControlID="orgList" Name="location" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="emailTxt" Name="email" PropertyName="Text" />
        </UpdateParameters>
    </asp:SqlDataSource>
</td>
<td style="width: 50px; height: 28px;">
</td>

<td style="width: 167px; height: 28px;">
    &nbsp;</td>
<td style="width :113px; height: 28px;">

</td>
</tr>
<tr>
<td style="height: 26px">

</td>
<td style="width: 122px; height: 26px;">
    </td>
<td align="center" colspan="2" style="height: 26px">
    <asp:Button ID="SubmitBut" runat="server" Text="Submit" />&nbsp;</td>
<td style="width: 50px; height: 26px;">
</td>

<td style="width: 167px; height: 26px;">
    &nbsp;</td>
<td style="width :113px; height: 26px;">

</td>
</tr>
</table>

</asp:Panel>
<asp:Panel id="Panel1" runat="server" Width="300px" Visible ="false" ><asp:Label id="Label4" runat="server" Text="Click Yes to continue or No to Stop."></asp:Label> <asp:Button id="Button1" runat="server" Text="Yes"></asp:Button> <asp:Button id="Button2" runat="server" Text="No"></asp:Button></asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

