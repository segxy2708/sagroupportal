<%@ Page Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="test.aspx.vb" Inherits="test" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="getFile" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT Location, FileName, FileType, FileSize, FileLoc, Sender FROM User_File_Details WHERE ([User] = @User ) ORDER BY DateCreated DESC, TimeCreated DESC">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="names" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None">
        <Columns>
            
            <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName" />
            <asp:TemplateField HeaderText ="File">
            <ItemTemplate >
            <asp:HyperLink ID="lik1" runat="server" ImageUrl="~/images/category.gif" NavigateUrl='<%# Eval("FileLoc") %>'
                                Text='<%# Eval("FileLoc") %>'></asp:HyperLink>
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:BoundField DataField="Sender" HeaderText="Sender" SortExpression="Sender" />
            <asp:BoundField DataField="FileSize" HeaderText="FileSize" SortExpression="FileSize" />
            <asp:BoundField DataField="FileType" HeaderText="FileType" SortExpression="FileType" />
        </Columns>
        <RowStyle BackColor="#E3EAEB" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#7C6F57" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT [User], [FileName], [FileLoc], [Sender], [FileSize], [FileType] FROM [User_File_Details] WHERE ([User] = @User) ORDER BY [DateCreated] DESC, [TimeCreated] DESC">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="names" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

