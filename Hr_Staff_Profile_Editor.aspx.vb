Imports System
imports System.data
imports System.data.oledb
imports System.data.sqlclient
imports System.Configuration
imports System.Collections
imports System.Web
imports System.Web.UI
imports Microsoft.VisualBasic
imports System.Web.UI.HTMLcontrols
imports System.Web.UI.Webcontrols
imports System.IO
imports Microsoft
Partial Class Hr_Staff_Profile_Editor
    Inherits System.Web.UI.Page

    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("~/session_timeout.aspx")
        End If
        If Session("Department") = "Legal/Company Secretary" Or Session("Department") = "Legal" Or Session("Department") = "Human Resources" Or Session("Department") = "Human Capital" Or Session("Department") = "EXECUTIVES" Or Session("role") = "Super Administrator" Then
            lblCompany.Text = Session("Group")
            lblDepartment.Text = Session("Department")
            lblApplicationDate.Text = Format(Now.Date, "MM/dd/yyyy")
        Else
            lblMessage.Text = "You are not eligible to view this portal"
            lblMessage.ForeColor = Drawing.Color.Red
        End If
    End Sub


    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        'Dim keys As Integer = GridView1.DataKeys(e.Item.ItemIndex).ToString()
        'Session("I") = keys
        'Response.Redirect(EditCommandColumn.ToString())
    End Sub

    Protected Sub drpDepart_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpDepart.SelectedIndexChanged
        'If drpDepart.Text = "All" Then


        '    Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        '    Dim cmd As SqlCommand = myconnection.CreateCommand()
        '    cmd.CommandType = CommandType.Text
        '    cmd.CommandText = "SELECT fullname FROM Directory WHERE (location = @location)"
        '    myconnection.Open()
        '    Dim parm As SqlParameter = cmd.CreateParameter()
        '    parm.ParameterName = "@location"
        '    parm.Value = Session("location")
        '    cmd.Parameters.Add(parm)
        '    Dim i As Object = cmd.ExecuteScalar()

        '    DropDownList1.DataSourceID = ""
        '    DropDownList1.DataSource = i()
        '    DropDownList1.DataBind()
        'End If
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT * FROM staff_details WHERE (staff = @staff)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@staff"
        parm.Value = DropDownList1.SelectedValue
        cmd.Parameters.Add(parm)

        Dim i As SqlDataReader = cmd.ExecuteReader()

        Dim name, depart, levels, designation, empdate, supervisor, updatedate, abasic, agross, mbasic, mgross

        If Not i Is Nothing Then

            If i.Read() Then

                name = i("staff")
                depart = i("Department")
                levels = i("levels")
                designation = i("designation")
                empdate = i("employed_date")
                supervisor = i("supervisor")
                updatedate = i("update_date")
                Session("checkname") = name

                txtEmpDate.Text = empdate

                DropDownList3.SelectedItem.Text = levels
                txtDesignation.Text = designation
            End If
        Else
            txtEmpDate.Text = ""

            DropDownList3.SelectedItem.Text = ""
            txtDesignation.Text = ""
        End If
        myconnection.Close()
        '--------------------Select Renumeration--------------------------
        Dim cmd1 As SqlCommand = myconnection.CreateCommand()
        cmd1.CommandType = CommandType.Text
        cmd1.CommandText = "SELECT * FROM Remuneration WHERE (staff = @staff)"
        myconnection.Open()
        Dim parm1 As SqlParameter = cmd.CreateParameter()
        parm1.ParameterName = "@staff"
        parm1.Value = DropDownList1.SelectedValue
        cmd1.Parameters.Add(parm1)
        Dim i1 As SqlDataReader = cmd1.ExecuteReader()

        If Not i1 Is Nothing Then


            If i1.Read() Then
                abasic = i1("annual_basic")
                agross = i1("annual_gross")
                mbasic = i1("monthly_basic")
                mgross = i1("monthly_gross")

                txtAnnualBasic.Text = abasic
                txtAnnualGross.Text = agross
                txtMonthlyBasic.Text = mbasic
                txtMonthlyGross.Text = mgross
            End If
        Else
            txtAnnualBasic.Text = ""
            txtAnnualGross.Text = ""
            txtMonthlyBasic.Text = ""
            txtMonthlyGross.Text = ""
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmdname As SqlCommand = myconnection.CreateCommand()
        cmdname.CommandType = CommandType.Text
        cmdname.CommandText = "SELECT level_id FROM staff_level WHERE (description = @description)"
        myconnection.Open()
        Dim cml As SqlParameter = cmdname.CreateParameter()
        cml.ParameterName = "@description"
        cml.Value = DropDownList3.Text
        cmdname.Parameters.Add(cml)
        Dim levelid As Object = cmdname.ExecuteScalar()

        Dim infos As New DirectoryTableAdapters.staff_detailsTableAdapter()
        Dim infos2 As New DirectoryTableAdapters.RemunerationTableAdapter
        Dim infos3 As New DirectoryTableAdapters.directoryTableAdapter

        If Not Session("checkname") Is Nothing Then
            infos.UpdateStaffDetails(DropDownList1.Text, drpDepart1.Text, DropDownList3.Text, txtDesignation.Text, "", DropDownList2.Text, Now.Date, DropDownList1.Text)
            infos2.UpdateRenumeration(DropDownList1.Text, txtAnnualBasic.Text, txtAnnualGross.Text, txtMonthlyBasic.Text, txtMonthlyGross.Text, DropDownList1.Text)
            infos3.UpdateStaffDirectory(drpDepart1.Text, DropDownList2.Text, levelid, DropDownList1.SelectedValue)
            lblMessage.Text = "Staff Information updated successfully"
            lblMessage.ForeColor = Drawing.Color.Blue
            Response.AppendHeader("Refresh", "5; URL=Default.aspx")
        Else
            infos.InsertStaffDetails(DropDownList1.Text, drpDepart1.Text, DropDownList3.Text, txtDesignation.Text, "", DropDownList2.Text, Now.Date)
            infos2.InsertRenumeration(DropDownList1.Text, txtAnnualBasic.Text, txtAnnualGross.Text, txtMonthlyBasic.Text, txtMonthlyGross.Text)
            infos3.UpdateStaffDirectory(drpDepart1.Text, DropDownList2.Text, levelid, DropDownList1.SelectedValue)
            lblMessage.Text = "Staff Information updated successfully"
            lblMessage.ForeColor = Drawing.Color.Blue
            Response.AppendHeader("Refresh", "5; URL=Default.aspx")
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub
End Class
