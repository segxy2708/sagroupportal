Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Partial Class CreateUsers
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Create Users Page"
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
        Me.idTxt.Text = (Convert.ToInt32(getMaxUser) + 1).ToString
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    'Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
    '    Me.RequestPan.Visible = True
    '    Me.SubmitBut.Visible = False
    '    Me.RequestPan.Focus()

    'End Sub

    Sub cleared()
        Me.PhoneNoTxt.Text = ""
        Me.faxTxt.Text = ""
        Me.StateList.SelectedIndex = 0
        Me.address1Txt.Text = ""
        Me.address2Txt.Text = ""
        Me.emailTxt.Text = ""
        Me.departmentList.SelectedIndex = 0
        Me.orgList.SelectedIndex = 0
        Me.buildingList.SelectedIndex = 0
        Me.LastNameTxt.Text = ""
        Me.FirstNameTxt.Text = ""
        Me.titleList.SelectedIndex = 0
        Me.uNametxt.Text = ""
        Me.pass1.Text = ""
        Me.pass2.Text = ""
        Me.emailTxt.Text = ""
        Me.CountryList.SelectedIndex = 0
        Me.UlevList.SelectedIndex = 0
        Me.BranchList.SelectedIndex = 0
        Me.SexList.SelectedIndex = 0
    End Sub
    Public Function getMaxUser()
        Dim str As String
        Dim query As String = "Select MAX(ID) from directory "
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function


    Function check_pic_ext(ByVal fileuploaded As FileUpload) As Boolean
        If fileuploaded.PostedFile.FileName.EndsWith("jpeg") Or fileuploaded.PostedFile.FileName.EndsWith("jpg") Or _
            fileuploaded.PostedFile.FileName.EndsWith("bmp") Or fileuploaded.PostedFile.FileName.EndsWith("png") Or _
            fileuploaded.PostedFile.FileName.EndsWith("tif") Then
            Return True
        End If
        Return False
    End Function

    Function check_cntl() As Boolean
        If Me.departmentList.SelectedIndex = 0 Or Me.StateList.SelectedIndex = 0 Or Me.orgList.SelectedIndex = 0 Or Me.BranchList.SelectedIndex = 0 Then
            Return False
        End If
    End Function


    Protected Sub SubmitBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SubmitBut.Click

        'Dim picLoc As String
        'Dim myis As Integer = CType(getMaxUser(), Integer) + 1
        Dim filename As String = Path.GetFileName(FileUpload1.FileName)
        Dim savePaths As String = "C:\Inetpub\wwwroot\sagroupportal\" + Session("names") + "\Images"
        Dim fulnam As String = Me.FirstNameTxt.Text & " " & Me.LastNameTxt.Text
        Dim mypath As String = fulnam + "\Profile Images\" + FileUpload1.FileName.ToString
        Dim mypaths As String = fulnam + "\Profile Images"
        Dim its As Integer = (Convert.ToInt32(getMaxUser) + 1)
        Dim fulnamParam As New System.Web.UI.WebControls.Parameter("fullname", TypeCode.String, fulnam)

        If Me.departmentList.SelectedIndex = 0 Or Me.StateList.SelectedIndex = 0 Or Me.orgList.SelectedIndex = 0 Or Me.BranchList.SelectedIndex = 0 Then
            Me.errMess.Text = "Department or State of Origin or Organisation or Branch not selected"
            Me.errMess.Focus()
        Else

            If Me.FileUpload1.HasFile Then
                If Me.FileUpload1.PostedFile.ContentLength < 10240000 Then

                    Try
                        System.IO.Directory.CreateDirectory(Server.MapPath(".\") & mypaths)
                        
                    Catch ex As Exception
                        'System.Windows.Forms.MessageBox.Show("error", "error", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly, False)
                        Me.Title = ex.Message
                    End Try
                    Try
                        System.IO.Directory.CreateDirectory(Server.MapPath(".\") & fulnam + "\My Documets")
                        Try
                            System.IO.Directory.CreateDirectory(Server.MapPath(".\") & fulnam + "\My Documets" & "\docs")
                        Catch ex As Exception

                        End Try
                        Try
                            System.IO.Directory.CreateDirectory(Server.MapPath(".\") & "\My Documets" & "\")
                        Catch ex As Exception

                        End Try
                    Catch ex As Exception

                    End Try

                    If check_pic_ext(FileUpload1) = True Then
                        Try

                            Dim myfileloc As New System.Web.UI.WebControls.Parameter("picloc", TypeCode.String, mypath)
                            Dim idnum As New System.Web.UI.WebControls.Parameter("ID", TypeCode.Int32, its.ToString)
                            FileUpload1.SaveAs(Server.MapPath(".") & "\" & mypath)

                            Me.SubmitSource.InsertParameters.Add(idnum)
                            Me.SubmitSource.InsertParameters.Add(myfileloc)
                            Me.SubmitSource.InsertParameters.Add(fulnamParam)

                            Me.SubmitSource.Insert()
                            'System.Windows.Forms.MessageBox.Show("")
                            Me.errMess.Text = "user " & fulnam & " created successfully"
                            Me.errMess.Focus()
                            Response.AppendHeader("Refresh", "2,URL=CreateUsers.aspx")
                            cleared()
                        Catch ex As Exception
                            Session("errorMessage") = ex.Message
                            Me.errMess.Text = ex.Message '"Error Creating User"
                            Me.Focus()
                        End Try
                    Else
                        Me.errMess.Text = "Invalid picture format"
                    End If


                Else
                    Me.errMess.Text = "File too large"
                    Me.errMess.Focus()
                End If

            Else
                Me.errMess.Text = "Nofile content"
                Me.errMess.Focus()
            End If
        End If
        Me.idTxt.Text = (Convert.ToInt32(getMaxUser) + 1).ToString

    End Sub

    Protected Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click
        Response.AppendHeader("Refresh", "2,URL=CreateUsers.aspx")
    End Sub
End Class
