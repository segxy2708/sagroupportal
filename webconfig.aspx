﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="webconfig.aspx.vb" Inherits="webconfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <h2>
        Read ASP.NET Configuration</h2>
    <p>
        This page displays the value of the <b>Impersonate</b> attribute of the <b>identity</b>
        section of an ASP.NET configuration.
    </p>
    <h3>
        Results</h3>
    <p>
        <asp:Label ID="ConfigId" BackColor="#dcdcdc" BorderWidth="1" runat="Server" /></p>
    </div>
    </form>
</body>
</html>
