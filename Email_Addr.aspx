<%@ Page Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="Email_Addr.aspx.vb" Inherits="Email_Addr" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="emails"
        Height="50px" Width="125px">
        <Fields>
            <asp:BoundField DataField="fullname" HeaderText="fullname" SortExpression="fullname" />
            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
        </Fields>
    </asp:DetailsView>
    <asp:SqlDataSource ID="emails" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT [fullname], [email] FROM [directory] WHERE ([location] = @location)">
        <SelectParameters>
            <asp:SessionParameter Name="location" SessionField="group" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">

</asp:Content>

