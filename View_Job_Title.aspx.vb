
Partial Class View_Job_Title
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Unresolved Request"
        Me.LinkButton1.Text = "Show Treated Ticket"
        Me.LinkButton2.Text = "Hide Treated Ticket"
        Me.LinkButton2.Visible = False
        Response.AppendHeader("Refresh", "10 URL=view_job_title.aspx")
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Me.Panel2.Visible = True
        Me.LinkButton1.Visible = False
        Me.LinkButton2.Visible = True
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Me.Panel2.Visible = False
        Me.LinkButton2.Visible = False
        Me.LinkButton1.Visible = True
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        GridView1.DataBind()
    End Sub
End Class
