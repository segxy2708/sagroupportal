Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Partial Class Marketers_movement
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("MarketerConnectionString").ConnectionString
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Display Date
        If Session("Department") = "Marketing" Or Session("id") = 363 Or Session("id") = 590 Or Session("id") = 13 Then
            Panel1.Visible = True
        Else
            'lblDate.Visible
            Label2.Text = "You are not Eligible to view this portal"
            Panel1.Visible = False
            Label2.ForeColor = Drawing.Color.Red
        End If
        lblDate.Text = Now.Date

        'Check the session, if is still active or not
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
        'HttpContext.Current.Response.Redirect("session_timeout.aspx")
        Try

            'Retrive id of the Marketer selected

            Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
            Dim cmd As SqlCommand = myconnection.CreateCommand()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = "SELECT id FROM directory WHERE (fullname like @fullname)"
            myconnection.Open()
            Dim parm As SqlParameter = cmd.CreateParameter()
            parm.ParameterName = "@fullname"
            parm.Value = "%" & Trim(DropDownList2.Text) & "%"
            cmd.Parameters.Add(parm)
            Dim i As Object = cmd.ExecuteScalar()

        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.ForeColor = Drawing.Color.Red
        End Try

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        'Try
        '--------------Retrieve Marketers id from Directory Table on the foxsuite1 Database
        DataList1.Visible = True
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT id FROM directory WHERE (fullname like @fullname)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@fullname"
        parm.Value = "%" & Trim(DropDownList2.Text) & "%"
        cmd.Parameters.Add(parm)
        Dim i As Object = cmd.ExecuteScalar()

        '--------------Retrieve Marketers Location from Directory Table on the foxsuite1 Database
        Dim cmd2 As SqlCommand = myconnection.CreateCommand()
        cmd2.CommandType = CommandType.Text
        cmd2.CommandText = "SELECT location FROM Directory WHERE (id = @id)"
        'myconnection.Open()
        Dim parm3 As SqlParameter = cmd.CreateParameter()
        parm3.ParameterName = "@id"
        parm3.Value = i
        cmd2.Parameters.Add(parm3)
        Dim company As Object = cmd2.ExecuteScalar()


        Dim se As New MarketerTableAdapters.MovementTableAdapter
        se.UpdateMovement("Close", CInt(i))
        se.InsertMovement(Now.Date, DropDownList2.Text, txtLocation.Text, txtClient.Text, txtAddress.Text, "Open", CInt(i), company)
        lblMessage.Text = "New Location had been set for " & DropDownList2.Text & " successfully"
        Response.AppendHeader("Refresh", "5; URL=Marketers_movement.aspx")
        'Catch ex As Exception
        '    lblMessage.Text = ex.Message
        '    lblMessage.ForeColor = Drawing.Color.Red
        'End Try
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        Response.AppendHeader("Refresh", "1; URL=Marketers_movement.aspx")
        DataList1.Visible = False
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT id FROM directory WHERE (fullname like @fullname)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@fullname"
        parm.Value = "%" & Trim(DropDownList2.Text) & "%"
        cmd.Parameters.Add(parm)
        Dim i As Object = cmd.ExecuteScalar()

        Dim cmd2 As SqlCommand = myconnection.CreateCommand()
        cmd2.CommandType = CommandType.Text
        cmd2.CommandText = "SELECT location FROM Directory WHERE (id = @id)"
        'myconnection.Open()
        Dim parm3 As SqlParameter = cmd.CreateParameter()
        parm3.ParameterName = "@id"
        parm3.Value = i
        cmd2.Parameters.Add(parm3)
        Dim company As Object = cmd2.ExecuteScalar()

        If i Is Nothing Then
            Label1.Visible = False
            LinkButton1.Visible = False
            LinkButton2.Visible = False
            Try
                Dim put As New MarketerTableAdapters.MovementTableAdapter
                put.UpdateMovement("Close", i)
                put.InsertMovement(Now.Date, DropDownList2.Text, txtLocation.Text, txtClient.Text, txtAddress.Text, "Open", CInt(i), company.ToString)
                lblMessage.Text = "Location had been set for " & DropDownList2.Text & " successfully"
                Response.AppendHeader("Refresh", "5; URL=Marketers_movement.aspx")
            Catch ex As Exception
                lblMessage.Text = ex.Message
                lblMessage.ForeColor = Drawing.Color.Red
            End Try
        Else
            Label1.Visible = True
            LinkButton1.Visible = True
            LinkButton2.Visible = True

        End If


    End Sub

    Protected Sub DropDownList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownList2.SelectedIndexChanged
        DataList1.Visible = True

        DataList1.DataSourceID = ""
        Dim i As New MarketerTableAdapters.MovementTableAdapter

        DataList1.DataSource = i.GetMarketerByNameandDate("%" & DropDownList2.Text & "%", lblDate.Text)
        DataList1.DataBind()

    End Sub


End Class
