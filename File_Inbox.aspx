<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="File_Inbox.aspx.vb" Inherits="File_Inbox" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table >
<tr>
<td>
</td>
<td style="width: 750px">
    <asp:LinkButton ID="LinkButton1" runat="server">My Files</asp:LinkButton>
    &nbsp;&nbsp; &nbsp;&nbsp;
    <asp:LinkButton ID="LinkButton2" runat="server" Visible="False">Department Files</asp:LinkButton>&nbsp;
    &nbsp; &nbsp; &nbsp;
    <asp:LinkButton ID="LinkButton3" runat="server" Visible="False">Group Files</asp:LinkButton>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:LinkButton ID="myGrpFileBut"
        runat="server">My Group File</asp:LinkButton>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</td>
<td>
    <asp:LinkButton ID="LinkButton4" runat="server">Hide</asp:LinkButton></td>
</tr>
<tr>
<td align="right" valign="middle">
    </td>
<td style="height: 250px; width: 750px;">
    &nbsp;<br />
    <br />
    <br />
    <asp:Panel ID="MyFilePan" runat="server" Height="600px" Visible="False" Width="100%"
        Wrap="False" ScrollBars="Auto" BorderStyle="Solid" BorderWidth="1px">
        <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" DataSourceID="getFile" ForeColor="#333333" 
            GridLines="None" Width="100%" AllowSorting="True" Height="600px" 
            EnableModelValidation="True">
            <RowStyle BackColor="#E3EAEB" />
            <Columns>
                <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName" />
                <asp:TemplateField HeaderText="File" >
                    <ItemTemplate>
                        <asp:HyperLink ID="lik1" runat="server" ImageUrl="~/images/category.gif" NavigateUrl='<%# Eval("FileLoc") %>'
                            Text='<%# Eval("FileLoc") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Sender" HeaderText="Sender" SortExpression="Sender" />
                <asp:BoundField DataField="FileSize" HeaderText="FileSize(Kb)" SortExpression="FileSize" />
                <asp:BoundField DataField="FileType" HeaderText="FileType" SortExpression="FileType" />
            </Columns>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#7C6F57" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
        <br />
        <asp:DataList ID="DataList2" runat="server" DataSourceID="getFile" DataKeyField="Location" Visible="False">
            <HeaderTemplate>
               <table border="0" >
                    <tr style="background-color: #99ff99;">
                        <!-- <td style="width:9px; border-style :solid " >
                             <asp:Label ID="FileDir" runat="server" Text="Select All" Width="20px"></asp:Label>&nbsp; Select All
                            <input  id="chkAll" onclick ="javascript:SelectAllCheckboxes(this);" runat ="server" type ="checkbox" title ="Select All"  />
                        </td> -->
                        <td style="width: 360px;"> 
                        <span >Filename</span>
                            
                        </td>
                       <!-- 
                        
                        <td style="width: 100px;">
                        <span >Size</span>
                            
                        </td>
                        <td style="width:150px;">
                            Type
                        </td> -->
                        
                        <td style ="width:200px;">
                        <!-- <asp:Label ID="sender" runat ="server" Text ="Sender"></asp:Label> -->
                        <span>Sender</span>
                        </td>
                        <td style="width: 70px" >
                        <span >Open</span>
                            
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                  <table>
                    <tr >
                        <!-- <td style="text-align :left ; border-style :solid ;" align ="left"  >
                            <asp:CheckBox ID="chkList" runat ="server" /></td> -->
                        <td   style="width :350px;"  >
                            <asp:Label ID="LocationLabel" runat="server" Text='<%# Eval("FileName") %>' ></asp:Label></td>
                        <!-- 
                        
                        <td style="border-style :solid ;" align ="left"  >
                            <asp:Label ID="sizeLab" runat="server" Text='<%# Eval("FileSize") %>'></asp:Label></td>
                        <td style="border-bottom: 1px solid; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;">
                            <asp:Label ID="FileTypeLab" runat="server" Text='<%# Eval("FileType") %>'></asp:Label></td> -->
                            
                        <td style ="width:200px" > 
                        <asp:label ID="send" runat ="server" Text ='<%# Eval("Sender") %>'></asp:label></td>
                        <td >
                            <asp:HyperLink ID="file_link" runat="server"  NavigateUrl='<%# Eval("FileLoc") %>'
                                Text='<%# Eval("Location") %>' ImageUrl="~/images/category.gif"></asp:HyperLink>
                        </td>
                        </tr> 
                </table>
                <br />
            </ItemTemplate>
        </asp:DataList></asp:Panel>
    &nbsp;&nbsp;<br />
    <br />
    <asp:Panel ID="Panel1" runat="server" Height="100%" ScrollBars="Auto" Width="100%" Visible="False">
        <asp:DataList ID="DataList1" runat="server" DataSourceID="mGrpFiles" DataKeyField="Location">
            <HeaderTemplate>
                <table border="0" >
                    <tr style="background-color: #99ff99;">
                        <!-- <td style="width:9px; border-style :solid " >
                             <asp:Label ID="FileDir" runat="server" Text="Select All" Width="20px"></asp:Label>&nbsp; Select All
                            <input  id="chkAll" onclick ="javascript:SelectAllCheckboxes(this);" runat ="server" type ="checkbox" title ="Select All"  />
                        </td> -->
                        <td style="width: 360px;">
                            <span >Filename</span>
                        </td>
                        <!-- 
                        
                        <td style="width: 100px;">
                        <span >Size</span>
                            
                        </td>
                        <td style="width:150px;">
                            Type
                        </td> -->
                        <td style ="width:200px;">
                            <!-- <asp:Label ID="sender" runat ="server" Text ="Sender"></asp:Label> -->
                            <span>Sender</span>
                        </td>
                        <td style="width: 70px" >
                            <span >Open</span>
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                <table>
                    <tr >
                        <!-- <td style="text-align :left ; border-style :solid ;" align ="left"  >
                            <asp:CheckBox ID="chkList" runat ="server" /></td> -->
                        <td   style="width :350px;"  >
                            <asp:Label ID="LocationLabel" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></td>
                        <!-- 
                        
                        <td style="border-style :solid ;" align ="left"  >
                            <asp:Label ID="sizeLab" runat="server" Text='<%# Eval("FileSize") %>'></asp:Label></td>
                        <td style="border-bottom: 1px solid; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;">
                            <asp:Label ID="FileTypeLab" runat="server" Text='<%# Eval("FileType") %>'></asp:Label></td> -->
                        <td style ="width:200px" >
                            <asp:Label ID="send" runat="server" Text='<%# Eval("Sender") %>'></asp:Label></td>
                        <td >
                            <asp:HyperLink ID="file_link" runat="server" ImageUrl="~/images/category.gif" NavigateUrl='<%# Eval("FileLoc") %>'
                                Text='<%# Eval("Location") %>'></asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <br />
            </ItemTemplate>
        </asp:DataList><asp:SqlDataSource ID="mGrpFiles" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
            SelectCommand="SELECT [User], FileName, Location, FileType, FileSize, DateCreated, TimeCreated, FileLoc, Sender, GroupName FROM User_File_Details WHERE ([User] = @User ) AND (GroupName IS NOT NULL) ORDER BY DateCreated DESC, TimeCreated DESC">
            <SelectParameters>
                <asp:SessionParameter Name="User" SessionField="names" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </asp:Panel>
</td>
<td>
    &nbsp;<asp:SqlDataSource ID="getFile" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        
        SelectCommand="SELECT Location, FileName, FileType, FileSize, FileLoc, Sender FROM User_File_Details WHERE ([User] = @User ) ORDER BY EntryDate DESC, TimeCreated DESC, DateCreated DESC">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="names" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="DeptSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT [Location], [FileName], [FileType], [FileSize], [FileLoc] FROM [User_File_Details] WHERE ([User] = @User) ORDER BY [DateCreated] DESC, [TimeCreated] DESC">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="Department" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="GropuSource" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT [Location], [FileName], [FileType], [FileSize], [FileLoc] FROM [User_File_Details] WHERE ([User] = @User) ORDER BY [DateCreated] DESC, [TimeCreated] DESC">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="Group" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</td>
</tr>
<tr>
<td align="right" valign="middle">
    <asp:LinkButton ID="LinkButton5" runat="server">Hide</asp:LinkButton></td>
<td style="height: 100px; width: 750px;">
    &nbsp;<asp:Panel ID="DeptPan" runat="server" Height="100%" Visible="False" Width="100%" ScrollBars="Auto" BorderStyle="Solid" BorderWidth="1px">
        <asp:DataList ID="DeptList" runat="server" DataSourceID="DeptSource" DataKeyField="Location">
            <HeaderTemplate>
                <table >
                    <tr style="width: 80%;">
                        <td style="width: 50px;">
                             <asp:Label ID="FileDir" runat="server" Text="Select All"></asp:Label> 
                            <input id="chkAll" onclick ="javascript:SelectAllCheckboxesDept(this);" runat ="server" type ="checkbox" title ="Select All"  />
                        </td>
                        <td style="width: 120px;">
                            <asp:Label ID="Filename" runat="server" Text="File"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="bre" runat="server" Text="Link"></asp:Label>
                        </td>
                        <td style="width: 30px;">
                            <asp:Label ID="FileLoc" runat="server" Text=""></asp:Label>
                        </td>
                        <td style="width: 50px;">
                            <asp:Label ID="FileSize" runat="server" Text="Size"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="FileType" runat="server" Text="Type"></asp:Label>
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                &nbsp;
                <table >
                    <tr>
                        <td style="width: 50px;">
                            <asp:CheckBox ID="chkList" runat ="server" />
                        </td>
                        <td style="width: 120px;">
                            <asp:Label ID="LocationLabel" runat="server" Text='<%# Eval("FileName") %>'></asp:Label><br />
                        </td>
                        <td>
                            <asp:HyperLink ID="file_link" runat="server"  NavigateUrl='<%# Eval("FileLoc") %>'
                                Text='<%# Eval("Location") %>'></asp:HyperLink>
                        </td>
                        <td style="width: 30px;">
                        </td>
                        <td style="width: 50px;">
                            <asp:Label ID="sizeLab" runat="server" Text='<%# Eval("FileSize") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="FileTypeLab" runat="server" Text='<%# Eval("FileType") %>'></asp:Label>
                        </td>
                </table>
                <br />
            </ItemTemplate>
        </asp:DataList></asp:Panel>
</td>
<td>
</td>
</tr>
<tr>
<td align="right" valign="middle">
    <asp:LinkButton ID="LinkButton6" runat="server">Hide</asp:LinkButton></td>
<td style="height: 100px; width: 750px;">
    &nbsp;<br />
    <asp:Panel ID="GroupPan" runat="server" Height="100%" Visible="False" Width="100%" ScrollBars="Auto" BorderStyle="Solid" BorderWidth="1px">
        <asp:DataList ID="GroupList" runat="server" DataSourceID="GropuSource" DataKeyField="Location">
            <HeaderTemplate>
                <table >
                    <tr style="width: 80%;">
                        <td style="width: 50px;">
                            <asp:Label ID="FileDir" runat="server" Text="Select All"></asp:Label>
                            <input id="chkAll" onclick ="javascript:SelectAllCheckboxesGrp(this);" runat ="server" type ="checkbox" title ="Select All"  />
                        </td>
                        <td style="width: 120px;">
                            <asp:Label ID="Filename" runat="server" Text="File"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="bre" runat="server" Text="Link"></asp:Label>
                        </td>
                        <td style="width: 30px;">
                            <asp:Label ID="FileLoc" runat="server" Text=""></asp:Label>
                        </td>
                        <td style="width: 50px;">
                            <asp:Label ID="FileSize" runat="server" Text="Size"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="FileType" runat="server" Text="Type"></asp:Label>
                        </td>
                    </tr>
                </table>
            </HeaderTemplate>
            <ItemTemplate>
                &nbsp;
                <table >
                    <tr>
                        <td style="width: 50px;">
                            <asp:CheckBox ID="chkList" runat ="server" />
                        </td>
                        <td style="width: 120px;">
                            <asp:Label ID="LocationLabel" runat="server" Text='<%# Eval("FileName") %>'></asp:Label><br />
                        </td>
                        <td>
                            <asp:HyperLink ID="file_link" runat="server" ImageUrl="images/category.gif" NavigateUrl='<%# Eval("FileLoc") %>'
                                Text='<%# Eval("Location") %>'></asp:HyperLink>
                        </td>
                        <td style="width: 30px;">
                        </td>
                        <td style="width: 50px;">
                            <asp:Label ID="sizeLab" runat="server" Text='<%# Eval("FileSize") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="FileTypeLab" runat="server" Text='<%# Eval("FileType") %>'></asp:Label>
                        </td>
                </table>
                <br />
            </ItemTemplate>
        </asp:DataList></asp:Panel>
</td>
<td>
</td>
</tr>
<tr >
<td style="width: 100px" >
    
</td>
<td style="width: 750px">
    &nbsp;<asp:Label Visible ="False" ID="FileOwner" runat ="server" ></asp:Label>&nbsp;
    <asp:Button ID="Button1" runat="server" Text="Button" Visible="False" />
    <asp:SqlDataSource ID="fileInbox" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT [File_Type] FROM [User_File_Details] WHERE ([User] = @User)">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="usernames" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    &nbsp;&nbsp;
    <asp:GridView ID="GridView1" runat="server" DataSourceID="getFile" 
        Visible="False" EnableModelValidation="True">
    <Columns >
    <asp:BoundField DataField ="Location" />
    <asp:BoundField DataField="FileType" />
    </Columns>
    </asp:GridView>
</td>
<td style="width: 100px">
    </td>
</tr>
<tr>
<td>
</td>
<td style="width: 750px">
    &nbsp;</td>
</tr>
</table>
    
    <asp:SqlDataSource ID="getfiles" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
        SelectCommand="SELECT Location, FileName, FileType, FileSize, FileLoc FROM User_File_Details WHERE ([User] = @User ) ORDER BY DateCreated DESC, TimeCreated DESC">
        <SelectParameters>
            <asp:SessionParameter Name="User" SessionField="names" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    &nbsp;<br />
</asp:Content>

