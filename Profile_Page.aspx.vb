Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO


Partial Class Profile_Page
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter
    Dim im As Image
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.mobileTxtH.Visible = False
        HideChangeProfile()
        HideImageControl()
        Me.ContactInfoPan.Visible = False
        Me.namelab.Text = Session("names")
        Dim userImage As Image = New Image
        userImage.ImageUrl = Session("myImageUrl")
        userImage.AlternateText = Session("names")
        userImage.Height = 100
        userImage.Width = 100
        Me.ImagePanel.Controls.Add(userImage)
        profiles()
        Me.Title = Session("names") + " Profile Page"
        Me.imgPan.Visible = False
        Me.messages.Text = Session("profilemessage")
        Me.statMsg.Text = ""
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Response.Redirect("view_staff_profile.aspx")
        'ImagePanel.Visible = False
    End Sub

    
    Protected Sub CName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CName.Click
        ShowChangeProfile()
        Me.myProfPan.Visible = False
    End Sub
    Private Sub ShowChangeProfile()
        Me.cunamelab.visible = True
        Me.ufirst.visible = True
        Me.uSecond.Visible = True
        Me.CULinkBut.Visible = True
        Me.InfoPanel.Visible = True
    End Sub

    Private Sub HideChangeProfile()
        Me.cunamelab.visible = False
        Me.ufirst.visible = False
        Me.uSecond.Visible = False
        Me.CULinkBut.Visible = False
        Me.InfoPanel.Visible = False
    End Sub

    Private Sub ShowImageControl()
        Me.ImChPanel.Visible = True
        Me.ChImBut.Visible = True
        Me.chImLab.Visible = True
        Me.ImageUpload.Visible = True
    End Sub

    Private Sub HideImageControl()
        Me.ImChPanel.Visible = False
        Me.ChImBut.Visible = False
        Me.chImLab.Visible = False
        Me.ImageUpload.Visible = False
    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
        ShowImageControl()
        Me.myProfPan.Visible = False
    End Sub

    Protected Sub CULinkBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CULinkBut.Click
        Dim strs As String = getPassword()
        If uFirst.Text = strs Then
            Me.PasswdUpdateSource.Update()
            ShowChangeProfile()
            Me.errMsg.Text = "Password Changed Successfully"
            Me.errMsg.Visible = True

            Response.AppendHeader("Refresh", "10; URL=default.aspx")
        Else
            Me.ShowChangeProfile()
            Me.errMsg.Text = "Invalid Old Password"
            Me.errMsg.Visible = True
        End If
    End Sub

    Private Function getPassword() As String
        Dim name As String = Session("names")
        Dim str As String
        Dim query As String = "Select (pw) from directory where fullname= '" + name + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Protected Sub LinkButton4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton4.Click
        'profiles()
        Me.myProfPan.Visible = True
    End Sub

    Private Function getParams(ByVal s As String, ByVal q As String) As String
        Dim str As String
        Dim query As String = "Select (" + q + ") from directory where fullname='" + s + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function

    Sub profiles()
        Dim profilename As String = Session("names")
        Dim welmesg As String = profilename + "'s Profile"
        Me.mesg.Text = welmesg
        im = New Image
        im.ID = "images1"
        If getParams(profilename, "picloc") = Nothing Or getParams(profilename, "picloc") = "" Then
            im.ImageUrl = "/images/AVATAR_19.jpg"
        Else
            im.ImageUrl = getParams(profilename, "picloc")
        End If

        im.AlternateText = "image"
        im.Height = 120
        im.Width = 120
        Me.imgPan.Controls.Add(im)
        Me.firstNameLab.Text = getParams(profilename, "first")
        Me.lastnameLab.Text = getParams(profilename, "last")
        Me.sexLab.Text = getParams(profilename, "Sex")
        Me.divLab.Text = getParams(profilename, "location")
        Me.DOBlab.Text = getParams(profilename, "zipcode")
        Me.deptLab.Text = getParams(profilename, "Department")
        Me.StateLab.Text = getParams(profilename, "State")
        Me.emailLab.Text = getParams(profilename, "email")
        Me.phoneNo.Text = getParams(profilename, "phone")
        Me.addressLabs.Text = getParams(profilename, "Address1")
    End Sub

    Protected Sub ChImBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChImBut.Click
        Dim filename = Me.ImageUpload.PostedFile.FileName

        Try
            Dim savepath As String = newPath(Me.ImageUpload.FileName, Server.MapPath(".") & "\" & Session("names") & "\" & "Profile Image")
            If Me.ImageUpload.HasFile Then
                If Me.ImageUpload.PostedFile.ContentLength < 2048000 Then
                    'create profile images directory
                    Try
                        System.IO.Directory.CreateDirectory(Server.MapPath(".") & "\" & Session("names") & "\" & "Profile Image")
                    Catch ex As Exception

                    End Try


                    Try
                        'Dim Tpath As String = Server.MapPath(".") & "\" & "Profile Image\" & filename
                        Dim path As String = Session("names") & "\" & "profile Image\" & savepath

                        Dim picLocation As New Web.UI.WebControls.Parameter("picloc", TypeCode.String, path)
                        Dim locs As New Web.UI.WebControls.Parameter("ImagePath", TypeCode.String, path)
                        Me.ImageUpload.SaveAs(Server.MapPath(".") & "\" & Session("names") & "\" & "\Profile Image\" & savepath)
                        Me.ChProImg.InsertParameters.Add(locs)
                        Me.ChProImg.Insert()
                        Me.chPicSource.UpdateParameters.Add(picLocation)
                        Me.chPicSource.Update()
                        Response.AppendHeader("Refresh", "5; URL=Profile_Page.aspx")
                        messages.Visible = True
                        messages.Text = "Profile Image Changed Successfully"
                        Session("myImageUrl") = path
                    Catch ex As Exception
                        messages.Text = "Profile Images Already Exist"
                        'System.Windows.Forms.MessageBox.Show(ex.Message, "error", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Error, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly, False)
                    End Try
                    messages.Visible = True
                    messages.Text = "Profile Image Uploaded and Changed Successfully"
                    Response.AppendHeader("Refresh", "10; URL=default.aspx")
                Else
                    Me.messages.Text = "Picture size too large"
                End If
            End If
        Catch ex As Exception
            Me.Title = ex.Message
        End Try
    End Sub

    Sub fileExist(ByVal path As String)
        Dim info As FileInfo = New FileInfo(path)
        Dim itExist As Boolean = info.Exists

    End Sub

    
    Protected Sub CCInfoBut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CCInfoBut.Click

        HideChangeProfile()
        HideImageControl()
        Me.ContactInfoPan.Visible = True
        Me.myProfPan.Visible = False
        Session("profilemessage") = ""
        
    End Sub

    Function newPath(ByVal filename As String, ByVal path As String) As String
        Dim tempFileName As String = ""
        Dim path2check As String = path + "\" + filename
        Try
            If File.Exists(path2check) Then
                Dim counter As Integer = 2
                While File.Exists(path2check)
                    filename = counter.ToString + filename
                    path2check = path + "\" + filename
                    counter = counter + 1
                End While
            End If
            Return filename
        Catch ex As Exception

        End Try
        
    End Function

    Function message4FileChange(ByVal old As String, ByVal new1 As String) As String
        Dim rtstr
        If old <> new1 Then
            rtstr = " your file " + old + " already exist. Filename has been change to  " + new1
        Else
            rtstr = ""
        End If
        Return rtstr
    End Function

    Protected Sub chAddr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chAddr.Click
        If chAddrTxt.Text = "" Or chAddrTxt.Text Is Nothing Then
            Me.statMsg.Text = "Please Enter Address"
        Else
            Me.chAddrSrc.Update()
            Me.statMsg.Text = "Address Changed Successfully"

        End If
        Me.ContactInfoPan.Visible = True

    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Try
        If Me.MobileNoTxt.Text = "" Or Me.MobileNoTxt.Text Is Nothing Then
            Me.statMsg.Text = "Please enter Mobile No"
        Else
            Me.mob_src.Update()
            Me.statMsg.Text = "Mobile No Change successfully"
        End If

        'Catch ex As Exception
        '    Me.statMsg.Text = ex.Message
        'End Try


    End Sub
End Class


