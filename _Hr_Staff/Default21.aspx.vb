
Partial Class _Hr_Staff_Default2
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("org") = "Legal/Company Secretary" Or Session("org") = "Legal" Or Session("org") = "Human Resources" Or Session("org") = "Human Capital" Or Session("org") = "EXECUTIVES" Then
            HyperLink1.Visible = True
            HyperLink2.Visible = True
            lblMessage.Visible = False

        Else
            HyperLink1.Visible = False
            HyperLink2.Visible = False
            lblMessage.Visible = True
            lblMessage.Text = "You are not eligible to view this portal"
            lblMessage.ForeColor = Drawing.Color.Red
        End If
    End Sub
End Class
