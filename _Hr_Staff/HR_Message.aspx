<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HR_Message.aspx.vb" Inherits="_Hr_Staff_HR_Message" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <table style="width: 552px; font-size: 10pt; font-family: Tahoma;">
                <tr>
                    <td colspan="3" style="text-align: center">
                        <strong>HR Message Portal</strong></td>
                </tr>
                <tr>
                    <td style="width: 107px">
                        News Headline:</td>
                    <td colspan="2">
                        <asp:TextBox ID="txtHeadline" runat="server" Width="392px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtHeadline"
                            ErrorMessage="News Headline Required">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr style="color: #000000">
                    <td style="width: 107px">
                        News Details:</td>
                    <td style="width: 174px">
                    </td>
                    <td style="width: 146px">
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox ID="txtNews" runat="server" Height="176px" TextMode="MultiLine" Width="504px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNews"
                            ErrorMessage="News Headline Required">*</asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:CheckBox ID="CheckBox1" runat="server" Text="Enable Mesage Now" /></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" BackColor="#E0E0E0"
                            BorderColor="#FF8000" BorderWidth="1px" Font-Names="Tahoma" Font-Size="Small" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 107px">
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Post News</asp:LinkButton></td>
                    <td style="width: 174px">
                    </td>
                    <td style="width: 146px">
                    </td>
                </tr>
            </table>
        </div>
    
    </div>
    </form>
</body>
</html>
