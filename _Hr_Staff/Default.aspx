﻿<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" title="Untitled Page" %>

<%@ Import Namespace="System"%>
<%@Import Namespace="System.data" %>
<%@Import Namespace="System.data.oledb"%>
<%@Import Namespace="System.data.sqlclient"%>
<%@Import Namespace="System.Configuration"%>
<%@Import Namespace="System.Collections"%>
<%@Import Namespace="System.Web"%>
<%@Import Namespace="System.Web.UI"%>
<%@Import Namespace="Microsoft.VisualBasic"%>
<%@Import Namespace="System.Web.UI.HTMLcontrols"%>
<%@Import Namespace="System.Web.UI.Webcontrols"%>
<%@Import Namespace="System.IO"%>
<%@Import Namespace="Microsoft"%>



<script runat="server">
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If
        
        If Session("Department") = "Legal/Company Secretary" Or Session("Department") = "Legal" Or Session("Department") = "Human Resources" Or Session("Department") = "Human Capital" Or Session("Department") = "EXECUTIVES" Then
            lblCompany.Text = Session("location")
            lblDepartment.Text = Session("org")
            lblApplicationDate.Text = Format(Now.Date, "MM/dd/yyyy")
        Else
            Response.Redirect("error.html")
        End If
    End Sub


    Protected Sub GridView1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        'Dim keys As Integer = GridView1.DataKeys(e.Item.ItemIndex).ToString()
        'Session("I") = keys
        'Response.Redirect(EditCommandColumn.ToString())
    End Sub

    Protected Sub drpDepart_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'If drpDepart.Text = "All" Then


        '    Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        '    Dim cmd As SqlCommand = myconnection.CreateCommand()
        '    cmd.CommandType = CommandType.Text
        '    cmd.CommandText = "SELECT fullname FROM Directory WHERE (location = @location)"
        '    myconnection.Open()
        '    Dim parm As SqlParameter = cmd.CreateParameter()
        '    parm.ParameterName = "@location"
        '    parm.Value = Session("location")
        '    cmd.Parameters.Add(parm)
        '    Dim i As Object = cmd.ExecuteScalar()

        '    DropDownList1.DataSourceID = ""
        '    DropDownList1.DataSource = i()
        '    DropDownList1.DataBind()
        'End If
    End Sub

    Protected Sub DropDownList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As SqlCommand = myconnection.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT * FROM staff_details WHERE (staff = @staff)"
        myconnection.Open()
        Dim parm As SqlParameter = cmd.CreateParameter()
        parm.ParameterName = "@staff"
        parm.Value = DropDownList1.Text
        cmd.Parameters.Add(parm)
        Dim i As SqlDataReader = cmd.ExecuteReader()
        
        
        Dim name, depart, levels, designation, empdate, supervisor, updatedate

        If i.Read() Then

            name = i("staff")
            depart = i("Department")
            levels = i("levels")
            designation = i("designation")
            empdate = i("employed_date")
            supervisor = i("supervisor")
            updatedate = i("update_date")
            Session("checkname") = name
            
            
            
        End If
 
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myconnection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmdname As SqlCommand = myconnection.CreateCommand()
        cmdname.CommandType = CommandType.Text
        cmdname.CommandText = "SELECT level_id FROM staff_level WHERE (description = @description)"
        myconnection.Open()
        Dim cml As SqlParameter = cmdname.CreateParameter()
        cml.ParameterName = "@description"
        cml.Value = DropDownList3.Text
        cmdname.Parameters.Add(cml)
        Dim levelid As Object = cmdname.ExecuteScalar()
        
        Dim infos As New DirectoryTableAdapters.staff_detailsTableAdapter
        Dim infos2 As New DirectoryTableAdapters.RemunerationTableAdapter
        Dim infos3 As New DirectoryTableAdapters.directoryTableAdapter
        
        If Not Session("checkname") Is Nothing Then
            infos.UpdateStaffDetails(DropDownList1.Text, drpDepart1.Text, DropDownList3.Text, txtDesignation.Text, txtEmpDate.Text, DropDownList2.Text, Now.Date, DropDownList1.Text)
            infos2.UpdateRenumeration(DropDownList1.Text, txtAnnualBasic.Text, txtAnnualGross.Text, txtMonthlyBasic.Text, txtMonthlyGross.Text, DropDownList1.Text)
            infos3.UpdateStaffDirectory(drpDepart1.Text, DropDownList2.Text, levelid, DropDownList1.SelectedValue)
            lblMessage.Text = "Staff Information updated successfully"
            lblMessage.ForeColor = Drawing.Color.Blue
            Response.AppendHeader("Refresh", "5; URL=Default.aspx")
        Else
            infos.InsertStaffDetails(DropDownList1.Text, drpDepart1.Text, DropDownList3.Text, txtDesignation.Text, txtEmpDate.Text , DropDownList2.Text, Now.Date)
            infos2.InsertRenumeration(DropDownList1.Text, txtAnnualBasic.Text, txtAnnualGross.Text, txtMonthlyBasic.Text, txtMonthlyGross.Text)
            infos3.UpdateStaffDirectory(drpDepart1.Text, DropDownList2.Text, levelid, DropDownList1.SelectedValue)
            lblMessage.Text = "Staff Information updated successfully"
            lblMessage.ForeColor = Drawing.Color.Blue
            Response.AppendHeader("Refresh", "5; URL=Default.aspx")
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Page.MasterPageFile = "~/" & Session("Theme")
            Page.Theme = "~/" & Session("Skin")
        Catch ex As Exception

        End Try
    End Sub
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
                    <table border="0" cellpadding="2" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" background="../../images/brownie.jpg" style="height: 23px">
                                <asp:Label ID="LblHeading" runat="server" CssClass="SectionName" ForeColor="DimGray"
                                    Text="..."></asp:Label>
                                <table style="width:100%;">
                                    <tr>
                                        <td colspan="6">
                                                                    <asp:Label ID="Label10" runat="server" CssClass="admintextbig2" Width="451px">Search 
                                                                    Criterial</asp:Label>
                                        </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                        <td>&nbsp;
                                            </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="text-align: left">
                                <asp:Panel ID="Panel2" runat="server">
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table border="1" bordercolor="silver" cellpadding="0" cellspacing="0" height="400"
                                    style="width: 494px">
                                        <tr>
                                            <td valign="top" class="style1">
                                                <table align="center" border="0" cellpadding="2" cellspacing="0" 
                                                style="700%; width: 906px;">
                                                    <tr>
                                                        <td align="center" style="width: 765px">
                                                            <asp:Label ID="lblError" runat="server"></asp:Label>
                                                            &nbsp;
                                                            <table border="0">
                                                                <tr>
                                                                    <td align="left">&nbsp;
</td>
                                                                    <td align="left" colspan="9">
                                                                        <asp:Label ID="Label11" runat="server" CssClass="admintextbold">Date:</asp:Label>
                                                                        <asp:Label ID="lblApplicationDate" runat="server"></asp:Label>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Company:<asp:Label ID="lblCompany" runat="server"></asp:Label>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="Label5" runat="server" CssClass="admintextbold">Department:</asp:Label>
                                                                        <asp:Label ID="lblDepartment" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" style="text-align: right">
                                                                        <asp:Label ID="Label1" runat="server" CssClass="admintextbold">Department:</asp:Label>
                                                                    </td>
                                                                    <td align="center" colspan="2" style="text-align: left">
                                                                        <asp:DropDownList ID="drpDepart" runat="server" AutoPostBack="True" 
                                                                            onselectedindexchanged="drpDepart_SelectedIndexChanged">
                                                                            <asp:ListItem>Engineering</asp:ListItem>
                                                                            <asp:ListItem>Admin</asp:ListItem>
                                                                            <asp:ListItem>Marketing</asp:ListItem>
                                                                            <asp:ListItem>Human Resources</asp:ListItem>
                                                                            <asp:ListItem>Finance</asp:ListItem>
                                                                            <asp:ListItem>Operations</asp:ListItem>
                                                                            <asp:ListItem>Information Technology</asp:ListItem>
                                                                            <asp:ListItem>Customer Care</asp:ListItem>
                                                                            <asp:ListItem>Human Capital</asp:ListItem>
                                                                            <asp:ListItem>Legal/Company Secretary</asp:ListItem>
                                                                            <asp:ListItem>EXECUTIVES</asp:ListItem>
                                                                            <asp:ListItem>Technical</asp:ListItem>
                                                                            <asp:ListItem>Investment</asp:ListItem>
                                                                            <asp:ListItem>Internal Control/Audit</asp:ListItem>
                                                                            <asp:ListItem>Research</asp:ListItem>
                                                                            <asp:ListItem>Stockbroking</asp:ListItem>
                                                                            <asp:ListItem>Asset Management</asp:ListItem>
                                                                            <asp:ListItem>Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Deputy Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Financial Advisory</asp:ListItem>
                                                                            <asp:ListItem>Leveraged Finance</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="center" class="style3" colspan="2">
                                                                        Staff:<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
                                                                            DataSourceID="SqlDataSource1" DataTextField="fullname" DataValueField="ID" 
                                                                            onselectedindexchanged="DropDownList1_SelectedIndexChanged" style="width: 87px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="center">&nbsp;
                                                                        </td>
                                                                    <td align="center">
                                                                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                                                            ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>" 
                                                                            SelectCommand="SELECT * FROM [directory] WHERE (([location] = @location) AND ([Department] = @Department))">
                                                                            <SelectParameters>
                                                                                <asp:SessionParameter Name="location" SessionField="location" Type="String" />
                                                                                <asp:ControlParameter ControlID="drpDepart" Name="Department" 
                                                                                    PropertyName="SelectedValue" Type="String" />
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
                                                                    </td>
                                                                    <td align="center" colspan="1" style="width: 101px">
                                                                    </td>
                                                                    <td align="center" style="width: 101px">&nbsp;
                                                                        </td>
                                                                    <td align="center" style="width: 101px">&nbsp;
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" colspan="9" style="text-align: left" valign="top">&nbsp;
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" class="style4" valign="top">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top" class="style3">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top" style="width: 252px">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top" style="width: 252px">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top" style="width: 252px">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top" style="text-align: left; width: 101px;">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top" style="text-align: left; width: 101px;">&nbsp;
                                                                        </td>
                                                                    <td align="left" valign="top" style="text-align: left; width: 101px;">&nbsp;
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="&nbsp;&lt;/td&gt;
                                                                &lt;td align=" class="style4" colspan="2" left"="">
                                                                        <asp:Label ID="Label2" runat="server" CssClass="admintextbold">Staff Details</asp:Label>
                                                                    </td>
                                                                    <td align="left" class="style3">&nbsp;
                                                                        </td>
                                                                    <td align="left" style="width: 274px">&nbsp;
                                                                        </td>
                                                                    <td align="left" style="width: 274px">&nbsp;
                                                                        </td>
                                                                    <td align="left" style="width: 274px">&nbsp;
                                                                        </td>
                                                                    <td align="left">
                                                                    </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="&nbsp;&lt;/td&gt;
                                                                &lt;td align=" class="style4" left"="">
                                                                        Department:</td>
                                                                    <td align="left">
                                                                        <asp:DropDownList ID="drpDepart1" runat="server" 
                                                                            onselectedindexchanged="drpDepart_SelectedIndexChanged" AutoPostBack="True">
                                                                            <asp:ListItem>Engineering</asp:ListItem>
                                                                            <asp:ListItem>Admin</asp:ListItem>
                                                                            <asp:ListItem>Marketing</asp:ListItem>
                                                                            <asp:ListItem>Human Resources</asp:ListItem>
                                                                            <asp:ListItem>Finance</asp:ListItem>
                                                                            <asp:ListItem>Operations</asp:ListItem>
                                                                            <asp:ListItem>Information Technology</asp:ListItem>
                                                                            <asp:ListItem>Customer Care</asp:ListItem>
                                                                            <asp:ListItem>Human Capital</asp:ListItem>
                                                                            <asp:ListItem>Legal/Company Secretary</asp:ListItem>
                                                                            <asp:ListItem>EXECUTIVES</asp:ListItem>
                                                                            <asp:ListItem>Technical</asp:ListItem>
                                                                            <asp:ListItem>Investment</asp:ListItem>
                                                                            <asp:ListItem>Internal Control/Audit</asp:ListItem>
                                                                            <asp:ListItem>Research</asp:ListItem>
                                                                            <asp:ListItem>Stockbroking</asp:ListItem>
                                                                            <asp:ListItem>Asset Management</asp:ListItem>
                                                                            <asp:ListItem>Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Deputy Managing Director</asp:ListItem>
                                                                            <asp:ListItem>Financial Advisory</asp:ListItem>
                                                                            <asp:ListItem>Leveraged Finance</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td align="left" class="style3">
                                                                        Level:</td>
                                                                    <td align="left" style="width: 274px">
                                                                        <asp:DropDownList ID="DropDownList3" runat="server" 
                                                                        DataSourceID="SqlDataSource2" DataTextField="description" 
                                                                        DataValueField="description">
                                                                        </asp:DropDownList>
                                                                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                                                                        ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>" 
                                                                        SelectCommand="SELECT [description] FROM [staff_level]"></asp:SqlDataSource>
                                                                    </td>
                                                                    <td align="left" style="width: 274px">
                                                                        Supervisor:</td>
                                                                    <td align="left" style="width: 274px">
                                                                        <asp:DropDownList ID="DropDownList2" runat="server" 
                                                                        DataSourceID="SqlDataSource3" DataTextField="fullname" DataValueField="ID">
                                                                        </asp:DropDownList>
                                                                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                                                                        ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString2 %>" 
                                                                        
                                                                        
                                                                        
                                                                            SelectCommand="SELECT ID, phone, fax, city, state, Address1, Address2, zipcode, email, Department, location, building, last, first, MI, picloc, office, Timestamp, title, background, empnum, listname, uname, pw, admin, surveyallow, emailpass, emailuser, remotemail, D11, address3, sharecal, cellphone, pager, country, supervisor, calaccess, projectview, lastlogin, visits, hppopup, status, flash, filelimit, timezone, getmail, emailpass2, emailuser2, remotemail2, emailpass3, emailuser3, remotemail3, fullname, background2, background3, spam, blogcomments, profilename, remotemailtype, remotemail2type, remotemail3type, blogdate, blogtitle, staff_level FROM directory WHERE (location = @location) AND (Department = @Department)">
                                                                            <SelectParameters>
                                                                                <asp:SessionParameter Name="location" SessionField="location" Type="String" />
                                                                                <asp:ControlParameter ControlID="drpDepart1" Name="Department" 
                                                                                PropertyName="SelectedValue" Type="String" />
                                                                            </SelectParameters>
                                                                        </asp:SqlDataSource>
                                                                    </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="&nbsp;&lt;/td&gt;
                                                                &lt;td align=" class="style4" left"="" style="text-align: left;">
                                                                        Employed Date:</td>
                                                                    <td align="left">
                                                                        <%--<cc1:gmdatepicker id="dateResume" runat="server" calendardaynameformat="FirstLetter"
                                                                        calendarfont-names="Arial" cssclass="admintextmedium" imageurl="../images/calsmall.gif"
                                                                        nextprevformat="CustomText" nonebuttontext="true" shownonebutton="False" 
                                                                        showtodaybutton="False">
                                                                            <CALENDARFONT Names="Arial" />
                                                                            <CALENDAROTHERMONTHDAYSTYLE BackColor="WhiteSmoke" />
                                                                            <CALENDARTODAYDAYSTYLE BorderColor="DarkRed" 
                                                                            BorderWidth="1px" Font-Bold="True" />
                                                                            <CALENDARTITLESTYLE BackColor="#E8F3FF" Font-Bold="True" Font-Names="Arial" 
                                                                            Font-Size="9pt" />
                                                                            <CALENDARDAYSTYLE BackColor="LightYellow" Font-Size="9pt" />
                                                                        </cc1:gmdatepicker>--%>
                                                                        <asp:TextBox ID="txtEmpDate" runat="server"></asp:TextBox>
                                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/SmallCalendar.gif" />
                                                                    </td>
                                                                    <td align="left" class="style3">
                                                                        Designation:</td>
                                                                    <td align="left" style="width: 274px">
                                                                        <asp:TextBox ID="txtDesignation" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" style="width: 274px">&nbsp;
                                                                        </td>
                                                                    <td align="left" style="width: 274px">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="&nbsp;&lt;/td&gt;
                                                                &lt;td align=" colspan="9" left"="" style="text-align: left;">
                                                                        <b>Remuneration Details</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="&nbsp;&lt;/td&gt;
                                                                &lt;td align=" class="style4" left"="" style="text-align: left;">
                                                                        Annual Basic Salary:</td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtAnnualBasic" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" class="style3">
                                                                        Annual Gross Salary:</td>
                                                                    <td align="left" style="width: 274px">
                                                                        <asp:TextBox ID="txtAnnualGross" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left" style="width: 274px">
                                                                        Monthly Basic Salary:</td>
                                                                    <td align="left" style="width: 274px; margin-left: 40px;">
                                                                        <asp:TextBox ID="txtMonthlyBasic" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left">
                                                                        Monthly Gross Salary:</td>
                                                                    <td align="left">
                                                                        <asp:TextBox ID="txtMonthlyGross" runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="&nbsp;&lt;/td&gt;
                                                                &lt;td align=" colspan="9" left"="" style="text-align: center;">
                                                                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="&nbsp;&lt;/td&gt;
                                                                &lt;td align=" class="style4" left"="" style="text-align: left;">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left" class="style3">&nbsp;
                                                                        </td>
                                                                    <td align="left" style="width: 274px">&nbsp;
                                                                        </td>
                                                                    <td align="left" style="width: 274px">&nbsp;
                                                                        </td>
                                                                    <td align="left" style="width: 274px; margin-left: 40px;">
                                                                        <asp:Button ID="Button1" runat="server" Text="Update" onclick="Button1_Click" />
                                                                    </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                    <td align="left">&nbsp;
                                                                        </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
    
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>
