
Partial Class _Hr_Staff_HR_Message
    Inherits System.Web.UI.Page

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        Dim i As New DirectoryTableAdapters.HR_MessageTableAdapter
        Dim chk
        If CheckBox1.Checked = True Then
            chk = 1
        Else
            chk = 0
        End If
        i.InsertHRMessage(Now.Date, Now.Date, txtHeadline.Text, txtNews.Text, chk)
        Response.AppendHeader("Refresh", "5; URL=HR_Message.aspx")
    End Sub
End Class
