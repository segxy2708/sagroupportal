Imports System
Imports System.Data
Imports System.Data.SqlClient
Partial Class EscalateDetails
    Inherits System.Web.UI.Page
    Private ConnectionString As String = ConfigurationManager.ConnectionStrings("foxsuite1ConnectionString").ConnectionString
    Private ConnectionString1 As String = ConfigurationManager.ConnectionStrings("sagroupportalDBConnectionString").ConnectionString
    Private myDataTable As DataTable
    Private myTableAdapter As SqlDataAdapter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Title = "Escalation Details"
        Session("tickNo") = retQuery("TicketNo")
        Session("getDatetime") = retQuery("Datetime")
        Session("PersonalDetails") = retQuery("PersonalDetails")
        Session("AffectedAsset") = retQuery("AffectedAsset")
        Session("symptoms") = retQuery("symptoms")
        Session("tickCreator") = retQuery("ticketCreator")
        If Request.QueryString Is Nothing Then
            Response.Redirect("sag_home.aspx")
        Else
            Me.details.Visible = True
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Public Function retQuery(ByVal mySel As String)
        Dim strs As String = Request.QueryString("ticketNo")
        Dim str As String
        Dim query As String = "SELECT " + mySel + " FROM Job_Title_Table WHERE TicketNo ='" + strs + "'"
        myTableAdapter = New SqlDataAdapter(query, ConnectionString)
        myDataTable = New DataTable()
        myTableAdapter.Fill(myDataTable)
        Try
            str = myDataTable.Rows(0)(0).ToString
            Return str
        Catch ex As Exception

        End Try
    End Function
End Class
