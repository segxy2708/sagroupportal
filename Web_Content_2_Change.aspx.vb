Imports System.IO
Imports System.Windows.Forms
Imports System.Web.Mail
Imports System
Imports System.Web.UI.WebControls
Partial Class Web_Content_2_Change
    Inherits System.Web.UI.Page
    

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

    End Sub

    Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.lab1.Visible = False
        If Me.ActionList.SelectedIndex = 0 Then
            HideAll()
        ElseIf Me.ActionList.SelectedIndex = 1 Then
            HideAll()
            Panel1.Visible = True
        ElseIf Me.ActionList.SelectedIndex = 2 Then
            HideAll()
            Panel2.Visible = True

        ElseIf Me.ActionList.SelectedIndex = 3 Then
            HideAll()
            Panel4.Visible = True
        End If
        Me.Title = "Web Content"
    End Sub
    Sub HideAll()
        Me.Panel1.Visible = False
        Me.Panel2.Visible = False
        Me.Panel3.Visible = False
        Me.Panel4.Visible = False

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim pguser As String = Session("names")
        Dim pgDiv As String = Session("Group")
        Dim pgMail As String = Session("email")
        Dim pgPhone As String = Session("phone")

        Dim strUser As New System.Web.UI.WebControls.Parameter("name", TypeCode.String, pguser)
        Dim strDiv As New System.Web.UI.WebControls.Parameter("Subsidiary", TypeCode.String, pgDiv)
        Dim strMail As New System.Web.UI.WebControls.Parameter("email", TypeCode.String, pgMail)
        Dim strPhone As New System.Web.UI.WebControls.Parameter("MobileNo", TypeCode.String, pgPhone)
        Dim d As New System.Web.UI.WebControls.Parameter("Date", TypeCode.String, ret2Date)
        Dim t As New System.Web.UI.WebControls.Parameter("Time", TypeCode.String, ret2Time)

        Dim pagename As String
        Dim actions As String


        If Me.ActionList.SelectedIndex = 0 Then

        ElseIf Me.ActionList.SelectedIndex = 1 Then
            actions = "New Content"
            If Me.deciBut.SelectedIndex = 0 Then
                pagename = Me.pgNameTxt.Text
            Else
                pagename = "No new page"
            End If
            Dim strAction As New System.Web.UI.WebControls.Parameter("Action", TypeCode.String, actions)
            Dim pgNam As New System.Web.UI.WebControls.Parameter("PageName", TypeCode.String, pagename)
            

            Me.NewContentSource.InsertParameters.Add(pgNam)
            Me.NewContentSource.InsertParameters.Add(strAction)
            Me.NewContentSource.InsertParameters.Add(strUser)
            Me.NewContentSource.InsertParameters.Add(strDiv)
            Me.NewContentSource.InsertParameters.Add(strMail)
            Me.NewContentSource.InsertParameters.Add(strPhone)
            Me.NewContentSource.InsertParameters.Add(d)
            Me.NewContentSource.InsertParameters.Add(t)

            'Me.NewContentSource.InsertParameters.Add(strUser)
            Me.NewContentSource.Insert()

            Me.messg.Text = "Suggestion sent successfully"
            sendmail(Session("email"), "systems@sagroupng.com")
            clear()
        ElseIf Me.ActionList.SelectedIndex = 2 Then
            actions = "Existing Content"
            Dim strAction As New System.Web.UI.WebControls.Parameter("Action", TypeCode.String, actions)
            If Me.change2List.SelectedIndex = 0 Then

            End If
            'me.ExistSource 
            Session("pageNames") = Me.pgTxt1.Text & " " & Me.pgTxt2.Text & Me.PgTxt3.Text
            Me.ExistSource.InsertParameters.Add(strAction)
            Me.ExistSource.InsertParameters.Add(strUser)
            Me.ExistSource.InsertParameters.Add(strDiv)
            Me.ExistSource.InsertParameters.Add(strMail)
            Me.ExistSource.InsertParameters.Add(strPhone)
            Me.ExistSource.InsertParameters.Add(d)
            Me.ExistSource.InsertParameters.Add(t)
            Me.ExistSource.Insert()

            Me.messg.Text = "Suggestion sent successfully"
            sendmail(Session("email"), "systems@sagroupng.com")
            clear()
        ElseIf Me.ActionList.SelectedIndex = 3 Then
            actions = "File-Upload Content"
            If Me.FileUpload1.HasFile Then
                Try
                    Dim path As String = "C:\Inetpub\wwwroot\sagroupportal\WebContentChange\"
                    Dim tempName As String = Me.FileUpload1.FileName
                    Dim permName As String = newPath(tempName, path)
                    Dim uPath As String = "WebContentChange\" & permName
                    If Me.FileUpload1.PostedFile.ContentLength < 1024000 Then
                        Dim filepath As New Parameter("UploadURL", TypeCode.String, uPath)
                        Dim strAction As New Parameter("Action", TypeCode.String, actions)

                        Me.FileUploadSource.InsertParameters.Add(filepath)
                        Me.FileUploadSource.InsertParameters.Add(strAction)
                        Me.FileUploadSource.InsertParameters.Add(strUser)
                        Me.FileUploadSource.InsertParameters.Add(strDiv)
                        Me.FileUploadSource.InsertParameters.Add(strMail)
                        Me.FileUploadSource.InsertParameters.Add(strPhone)
                        Me.FileUploadSource.InsertParameters.Add(d)
                        Me.FileUploadSource.InsertParameters.Add(t)
                        Me.FileUploadSource.Insert()
                        FileUpload1.SaveAs(uPath)
                        Me.messg.Text = "Suggestion sent successfully"
                        sendmail(Session("email"), "systems@sagroupng.com")
                        clear()
                    Else
                        Me.messg.Text = "File too Large"
                    End If
                Catch ex As Exception
                    Me.messg.Text = ex.Message '"Error Registering request"
                End Try
            Else
                Me.messg.Text = "Please Select File"
            End If
        End If
        
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Public Sub sendmail(ByVal from As String, ByVal tos As String)
        Dim mymail As MailMessage = New MailMessage()
        mymail.From = from
        mymail.to = tos
        mymail.Subject = "Request for Web Content Changes"
        ' 
        mymail.BodyFormat = MailFormat.Text
        mymail.Priority = MailPriority.High
        Dim message As StringBuilder = New StringBuilder
        message.Append("you have a request from " + Session("names") + vbCrLf + " ")
        mymail.BodyFormat = MailFormat.Text
        mymail.Body = message.ToString

        SmtpMail.Send(mymail)

    End Sub

    Protected Sub RadioButtonList2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles change2List.SelectedIndexChanged
        If Me.change2List.SelectedValue = "Yes" Then
            Panel3.Visible = True
        Else
            Panel3.Visible = False
        End If
    End Sub

    Protected Sub RadioButtonList3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList3.SelectedIndexChanged
        If Me.RadioButtonList3.SelectedValue = "No" Then
            Me.appendTxt3.Visible = True
            Me.lab1.Visible = True
        Else
            Me.lab1.Visible = False
            Me.appendTxt3.Visible = False
        End If
    End Sub
    Public Function ret2Date() As String
        Return DateTime.Now.Day & "/" & DateTime.Now.Month & "/" & DateTime.Now.Year
    End Function
    Public Function ret2Time() As String
        Return DateTime.Now.Hour & ":" & DateTime.Now.Minute & ":" & DateTime.Now.Second
    End Function

    Protected Sub deciBut_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles deciBut.SelectedIndexChanged
        If Me.deciBut.SelectedValue = "Yes" Then
            Me.Label6.Visible = True
            Me.pgNameTxt.Visible = True
        Else
            Me.Label6.Visible = False
            Me.pgNameTxt.Visible = False
        End If
    End Sub
    Sub clear()
        Me.ExpectTxt.Text = ""
        Me.capTxt.Text = ""
        Me.pgNameTxt.Text = ""
        'Me.deciBut.SelectedIndex = 1
        Me.pgMenuTxt.Text = ""
        Me.pgTxt1.Text = ""
        Me.pgTxt2.Text = ""
        Me.PgTxt3.Text = ""
        Me.CurrTxt.Text = ""
        Me.ExpectedTxt.Text = ""
        Me.BriefInfoTxt.Text = ""
        Me.appendTxt3.Text = ""
        Me.DisplayTxt.Text = ""
        Me.CaptionTxt.Text = ""
    End Sub

    Public Function newPath(ByVal filename As String, ByVal path As String) As String
        Dim tempFileName As String = ""
        Dim path2check As String = path + "\" + filename
        If File.Exists(path2check) Then
            Dim counter As Integer = 2
            While File.Exists(path2check)
                filename = counter.ToString + filename
                path2check = path + "\" + filename
                counter = counter + 1
            End While
        End If
        Return filename
    End Function

    Function message4FileChange(ByVal old As String, ByVal new1 As String) As String
        Dim rtstr
        If old <> new1 Then
            rtstr = " your file " + old + " already exist. Filename has been change to  " + new1
        Else
            rtstr = ""
        End If
        Return rtstr
    End Function
End Class
