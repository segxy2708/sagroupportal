﻿<%@ Page Title="" Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="web_update_request.aspx.vb" Inherits="web_update_request" Debug ="true"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
        AutoGenerateColumns="False" DataSourceID="view_update_request_src" 
        EnableModelValidation="True" CellPadding="4" ForeColor="#333333" 
        GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="Subsidiary" HeaderText="Subsidiary" 
                SortExpression="Subsidiary" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            
            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
            <asp:BoundField DataField="Action" HeaderText="Action" 
                SortExpression="Action" />
            <asp:BoundField DataField="Caption" HeaderText="Caption" 
                SortExpression="Caption" />
            <asp:BoundField DataField="PageName" HeaderText="Page Name" 
                SortExpression="PageName" />
            <asp:BoundField DataField="DisplayMenu" HeaderText="Display Menu" 
                SortExpression="DisplayMenu" />
            <asp:BoundField DataField="CurrentText" HeaderText="Current Text" 
                SortExpression="CurrentText" />
            <asp:BoundField DataField="ExpectedText" HeaderText="Expected Text" 
                SortExpression="ExpectedText" />
            <asp:BoundField DataField="BriefInfo" HeaderText="BriefInfo" 
                SortExpression="BriefInfo" />
         <asp:TemplateField HeaderText="File" >
                    <ItemTemplate>
                        <asp:HyperLink ID="lik1" runat="server" ImageUrl="~/images/category.gif" NavigateUrl='<%# Eval("UploadURL") %>'
                            Text='<%# Eval("UploadURL") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>     
            <asp:BoundField DataField="Date" HeaderText="Date Requested" SortExpression="Date" />
            <asp:BoundField DataField="Time" HeaderText="Time Requested" SortExpression="Time" />
        </Columns>
        <EditRowStyle BackColor="#7C6F57" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#E3EAEB" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    <asp:SqlDataSource ID="view_update_request_src" runat="server" 
        ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>" 
        SelectCommand="SELECT * FROM [SuggestionTab] ORDER BY [Date], [Time]">
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

