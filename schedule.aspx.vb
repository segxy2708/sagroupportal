
Partial Class schedule
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.IsNewSession Then
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End If

        txtDate.Text = Session("dates")
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim appoint As New PortalsTableAdapters.schedulesTableAdapter
            appoint.InsertSchedule(Session("id"), txtDate.Text, txtTime.Text, txtSubject.Text, txtDetails.Text, 1)
            lblMessage.Text = "Appointment Schedule save successfully"
            lblMessage.ForeColor = Drawing.Color.Blue
            Response.AppendHeader("Refresh", "5; URL=schedule.aspx")
        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.ForeColor = Drawing.Color.Red
        End Try
    End Sub
End Class
