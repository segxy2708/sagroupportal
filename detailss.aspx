<%@ Page Language="VB" MasterPageFile="~/sa_Glass_Theme.master" AutoEventWireup="false" CodeFile="detailss.aspx.vb" Inherits="detailss" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:DataList ID="DataList1" runat="server" DataKeyField="TicketNo" DataSourceID="complainDetails" CellPadding="4" ForeColor="#333333" Font-Size="16px" Width="827px">
 <HeaderTemplate >
 
 </HeaderTemplate>
        <ItemTemplate>
        <strong >
            Ticket No:</strong>
            <asp:Label ID="TicketNoLabel" runat="server" Text='<%# Eval("TicketNo") %>' ></asp:Label><br /><br />
            <strong > Generator:</strong>
            <asp:Label runat ="server" ID="ticCreator" Text ='<%# Eval("TicketCreator") %>'></asp:Label><br /><br />
            <strong >Date and Time:</strong>
            <asp:Label ID="DateTimeLabel" runat="server" Text='<%# Eval("DateTime") %>'></asp:Label><br /><br />
            <strong>  Attempts:</strong>
            <asp:Label ID="PersonalDetailsLabel" runat="server" Text='<%# Eval("PersonalDetails") %>'>
                </asp:Label><br /><br />
            <strong > Hardware:</strong>
            <asp:Label ID="AffectedAssetLabel" runat="server" Text='<%# Eval("AffectedAsset") %>'>
                </asp:Label><br /><br />
            <strong > Descriptions:</strong>
            <asp:Label ID="SymptomsLabel" runat="server" Text='<%# Eval("Symptoms") %>'></asp:Label><br /><br />
            <strong >1st Level Supprot Resolution:</strong>
            <asp:Label ID="FistLevel" runat ="server" Text ='<%# Eval("StepsTaken") %>'></asp:Label>
            <br /><br />
            <strong >Final Resolution:</strong>
            <asp:Label runat ="server" ID="resolve" Text ='<%# Eval("Resolution") %>' ></asp:Label>
        </ItemTemplate>
     <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
     <AlternatingItemStyle BackColor="White" />
     <ItemStyle BackColor="#EFF3FB" />
     <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
     <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    </asp:DataList>
  <asp:SqlDataSource ID="complainDetails" runat="server" ConnectionString="<%$ ConnectionStrings:foxsuite1ConnectionString %>"
        SelectCommand="SELECT TicketNo, DateTime, PersonalDetails, AffectedAsset, Symptoms, StepsTaken, Attendance, Resolution, TicketCreator FROM Job_Title_Table WHERE (TicketNo = @TicketNo)"
        UpdateCommand="UPDATE Job_Title_Table SET Treated = @Treated, DateTreated = @DateTreated, Attendance = @Attendance, Month = @Month WHERE (TicketNo = @TicketNo)">
        <SelectParameters>
            <asp:QueryStringParameter Name="TicketNo" QueryStringField="ticketNo" Type="String" />
        </SelectParameters>
        <UpdateParameters>
            <asp:QueryStringParameter Name="TicketNo" QueryStringField="ticketNo" />
        </UpdateParameters>
    </asp:SqlDataSource>
    &nbsp;&nbsp;<br />
    <br />
    <br />
    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="Job_Ticket_Summary.aspx">Back</asp:HyperLink><br />
    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

