Imports System
Imports System.data
Imports System.data.oledb
Imports System.data.sqlclient
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Web.UI.HTMLcontrols
Imports System.Web.UI.Webcontrols
Imports System.IO
Partial Class chat
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AppendHeader("Refresh", "120; URL=chat.aspx")
    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try

            Page.MasterPageFile = Session("Theme")
            Page.Theme = Session("Skin")
        Catch ex As Exception
            HttpContext.Current.Response.Redirect("session_timeout.aspx")
        End Try
    End Sub

    Protected Sub DataList1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles DataList1.EditCommand
        Dim keys As Integer = DataList1.DataKeys(e.Item.ItemIndex).ToString()
        Session("Staffid") = keys
        Response.Redirect(e.CommandArgument.ToString())
    End Sub

End Class
