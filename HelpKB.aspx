<%@ Page Language="VB" MasterPageFile="~/Main.master" AutoEventWireup="false" CodeFile="HelpKB.aspx.vb" Inherits="HelpKB" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
        <td>
        </td>
            <td>
            </td>
            <td style="width: 502px;"><asp:Label ID="Label1" runat="server" Text="Welcome To SA Group Support Help"></asp:Label>
            </td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
        <td style="height: 26px">
        </td>
            <td style="height: 26px">
               
            </td>
            <td style="height: 26px; width: 502px;" valign="bottom" >
             <asp:TextBox ID="SearchTxt" runat="server" AutoCompleteType="Search"></asp:TextBox><asp:ImageButton ID="SearchBtn" runat="server" Height="20px" ImageUrl="~/images/search3.jpg"
                    Width="50px" ImageAlign="Bottom" />
                <asp:Button ID="SearchBtn1" runat="server" Text="Search" Visible="False" /></td>
            <td style="height: 26px; width: 3px;">
            </td>
        </tr>
        <tr>
        <td>
        </td>
            <td>
            </td>
            <td style="width: 502px" >
                &nbsp;<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sagroupportalDBConnectionString %>"
                    SelectCommand="SELECT Symptoms,Resolution  FROM [ResolutionsKB] WHERE ([Symptoms] LIKE '%' + @Symptoms + '%')">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="SearchTxt" Name="Symptoms" PropertyName="Text" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
                <br />
                <br />
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4"
                    DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" ToolTip="Unattended Request" DataKeyNames="Symptoms">
                    <RowStyle BackColor="#E3EAEB" HorizontalAlign="Justify" />
                    <EmptyDataRowStyle BackColor="LightGoldenrodYellow" />
                    <Columns>
                    <asp:HyperLinkField HeaderText ="Search Result" DataTextField ="Symptoms" DataNavigateUrlFields ="Symptoms" DataNavigateUrlFormatString ="HelpKB.aspx?search={0}" ></asp:HyperLinkField>
                        <asp:BoundField DataField="Symptoms" HeaderText="Symptoms" ReadOnly="True" SortExpression="Symptoms" />
                        <asp:BoundField DataField="Resolution" HeaderText="Resolution" SortExpression="Resolution" />
                    </Columns>
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <AlternatingRowStyle BackColor="White" />
                </asp:GridView>
                <br />
                &nbsp;</td>
            <td style="width: 3px">
            </td>
        </tr>
        <tr>
        <td>
        </td>
        <td>
        </td>
        <td style="width: 502px; height: 200px">
            <asp:Panel ID="SearchPanel" runat="server" Height="100%" Width="499px" ScrollBars="Auto" Visible="False">
                <asp:Label ID="items1" runat="server" Font-Bold="True" Text="Label" Width="100%"></asp:Label><br />
                <asp:TextBox ID="resultTxt" runat="server" Height="100%" Text=""
                    TextMode="MultiLine" Width="100%" ReadOnly="true" BackColor="Silver" Wrap="False" EnableTheming="False"></asp:TextBox>&nbsp;
            </asp:Panel>
        </td>
        <td style="width: 3px">
                <asp:GridView ID="GridView2" runat="server" Visible="False">
                </asp:GridView>
        </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>

